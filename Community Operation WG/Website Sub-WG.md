# 社区官网工作组
## 工作组目标
保持官网功能正常运行，持续优化官网视觉、信息等重要部分。洞察官网用户使用情况，提升交互体验。

## 工作组范围
1.跟踪官网内容，实时刷新过期信息
2.官网板块功能排查及运维
3.官网界面设计优化
4.定期会议提出优化/运维建议
5.官网用户建议收集反馈
6.官网数字化运营：数据统计、数据分析、数据反馈

## 工作组责任人	
| 姓名  | 职责 | 邮箱 |
|-----|----|----|
| 张睿瑶 |  内容/架构  |  zhangruiyao3@huawei.com  |
| 徐李斌 |  视觉/交互  |  xulibin3@huawei.com  |
| 秦若晨 |  视觉/交互  |  qinruochen1@huawei.com  |
| 王俊恒 |  数据/用户  |  wangjunheng.wangjunheng@huawei.com |
| 赵龙飞 |  数据/用户  |  zhaolongfei1@huawei.com |

## 工作组联系方式：
邮件列表：main@edgegallery.groups.io