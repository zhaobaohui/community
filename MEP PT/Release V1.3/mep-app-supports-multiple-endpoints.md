### Source of Requirement
- EdgeGallery version plan to enhance service to support multiple endPointInfo.

### Requirement Description
- MEC application service supports multiple endPointInfo.

### Usecase

- MEC service registration, modification and delete.

![Flow of MEC service registration and update](images/multiple-endpoint-flow.png "images/multiple-endpoint-flow.png")

1. At present, the MEP service registration related interface only supports a single endPointInfo. It is necessary to modify the service registration, modify, and delete related interfaces. Mep and mep-agent must be modified simultaneously

###  EPIC && Story

- #I424FZ【MEP】Service support multiple-endPointInfo.

### Proposed Changes
![endpoint structure](images/endpoint_struct.png "images/endpoint_struct.png")

### Interface definition
No new interface changes for this requirement.
Reference: https://forge.etsi.org/rep/mec/gs011-app-enablement-api/blob/master/MecAppSupportApi.yaml

#### MEP Server changes:

1. It is necessary to modify the service registration, modify, and delete related interfaces. Mep and mep-agent must be modified simultaneously.

####  Security Design applicability:
1. No seperate security to be considered for this feature as it is modifying existing implementation.

### Estimate effort
1.0K Loc (including dev and unit tests)