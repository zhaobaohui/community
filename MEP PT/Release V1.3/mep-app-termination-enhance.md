### Source of Requirement
- EdgeGallery version plan to enhance the Appliation Termination.

### Requirement Description
- MEC application graceful termination/stop.

### Usecase

- MEC service termination confirm notification

![Flow of MEC service termination notification](images/termination_confirm_flow.png "images/termination_confirm_flow.png")

1. After the MEC platform receives a request to terminate or stop a MEC application instance from MECM, the MEC platform notifies the MEC application instance that it will be terminated or stopped soon incase if graceful termination/stop is subscribed. In the notification message, the MEC platform indicates to the MEC application instance about the time interval which the application can utilise for the termination process. The time interval is set according to the graceful termination/stop timeout value in the received request to terminate or stop. 

2. When this timer expires, the MEC platform continue the termination flow of the MEC application instance or stop MEC application instance flow
by 
    - Delete AK/SK of this Application.
    - Unregister this application and its services from service registry and DB.
    - Delete all the configures DNS Rules and Traffic Rules if any.


###  EPIC && Story

- #I424G2【MEP】MEC application graceful termination/stop.

### Proposed Changes
![Notification request structure](images/notification_struct.png "images/notification_struct.png")
![Notification response structure](images/notification-resp-structure.png "images/notification-resp-structure")

#### 1. Current End to End Flow for Application Termination
![flow](images/current-app-termination.png)

#### 2. New Flow for Application termination
![flow](images/terminate-enhancement.png)

### Interface definition
#### 1. (MEP -> APP)
MEP server call the callback API to notify the terminate notification if only subscribed.

Request body:

 | **Name** | **Type** | **Description** | **Required** |
 | --- | --- | --- | --- |
 | notificationType |  String  | Shall be set to "AppTerminationNotification"| Yes |
 | operationAction |  enum  | Operation that is being performed on the MEC application instance | Yes |
 | maxGracefulTimeout | Uint32  | Maximum non-zero timeout value in seconds for graceful termination or graceful stop of an application instance.| Yes |
 | _links |  Object  | Object containing hyperlinks related to the resource.| Yes |
 | >subscription  |  LinkType  | A link to the related subscription.| Yes |
 | >confirmTermination  |  LinkType  | Link to the task resource where to confirm termination/stop in case the application is ready to be terminated, or to be considered stopped by the MEC Platform, before expiry of the timeout. | Yes |

#### Example Request:
```
POST {callback uri}
  {
    "notificationType": "AppTerminationNotification",
    "operationAction": "TERMINATING",
    "maxGracefulTimeout": "30",
    "_links": {
      "subscription": "mep/mec_app_support/v1/applications/6abe4782-2c70-4e47-9a4e-0ee3a1a0fd1e/subscriptions/6abe4782",
      "confirmTermination": "mep/mec_app_support/v1/applications/6abe4782-2c70-4e47-9a4e-0ee3a1a0fd1e/confirm_termination",
    },
  }
```

#### Example response: 
```
HTTP/1.1 204 No Content
```

#### 2. (APP -> MEP)
| Module | Interface | Detials  |  Parameter |
|--------|-----------|-----------|--------|
| mep-server | Mp1 | /mep/mec_app_support/v1/applications/:appInstanceId/confirm_termination | appInstanceId - Applicaition Instance Identifier. |


#### Response Body:

| **Name** | **Type** | **Description** | **Required** |
| --- | --- | --- | --- |
| operationAction |  enum  | Operation that is being performed on the MEC application instance enum {STOPPPING, TERMINATING }. **Note:** The value shall match that sent in the corresponding AppTerminationNotification. | Yes |

#### Example Request: 
```
POST /mep/mec_app_support/v1/applications/:appInstanceId/confirm_termination
{
  "operationAction": "TERMINATING"
}
```

#### Example response: 
```
HTTP/1.1 204 No Content
```

#### MEP Server changes:

1. MEP server call the callback API to notify the terminate notification if only subscribed.
2. Start timer with a time interval grace timeout and when the timer is expired MEC platform should continue the termination flow.


####  Security Design applicability:
1. No seperate security to be considered for this feature as it is an extenstion to the current implementation.


### Estimate effort
1.0K Loc (including dev and unit tests)