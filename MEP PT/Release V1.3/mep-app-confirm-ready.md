### Source of Requirement
- EdgeGallery version plan to enhance the Application Deployment.

### Requirement Description
- Confirm ready task

### Usecase

- MEC service confirm ready notification

![Flow of MEC service availability notification](images/confirm_ready_task.png "images/confirm_ready_task.png")


1. The MEC application instance indicates to the MEC platform that it is up and running. 

###  EPIC && Story

- #I424GI【MEP】Support confirm ready task.

### Proposed Changes
![Notification structure](images/confirm-ready-struct.png "images/confirm-ready-struct")

### Interface definition
#### 1. (APP -> MEC)
| Module | Interface | Detials  |  Parameter |
|--------|-----------|-----------|--------|
| mep-server | Mp1 | /mep/mec_app_support/v1/applications/{appInstanceId}/confirm_ready | appInstanceId - Applicaition Instance Identifier. |

Reference: https://forge.etsi.org/rep/mec/gs011-app-enablement-api/raw/master/MecServiceMgmtApi.yaml

#### Example Request:
```
HTTP/1.1 200 OK
[
  {
    "indication": "READY"
  }
]
```


#### MEP Server changes:

1. Notification mechanism to be added to receive confirm ready notification from subscribed application.

####  Security Design applicability:
1. No seperate security to be considered for this feature as it is an extenstion to the current implementation.


### Estimate effort
0.2K Loc (including dev and unit tests)