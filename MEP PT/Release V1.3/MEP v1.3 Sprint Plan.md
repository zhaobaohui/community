# MEP Sprint Plan
Sprint1: 2021-7-26 ~ 2021-8-13  
Sprint2: 2021-8-16 ~ 2021-8-28  
Sprint3: 2021-8-30 ~ 2021-9-10
## MEP V1.3 Requirtments
| Story Name | Target Sprint | 2021-7-26 ~ 2021-7-31 | 2021-8-2 ~ 2021-8-6 | 2021-8-9 ~ 2021-8-13 | 2021-8-16 ~ 2021-8-20 | 2021-8-23 ~ 2021-8-28 | 2021-8-30 ~ 2021-9-3 | 2021-9-6 ~ 2021-9-10 |
|-----------|-------------|----------|-----------|-------------|----------|----------|----------|----------|
| [【MEP】统计MEP接口调用方数据，供审计使用](https://e.gitee.com/OSDT/dashboard?issue=I424EL) | Sprint1 |   doing   | | done|  |    |  ||
| [【MEP】Service support multiple-endPointInfo](https://e.gitee.com/OSDT/dashboard?issue=I424FZ) | Sprint2 |   | start next week|  |  |  will finish thie week  |  |done|
| [【MEP】Support confirm ready task](https://e.gitee.com/OSDT/dashboard?issue=I424GI) | Sprint1 |   doing   |doing | done |  |    |  ||
| [【MEP】MEC application graceful termination/stop](https://e.gitee.com/OSDT/dashboard?issue=I424G2) | Sprint3 |  | | doing |  |  finish code this week, and will integrated test with mecm in spirnt3  |  | done|






