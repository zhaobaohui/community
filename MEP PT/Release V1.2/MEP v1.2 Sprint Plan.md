# MEP Sprint Plan
Sprint1: 2021-5-6 ~ 2021-5-21  
Sprint2: 2021-5-24 ~ 2021-6-4  
Sprint3: 2021-6-7 ~ 2021-6-18
## MEP V1.2 Requirtments
| Story Name | Target Sprint | 2021-5-6 ~ 2021-5-14 | 2021-5-17 ~ 2021-5-21 | 2021-5-24 ~ 2021-5-28 | 2021-5-31 ~ 2021-6-4 | 2021-6-7 ~ 2021-6-11 | 2021-6-14 ~ 2021-6-18 |
|-----------|-------------|----------|-----------|-------------|----------|----------|----------|
| [【MEP】UEG MP1 interface integration](https://gitee.com/OSDT/dashboard/issues?id=I2E0RX) | Sprint2 |   doing   | doing  |  doing  |  doing  |    |  |
| [【MEP】MEP adapter register to EdgeGallery MEP and UPF](https://gitee.com/OSDT/dashboard/issues?id=I2E0SN) | Sprint2 |   doing   | doing  |  doing  | done |    |  |
| [【MEP】Public the UPF capability to EdgeGallery](https://gitee.com/OSDT/dashboard/issues?id=I2E0SR) | Sprint2 |   doing   |  doing |  doing  | done |    |  |
| [【MEP】Sync services bi-directional(1. UEG-EG, 2. EG-UEG)](https://gitee.com/OSDT/dashboard/issues?id=I3T0UU) | Sprint3 |   NA   |  NA |  NA  |  doing|  doing  | finished coding and integrate test  |
| [【MEP】mep function enhance](https://gitee.com/OSDT/dashboard?issue_id=I3OZZX) | Sprint2 |      |  doing |doing  |    |    |  |
| [【MEP】mep-agent实现订阅接口](https://e.gitee.com/OSDT/issues/list?issue=I3S46Q) | Sprint2 |      |   |  doing  |  doing  | doing   |  |
| [【MEP】服务发现增加鉴权认证](https://e.gitee.com/OSDT/issues/list?issue=I3S46O) | Sprint2 |      |   |    |  done  |    |  |
| [【MEP】ESTI api implement](https://gitee.com/OSDT/dashboard?issue_id=I3OZZ5) | Sprint3 |  NA    |  NA |   NA |  doning |   doing | done |
| [【MEP】timing api](https://e.gitee.com/OSDT/issues/list?issue=I3S47J) | Sprint3 |   NA   | NA  |  NA  |  doning  |  done  | done |
| [【MEP】transport api](https://e.gitee.com/OSDT/issues/list?issue=I3S47F) | Sprint3 |   NA   |  NA |  NA  | doning  |doing | done|
| [【MEP】Code quality improvement](https://gitee.com/OSDT/dashboard?issue_id=I3OW9P) | Sprint2 |  doing  | doing  |  done  |    |    |  |
| [【MEP】接口响应规范优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYBC) | Sprint3 |     |   |    |    |    |  |
| [【MEP】关键数据持久化](https://e.gitee.com/OSDT/issues/list?issue=I3OY7Y) | Sprint2 |     |   |  done  |    |    |  |
| [【MEP】MEP data persistence](https://e.gitee.com/OSDT/issues/list?issue=I3OZX3) | Sprint2 |    |   |  done    |    |    |  |
| [【MEP】Persistent interface support for network isolation](https://e.gitee.com/OSDT/issues/list?issue=I3NQ53) | Sprint1 |  done   |   |    |    |    |  |
| [【developer】虚机mepagent的集成](https://e.gitee.com/OSDT/issues/list?issue=I3P0ES) | Sprint2 |  |   |    |    |    |  |
| [【HealthCheck】各Edge收集Applcm、mep健康信息](https://e.gitee.com/OSDT/issues/list?issue=I3PS0V) | Sprint2 |  |   |    |    |    |  |



