### MEP Data Persistence

Provide data persistence to the MEP data-base and configurations.

#### 1. MEP service centre backend
Persistence support for the MEP service centre etcd back end. This will ensure the services, subscriptions and other configurations registered to the mep-server is intact even after the reboot.

```
Container: mepserver
MountPath: /usr/mep/data
```

#### 2. DNS server backend
DNS server uses boltDB to store the DNS entries. Persistence on this data store ensures the dns entries are intact even after the reboot of dns container.

```
Container: dnsserver
MountPath: /usr/mep/data
```

#### 3. Postgres DB
Postgres DB is used by both kong API-GW and for mep-auth data storage.

```
Container: postgres
MountPath: /var/lib/postgresql/data
```

#### 4. Elastic DB
Elastic-search DB is using in the mep to store the statistics and logs, which need persistence to keep the statistics consistent.

```
Container: mep-elasticsearch
MountPath: /usr/share/elasticsearch/data
```