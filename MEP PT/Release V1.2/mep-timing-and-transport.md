## 1. Transport information query
Transport information query provides a standardized means to the MEC applications to discover the available transports supported in MEP. The transport information received from the MEP can be used by the applications to register their services. 

URL
```
GET mep/mec_service_mgmt/v1/transports
```

Return Parameters:

| Name   | Type   | Description   | Required |
|--------|--------|--------|------|
| id | string | Transport ID. | Yes |
| name | string | Transport name. | Yes |
| description | string | Description about the transport. | No |
| type | TransportType | Transport type. | Yes |
| protocol | string | Name of the protocol used. | Yes |
| version | string | Version of the protocol used. | Yes |
| endpoint | EndPointInfo | Information about the endpoint to access the transport. | Yes |
| security | SecurityInfo | Information about the security used by the transport. | Yes |
| implSpecificInfo | Object | Any other implementation specific information. | No |

Example Response: 
```
HTTP/1.1 200 OK
[
  {
    "id": "TransId12345",
    "name": "REST",
    "description": "REST API",
    "type": "REST_HTTP",
    "protocol": "HTTP",
    "version": "2.0",
    "endpoint": {},
    "security": {
      "oAuth2Info": {
        "grantTypes": [
          "OAUTH2_CLIENT_CREDENTIALS"
        ],
        "tokenEndpoint": "/mecSerMgmtApi/security/TokenEndPoint"
      }
    },
    "implSpecificInfo": {}
  }
]
```

## 2. Time of Day (ToD)
MEC applications will query for the time of the day requests over Mp1 interface. Applications will use the timing information to synch various resources such as logs, notifications etc.

![MEP time diagram](mep-timing.png)

There are two different apis to support the timing functionality.
### 2.1 Get platform current time
Retrieves current time of the platform.

URL
```
GET mep/mec_app_support/v1/timing/current_time
```

Return Parameters:

| Name   | Type   | Description   |
|--------|--------|--------|
| seconds | integer | The seconds part of the time in unix time format. |
| nanoSeconds | integer | The nano-seconds part of the time in unix time format. |
| timeSourceStatus | enum{TRACEABLE, NONTRACEABLE} | 1 = TRACEABLE - time source is locked to the UTC time source. 2 = NONTRACEABLE - time source is not locked to the UTC time source |

Eample Response: 
```
HTTP/1.1 200 OK
{
  "seconds": 1577836800,
  "nanoSeconds": 0,
  "timeSourceStatus": "TRACEABLE"
}
```


### 2.2 Query timing capability
Retrieves the information on the platform timing capability.

URL
```
GET mep/mec_app_support/v1/timing/timing_caps
```

Return Parameters:

| Name   | Type   | Description   | Required | 
|--------|--------|--------|------|
| timeStamp | Object | Time stamp information. | No |
| >seconds  | integer | The seconds part of the time in unix time format. | Yes |
| >nanoSeconds | integer  | The nano-seconds part of the time in unix time format. | Yes |
| ntpServers | Array[Object] | Details about all NTP server available in the platform. | No |
| >ntpServerAddrType  | enum{IP_ADDRESS, DNS_NAME} | Address type. | Yes |
| >ntpServerAddr | string | NTP server address. | Yes |
| >minPollingInterval | integer | Minimum poll interval for NTP messages, in seconds as a power of two Range: 3…17  | Yes |
| >maxPollingInterval | integer | Maximum poll interval for NTP messages, in seconds as a power of two Range: 3…17 | Yes |
| >localPriority | integer | Local priority value. | Yes |
| >authenticationOption  | enum{NONE, SYMMETRIC_KEY, AUTO_KEY} | Available authentication option. | Yes |
| >authenticationKeyNum | integer | Authentication key number. This configuration is valid if selected authenticationOption is SymmetricKey  | Yes |
| ptpMasters | Array[Object] | Details about all PTP server available in the platform. | No |
| >ptpMasterIpAddress  | string | PTP master IP address. | Yes |
| >ptpMasterLocalPriority  | integer | Local priority number. | Yes |
| >delayReqMaxRate  | integer | Acceptable maximum rate of the Delay_Req messages in packets per second | Yes |


Example Response: 
```
HTTP/1.1 200 OK
{
  "timeStamp": {
    "seconds": 1577836800,
    "nanoSeconds": 0
  },
  "ntpServers": [
    {
      "ntpServerAddrType": "IP_ADDRESS",
      "ntpServerAddr": "192.0.2.0",
      "minPollingInterval": 3,
      "maxPollingInterval": 17,
      "localPriority": 1,
      "authenticationOption": "NONE",
      "authenticationKeyNum": 1
    }
  ],
  "ptpMasters": [
    {
      "ptpMasterIpAddress": "192.0.2.0",
      "ptpMasterLocalPriority": 1,
      "delayReqMaxRate": 10
    }
  ]
}
```
