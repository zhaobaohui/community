# 服务发现增加鉴权认证
- MECM配置app关于服务依赖的信息到MEP，MEP入库存储
- MEP在服务发现接口中，查询当前app配置的依赖服务信息，只返回已经配置的service

不涉及新接口增加，需要修改原有接口

URL：

```
PUT /mep/appMng/v1/applications/{appInstanceId}/confs
```

请求参数：

|名称    |        类型    | 描述     |                           IN   |    必选|
  | ---| ---| ---| ---| ---| 
 |  Content-Type    | String   | MIME类型，填"application/json"                       |     header|   是| 
  |appInstanceId |  String |  APP实例ID（UUID）             |      path |    是|

Body参数：

  |  名称     |                     类型 |              描述        |             必选| 
  | ---| ---| ---| ---|
  | authInfo|                      Object|            用户信息      |         是| 
  | &gt;credentials|               Object|            证书信息      |   是      | 
  | &gt;&gt;accessKeyId|         String  |          AK             |                是|
  | &gt;&gt;secretKey|           String  |          SK             |                是|
  | appInfo|                       Object|            app信息       | 是| 
  | &gt;appName|                 String  |            app名称       |   是      | 
  | &gt;requiredServices| Array[String]  |         app依赖的服务列表 |   是      |

请求示例：

```
PUT /mep/appMng/v1/applications/5abe4782-2c70-4e47-9a4e-0ee3a1a0fd1f/confs
{
    "header": [
        {
            "key": "Content-Type",
            "value": "application/json"
        }
    ],
    "body": {
        "authInfo": {
            "credentials": {
                "accessKeyId": "QVUJMSUMgS0VZLS0tLS0",
                "secretKey": "DXPb4sqElKhcHe07Kw5uorayETwId1JOjjOIRomRs5wyszoCR5R7AtVa28KT3lSc"
            }
        },
        "appInfo": {
            "appName": "name1",
            "requiredServices": [
                "serviceA",
                "serviceB"
            ]
        }
    }
}
```

返回参数：

返回码：200


  |  名称     |                     类型 |              描述        |             必选| 
  | ---| ---| ---| ---|
  | authInfo|                      Object|            用户信息      |         是| 
  | &gt;credentials|               Object|            证书信息      |   是      | 
  | &gt;&gt;accessKeyId|         String  |          AK             |                是|
  | &gt;&gt;secretKey|           String  |          SK             |                是|
  | appInfo|                       Object|            app信息       | 是| 
  | &gt;appName|                 String  |            app名称       |   是      | 
  | &gt;requiredServices| Array[String]  |            app依赖的服务 |   是      |

返回示例：

```
HTTP/1.1 200 OK
{
    "header": [
        {
            "key": "Content-Type",
            "value": "application/json"
        }
    ],
    "body": {
        "authInfo": {
            "credentials": {
                "accessKeyId": "QVUJMSUMgS0VZLS0tLS0",
                "secretKey": "DXPb4sqElKhcHe07Kw5uorayETwId1JOjjOIRomRs5wyszoCR5R7AtVa28KT3lSc"
            }
        },
        "appInfo": {
            "appName": "name1",
            "requiredServices": [
                "serviceA",
                "serviceB"
            ]
        }
    }
}
```


# app实例删除增强
app instance删除时，同步删除kong api gateway中的service信息，目前注册service到kong api gateway的时候有uuid，需要一起调整

# mep-agent实现服务订阅接口
```
sequenceDiagram
MepAgent->>MEP: Subscribe service
MEP->>MepAgent: Notification
```
![Mep-agent Subscribe sequenceDiagram](Mep-agent订阅通知流程图.png)

1、Mep-agent启动的时候，根据yaml文件中配置的依赖服务自动订阅mep中的服务查询服务的信息（主要是endpoint）存储到内存中

2、Mep中服务变更，通知到Mep-agent，Mep-agent根据最新的信息更新内存数据

3、APP从Mep-agent中获取endpoint信息时，先从内存中获取，如果获取不到，增加一次直接调用MEP获取


# 服务更新接口优化
服务更新时同步更新kong api gateway中注册的信息，如果已存在相同的url不做任何处理，否则进行更新或者删除操作
