### 外部接口调用列表

| 调用方       | 被调用方    | URI                                                          | 方法 |
| ------------ | ----------- | ------------------------------------------------------------ | ---- |
| Appstore   | ATP      | /edgegallery/atp/v1/tasks                                    | POST |
| Appstore   | ATP      | /edgegallery/atp/v1/tasks/{taskId}                           | GET  |
| China Unicom | user-mgmt   | /login                                                       | POST |
| China Unicom | user-mgmt   | /auth/login-info                                             | GET  |
| China Unicom | Appstore  | /mec/appstore/v1/apps                                        | GET  |
| China Unicom | Appstore  | /mec/appstore/v1/apps/{appId}                                | GET  |
| China Unicom | Appstore  | /mec/appstore/v1/apps/{appId}/packages                       | GET  |
| China Unicom | Appstore  | /mec/appstore/v1/apps/{appId}/packages/{packageId}/action/download | GET  |
|    ATP       |    APM      |/apm/v1/tenants/{tenantId}/packages/upload                    |   POST   |
|    ATP       |    APM      |/apm/v1/tenants/{tenantId}/packages/{packageId}               |   GET    |
|    ATP       | inventory   |/inventory/v1/mechosts                                        |   GET    |
|    ATP       |    Appo     |/appo/v1/tenants/{tenantId}/app_instances                     |   POST   |
|    ATP       |    Appo     |/appo/v1/tenants/{tenantId}/app_instances/{appInstantiateId}  |   POST   |
|    ATP       |    Appo     |/appo/v1/tenants/{tenantId}/app_instance_infos/{appInstantiateId}  |   GET   |
|    ATP       |    Appo     |/appo/v1/tenants/{tenantId}/app_instances/{appInstantiateId}  |   DELETE   |
|    ATP       |    Appo     |/appo/v1/tenants/{tenant_id}/hosts/{host_ip}/kpi              |   GET   |
|    ATP       |    Appstore |/mec/appstore/v1/apps/{appId}/packages/{packageId}            |   GET   |
|    ATP       |    Appstore |/mec/appstore/v1/apps/{appId}/packages/{packageId}action/download  |   GET   |
|    Lab       |    applcm   |/lcmcontroller/v1/configuration	|    POST    |
|    Lab       |    applcm   |/lcmcontroller/v1/tenants/{tenantId}/hosts/{hostIp}/kpi	|    GET    |
|    Lab       |    applcm   |/lcmcontroller/v1/tenants/{tenantId}/hosts/{hostIp}/mep_capabilities	|    GET    |
| developer | ATP      | /edgegallery/atp/v1/tasks                                    | POST |
| developer | ATP      | /edgegallery/atp/v1/tasks/{taskId}                           | GET  |
| developer  | Appstore  | /mec/appstore/v1/apps                                        | POST |
| developer  | Appstore  | /apps/{appId}/packages/{packageId}/action/publish                                | POST  |
| developer  |    applcm   |/lcmcontroller/v1/health	|    GET  |
| developer  |    applcm   |/lcmcontroller/v1/tenants/tenantId/app_instances/appInstanceId/instantiate	|    POST  |
| developer  |    applcm   |/lcmcontroller/v1/tenants/tenantId/packages/packageId	|    POST  |
| developer  |    applcm   |/lcmcontroller/v1/tenants/tenantId/packages/packageId/hosts/hostIp	|    POST  |
| developer  |    applcm   |/lcmcontroller/v1/tenants/tenantId/packages/packageId	|    GET    |
| developer  |    applcm   |/lcmcontroller/v1/tenants/tenantId/app_instances/appInstanceId/images	|    POST  |
| developer  |    applcm   |/lcmcontroller/v1/tenants/tenantId/app_instances/appInstanceId/terminate	|    POST  |
| developer  |    applcm   |/lcmcontroller/v1/tenants/tenantId/app_instances/appInstanceId	|    GET    |
| developer  |    applcm   |/lcmcontroller/v1/tenants/tenantId/app_instances/appInstanceId/images/imageId/file	|    POST  |
| developer  |    applcm   |/lcmcontroller/v1/configuration	|    GET    |
| developer  |    applcm   |/lcmcontroller/v1/tenants/{tenantId}/hosts	|    POST|
|    MECM    |    Appstore |/mec/appstore/v1/apps/                                        |   GET   |
|    MECM    |    Appstore |/mec/appstore/v1/apps/{appId}/packages                        |   GET   |
|    MECM    |    Appstore |/mec/appstore/v1/apps/{appId}/packages/{packageId}            |   GET   |
|    MECM    |    Appstore |/mec/appstore/v1/apps/{appId}/packages/{packageId}action/download  |   GET   |
