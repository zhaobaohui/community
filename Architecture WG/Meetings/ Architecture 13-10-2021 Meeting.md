## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | 腾讯会议 404 517 870|


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- TBA


## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  EdgeGallery新需求贡献讨论 |  | 范桂飓 |
|  V1.3 版本Review |  | All |


## Action Items
|  Items | Assignee   |
|---|---|
| TBA |  TBA |


