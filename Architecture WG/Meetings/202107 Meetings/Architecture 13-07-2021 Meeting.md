## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://us02web.zoom.us/j/86421930694?pwd=bHdoZWMrbTFiSDQxQmFlK2VrRDBzZz09 Pwd:030836 |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- 刘辉（紫金山）
- 张倍源
- 张海龙
- 高国桢
- 周文敬
- 鹿鑫
- 李强
- 李治谦



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  新增需求Review | MECM 新增关于VM与监控几条需求，Installer也增加一条新需求。目前需求分析进度较满，需要尽快讲工作量评估出来以供排序。 | 高维涛 |





## Action Items
|  Items | Assignee   |
|---|---|
| 分析1.2 版本迭代出口质量较低原因，拉通测试工作组同时输出版本质量分析报告，进行迭代出口质量改进。 | @张倍源  |



