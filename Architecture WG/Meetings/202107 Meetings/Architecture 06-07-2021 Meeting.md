## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://us02web.zoom.us/j/86421930694?pwd=bHdoZWMrbTFiSDQxQmFlK2VrRDBzZz09 Pwd:030836 |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- 刘辉（紫金山）
- 李剑锋（安恒）
- 陈共龙（腾讯）
- 张倍源
- 李强
- 鹿鑫
- 高国桢
- 刘慧玲
- 李治谦
- 屈静静
- 周文敬



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  1.3 版本需求分析开工 | 1.3 版本需求已经归档在：https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v1.3/v1.3_requirements.md 。本周开始进行需求分析，请大家将分析文档归档在v1.3目录下 | 高维涛 |
| 架构组流程优化| 针对上几个版本的经验，我们需要优化几个问题：1. 从1.3开始在每个迭代进行一次API Review 2. 分析上个版本迭代出口质量较低的问题，输出质量分析报告并进行改进。 | 高维涛 |




## Action Items
|  Items | Assignee   |
|---|---|
| 分析1.2 版本迭代出口质量较低原因，拉通测试工作组同时输出版本质量分析报告，进行迭代出口质量改进。 | @张倍源  |



