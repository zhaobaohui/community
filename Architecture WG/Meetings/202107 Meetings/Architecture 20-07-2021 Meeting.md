## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://us02web.zoom.us/j/86421930694?pwd=bHdoZWMrbTFiSDQxQmFlK2VrRDBzZz09 Pwd:030836 |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- TBA



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  1.3 需求设计Review | Review了目前已经更新到Wiki上的需求，剩余的需求待更新完后，本周完成更新。 | 高维涛 |





## Action Items
|  Items | Assignee   |
|---|---|
| 分析1.2 版本迭代出口质量较低原因，拉通测试工作组同时输出版本质量分析报告，进行迭代出口质量改进。 | @张倍源  |



