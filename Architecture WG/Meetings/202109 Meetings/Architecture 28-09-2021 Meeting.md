## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | 腾讯会议 404 517 870|


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- 范桂飓（Juniper）
- 张立岗
- 张王（联通）
- 刘辉（紫金山实验室）
- 谢瑞涛（华三）
- 陈传雨
- 周俊
- 李强
- 鹿鑫
- 刘慧玲
- 李治谦
- 周文敬
- 影
- 罗小云


## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  EdgeGallery新需求讨论 | 桂飓介绍了在EdgeGallery上的的相关需求与问题，社区针对相关问题进行了讨论 | 范桂飓 |
|  V1.3 版本架构图讨论 |  | All |


## Action Items
|  Items | Assignee   |
|---|---|
| 针对行业在EdgeGallery成立类似SIG的组织，每个行业公司主导此SIG，可以在TSC和Borad讨论 |  范桂飓、高维涛 |
| 针对多沙箱需求，联系桂飓分析多沙箱相关需求| 张海龙，陈传雨，范桂飓 |
| 优化EdgeGallery北向接口调用方式，明确EdgeGallery验证码、Token获取方式与代码流程接口| 周艳兵 |
| 优化EdgeGallery网络的灵活配置 | 李治谦，陈传雨 |
| 完善MECM代码编译部署流程文档 | 李治谦 |
| 参考Openstack Horizon项目，是否可以模块化构造前台展示的Panel| 杨阳 |
| 通过类似导入K8S Configuration file支持Openstack 不同角色OpenRC配置文件，对于Openstack进行操作，同时优化Edgegallery 资源监控相关能力| 李治谦 |



