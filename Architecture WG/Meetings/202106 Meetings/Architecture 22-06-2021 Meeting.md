## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://us02web.zoom.us/j/86421930694?pwd=bHdoZWMrbTFiSDQxQmFlK2VrRDBzZz09  |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- TBA



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  1.3 版本需求收集启动 | 本周开始启动1.3 版本需求收集 | 高维涛 |
| 1.2 架构图定稿| 1.2 版本架构图已经定稿，新增健康检查，南向生态集成fledge等功能  | 高维涛 |




## Action Items
|  Items | Assignee   |
|---|---|
| TBA | TBA |



