## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://welink-meeting.zoom.us/j/890620110  |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- 刘辉（PML）
- 张海龙
- 罗小云
- 高国桢
- 扈冰
- 李强
- 李治谦
- 鹿鑫
- 余宇清（中移动）
- 张倍源
- 周艳兵



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  签名认证需求讨论 | 需要明确几个问题，一个是范围和问题，第二个就是后续的扩展，比如对接多个系统，公私钥的来源。第三个就是最终的形态，是个独立工具还是开发者平台。第四是接口，第五是工作量 | 陈传雨，张海龙 |
| 1.2 架构图Review|  1.2 版本架构新增树莓派的支持，新增镜像管理，新增HC，整合并体现边缘自治模块。 | 高维涛 |




## Action Items
|  Items | Assignee   |
|---|---|
| TBA | TBA |



