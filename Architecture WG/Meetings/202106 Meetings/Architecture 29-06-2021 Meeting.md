## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://us02web.zoom.us/j/86421930694?pwd=bHdoZWMrbTFiSDQxQmFlK2VrRDBzZz09  |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- TBA



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  1.3 版本需求初稿Review | 从各PTL收集了1.3 版本需求规划以及遗留的问题，下周开始进行需求分析。 | 高维涛 |
| 1.2 遗留问题Review| Review了1.3相关遗留问题，对于步明确问题，需要后续分析有明确输出后再来重新评审。  | 张倍源 |




## Action Items
|  Items | Assignee   |
|---|---|
| TBA | TBA |



