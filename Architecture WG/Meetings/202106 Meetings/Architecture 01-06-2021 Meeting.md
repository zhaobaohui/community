## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://welink-meeting.zoom.us/j/377974013  |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- Kanag
- 刘辉（紫金山）
- 陈共龙（腾讯）
- 余宇清（腾讯）
- 李强
- 刘慧玲
- 李剑锋（安恒）
- 鹿鑫
- 张海龙
- 张琪昱
- 程润东



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  小程序架构review-FaaS平台集成 | Kanag introduce the opensource FaaS platform's comparison, which the initial selection is kubeless. regrading the integration with EdgeGallery, we will focus on the tooling for FaaS, and the unified architecture to let the platform agnostic. | Kanag |




## Action Items
|  Items | Assignee   |
|---|---|
| TBA | TBA |



