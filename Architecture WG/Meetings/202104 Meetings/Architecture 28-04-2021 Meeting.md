## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://welink-meeting.zoom.us/j/205374042  |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- TBA



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  1.2 版本需求概要设计 |  | All |






## Action Items
|  Items | Assignee   |
|---|---|
| TBA  | TBA|TBA



