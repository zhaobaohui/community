## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://welink-meeting.zoom.us/j/659294527  |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- 刘辉（紫金山）
- 陈共龙（腾讯）
- 游永明（申迪）
- 潘广进（上海大学）
- 崔建伟
- 程润东
- 扈冰
- 刘慧玲
- 李治谦
- 罗小云
- 鹿鑫
- 李强
- 张阿利
- 张倍源
- 张海龙
- 赵宝辉
- 周文敬
- 周艳兵



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  1.2 版本需求概要设计 | Review了版本需求，需要大家本周完成概要设计和工作量评估 | All |






## Action Items
|  Items | Assignee   |
|---|---|
| TBA  | TBA|TBA



