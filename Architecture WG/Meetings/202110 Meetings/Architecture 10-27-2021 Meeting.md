## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | 腾讯会议 https://meeting.tencent.com/dm/ubUAgELew0ip会议 ID：303 955 710|


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- TBA


## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  EdgeGallery新版本命名 | 新版本版本编号V1.5， 颜色以G开头，目前备选:Green/Gold/ | All |
|  新版本需求讨论 |  | All |


## Action Items
|  Items | Assignee   |
|---|---|
| TBA |  TBA |


