## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://welink-meeting.zoom.us/j/377974013  |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- 陈共龙（腾讯）
- 范桂飓（九州云)
- 刘辉（紫金山）
- 罗小云
- 刘慧玲
- 李强
- 鹿鑫
- 张海龙
- 陈传雨
- 李治谦
- 杨阳
- 周文敬



## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  1.2 版本架构Review | Review了所有项目的架构，Developer架构图需要刷新，其他项目需要尽快刷新自己的API文档，在开发之前定稿 | All |




## Action Items
|  Items | Assignee   |
|---|---|
| 确定是否做过API Formation的接口都刷新到V2  | 罗小云|



