## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | https://welink-meeting.zoom.us/j/540307400  |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- 陈共龙（腾讯）
- 李剑锋（安恒）
- 刘辉（紫金山）
- 张倍源
- 刘慧玲
- 李强
- 张琪昱





## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  EdgeGallery手机应用服务端设计 | review 手机应用服务端的详细设计与接口设计，重点设计通过微信一键登陆，应用体验等功能 | 张倍源 |
| EdgeT Portal Requirement review| we reviewed the edgeT v1.2 plan, regarding the portal, currently mostly used for EG deployment. later release we need to focus on the test cases increase. | Kanag |




## Action Items
|  Items | Assignee   |
|---|---|
|   | |



