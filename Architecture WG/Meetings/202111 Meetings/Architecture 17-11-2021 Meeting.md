## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   | 腾讯会议 718 784 276 |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- TBA


## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  新建仓库讨论 |  | 建伟，鹿鑫 |
|  V1.5 版本架构Review |  | All |


## Action Items
|  Items | Assignee   |
|---|---|
| TBA |  TBA |


20

## Conclusion
1.同意增加公共服务模块，以微服务的方式对外提供REST接口供需要的模块调用，详细信息如下：
New repo name :  https://gitee.com/edgegallery/common-service
Purpose :为EdgeGallery各组件提供公共服务，两个及以上模块用到的功能由公共服务实现，各模块按需部署公共服务，优化代码架构，方便后期维护。

Type: Open
Owner and Responsible Project Team/Working Group: 
Owner Email：崔建伟
PT/WG Name ：架构组

Committer and contributor: 
Committer：高维涛、陈传雨、张倍源、崔建伟
Contributor：All

Target EdgeGallery Version: v1.5
Requirements（Epics/Stories）：#I48GPB
