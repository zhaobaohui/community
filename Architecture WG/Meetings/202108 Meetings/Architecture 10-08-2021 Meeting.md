## Meeting Logistics

| Meeting Day  |  Meeting Time  | Conference Bridge  |
|---|---|---|
| Tuesdays  | 7:00 PM to 8:00 PM (UTC+8:00)   |https://us02web.zoom.us/j/794508490?pwd=RlFPZlplZWRqUDVWV056TDd3cXRTUT09 |


## Meeting Attendees
- [高维涛](https://gitee.com/Gao_Victor)
- 刘慧玲
- 鹿鑫
- 张倍源
- 张海龙
- 罗小云
- 周文敬
- 李强


## Meeting Agenda & Minutes
|  Agenda Items  |  Notes & Materials   |  Proposer |
|---|---|---|
|  1.3 架构Review  | 会上Review了ATP，APPStore，Developer，MEP的1.3 版本架构与相关接口变更。需要ATP考虑如果单独运行的话，需要解耦哪些依赖。需要Developer平台分析并解耦IP固定分配的方式，通过Openstack IP分配机制进行制定而不是自己维护一个单独的表 | 高维涛 |





## Action Items
|  Items | Assignee   |
|---|---|
| TBA | TBA  |



