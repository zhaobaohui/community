### 发布时间

2021.4.15

### 项目概述 Project Overview

User Management 用户管理模块，为EdgeGallery提供了基本的用户增删改查功能，定义了用户的角色与权限，并且支持了单点登录（Single Sign On）能力。 目前支持的用户角色有：Admin/Teant/Guest，支持的平台有：AppStore/Developer/MECM/ATP/LAB。


### 从上一个版本开始架构的变化 Architecture changes from last release

无架构变化

### v1.2 引入的新功能特性 New component capabilites for v1.2

1. 用户注册、登录、发送验证码到用户手机或邮箱这三个场景支持与后台交互的验证方式，提升安全性。
2. admin用户支持首次登录后强制修改密码；非guest用户支持每90天必须修改密码。

### 数据库结构变更 Database Structure Change


用户信息表(tbl_tenant)结构变更：

- 新增字段：“PWEFFECTTIME”，用来记录当前密码的生效时间。

### 新增或修改的接口 New or modified interfaces

| API                                 | 方法 | API说明                  | 新增or修改 | 请求体                           | 返回体                          |
| ----------------------------------- | ---- | ------------------------ | ---------- | -------------------------------- | ------------------------------- |
| /v1/identity/verifycode-image       | GET  | 获取带验证码的图片       | 新增       | 无                               | binary stream                   |
| /v1/identity/verifycode-image/precheck | GET  | 预校验输入的图形验证码是否正确 | 新增       | 无                               | {<br>  "checkResult": true<br>} |
| /login                              | POST | 登录                     | 修改，增加verifyCode查询参数       | 不变 | 不变                            |
| /v1/identity/sms                    | POST | 发送短信验证码           | 修改，增加verifyCode查询参数       | 不变 | 不变                            |
| /v1/identity/mail                   | POST | 发送邮箱验证码           | 修改，增加verifyCode查询参数       | 不变 | 不变                            |

#### 如果有修改的接口，他们是否是后向兼容的 If modified, are the backwards compatible

兼容

### 接口API参考文档 Reference to the interfaces

https://gitee.com/edgegallery/docs/blob/master/Projects/User%20Management/User_Interfaces.md

### 系统的限制目前有哪些 What are the system limits

1.2版本新增特性无限制