### 发布时间 详细设计开始的第一周

详细设计开始时间: 2021.4.15
### 项目概述 Project Overview
应用测试认证服务，可以让用户选择应用包的测试场景，执行对应场景的测试用例。社区场景会对应用包进行安全性测试、遵从性测试（根据APPD、ETSI标准验证包结构）以及沙箱测试（应用包生命周期测试）， 对于测试通过的应用包，才可以发布到应用商店。


### 从上一个版本开始架构的变化 Architecture changes from last release
无架构变化

数据库测试场景、测试套、测试用例表新增了createTime字段。

需要执行如下语句：

alter table TEST_CASE_TABLE add column CREATETIME TIMESTAMP NULL; 

alter table TEST_SCENARIO_TABLE add column CREATETIME TIMESTAMP NULL;

alter table TEST_SUITE_TABLE add column CREATETIME TIMESTAMP NULL;


### v1.2 引入的新功能特性 New component capabilites for v1.2
1. 管理面展示用户面贡献的测试用例，支持批量删除和脚本用例下载。
2. 支持测试场景、测试套、测试用例批量导入

### 新增或者修改的接口 New or modified interfaces
* 新增批量删除贡献用例接口
* 新增下载贡献用例脚本接口
* 新增导入测试模型接口

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible
兼容

### 接口API 简述 interface naming
| API                                          | 方法   | API说明  | 请求体                                            | 返回体                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|----------------------------------------------|------|--------|------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| edgegallery/atp/v1/contributions/batch_delete                     | POST | 创批量删除贡献用例 | {"ids":["id1","id2"]} | {"failed":["id3"]}                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| edgegallery/atp/v1/contributions/ {id}/action/download | GET| 下载贡献用例脚本 | -- | binary stream|
| edgegallery/atp/v1/testmodels/action/import | POST| 导入测试模型 | {file:file} | {"retCode" : 5000,"failures" : [{"id" : "string","nameEn" : "string","type" : "testScenario","errCode": "string", "errMsg": 1000,"params": "string"},{"id" : "string","nameEn" : "string","type" : "testSuite","errCode": 1000,"errCode": "string", "params": "string" },{"id" : "string","nameEn" : "string","type" : "testCase","errCode": 1000, "errCode": "string","params": "string" }]}|

### 接口API参考文档 Reference to the interfaces
https://gitee.com/edgegallery/docs/blob/master/Projects/ATP/ATP_Interfaces.md

### 系统的限制目前有哪些 What are the system limits
None