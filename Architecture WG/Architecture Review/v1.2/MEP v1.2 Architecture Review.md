### 发布时间 详细设计开始的第一周
Detailed design start time: 4.15

Release time: 7.7

### 项目概述 Project Overview
MEP(MEC Platform) implmentation based on ETSI specification. The MEP server interface is divided into two categories, one is the Mp1 interface that follows the ETSI MEC 011 v2.1.1 standard, which mainly provides functions such as service registration discovery, app status notification subscription, and Dns rule acquisition; another is Mm5 interface, which mainly provides configuration management functions for MECM/MEPM.

### 从上一个版本开始架构的变化 Architecture changes from last release
None

### v1.2 引入的新功能特性 New component capabilites for v1.2
* mep-adapter support UEG MP1 interface integration. [Design](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.2/MEP-Adapter.pptx)
* MEP data persistence. [Design](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.2/mep-persistence.md)
* ETSI timing and transport api implement. [Design](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.2/mep-timing-and-transport.md)
* MEP function enhance. [Design](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.2/mep_enhance.md)


### 新增或者修改的接口 New or modified interfaces
* Config app instance info interface.
* Transport information query interface.
* Get platform current time interface.
* Query timing capability interface.

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible

### 接口API 简述 interface naming

|  Method | URL  | Description|
|---|---|---|
| PUT | /mep/appMng/v1/applications/{appInstanceId}/confs | Config app instance info interface |
| GET | /mep/mec_service_mgmt/v1/transports | Transport information query interface |
| GET | /mep/mec_app_support/v1/timing/current_time | Get platform current time interface |
| GET | /mep/mec_app_support/v1/timing/timing_caps | Query timing capability interface |


### 接口API参考文档 Reference to the interfaces
* Config app instance info [interface](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.2/mep_enhance.md)
* Transport and timing [interface](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.2/mep-timing-and-transport.md)

### 系统的限制目前有哪些 What are the system limits
None