### 发布时间
2021.4

### 项目概述 Project Overview
Developer是开发者开发和测试边缘应用的edgegallery平台，该平台提供了开发工具、开放的API能力、集成测试验证环境等，完成测试后可直接生成统一规范的应用包，
使开发者更加方便快捷的开发应用并集成到edgegallery平台，开发者最终也可以选择发布自己的应用到appstore或者发布生态API。

v1.2的目标：
- 虚机支持增强：补齐input参数、规则配置
- 虚机镜像管理支持：提供管理员和用户镜像的增删改查能力，自定义镜像上传下载
- 容器镜像管理：支持从其他镜像仓库同步镜像，镜像上传界面优化
- 容器应用隔离：helmchart支持namespace隔离，nodeposrt自动分配
- 沙箱环境配置优化：developer可以单独配置lcm以及mechost

### v1.2引入的新功能特性 New component capabilites for v1.2
* developer界面整体重构，美观和易用性增强
* 虚机镜像管理支持：提供管理员和用户镜像的增删改查能力，自定义镜像上传下载

### 新增或者修改的接口  New or modified interfaces

| API | 方法 | API说明 | 修改或新增 | 说明 |
| --- | --- | --- | --- | --- |
| /mec/developer/v1/vm/systemimages/list | POST | 查询虚机镜像列表 | 新增 |   |
| /mec/developer/v1/vm/systemimages/ | POST | 新增虚机镜像信息 | 新增 |  |
| /mec/developer/v1/vm/systemimages/{systemId} | PUT | 修改虚机镜像信息 | 新增| |
| /mec/developer/v1/vm/systemimages/{systemId}    | DELETE | 新增沙箱环境接口 | 新增|  |
| /mec/developer/v1/vm/systemimages/{systemId}/upload | PUT | 上传虚机镜像文件 | 新增 |  |

#### 如有， 他们是否是向后兼容的 If modified, are the backwards compatible
是

### 接口API简述 interface naming

https://gitee.com/edgegallery/docs/blob/master/Projects/Developer/Developer_Interfaces.md

### 系统的限制目前有哪些  What are the system limits
如操作系统，用户权限等等
新增admin用户权限的相关操作