### 发布时间 
2021.4

### 项目概述 Project Overview
AppStore是开发者发布和上线App应用的市场，由Developer平台开发的边缘应用，经过测试可以直接分享到AppStore应用商店。AppStore分为前后台两个部分，appstore-be是后台部分，提供主要功能接口供前台或其他三方系统调用，appstore-fe是前台部分，提供界面展示。

### 从上一个版本开始架构的变化 Architecture changes from last release
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/155216_ad5f3621_8354563.png "app_structure.png")

### v1.2 引入的新功能特性 New component capabilites for v1.2(Chocolate)
* 上传应用可以设置应用的展示模式

  1.应用展示模式有：应用发布后不在本应用仓库展示、在本仓库展示、可推广到其他应用仓库；

  2.可推广到其他应用仓库的前提是在本仓库可见，默认为public；

  3.应用展示在数据库增加字段showType varchar(100) default public，取值为public、inner-public、private。

  

* 可以支持修改应用属性

  1.在“我的应用”中增加操作“修改”，可以修改应用属性；

  2.可以修改的属性包括：行业、类型、图标、视频、架构、描述、应用展示。

  

* 管理员角色可以对应用进行管理

  1.Admin角色增加一级菜单“应用管理”；

  2.管理包括：近期热门应用管理、应用仓库显示应用列表管理、可推广应用列表管理；

  3.三个页签统一格式，近期热门应用为应用仓库展示列表的子集。

  

### 新增或者修改的接口 New or modified interfaces

| **API**                                | **方法** | API说明             | **变更类型** | **说明**                                                     |
| -------------------------------------- | -------- | ------------------- | ------------ | ------------------------------------------------------------ |
| /mec/appstore/v1/apps | GET | 获取应用列表 | 修改 | 1、响应增加deployMode字段，标识是虚机应用还是容器应用。 2、响应增加isHotApp字段，标识是否为近期热门应用。 3、userId请求参数为空时，只返回已发布showType非private的应用；非空时，返回当前用户所有的应用 |
| /mec/appstore/v1/apps/{appId} | GET | 根据appId获取应用 | 修改 | 1、响应增加deployMode字段，标识是虚机应用还是容器应用。 2、响应增加isHotApp字段，标识是否为近期热门应用。 3、userId请求参数为空时，只返回已发布showType非private的应用；非空时，返回当前用户所有的应用 |
| /mec/appstore/v1/apps/{appId}/packages | GET | 根据appId获取应用包列表 | 修改 | 1、响应增加deployMode字段，标识是虚机应用还是容器应用。  2、userId请求参数为空时，只返回已发布showType非private的应用包；非空时，返回当前用户所有的应用包 |
| /mec/appstore/v1/apps/{appId}/packages/{packageId} | GET | 根据appId和packageId获取应用包 | 修改 | 1、响应增加deployMode字段，标识是虚机应用还是容器应用。  2、userId请求参数为空时，只返回已发布showType非private的应用包；非空时，返回当前用户所有的应用包 |
| /mec/appstore/v1/packages | GET | 获取应用包列表 | 修改 | 1、响应增加deployMode字段，标识是虚机应用还是容器应用。  2、userId请求参数为空时，只返回已发布showType非private的应用包；非空时，返回当前用户所有的应用包 |
| /mec/appstore/v1/apps/hotapps                                | PUT | 修改热门应用 | 新增 | 添加/删除近期热门应用，请求参数为应用ID列表。 |
| /mec/appstore/v1/packages/pushable | GET | 获取可推送应用列表 | 修改 | 只返回已发布且showType为public属性的应用包 |
| /mec/appstore/v1/packages/{packages}/pushable | GET | 根据packageId获取可推送应用列表 | 修改 | 只返回已发布且showType为public属性的应用包 |
| /mec/appstore/v1/packages/pullable | GET | 获取可拉取应用列表 | 修改 | 只返回已发布且showType为public属性的应用包 |
| /mec/appstore/v1/apps/{appId}/package/{packageId}  | PUT | 修改应用属性 | 新增 | 修改应用属性，包括应用的行业、类型、图标、视频、架构、描述、展示类型 |

### 分页查询接口URI变更

| **API**                                | **方法** | API说明             | **变更类型** | **说明**                                                     |
| -------------------------------------- | -------- | ------------------- | ------------ | ------------------------------------------------------------ |
| /mec/appstore/v2/query/apps       | POST   | 获取应用            | 新增         | 请求参数增加字段appName、limit、offset、sortItem、sortType;  后台进行分页查询、支持模糊查询 ，按条件排序，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/apps/{appId}/packages                  | GET      | 根据appid获取应用包            | 新增         | 请求参数增加字段appName、limit、offset、sortItem、sortType;  后台进行分页查询、支持模糊查询 ，按条件排序，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/packages              | GET      | 获取app应用包       | 新增         | 请求参数在v1接口基础上增加字段appName、limit、offset;  后台进行分页查询、支持模糊查询 ，按条件排序，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/appstores             | GET      | 获取appStore        | 新增       | 请求参数增加字段 appStoreName、limit、offset、sortItem、sortType;  后台进行分页查询、支持模糊查询 ，按条件排序，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/packages/pushable     | GET      | 获取应用推送app列表 | 新增         | 请求参数在v1接口基础上增加字段appName、limit、offset，order,prop ;  后台进行分页查询、支持模糊查询，按条件排序 ，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/packages/pullable     | GET      | 获取应用拉取app列表 | 新增         | 请求参数在v1接口基础上增加字段appName、limit、offset、sortItem、sortType  后台进行分页查询、支持模糊查询 ，按条件排序，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/packages/{platformId}/pullable     | GET      | 根据appstoreid获取应用拉取app列表 | 新增         | 请求参数在v1接口基础上增加字段appName、limit、offset、sortItem、sortType;  后台进行分页查询、支持模糊查询 ，按条件排序，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/messages              | GET      | 操作分析消息列表    | 新增         | 请求参数在v1接口基础上增加字段appName、limit、offset、sortItem、sortType;  后台进行分页查询、支持模糊查询，按条件排序 ，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/apps/{appId}/comments | GET      | 获取应用评论信息    | 新增         | 请求参数在v1接口基础上增加字段limit、offset;  后台进行分页查询、支持模糊查询 ，按条件排序，响应参数增加总数量total、limit、offset。 |

### 接口响应规范化新增接口

| **API**                                | **方法** | API说明             | **变更类型** | **说明**                                                     |
| -------------------------------------- | -------- | ------------------- | ------------ | ------------------------------------------------------------ |
| /mec/appstore/v2/apps/{appId}/packages/{packageId}/action/publish | POST | 发布应用 | 新增 | 响应规范化：<br />{<br/>  "data" : string,<br/>  "retCode" : int,<br/>  "message": string,<br/>  "params": [string]<br/>} |
| /mec/appstore/v2/apps/{appId}/packages/{packageId} | GET | 根据appId和packageId获取应用包 | 新增 | 响应规范化：<br />{<br/>  "data" : PackageDto,<br/>  "retCode" : int,<br/>  "message": string,<br/>  "params": [string]<br/>} |
| /mec/appstore/v2/apps | POST | 上传应用 | 新增 | 响应规范化：<br />{<br/>  "data" : RegisterRespDto,<br/>  "retCode" : int,<br/>  "message": string,<br/>  "params": [string]<br/>} |
| /mec/appstore/v2/apps/{appId} | GET | 根据appId获取应用 | 新增 | 响应规范化：<br />{<br/>  "data" : AppDto,<br/>  "retCode" : int,<br/>  "message": string,<br/>  "params": [string]<br/>} |
| /mec/appstore/v2/messages | POST | 添加消息 | 新增 | 响应规范化：<br />{<br/>  "data" : string,<br/>  "retCode" : int,<br/>  "message": string,<br/>  "params": [string]<br/>} |

### 删除接口
| API | 方法 | API说明 |
| --- | --- | --- |
| /mec/appstore/v1/apps/{appId}/action/download | GET | 根据appId下载最新版本应用包 |

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible
1、AppStore之前的查询接口没有做分页，默认返回的只有前100条数据，随着后面数据量增大，请使用方切换V2接口进行分页查询；

2、这个版本对应用展示进行了管理，如果应用不是public的，将无法对外分享。

### 接口API 简述 interface naming

https://gitee.com/edgegallery/docs/blob/master/Projects/APPSTORE/AppStore_Interfaces.md

### 系统的限制目前有哪些 What are the system limits