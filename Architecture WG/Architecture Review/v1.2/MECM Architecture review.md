### 发布时间 详细设计开始的第一周
Detailed design start time: 1.15

Release time: 3.31

### 项目概述 Project Overview
MECM (multi access edge compute manager) provides orchestration and life cycle management of application in edgegallery architecture. MECM provides various features including application on-boarding, application orchestration by selecting appropriate edge based on deployment strategy, application life cycle management, homing and placement of application based on analytics and policies, application/edge resource monitoring and provides unified topology view.
![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/144314_28f68516_7624954.png "MECM ArchitectureV1.2.PNG")

### 从上一个版本开始架构的变化 Architecture changes from last release
架构无变化。

### v1.1 引入的新功能特性 New component capabilities for v1.1
* MECM支持虚机应用部署. [Design](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.2/EdgeGallery%E6%94%AF%E6%8C%81%E8%99%9A%E6%9C%BA%E5%BA%94%E7%94%A8%E5%A2%9E%E5%BC%BAVM%20APP%20Support%20V1.2.pptx)
* MECM支持VMAPP包管理. [Design](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.2/MECM%20Container-VM%20based%20application%20package%20management.pptx)
* MECM边缘自治增强 [Design](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.2/the%20combination%20between%20MEPM-portal%20and%20MEP-portal.md)
* MECM支持applcm与apprulemgr注册统一. [Design](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.2/Unify%20Applcm%20and%20apprulemgr%20registration%20to%20mepm%20registration.pptx)
* MECM支持前台节点位置可自定义. [Design](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.2/Location%20customization%20requirement.md)


### 新增或者修改的接口 New or modified interfaces
新增接口：
* OSPlugin新增上传包接口
* OSPlugin新增删除包接口

修改接口：
* APPO实例化APP接口，支持扩展输入参数.
* APPO批量实例化接口
* APPLCM实例化APP接口，支持扩展输入参数.
* APPLCM and APP rule manager configuration APIs
  will be removed and provide MEPM configuration APIs.

接口无变化但涉及逻辑变更的接口：
* APPO终止APP接口
* APPO批量终止接口
* APPLCM终止APP接口
* APM同步应用接口
* APM分发APP接口
* APM上传APP包接口
* APM下载CSAR包接口
* APPLCM分发APP接口
* APPLCM查询分发状态接口
* APPLCM同步包接口
* APPLCM删除分发接口

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible
确实涉及对MECM与其他模块之间接口的更改
MECM接口更改不向后兼容

### 接口API 简述 interface naming

|  Method | URL  | Description|
|---|---|---|
| POST  | /lcmcontroller/v1/app_instance/{appInstanceId}/instantiate | 实例化APP |
| POST  | /inventory/v1/location | 增加节点位置 |
| DELETE  | /inventory/v1/location | 删除节点位置 |
| GET  | /inventory/v1/location | 获取节点位置 |
| PUT  | /inventory/v1/location | 修改节点位置 |
| POST  | /inventory/v1/mepms | Adds MEPM configuration |
| PUT  | /inventory/v1/mepms/{mepm_id} | Updates MEPM configuration |
| GET  | /inventory/v1/mepms/{mepm_id} | Retrieves MEPM configuration |
| GET  | /inventory/v1/mepms | Retrieves all MEPM configuration |
| DELETE  | /inventory/v1/mepms/{mepm_id} | Deletes MEPM configuration |
| GET  | /apm/v1/tenants/{tenant_id}/packages/{app_package_id}/image/download | Downloads application image |
| POST  | /appo/v1/tenants/{tenant_id}/app_instances | Creates application instance |


### 接口API参考文档 Reference to the interfaces
http://docs.edgegallery.org/en/latest/Projects/MECM/MECM.html 

### 系统的限制目前有哪些 What are the system limits
No Change in this release
|  Category | Max Value  |
|---|---|
| No: of Tenants | 20 |
| No: of Application Instances per tenant | 50 |
| No: of Rules per tenant | 50 |
| No: of AppStore, MEC Host, MEPM | 50 |
