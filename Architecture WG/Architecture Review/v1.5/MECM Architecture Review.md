### 发布时间

2021.12

### 项目概述 Project Overview

MECM是边缘计算系统中的管控部分，负责实现边缘节点及资源管理和EG应用生命周期管理，并计划在V1.5版本引入轻量的云管功能（OpenStack）

### 从上一个版本开始架构的变化 Architecture changes from last release
新增Resource Manager模块（中心侧ResourceMgr，边缘侧ResController）
![架构变化](https://images.gitee.com/uploads/images/2021/1102/164622_c43de297_7786397.png "屏幕截图.png")

### v1.5 引入的新功能特性 New component capabilites for v1.5

1. 云管功能实现，新增三方系统（OpenStack）云管功能。

![ResController纳入Ingress路由](https://images.gitee.com/uploads/images/2021/1102/190711_5377c134_7786397.png "屏幕截图.png")

2. 支持边缘节点openrc按用户隔离


3. MECM支持zip格式应用包.


详细需求分析文档。


### 功能调用序列变化
![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/170635_d9828b6e_7786397.png "屏幕截图.png")

### 新增或修改的接口 New or modified interfaces
v1.5 版本新增接口，详情如下：
[虚机管理接口列表](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.5/v1.5newInterface-servers.md)

[镜像管理接口列表](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.5/v1.5newInterface-Image.md)

[规格管理接口列表](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.5/v1.5newInterface-Flavor.md)

[网络管理接口列表](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.5/v1.5newInterface-Network.md)

[安全组管理接口列表](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.5/v1.5newInterface-SecurityGroup.md)