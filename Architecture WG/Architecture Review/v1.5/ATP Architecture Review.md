### 发布时间 详细设计开始的第一周

详细设计开始时间: 2021.11.16
### 项目概述 Project Overview
应用测试认证服务，可以让用户选择应用包的测试场景，执行对应场景的测试用例。社区场景会对应用包进行安全性测试、遵从性测试（根据APPD、ETSI标准验证包结构）以及沙箱测试（应用包生命周期测试）， 对于测试通过的应用包，才可以发布到应用商店。


### 从上一个版本开始架构的变化 Architecture changes from last release
无架构变化

数据库变更：  
无


### v1.5 引入的新功能特性 New component capabilites for v1.3
无

### 新增或者修改的接口 New or modified interfaces
无

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible
NA

### 接口API 简述 interface naming
NA

### 接口API参考文档 Reference to the interfaces
https://gitee.com/edgegallery/docs/blob/master/Projects/ATP/ATP_Interfaces.md

### 系统的限制目前有哪些 What are the system limits
None