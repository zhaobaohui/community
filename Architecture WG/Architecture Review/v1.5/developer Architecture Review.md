### 发布时间

2021.12

### 项目概述 Project Overview

Developer是开发者开发和测试边缘应用的edgegallery平台，同时支持虚机和容器应用。该平台提供了应用打包、集成测试等功能，完成测试后可直接生成统一规范的应用包，
使开发者更加方便快捷的开发应用并集成到edgegallery平台，开发者最终也可以选择发布自己的应用到应用仓库

### 从上一个版本开始架构的变化 Architecture changes from last release
无架构变化

### v1.5 引入的新功能特性 New component capabilites for v1.5

1. 部署调测时，沙箱可显式选择，显示沙箱资源信息

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/105256_d11188a7_7625288.png "屏幕截图.png")

2. 支持虚机拉起后远程VNC（当前为SSH无法满足windows及桌面版linux场景）

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/105318_ba4e13dc_7625288.png "屏幕截图.png")

3. 支持应用多虚拟机场景：appd支持配置多虚机，lcm支持多虚机部署调测

4. 支持灵活打包能力，应用包APPD文件、镜像描述文件在线修改能力

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/105535_113fd70a_7625288.png "屏幕截图.png")

5. 支持自定义VM的Flavor（当前为固定规格列表）,支持网络自定义

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/105645_8dad3b78_7625288.png "屏幕截图.png")

6. 后台数据模型、接口重构以及流程优化

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/105731_4e167aa9_7625288.png "屏幕截图.png")

7.profile 模型支持（容器）
![输入图片说明](https://images.gitee.com/uploads/images/2021/1117/184111_934df359_7625288.png "屏幕截图.png")

8. 容器流程优化，支持helmcart包上传

详细需求分析文档。


### 数据库结构变更 Database Structure Change
v1.5 版本对数据表结构进行重构，详情如下

    CREATE TABLE IF NOT EXISTS "tbl_application" (
    "id" varchar(255) NOT NULL,
    "name" varchar(255) NOT NULL,
    "description" varchar(255) DEFAULT NULL,
    "version" varchar(255) NOT NULL,
    "provider" varchar(255) NOT NULL,
    "architecture" varchar(255) DEFAULT NULL,
    "app_class" varchar(255) DEFAULT NULL,
    "type" varchar(255) DEFAULT NULL,
    "industry" varchar(255) DEFAULT NULL,
    "icon_file_id" varchar(255) DEFAULT NULL,
    "guide_file_id" varchar(255) DEFAULT NULL,
    "app_create_type" varchar(255) DEFAULT NULL,
    "create_time" timestamptz(6)  DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "user_id" varchar(255) DEFAULT NULL,
    "user_name" varchar(255) DEFAULT NULL,
    "mep_host_id" varchar(255) DEFAULT NULL,
    CONSTRAINT  "tbl_application_unique_name_version" UNIQUE ("name","version"),
    CONSTRAINT "tbl_application_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_container_helm_chart" (
    "id" varchar(255) NOT NULL,
    "app_id" varchar(255) NOT NULL,
    "name" varchar(255) DEFAULT NULL,
    "helm_chart_file_id" text DEFAULT NULL,
    CONSTRAINT "tbl_container_helm_chart_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_vm" (
    "id" varchar(255) NOT NULL,
    "app_id" varchar(255) DEFAULT NULL,
    "name" varchar(255) NOT NULL,
    "flavor_id" varchar(255) DEFAULT NULL,
    "image_id" int4 DEFAULT NULL,
    "target_image_id" int4 DEFAULT NULL,
    "user_data" text DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "area_zone" varchar(255) DEFAULT NULL,
    "flavor_extra_specs"  text DEFAULT NULL,
    CONSTRAINT "tbl_vm_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_network" (
    "id" varchar(255) NOT NULL,
    "app_id" varchar(255) DEFAULT NULL,
    "name" varchar(255) NOT NULL,
    "description" varchar(255) DEFAULT NULL,
    CONSTRAINT "tbl_network_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_vm_port" (
    "id" varchar(255) NOT NULL,
    "vm_id" varchar(255) DEFAULT NULL,
    "name" varchar(255) DEFAULT NULL,
    "description" varchar(255) DEFAULT NULL,
    "network_name" varchar(255) DEFAULT NULL,
    CONSTRAINT "tbl_vm_port_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_vm_certificate" (
    "vm_id" varchar(255) DEFAULT NULL,
    "certificate_type" varchar(255) NOT NULL,
    "pwd_certificate" text DEFAULT NULL,
    "key_pair_certificate" text DEFAULT NULL,
    CONSTRAINT "tbl_vm_certificate_pkey" PRIMARY KEY ("vm_id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_vm_flavor" (
    "id" varchar(255) NOT NULL,
    "name" varchar(255) NOT NULL,
    "description" varchar(255) DEFAULT NULL,
    "architecture" varchar(255) DEFAULT NULL,
    "cpu" text DEFAULT NULL,
    "memory" varchar(255) DEFAULT NULL,
    "system_disk_size" int4 DEFAULT NULL,
    "data_disk_size"  int4 DEFAULT NULL,
    "gpu_extra_info" text DEFAULT NULL,
    "other_extra_info" text DEFAULT NULL,
    CONSTRAINT "tbl_vm_flavor_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_vm_image" (
    "id" SERIAL,
    "name" varchar(255) NOT NULL,
    "visible_type" varchar(255) DEFAULT NULL,
    "os_type" varchar(255) DEFAULT NULL,
    "os_version" varchar(255) DEFAULT NULL,
    "os_bit_type" varchar(255) DEFAULT NULL,
    "system_disk_size" int4 DEFAULT NULL,
    "image_file_name" varchar(255) DEFAULT NULL,
    "image_format" varchar(255) DEFAULT NULL,
    "down_load_url" varchar(255) DEFAULT NULL,
    "file_md5" varchar(255) DEFAULT NULL,
    "image_size" bigint DEFAULT NULL,
    "image_slim_status" varchar(50) DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "create_time" timestamptz(6)  DEFAULT NULL,
    "modify_time" timestamptz(6)  DEFAULT NULL,
    "upload_time" timestamptz(6)  DEFAULT NULL,
    "user_id" varchar(255) DEFAULT NULL,
    "user_name" varchar(255) DEFAULT NULL,
    "file_identifier" varchar(128) DEFAULT NULL,
    "error_type" varchar(32) DEFAULT NULL,
    CONSTRAINT "tbl_vm_image_uniqueName" UNIQUE ("name","user_id"),
    CONSTRAINT "tbl_vm_image_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_container_image" (
    "image_id" varchar(255) NOT NULL,
    "image_name" varchar(255) NOT NULL,
    "image_version" varchar(255) NOT NULL,
    "user_id" varchar(255) NOT NULL,
    "user_name" varchar(255) NOT NULL,
    "upload_time" timestamptz(0) DEFAULT NULL,
    "create_time" timestamptz(0) DEFAULT NULL,
    "image_status" varchar(255) DEFAULT NULL,
    "image_type" varchar(255) DEFAULT NULL,
    "image_path" text DEFAULT NULL,
    "file_name" varchar(255) DEFAULT NULL,
    CONSTRAINT  "tbl_container_image_uniqueName" UNIQUE ("image_name","image_version","user_name"),
    CONSTRAINT "tbl_container_image_pkey" PRIMARY KEY ("image_id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_app_traffic_rule" (
    "app_id" varchar(255) NOT NULL,
    "traffic_rule_id" varchar(255) NOT NULL,
    "action" varchar(255) DEFAULT NULL,
    "priority" int4 DEFAULT NULL,
    "filter_type" varchar(255) DEFAULT NULL,
    "traffic_filter" text DEFAULT NULL,
    "dst_interface" text DEFAULT NULL,
    CONSTRAINT  "tbl_app_traffic_rule_unique_id_traffic_rule" UNIQUE ("app_id","traffic_rule_id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_app_dns_rule" (
    "app_id" varchar(255) NOT NULL,
    "dns_rule_id" varchar(255) NOT NULL,
    "domain_name" varchar(255) DEFAULT NULL,
    "ip_address_type" varchar(255) DEFAULT NULL,
    "ip_address" varchar(255) DEFAULT NULL,
    "ttl" varchar(255) DEFAULT NULL,
    CONSTRAINT  "tbl_app_dns_rule_unique_id_dns_rule" UNIQUE ("app_id","dns_rule_id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_app_service_produced" (
    "app_id" varchar(50) NOT NULL,
    "app_service_produced_id" varchar(50) NOT NULL,
    "one_level_name" varchar(100) NOT NULL,
    "one_level_name_en" varchar(100) NOT NULL,
    "two_level_name" varchar(100) NOT NULL,
    "description" varchar(500) NOT NULL,
    "api_file_id" varchar(50) NOT NULL,
    "guide_file_id" varchar(50) NOT NULL,
    "icon_file_id" varchar(50) NOT NULL,
    "service_name" varchar(50) NOT NULL,
    "internal_port" int4 NOT NULL,
    "version" varchar(30) NOT NULL,
    "protocol" varchar(30) NOT NULL,
    "author" varchar(50) NOT NULL,
    "experience_url" varchar(500) DEFAULT NULL,
    "dns_rule_id_list" text DEFAULT NULL,
    "traffic_rule_id_list" text DEFAULT NULL,
    CONSTRAINT  "tbl_app_service_produced_unique_id_name" UNIQUE ("app_id","service_name")
    );

    CREATE TABLE IF NOT EXISTS "tbl_app_service_required" (
    "app_id" varchar(255) NOT NULL,
    "id" varchar(255) NOT NULL,
    "one_level_name" varchar(255) NOT NULL,
    "one_level_name_en" varchar(255) NOT NULL,
    "two_level_name" varchar(255) NOT NULL,
    "two_level_name_en" varchar(255) NOT NULL,
    "ser_name" varchar(255) NOT NULL,
    "version" varchar(255) DEFAULT NULL,
    "requested_permissions" bool DEFAULT NULL,
    "ser_app_id" varchar(255) DEFAULT NULL,
    "package_id" varchar(255) DEFAULT NULL,
    CONSTRAINT  "tbl_app_service_required_unique_id_name" UNIQUE ("app_id","ser_name")
    );

    CREATE TABLE IF NOT EXISTS "tbl_app_certificate" (
    "app_id" varchar(255) NOT NULL,
    "ak" text DEFAULT NULL,
    "sk" text DEFAULT NULL,
    CONSTRAINT "tbl_app_certificate_pkey" PRIMARY KEY ("app_id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_vm_instantiate_info" (
    "vm_id" varchar(255) NOT NULL,
    "operation_id" varchar(255) DEFAULT NULL,
    "app_package_id" varchar(255) DEFAULT NULL,
    "distributed_mec_host" varchar(255) DEFAULT NULL,
    "mepm_package_id" varchar(255) DEFAULT NULL,
    "app_instance_id" varchar(255) DEFAULT NULL,
    "vm_instance_id" varchar(255) DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "vnc_url" varchar(255) DEFAULT NULL,
    "log" text DEFAULT NULL,
    "instantiate_time" timestamptz(6)  DEFAULT NULL,
    CONSTRAINT "tbl_vm_instantiate_info_pkey" PRIMARY KEY ("vm_id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_vm_port_instantiate_info" (
    "vm_id" varchar(255) NOT NULL,
    "network_name" varchar(255) NOT NULL,
    "ip_address" varchar(255) DEFAULT NULL,
    CONSTRAINT  "tbl_vm_port_instantiate_info_unique_id_name" UNIQUE ("vm_id","network_name")
    );

    CREATE TABLE IF NOT EXISTS "tbl_vm_image_export_info" (
    "vm_id" varchar(255) NOT NULL,
    "operation_id" varchar(255) DEFAULT NULL,
    "image_instance_id" varchar(255) DEFAULT NULL,
    "name" varchar(255) DEFAULT NULL,
    "image_file_name" varchar(255) DEFAULT NULL,
    "format" varchar(255) DEFAULT NULL,
    "download_url" varchar(255) DEFAULT NULL,
    "check_sum" varchar(255) DEFAULT NULL,
    "image_size" varchar(255) DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "log" text DEFAULT NULL,
    "create_time" timestamptz(6)  DEFAULT NULL,
    CONSTRAINT "tbl_vm_image_export_info_pkey" PRIMARY KEY ("vm_id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_container_app_instantiate_info" (
    "app_id" varchar(255) NOT NULL,
    "app_package_id" varchar(255) DEFAULT NULL,
    "distributed_mec_host" varchar(255) DEFAULT NULL,
    "app_instance_id" varchar(255) DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "log" text DEFAULT NULL,
    "instantiate_time" timestamptz(6)  DEFAULT NULL,
    CONSTRAINT "tbl_container_app_instantiate_info_pkey" PRIMARY KEY ("app_id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_k8s_pod_instantiate_info" (
    "name" varchar(255) NOT NULL,
    "app_id" varchar(255) NOT NULL,
    "pod_status" varchar(255) DEFAULT NULL,
    "events_info" text DEFAULT NULL,
    CONSTRAINT  "tbl_k8s_pod_instantiate_info_unique_id_name" UNIQUE ("app_id","name")
    );

    CREATE TABLE IF NOT EXISTS "tbl_container_instantiate_info" (
    "name" varchar(255) NOT NULL,
    "pod_name" varchar(255) NOT NULL,
    "cpu_usage" varchar(255) DEFAULT NULL,
    "mem_usage" varchar(255) DEFAULT NULL,
    "disk_usage" varchar(255) DEFAULT NULL,
    CONSTRAINT  "tbl_container_instantiate_info_unique_id_name" UNIQUE ("pod_name","name")
    );

    CREATE TABLE IF NOT EXISTS "tbl_k8s_service_instantiate_info" (
    "name" varchar(255) NOT NULL,
    "app_id" varchar(255) NOT NULL,
    "type" varchar(255) DEFAULT NULL,
    CONSTRAINT "tbl_k8s_service_instantiate_info_pkey" PRIMARY KEY ("name")
    );

    CREATE TABLE IF NOT EXISTS "tbl_k8s_service_port_instantiate_info" (
    "port" varchar(255) NOT NULL,
    "service_name" varchar(255) NOT NULL,
    "target_port" varchar(255) DEFAULT NULL,
    "node_port" varchar(255) DEFAULT NULL,
    CONSTRAINT "tbl_k8s_service_port_instantiate_info_pkey" PRIMARY KEY ("service_name")
    );

    CREATE TABLE IF NOT EXISTS "tbl_operation_status" (
    "id" varchar(255) NOT NULL,
    "user_name" varchar(255) NOT NULL,
    "object_type" varchar(255) DEFAULT NULL,
    "object_id" varchar(255) DEFAULT NULL,
    "object_name" varchar(255) DEFAULT NULL,
    "operation_name" varchar(255) DEFAULT NULL,
    "progress" int4 DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "error_msg" text DEFAULT NULL,
    "create_time" timestamptz(6) DEFAULT NULL,
    "update_time" timestamptz(6)  DEFAULT NULL,
    CONSTRAINT "tbl_operation_status_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_action_status" (
    "id" varchar(255) NOT NULL,
    "operation_id" varchar(255) NOT NULL,
    "object_type" varchar(255) DEFAULT NULL,
    "object_id" varchar(255) DEFAULT NULL,
    "action_name" varchar(255) DEFAULT NULL,
    "progress" int4 DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "error_msg" text DEFAULT NULL,
    "status_log" text DEFAULT NULL,
    "update_time" timestamptz(6)  DEFAULT NULL,
    CONSTRAINT "tbl_action_status_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_app_package" (
    "id" varchar(255) NOT NULL,
    "app_id" varchar(255) NOT NULL,
    "package_file_name" varchar(255) DEFAULT NULL,
    CONSTRAINT "tbl_app_package_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_atp_test_task" (
    "id" varchar(255) NOT NULL,
    "app_id" varchar(255) NOT NULL,
    "app_name" varchar(255) DEFAULT NULL,
    "status" varchar(255) DEFAULT NULL,
    "create_time" varchar(255)  DEFAULT NULL,
    CONSTRAINT "tbl_atp_test_task_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_reverse_proxy" (
    "id" varchar(255) NOT NULL,
    "dest_host_id" varchar(255) NOT NULL,
    "dest_host_port" int4 NOT NULL,
    "proxy_port" int4 NOT NULL,
    "type" int4 NOT NULL,
    CONSTRAINT "tbl_reverse_proxy_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_profile" (
    "id" varchar(255) NOT NULL,
    "name" varchar(255) NOT NULL,
    "description" varchar(255) DEFAULT NULL,
    "description_en" varchar(255) DEFAULT NULL,
    "file_path" varchar(255) NOT NULL,
    "deploy_file_path" TEXT NOT NULL,
    "config_file_path" varchar(255) DEFAULT NULL,
    "seq" varchar(255) NOT NULL,
    "create_time" timestamptz(6)  NOT NULL,
    "type" varchar(255) NOT NULL,
    "industry" varchar(255) NOT NULL,
    "topo_file_path" varchar(255) DEFAULT NULL,
    CONSTRAINT "tbl_profile_pkey" PRIMARY KEY ("id")
    );

    CREATE TABLE IF NOT EXISTS "tbl_app_script" (
    "id" varchar(255) NOT NULL,
    "app_id" varchar(255) NOT NULL,
    "name" varchar(255) DEFAULT NULL,
    "script_file_id" text DEFAULT NULL,
    "create_time" timestamptz(6)  NOT NULL,
    CONSTRAINT "tbl_app_script_pkey" PRIMARY KEY ("id")
    );

### 新增或修改的接口 New or modified interfaces
v1.5 版本对接口进行重构，详情如下：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/113138_296641aa_7625288.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/113225_d15776c6_7625288.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/113321_94275900_7625288.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/113345_6980b9c7_7625288.png "屏幕截图.png")