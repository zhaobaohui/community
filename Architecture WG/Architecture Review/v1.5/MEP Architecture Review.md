### 发布时间 详细设计开始的第一周
2021.11

### 项目概述 Project Overview
MEP(MEC Platform) implmentation based on ETSI specification. The MEP server interface is divided into two categories, one is the Mp1 interface that follows the ETSI MEC 011 v2.1.1 standard, which mainly provides functions such as service registration discovery, app status notification subscription, and Dns rule acquisition; another is Mm5 interface, which mainly provides configuration management functions for MECM/MEPM.

### 从上一个版本开始架构的变化 Architecture changes from last release
None

### v1.5 引入的新功能特性 New component capabilites for v1.5
None


### 新增或者修改的接口 New or modified interfaces
None

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible
None

### 接口API 简述 interface naming
None

### 系统的限制目前有哪些 What are the system limits
None