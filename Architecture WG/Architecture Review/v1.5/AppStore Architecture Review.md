### 发布时间 
2021.11

### 项目概述 Project Overview
AppStore是开发者发布和上线App应用的市场，由Developer平台开发的边缘应用，经过测试可以直接分享到AppStore应用商店。AppStore分为前后台两个部分，appstore-be是后台部分，提供主要功能接口供前台或其他三方系统调用，appstore-fe是前台部分，提供界面展示。

### 从上一个版本开始架构的变化 Architecture changes from last release
无架构变化

### v1.5 引入的新功能特性 New component capabilites for v1.5(Chocolate)
* 上传应用做完整性校验

  上传的应用包会解析manifest文件，进行完整性校验：

  •    对source文件做hash校验，检查APPD文件、镜像文件等是否被改变；

  •    对签名值进行校验，校验签名是否正确。
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/173610_5d96b60f_8354563.png "mf.png")

  

* APP变现

  应用发布前增加了定价的选项，在APP Store中会显示定价信息与“订购”按钮。选择“订购”后，消息并没有发送到MEAO进行真实部署。

  经过与运营商讨论，可以分为两个步骤，产品发布与订购。这里有一个运营商中台的概念，很多功能是在产品发布与销售流程中，但是由中台统一提供。

  1. 产品发布：产商品中心，主要是第三方合作伙伴开发好的应用上架，进入销售清单，从而可以利用运营商的销售渠道推广。
  1. 订购：销售中心，主要是企业用户购买产品，业务开通，费用结算等。


  将应用变现流程归纳如下图所示：


  ![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/175218_b6552601_6560208.png "屏幕截图.png")

  进一步细分订购流程：

  1. 用户订购信息进入订单中心（中台能力）
  1. 审核无误后通知MEC运营进行应用部署
  1. 部署结果反馈订单中心
  1. 生成用户使用记录（原始记录）
  1. 生成记账实例（具体费用、中台能力）

  其中1、5的中台能力不需要EdgeGallery来实现。

### 新增或者修改的接口 New or modified interfaces

| URL                                                          | Method | Change Type | Request                                                      | Response                                                     |
| ------------------------------------------------------------ | ------ | ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| /mec/appstore/v1/experience/package/{packageId}/status       | GET    | Add         | packageId                                                    | {<br />     “data”: “experienceStatus”, <br />     “retCode”:  int,<br />     “message”: string, <br />     “params”:  []  <br />} |
| /mec/appstore/v2/packages/action/query                       | POST   | Add         | {<br />    “limit”: 10,<br />    “offset”: 0,<br />    “appName”: “”,<br />    “status”: [“”], <br />   “sortItem”: “”, <br />   “sortType”: “desc” <br /> } | Page<PackageDto>                                             |
| /mec/appstore/v2/packages                                    | Get    | Modify      | 游客可以调用；<br />可支持按照创建时间范围分页查询，如：<br />?startTime=2021-07-02&endTime=2021-07-03&limit=10&offset=0 | Page<PackageDto>                                             |
| /mec/appstore/v2/query/apps变更为：  /mec/appstore/v2/apps/action/query | POST   | Modify      | 查询条件中的status、appName字段移到QueryAppCtrlDto结构体内，复用已有结构。 | Page<AppDto>  AppDto中最新已发布应用包的packageId和exprienceAble信息，供首页、应用仓库、热门应用查询。 |

### App变现特性的接口变更

#### 扩展接口 Extend interfaces

#####  发布应用v1/v2（appstore前台调用的发布）

前台发布应用时设置定价，然后提交发布处理。

POST /mec/appstore/v1/apps/{appId}/packages/{packageId}/action/publish

POST /mec/appstore/v2/apps/{appId}/packages/{packageId}/action/publish

当前的发布应用接口没有请求体数据，新增请求体传递两个属性：是否免费、价格，这个可以扩展，比如增加硬件配套设备信息等，当前默认支持定价信息。

{

  "isFree": false,

  "price": 299

}

#### 新增接口--订单管理(New interfaces--order management)

##### 1 订购（创建订单）

```
Resource URI: /mec/appstore/v1/orders
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name         | Decription   | Data Type | Request Type | Required |
| ------------ | ------------ | --------- | ------------ | -------- |
| appId        | APPID        | String    | request body | 是       |
| appPackageId | APP包ID      | String    | request body | 是       |
| appName      | 应用名       | String    | request body | 是       |
| mecHostIp    | 边缘节点IP   | String    | request body | 是       |
| mecHostCity  | 边缘节点位置 | String    | request body | 是       |

Example request:

POST /mec/appstore/v1/orders

```json
{
  "appId": "",
  "appPackageId": "",
  "appName": "",
  "mecHostIp": "",
  "mecHostCity": ""
}
```



正常响应中的业务字段：

| Name     | Decription | Data Type | Response Type |
| -------- | ---------- | --------- | ------------- |
| orderId  | 订单ID     | String    | response body |
| orderNum | 订单编号   | String    | response body |




Example response:

```json
response 200 OK
{
  "data": {
      "orderId": "",
      "orderNum": "",
  },
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 2 查询订单列表

（1）admin作为超级管理员，查询时返回所有用户（包括自己）的订单

（2）非admin用户（包括ADMIN和TENANT两种角色的用户）查询时只返回自己的订单

```
Resource URI: /mec/appstore/v1/orders/list
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name           | Decription                                                   | Data Type | Request Type | Required |
| -------------- | ------------------------------------------------------------ | --------- | ------------ | -------- |
| appId          | 应用ID（支持按照选定的特定APP查询订单）                      | String    | request body | 否       |
| orderNum       | 订单编号（支持按照订单编号模糊查询）                         | String    | request body | 否       |
| status         | 订单状态（支持按照选定的特定状态查询订单）                   | String    | request body | 否       |
| orderTimeBegin | 下单时间范围的起始时间（支持按照下单时间范围查询订单）       | String    | request body | 否       |
| orderTimeEnd   | 下单时间范围的结束时间（支持按照下单时间范围查询订单）       | String    | request body | 否       |
| offset         | 分页查询起始位置                                             | int       | request body | 是       |
| limit          | 分页查询每页查询数量                                         | int       | request body | 是       |
| sortItem       | 排序字段（ORDERTIME：支持按照下单时间排序。当不指定时，后台默认按照下单时间降序排序） | String    | request body | 否       |
| sortType       | 排序方式（ASC--升序;DESC--降序）                             | String    | request body | 否       |

Example request:

POST /mec/appstore/v1/orders/list

```json
{
  "appId": "",
  "orderNum": "",
  "status": "",
  "orderTimeBegin": "",
  "orderTimeEnd": "",
  "queryCtrl": {
    "offset": 0,
    "limit": 20,
    "sortItem": "ORDERTIME",
    "sortType": "DESC"
  }
}
```



正常响应中的业务字段：

| Name        | Decription                                             | Data Type | Response Type |
| ----------- | ------------------------------------------------------ | --------- | ------------- |
| totalCount  | 符合查询条件的订单总数                                 | int       | response body |
| orderList   | 订单列表                                               | List      | response body |
| orderId     | 订单ID                                                 | String    | response body |
| orderNum    | 订单编号                                               | String    | response body |
| userId      | 订单用户ID                                             | String    | response body |
| userName    | 订单用户名称                                           | String    | response body |
| appId       | 应用ID                                                 | String    | response body |
| appName     | 应用名称                                               | String    | response body |
| orderTime   | 下单时间                                               | String    | response body |
| operateTime | 除下单之外的操作（部署、退订、去部署、激活）的时间     | String    | response body |
| status      | 订单状态，当状态为ACTIVATING时，调用MECM接口刷新状态。 | String    | response body |
| mecHostIp   | 边缘节点IP                                             | String    | response body |
| mecHostCity | 边缘节点位置                                           | String    | response body |
| detailCn    | 订单操作详情中文                                       | String    | response body |
| detailEn    | 订单操作详情英文                                       | String    | response body |




Example response:

```json
response 200 OK
{
  "results": [
    {
      "orderId": "",
      "orderNum": "",
      "userId": "",
      "userName": "",
      "appId": "",
      "appName": "",
      "orderTime": "",
      "operateTime": "",
      "status": "",
      "mecHostIp": "",
      "mecHostCity": "",
      "detailCn": "",
      "detailEn": ""
    },
    {...}
  ],
  "offset": 0,
  "limit": 20,
  "total": 100
}
```



##### 3 退订

用户（包括ADMIN和TENANT两种角色的用户）只能对自己的订单执行退订

```
Resource URI: /mec/appstore/v1/orders/{orderId}/deactivation
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name    | Decription | Data Type | Request Type | Required |
| ------- | ---------- | --------- | ------------ | -------- |
| orderId | 订单ID     | String    | path param   | 是       |

Example request:

POST /mec/appstore/v1/orders/5a75c7f6-5b1d-4c87-a92f-7c7254b25f39/deactivation



正常响应中的业务字段：

无




Example response:

```json
response 200 OK
{
  "data": null,
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 4 激活

用户（包括ADMIN和TENANT两种角色的用户）只能对自己的订单执行激活操作

```
Resource URI: /mec/appstore/v1/orders/{orderId}/activation
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name    | Decription | Data Type | Request Type | Required |
| ------- | ---------- | --------- | ------------ | -------- |
| orderId | 订单ID     | String    | path param   | 是       |

Example request:

POST /mec/appstore/v1/orders/5a75c7f6-5b1d-4c87-a92f-7c7254b25f39/activation



正常响应中的业务字段：

无




Example response:

```json
response 200 OK
{
  "data": null,
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



#### 封装Mecm侧的接口

##### 1 获取所有的边缘节点列表

AppStore前台需要展示边缘节点列表，该列表来源于Mecm，由AppStore后台封装一个接口供前台调用。

```
Resource URI: /mec/appstore/v1/mechosts
Method: GET
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name      | Decription | Data Type | Request Type  | Required |
| --------- | ---------- | --------- | ------------- | -------- |
| appId     | 应用ID     | String    | request param | 否       |
| packageId | 应用包ID   | String    | request param | 否       |

Example request:

GET /mec/appstore/v1/mechosts/?appId=995f9f7b7b8a482a8dc1e10028ec205d&packageId=106454904ffb46a0840951706399377f



正常响应中的业务字段：

| Name        | Decription   | Type          |
| ----------- | ------------ | ------------- |
| mechostIp   | 边缘节点IP   | response body |
| mechostName | 边缘节点名称 | response body |
| mechostCity | 边缘节点位置 | response body |
| vim         | 边缘节点系统 | response body |
| affinity    | 边缘节点架构 | response body |



Example response:

```json
response 200 OK
{
  "data": [
      {
          "mechostIp": "119.8.47.5",
          "mechostName": "",
          "mechostCity": "",
          "vim": "",
          "affinity": ""
      }
  ],
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



## 数据库变更(Database Change)

| Table   Name          | Field Name       | Field Type | Field Length | Default | Value        | Change Type | Compatibility |
| --------------------- | ---------------- | ---------- | ------------ | ------- | ------------ | ----------- | ------------- |
| Catalog_package_table | experienceStatus | varchar    | 100          | NULL    | 当前体验状态 | Add         | Compatible    |



### App变现特性的数据库变更

#### 扩展的表Extend tables

##### 1 应用表--app_table

| 字段名 | 字段含义         | 字段类型      | 字段长度 | 是否允许为空 | 默认值 | 是否主键 | 变更类型 |
| ------ | ---------------- | ------------- | -------- | ------------ | ------ | -------- | -------- |
| ISFREE | 是否免费         | BOOLEAN       | --       | NULL         | true   | 否       | 新增字段 |
| PRICE  | 价格（RMB/小时） | NUMERIC(10,2) | --       | NULL         |        | 否       | 新增字段 |



#### 新增表New tables

##### 1 订单表--app_order

| 字段名            | 字段含义                                               | 字段类型  | 字段长度 | 是否允许为空 | 是否主键 |
| ----------------- | ------------------------------------------------------ | --------- | -------- | ------------ | -------- |
| ORDERID           | 订单ID                                                 | VARCHAR   | 200      | NOT NULL     | 是       |
| ORDERNUM          | 订单编号（用户可见）                                   | VARCHAR   | 50       | NOT NULL     | 否       |
| USERID            | 订单用户ID                                             | VARCHAR   | 100      | NOT NULL     | 否       |
| USERNAME          | 订单用户名称                                           | VARCHAR   | 100      | NOT NULL     | 否       |
| APPID             | 应用ID                                                 | VARCHAR   | 200      | NOT NULL     | 否       |
| APPPACKAGEID      | 应用包ID                                               | VARCHAR   | 200      | NOT NULL     | 否       |
| APPNAME           | 应用名称                                               | VARCHAR   | 100      | NULL         | 否       |
| ORDERTIME         | 下单时间                                               | TIMESTAMP |          | NOT NULL     | 否       |
| OPERATETIME       | 记录除下单之外的操作（部署、退订、去部署、激活）的时间 | TIMESTAMP |          | NULL         | 否       |
| STATUS            | 订单状态                                               | VARCHAR   | 50       | NOT NULL     | 否       |
| MECM_HOSTIP       | 订购时选择的要部署的边缘节点IP地址                     | VARCHAR   | 1024     | NULL         | 否       |
| MECM_HOSTCITY     | 订购时选择的要部署的边缘节点位置                       | VARCHAR   | 1024     | NULL         | 否       |
| MECM_APPPACKAGEID | 应用包上传到MECM后生产的APP Package ID                 | VARCHAR   | 200      | NULL         | 否       |
| DETAILCN          | 订单操作详情（中文），记录订单操作活动和时间           | text      |          | NULL         | 否       |
| DETAILEN          | 订单操作详情（英文），记录订单操作活动和时间           | text      |          | NULL         | 否       |

（1）基于ORDERNUM创建唯一索引；基于USERID字段创建普通索引

（2）订单状态定义

| 状态含义             | 状态值            | 可进行的操作                                                 |
| -------------------- | ----------------- | ------------------------------------------------------------ |
| 正在激活             | ACTIVATING        | 正在激活的订单禁止任何操作<br>后台如果收到对同一个订单进行激活操作则失败，返回错误 |
| 激活失败             | ACTIVATE_FAILED   | 提供激活操作入口。即激活失败状态下，可以再次执行激活         |
| 已激活（部署成功）   | ACTIVATED         | 提供退订操作入口                                             |
|                      |                   |                                                              |
| 正在停用             | DEACTIVATING      | 正在停用的订单禁止任何操作<br>后台如果收到对同一个订单进行激活操作则失败，返回错误 |
| 停用失败             | DEACTIVATE_FAILED | 提供退订操作入口。即停用失败状态下，可以再次执行退订         |
| 已停用（去部署成功） | DEACTIVATED       | 提供激活操作入口                                             |

（3）状态设置

​      (a) 订购下单：提交至后台，后台创建订单成功后，首先设置订单状态为“正在激活“，然后查询订单时，如果还是正在激活状态，调用mecm接口获取部署状态，如果返回状态为“Finished"则设置为“已激活”，如为”Distribute Error"、”Instantiate Error"和”Create Error"设置为“激活失败”。

​      (b) 激活：提交至后台，后台首先设置订单状态为“正在激活"，然后查询订单时，如果还是正在激活状态，调用mecm接口获取部署状态，如果返回状态为“Finished"则设置为“已激活”，如为”Distribute Error"、”Instantiate Error"和”Create Error"设置为“激活失败”。

​      (c) 退订：提交到后台，后台首先设置订单状态为“正在停用”，然后调用mecm接口删除实例，如果成功则设置为“已停用”，否则设置为“停用失败”。

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible

兼容

### 接口API 简述 interface naming

https://gitee.com/edgegallery/docs/blob/master/Projects/APPSTORE/AppStore_Interfaces.md

### 系统的限制目前有哪些 What are the system limits