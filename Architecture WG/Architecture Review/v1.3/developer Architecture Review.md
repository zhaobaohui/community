### 发布时间 
2021.8

### 项目概述 Project Overview
Developer是开发者开发和测试边缘应用的edgegallery平台，该平台提供了开发工具、开放的API能力、集成测试验证环境等，完成测试后可直接生成统一规范的应用包，
使开发者更加方便快捷的开发应用并集成到edgegallery平台，开发者最终也可以选择发布自己的应用到appstore或者发布生态API。

### 从上一个版本开始架构的变化 Architecture changes from last release
![输入图片说明](https://images.gitee.com/uploads/images/2021/0810/175718_def37c77_7625288.png "屏幕截图.png")

### v1.3 引入的新功能特性 New component capabilites for v1.2(Chocolate)
* 应用包签名

  1. 公私钥文件做成可配置，用户可以使用自己的密钥，部署时指定为自己的公私钥文件，没有的话就用EG默认的密钥；

  2. 应用包中对于镜像文件、镜像描述文件、APPD文件进行hash计算，同时对Source、Alagorithm、Hash信息进行数字签名；

* 容器镜像管理

  支持容器镜像的增删改查

  容器镜像支持用户隔离，对接Harbor不同的项目

  支持容器镜像的上传、下载、删除、取消等操作


* 虚机应用部署调测优化

  1. 新增虚机部署调测-释放环境接口

  2. 虚机应用部署调测，支持网络ip自动分配，修复ip被占用问题

* 能力中心管理优化

  1. 新增在线体验字段

  2. 能力中心管理支持可编辑

  
  

### 新增或者修改的接口 New or modified interfaces

| **API**                                | **方法** | API说明      | **变更类型** | **说明**         |
| -------------------------------------- | -------- | ------------ | ------------ | ---------------- |
| /mec/developer/v2/container/images        | POST| 新增容器镜像 | 新增         | 容器镜像管理接口 |
| /mec/developer/v2/container/images/list          | GET     | 查询容器镜像接口     | 新增         | 容器镜像管理接口 |
| /mec/developer/v2/container/images/{imageId} | PUT      | 修改镜像信息     | 新增         | 容器镜像管理接口 |
| /mec/developer/v2/container/images/{imageId} | DELETE   | 删除镜像信息     | 新增         | 容器镜像管理接口 |
| /mec/developer/v2/container/images/{imageId}/download | GET   | 下载镜像     | 新增         | 容器镜像管理接口 |
| /mec/developer/v2/container/images/{imageId}/upload |POST  | 上传镜像信息     | 新增         | 容器镜像管理接口 |



## 数据库变更(Database Change)
新增 tbl_container_image表

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible

兼容

### 接口API 简述 interface naming

https://gitee.com/edgegallery/docs/blob/master/Projects/APPSTORE/AppStore_Interfaces.md

### 系统的限制目前有哪些 What are the system limits