### 发布时间 详细设计开始的第一周
2021.8

### 项目概述 Project Overview
MEP(MEC Platform) implmentation based on ETSI specification. The MEP server interface is divided into two categories, one is the Mp1 interface that follows the ETSI MEC 011 v2.1.1 standard, which mainly provides functions such as service registration discovery, app status notification subscription, and Dns rule acquisition; another is Mm5 interface, which mainly provides configuration management functions for MECM/MEPM.

### 从上一个版本开始架构的变化 Architecture changes from last release
None

### v1.3 引入的新功能特性 New component capabilites for v1.3
* 统计MEP接口调用方数据，供审计使用. [Design](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.3/MEP%20v1.3%20requirements.md)
* Service支持多endPointInfo. [Design](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.3/mep-app-supports-multiple-endpoints.md)
* MEC application graceful termination/stop. [Design](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.3/mep-app-termination-enhance.md)
* App confirm ready task. [Design](https://gitee.com/edgegallery/community/blob/master/MEP%20PT/Release%20V1.3/mep-app-confirm-ready.md)


### 新增或者修改的接口 New or modified interfaces
* Confirm ready task interface.
* MEC application graceful termination/stop interface.
* App service interface.

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible
兼容

### 接口API 简述 interface naming

|  Method | URL  | Description|
|---|---|---|
| POST | /mep/mec_app_support/v1/applications/{appInstanceId}/confirm_ready | Confirm ready task interface |
| POST | /mep/mec_app_support/v1/applications/:appInstanceId/AppInstanceTermination | MEC application graceful termination/stop interface |
| POST/PUT/DELETE | /mec_service_mgmt/v1/applications/{appInstanceId}/services | App service interface |

### 系统的限制目前有哪些 What are the system limits
None