

### 数据库结构变更(Database Change)

数据库结构变更均在AppStore侧。Developer侧无数据库变更。



#### 扩展的表Extend tables

##### 1 应用表--app_table

| 字段名 | 字段含义         | 字段类型      | 字段长度 | 是否允许为空 | 默认值 | 是否主键 | 变更类型 |
| ------ | ---------------- | ------------- | -------- | ------------ | ------ | -------- | -------- |
| ISFREE | 是否免费         | BOOLEAN       | --       | NULL         | true   | 否       | 新增字段 |
| PRICE  | 价格（RMB/小时） | NUMERIC(10,2) | --       | NULL         |        | 否       | 新增字段 |



#### 新增表New tables

##### 1 订单表--app_order

| 字段名            | 字段含义                                               | 字段类型  | 字段长度 | 是否允许为空 | 是否主键 |
| ----------------- | ------------------------------------------------------ | --------- | -------- | ------------ | -------- |
| ORDERID           | 订单ID                                                 | VARCHAR   | 200      | NOT NULL     | 是       |
| ORDERNUM          | 订单编号（用户可见）                                   | VARCHAR   | 50       | NOT NULL     | 否       |
| USERID            | 订单用户ID                                             | VARCHAR   | 100      | NOT NULL     | 否       |
| USERNAME          | 订单用户名称                                           | VARCHAR   | 100      | NOT NULL     | 否       |
| APPID             | 应用ID                                                 | VARCHAR   | 200      | NOT NULL     | 否       |
| APPPACKAGEID      | 应用包ID                                               | VARCHAR   | 200      | NOT NULL     | 否       |
| ORDERTIME         | 下单时间                                               | TIMESTAMP |          | NOT NULL     | 否       |
| OPERATETIME       | 记录除下单之外的操作（部署、退订、去部署、激活）的时间 | TIMESTAMP |          | NULL         | 否       |
| STATUS            | 订单状态                                               | VARCHAR   | 50       | NOT NULL     | 否       |
| MECM_HOSTIP       | 订购时选择的要部署的边缘节点IP地址                     | VARCHAR   | 1024     | NULL         | 否       |
| MECM_APPID        | 应用包上传到MECM后生成的APP ID                         | VARCHAR   | 200      | NULL         | 否       |
| MECM_APPPACKAGEID | 应用包上传到MECM后生产的APP Package ID                 | VARCHAR   | 200      | NULL         | 否       |
| MECM_INSTANCEID   | 应用部署到MECM的实例ID                                 | VARCHAR   | 200      | NULL         | 否       |

（1）基于ORDERNUM创建唯一索引；基于USERID字段创建普通索引

（2）订单状态定义

| 状态含义           | 状态值            | 可进行的操作                                                 |
| ------------------ | ----------------- | ------------------------------------------------------------ |
| 正在激活           | ACTIVATING        | 正在激活的订单禁止任何操作<br>后台如果收到对同一个订单进行激活操作则失败，返回错误 |
| 激活失败           | ACTIVATE_FAILED   | 提供激活操作入口。即激活失败状态下，可以再次执行激活         |
| 已激活（部署成功） | ACTIVATED         | 提供退订操作入口                                             |
|                    |                   |                                                              |
| 正在停用           | DEACTIVATING      | 正在停用的订单禁止任何操作<br>后台如果收到对同一个订单进行激活操作则失败，返回错误 |
| 停用失败           | DEACTIVATE_FAILED | 提供退订操作入口。即停用失败状态下，可以再次执行退订         |
| 已停用（拆除成功） | DEACTIVATED       | 提供激活操作入口                                             |

（3）状态设置

​      (a) 订购下单：提交至后台，后台创建订单成功后，首先设置订单状态为“正在激活“，然后调用mecm接口进行应用包上传、部署。如果上传、部署操作成功则设置订单状态为“已激活”，否则设置为“激活失败”。

​      (b) 激活：提交至后台，后台首先设置订单状态为“正在激活"，然后调用mecm接口进行应用包上传、部署。如果上传、部署操作成功则设置订单状态为“已激活”，否则设置为“激活失败”。

​      (c) 退订：提交到后台，后台首先设置订单状态为“正在停用”，然后调用mecm接口进行拆除。如果拆除成功则设置订单状态为“已停用”，否则设置为“停用失败“。



##### 2 账单表--app_bill

| 字段名      | 字段含义                                                     | 字段类型      | 字段长度 | 是否允许为空 | 是否主键/外键 |
| ----------- | ------------------------------------------------------------ | ------------- | -------- | ------------ | ------------- |
| BILLID      | 账单ID                                                       | VARCHAR       | 200      | NOT NULL     | 是            |
| ORDERID     | 订单ID                                                       | VARCHAR       | 200      | NOT NULL     | 否            |
| CREATETIME  | 账单产生时间                                                 | TIMESTAMP     | --       | NOT NULL     | 否            |
| USERID      | 账单用户ID                                                   | VARCHAR       | 100      | NOT NULL     | 否            |
| USERNAME    | 账单用户名称                                                 | VARCHAR       | 100      | NOT NULL     | 否            |
| BILLTYPE    | 主类型(IN:入账/OUT:出账)                                     | VARCHAR       | 5        | NOT NULL     | 否            |
| BILLSUBTYPE | 子类型(OPERATOR:运营商分摊收入/APPSUPPLY:提供应用的收入/APPCONSUME:应用消费支出) |  VARCHAR       | 20        | NOT NULL     | 否            |
| BILLAMOUNT  | 账单金额（如果是出账，账单金额是分摊到运营商+供应商的费用之和） | NUMERIC(10,2) | --       | NULL         | 否            |
| OPERATORFEE | 如果是出账，记录分摊到运营商的费用                           | NUMERIC(10,2) | --       | NULL         | 否            |
| SUPPLIERFEE | 如果是出账，记录分摊到供应商的费用                           | NUMERIC(10,2) | --       | NULL         | 否            |

（1）基于ORDERID字段设置外键，引用app_order表的ORDERID



##### 3 分账配置表--app_split_config

| 字段名     | 字段含义 | 字段类型     | 字段长度 | 是否允许为空 | 默认值 | 是否主键 |
| ---------- | -------- | ------------ | -------- | ------------ | ------ | -------- |
| APPID      | 应用ID   | VARCHAR      | 200      | NOT NULL     |        | 是       |
| SPLITRATIO | 分账比例 | NUMERIC(3,2) | --       | NULL         |        | 否       |

（1）对于全局的分账比例配置，通过APPID=all来存放配置



### 接口定义(interfaces define)

#### 扩展接口 Extend interfaces

##### 1 应用发布--developer侧



POST /mec/developer/v1/projects/{projectId}/action/upload



当前的发布应用接口没有请求体数据，新增请求体传递两个属性：是否免费、价格

{

  "isFree": false,

  "price": 299

}



接口实现中调用了appstore侧的“发布应用v1”接口，再把新增属性传递过去



##### 2 发布应用v1（developer侧调用的发布）



POST /mec/appstore/v1/apps/{appId}/packages/{packageId}/action/publish



当前的发布应用v1接口没有请求体数据，新增请求体传递两个属性：是否免费、价格

{

  "isFree": false,

  "price": 299

}



##### 3 发布应用v2（appstore前台调用的发布）



v2接口供AppStore前台的发布操作使用。AppStore前台发布应用时设置定价，然后提交发布处理。

POST /mec/appstore/v2/apps/{appId}/packages/{packageId}/action/publish



当前的发布应用v2接口没有请求体数据，新增请求体传递两个属性：是否免费、价格

{

  "isFree": false,

  "price": 299

}



##### 4 查询单个应用的详情v1



GET /mec/appstore/v1/apps/{appId}



响应结构中增加两个属性：是否免费、价格

{

  "free": false,

  "price": 299

}



##### 5 查询单个应用的详情v2



GET  /mec/appstore/v2/apps/{appId}

变更内容同上



##### 6 查询应用列表v2（应用仓库加载应用列表）

POST /mec/appstore/v2/query/apps

响应结构包括一个APP列表，每个APP的结构增加两个属性：是否免费、价格

同上



#### 新增接口--订单管理(New interfaces--order management)

##### 1 订购（创建订单）

```
Resource URI: /mec/appstore/v1/orders
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name         | Decription | Data Type | Request Type  | Required |
| ------------ | ---------- | --------- | ------------- | -------- |
| appId        | APPID      | String    | request body  | 是       |
| appPackageId | APP包ID    | String    | request body  | 是       |
| mecHostIp    | 边缘节点IP | String    | request body  | 是       |

Example request:

POST /mec/appstore/v1/orders

```json
{
  "appId": "",
  "appPackageId": "",
  "mecHostIp": ""
}
```



正常响应中的业务字段：

| Name     | Decription | Data Type | Response Type |
| -------- | ---------- | --------- | ------------- |
| orderId  | 订单ID     | String    | response body |
| orderNum | 订单编号   | String    | response body |




Example response:

```json
response 200 OK
{
  "data": {
      "orderId": "",
      "orderNum": "",
  },
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 2 查询订单列表

（1）admin作为超级管理员，查询时返回所有用户（包括自己）的订单

（2）非admin用户（包括ADMIN和TENANT两种角色的用户）查询时只返回自己的订单

```
Resource URI: /mec/appstore/v1/orders/list
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name           | Decription                                                   | Data Type | Request Type  | Required |
| -------------- | ------------------------------------------------------------ | --------- | ------------- | -------- |
| appId          | 应用ID（支持按照选定的特定APP查询订单）                      | String    | request body  | 否       |
| orderNum       | 订单编号（支持按照订单编号模糊查询）                         | String    | request body  | 否       |
| status         | 订单状态（支持按照选定的特定状态查询订单）                   | String    | request body  | 否       |
| orderTimeBegin | 下单时间范围的起始时间（支持按照下单时间范围查询订单）       | String    | request body  | 否       |
| orderTimeEnd   | 下单时间范围的结束时间（支持按照下单时间范围查询订单）       | String    | request body  | 否       |
| offset         | 分页查询起始位置                                             | int       | request body  | 是       |
| limit          | 分页查询每页查询数量                                         | int       | request body  | 是       |
| sortItem       | 排序字段（ORDERTIME：支持按照下单时间排序。当不指定时，后台默认按照下单时间降序排序） | String    | request body  | 否       |
| sortType       | 排序方式（ASC--升序;DESC--降序）                             | String    | request body  | 否       |

Example request:

POST /mec/appstore/v1/orders/list

```json
{
  "appId": "",
  "orderNum": "",
  "status": "",
  "orderTimeBegin": "",
  "orderTimeEnd": "",
  "queryCtrl": {
    "offset": 0,
    "limit": 20,
    "sortItem": "ORDERTIME",
    "sortType": "DESC"
  }
}
```



正常响应中的业务字段：

| Name        | Decription                                                   | Data Type | Response Type |
| ----------- | ------------------------------------------------------------ | --------- | ------------- |
| totalCount  | 符合查询条件的订单总数                                       | int       | response body |
| orderList   | 订单列表                                                     | List      | response body |
| orderId     | 订单ID                                                       | String    | response body |
| orderNum    | 订单编号                                                     | String    | response body |
| userId      | 订单用户ID                                                   | String    | response body |
| userName    | 订单用户名称                                                 | String    | response body |
| appId       | 应用ID                                                       | String    | response body |
| appName     | 应用名称（应用可能被删除，但订单会保留。此种清空下应用名称为空，页面上显示“应用已删除”，并且不提供激活操作） | String    | response body |
| appVersion  | 应用版本（同上）                                             | String    | response body |
| orderTime   | 下单时间                                                     | String    | response body |
| operateTime | 除下单之外的操作（部署、退订、去部署、激活）的时间           | String    | response body |
| status      | 订单状态                                                     | String    | response body |
| mecHostIp   | 边缘节点IP                                                   | String    | response body |
| mecHostName | 边缘节点名称                                                 | String    | response body |
| mecHostCity | 边缘节点位置                                                 | String    | response body |




Example response:

```json
response 200 OK
{
  "results": [
    {
      "orderId": "",
      "orderNum": "",
      "userId": "",
      "userName": "",
      "appId": "",
      "appName": "",
      "appVersion": "",
      "orderTime": "",
      "operateTime": "",
      "status": "",
      "mecHostIp": "",
      "mecHostName": "",
      "mecHostCity": ""
    },
    {...}
  ],
  "offset": 0,
  "limit": 20,
  "total": 100
}
```



##### 3 退订

（1）admin作为超级管理员，可以对所有用户的订单执行退订

（2）非admin用户（包括ADMIN和TENANT两种角色的用户）只能对自己的订单执行退订

```
Resource URI: /mec/appstore/v1/orders/{orderId}/deactivation
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name    | Decription | Data Type | Request Type  | Required |
| ------- | ---------- | --------- | ------------- | -------- |
| orderId | 订单ID     | String    | path param    | 是       |

Example request:

POST /mec/appstore/v1/orders/5a75c7f6-5b1d-4c87-a92f-7c7254b25f39/deactivation



正常响应中的业务字段：

无




Example response:

```json
response 200 OK
{
  "data": null,
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 4 激活

（1）admin作为超级管理员，可以对所有用户的订单执行激活

（2）非admin用户（包括ADMIN和TENANT两种角色的用户）只能对自己的订单执行激活操作

```
Resource URI: /mec/appstore/v1/orders/{orderId}/activation
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name    | Decription | Data Type | Request Type  | Required |
| ------- | ---------- | --------- | ------------- | -------- |
| orderId | 订单ID     | String    | path param    | 是       |

Example request:

POST /mec/appstore/v1/orders/5a75c7f6-5b1d-4c87-a92f-7c7254b25f39/activation



正常响应中的业务字段：

无




Example response:

```json
response 200 OK
{
  "data": null,
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



#### 新增接口--应用分账配置(New interfaces--split configuration)

##### 1 查询应用的分账配置

（1）只有作为超级管理员的admin可以对分账配置进行管理

（2）分账比例配置查询接口返回所有应用的分账配置，包括一条全局的分账配置。不支持分页。（只展示全局配置和已经单独添加了分账比例配置的应用。单独添加具体应用的配置，我们认为比较少）

```
Resource URI: /mec/appstore/v1/apps/splitconfigs/
Method: GET
Role: APPSTORE_ADMIN
```

请求：无请求信息

Example request:

GET /mec/appstore/v1/apps/splitconfigs/



正常响应中的业务字段：

| Name       | Decription                                | Data Type | Reponse Type  |
| ---------- | ----------------------------------------- | --------- | ------------- |
| appId      | 应用ID<br>当appId=all时，对应全局分账配置 | String    | response body |
| appName    | 应用名称<br/>当appId=all时为空            | String    | response body |
| provider   | 应用厂商<br/>当appId=all时为空            | String    | response body |
| splitRatio | 分账比例                                  | double    | response body |



Example response:

```json
response 200 OK
{
  "data": [
      {
          "appId": "all",
          "appName": "",
          "provider": "",
          "splitRatio": 0.15
      },
      {
          "appId": "xxxxxxxxxxxx",
          "appName": "PCB检测",
          "provider": "Some Company",
          "splitRatio": 0.25
      }
  ],
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 2 添加应用的分账配置

（1）只有作为超级管理员的admin可以对分账配置进行管理

```
Resource URI: /mec/appstore/v1/apps/splitconfigs
Method: POST
Role: APPSTORE_ADMIN
```

请求：

| Name       | Decription | Data Type      | Request Type  | Required |
| ---------- | ---------- | -------------- | ------------- | -------- |
| appIds     | 应用ID列表 | List of String | request body  | 是       |
| splitRatio | 分账比例   | double         | request body  | 是       |

Example request:

添加具体应用的分账配置：

POST /mec/appstore/v1/apps/splitconfigs

{

  "appIds": ["", ""],

  "splitRatio": 0.25

}



正常响应中的业务字段：

无



Example response:

```json
response 200 OK
{
  "data": null,
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 3 修改应用的分账配置

（1）只有作为超级管理员的admin可以对分账配置进行管理

```
Resource URI: /mec/appstore/v1/apps/splitconfigs/{appId}
Method: PUT
Role: APPSTORE_ADMIN
```

请求：

| Name       | Decription | Data Type | Request Type  | Required |
| ---------- | ---------- | --------- | ------------- | -------- |
| appId      | 应用ID     | String    | path param    | 是       |
| splitRatio | 分账比例   | double    | request body  | 是       |

Example request:

修改全局分账配置：

PUT /mec/appstore/v1/apps/splitconfigs/all

{

  "splitRatio": 0.25

}



修改具体应用的分账配置：

PUT /mec/appstore/v1/apps/splitconfigs/32078d92-cc42-42d5-aebb-d7b94d488461

{

  "splitRatio": 0.25

}



正常响应中的业务字段：

无



Example response:

```json
response 200 OK
{
  "data": null,
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 4 删除应用的分账配置

（1）只有作为超级管理员的admin可以对分账配置进行管理

```
Resource URI: /mec/appstore/v1/apps/splitconfigs/{appId}
Method: DELETE
Role: APPSTORE_ADMIN
```

请求：

| Name   | Decription | Data Type      | Request Type  | Required |
| ------ | ---------- | -------------- | ------------- | -------- |
| appId      | 应用ID     | String    | path param    | 是       |

Example request:

删除具体应用的分账配置：

DELETE /mec/appstore/v1/apps/splitconfigs/xxxxxxxxxxxxxxxxxxxx



正常响应中的业务字段：

无



Example response:

```json
response 200 OK
{
  "data": null,
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



#### 新增接口--运营情况与账单查询统计(New interfaces--operational situation and bill query and statistic)

##### 1 账单列表查询

（1）admin作为超级管理员，查询时返回所有用户（包括自己）的账单

（2）非admin用户（包括ADMIN和TENANT两种角色的用户）查询只返回自己的账单

```
Resource URI: /mec/appstore/v1/bills/list
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name      | Decription                                                   | Data Type | Request Type  | Required |
| --------- | ------------------------------------------------------------ | --------- | ------------- | -------- |
| startTime | 开始时间                                                     | String    | request body  | 否       |
| endTime   | 结束时间                                                     | String    | request body  | 否       |
| offset    | 分页查询起始位置                                             | int       | request body  | 是       |
| limit     | 分页查询每页查询数量                                         | int       | request body  | 是       |
| sortItem  | 排序字段（CREATETIME：支持按照账单产生排序。当不指定时，后台默认按照账单产生时间降序排序） | String    | request body  | 否       |
| sortType  | 排序方式（ASC--升序;DESC--降序）                             | String    | request body  | 否       |

Example request:

POST /mec/appstore/v1/bills/list

```json
{
  "startTime": "",
  "endTime": "",
  "queryCtrl": {
    "offset": 0,
    "limit": 20,
    "sortItem": "CREATETIME",
    "sortType": "DESC"
  }
}
```



正常响应中的业务字段：

| Name         | Decription                                                   | Data Type | Response Type |
| ------------ | ------------------------------------------------------------ | --------- | ------------- |
| totalCount   | 符合查询条件的账单记录总数                                   | int       | response body |
| billList     | 账单列表                                                     | List      | response body |
| billId       | 账单ID                                                       | String    | response body |
| createTime   | 计费时间（即账单产生时间）                                   | String    | response body |
| billUserId   | 账单用户ID                                                   | String    | response body |
| billUserName | 账单用户名称                                                 | String    | response body |
| orderId      | 账单记录所属的订单ID                                         | String    | response body |
| orderNum     | 账单记录所属的订单编号                                       | String    | response body |
| orderUserId  | 订单用户ID                                                   | String    | response body |
| orderUserName| 订单用户名称                                                 | String    | response body |
| appId        | 订单对应的应用ID                                                       | String    | response body |
| appName      | 订单对应的应用名称                                                     | String    | response body |
| provider     | 订单对应的应用提供商                                                   | String    | response body |
| billType     | 账单主类型(IN:入账/OUT:出账)                            | String    | response body |
| billSubType  | 账单子类型(OPERATOR:运营商分摊收入/APPSUPPLY:提供应用的收入/APPCONSUME:应用消费支出)  | String    | response body |
| billAmount   | 账单金额（如果是出账，账单金额是分摊到运营商+供应商的费用之和） | double    | response body |
| operatorFee  | 如果是出账，分摊到运营商的费用                               | double    | response body |
| supplierFee  | 如果是出账，分摊到供应商的费用                               | double    | response body |




Example response:

```json
response 200 OK
   
{
  "results": [
      {
        "billId": "",
        "createTime": "",
        "billUserId": "",
        "billUserName": "",
        "orderId": "",
        "orderNum": "",
        "orderUserId": "",
        "orderUserName": "",
        "appId": "",
        "appName": "",
        "provider": "",
        "billType": "OUT",
        "billSubType": "APPCONSUME",
        "billAmount": 353.00,
        "operatorFee": 230.00,
        "supplierFee": 123.00
      },
      {...}
  ],
  "offset": 0,
  "limit": 20,
  "total": 100
}
```



##### 2 总体收支查询（运营商/应用供应商/企业三种类型的用户都通过该接口来获取）

```
Resource URI: /mec/appstore/v1/bills/statistics/overall
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name      | Decription | Data Type | Request Type  | Required |
| --------- | ---------- | --------- | ------------- | -------- |
| startTime | 开始时间   | String    | request body  | 是       |
| endTime   | 结束时间   | String    | request body  | 是       |



Example request:

POST /mec/appstore/v1/bills/statistics/overall

{

  "startTime": "",

  "endTime": ""

}



正常响应中的业务字段：

| Name      | Decription | Data Type | Reponse Type  |
| --------- | ---------- | --------- | ------------- |
| incomeNum | 总收入额   | double    | response body |
| expendNum | 总支出额   | double    | response body |



Example response:

```json
response 200 OK
{
  "data": {
      "incomeNum": 2000.00,
      "expendNum": 1000.00
  },
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 3 TopN应用销售排行统计（运营商/应用供应商）

（1）超级管理员admin用户：作为运营商帐号，统计所有用户（包括自己）的应用的销售情况

（2）非admin用户（包括ADMIN和TENANT两种角色的用户）只统计自己的应用的销售情况

```
Resource URI: /mec/appstore/v1/bills/statistics/sales/topapps
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name         | Decription                                                   | Data Type | Request Type  | Required |
| ------------ | ------------------------------------------------------------ | --------- | ------------- | -------- |
| startTime    | 开始时间                                                     | String    | request body  | 是       |
| endTime      | 结束时间                                                     | String    | request body  | 是       |
| sortType     | 排序方式（ASC--升序，即TopN最差的应用排行;DESC--降序，即TopN最好的应用排行，默认按照DESC排序） | String    | request body  | 否       |
| topNum       | TopN的N值（默认值5，取值范围1~20）                           | int       | request body  | 否       |
| topCriterion | TopN的标准（销售金额：SaleAmount/销量：SaleCount，默认按照销售金额） | String    | request body  | 否       |



Example request:

POST /mec/appstore/v1/bills/statistics/sales/topapps

{

  "startTime": "",

  "endTime": "",

  "sortType": "DESC"

  "topNum": 5,

  "topCriterion": "SaleAmount"

}



正常响应中的业务字段：

| Name       | Decription | Data Type | Reponse Type  |
| ---------- | ---------- | --------- | ------------- |
| appId      | 应用ID     | String    | response body |
| appName    | 应用名称   | String    | response body |
| saleAmount | 销售额     | double    | response body |
| saleCount  | 销量       | long      | response body |



Example response:

```json
response 200 OK
{
  "data": [{
        "appId": "",
        "appName": "",
        "saleAmount": 20000.00,
        "saleCount": 5
     },
     {
        "appId": "",
        "appName": "",
        "saleAmount": 15000.00,
        "saleCount": 6
     }
  ],
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



##### 4 APP订购费用统计

（1）只统计当前登录帐号自己订购费用情况

（2）统计返回订购费用排行Top5的应用，其它应用的订购费用汇总后按照“其它(other)”返回

```
Resource URI: /mec/appstore/v1/bills/statistics/orders/topapps
Method: POST
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：

| Name      | Decription | Data Type | Request Type  | Required |
| --------- | ---------- | --------- | ------------- | -------- |
| startTime | 开始时间   | String    | request body  | 是       |
| endTime   | 结束时间   | String    | request body  | 是       |



Example request:

POST /mec/appstore/v1/bills/statistics/orders/topapps

{

  "startTime": "",

  "endTime": ""

}



正常响应中的业务字段：

| Name        | Decription | Data Type | Reponse Type  |
| ----------- | ---------- | --------- | ------------- |
| appId       | 应用ID     | String    | response body |
| appName     | 应用名称   | String    | response body |
| orderAmount | 订购费用   | double    | response body |



Example response:

```json
response 200 OK
{
  "data": [{
        "appId": "",
        "appName": "",
        "orderAmount": 20000.00
     },
     {
        "appId": "",
        "appName": "",
        "orderAmount": 15000.00
     }
     ...
     {
        "appId": "other",
        "appName": "other",
        "orderAmount": 1000.00
     }
  ],
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



#### 新增的接口--封装Mecm侧的接口

##### 1 获取所有的边缘节点列表

AppStore前台需要展示边缘节点列表，该列表来源于Mecm，由AppStore后台封装一个接口供前台调用。

```
Resource URI: /mec/appstore/v1/mechosts
Method: GET
Role: APPSTORE_ADMIN or APPSTORE_TENANT
```

请求：无请求信息

Example request:

GET /mec/appstore/v1/mechosts/



正常响应中的业务字段：

| Name        | Decription   | Type          |
| ----------- | ------------ | ------------- |
| mechostIp   | 边缘节点IP   | response body |
| mechostName | 边缘节点名称 | response body |
| mechostCity | 边缘节点位置 | response body |



Example response:

```json
response 200 OK
{
  "data": [
      {
          "mechostIp": "119.8.47.5",
          "mechostName": "",
          "mechostCity": ""
      }
  ],
  "retCode": 0,
  "params": ["", ""],
  "message": ""
}
```



### 后台定时处理任务

#### 账单生成任务

每天凌晨1:00启动账单生成任务，遍历所有订单，根据订单的状态、所属应用的定价、分账比例设置来生成账单。