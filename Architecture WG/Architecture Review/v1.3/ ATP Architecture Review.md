### 发布时间 详细设计开始的第一周

详细设计开始时间: 2021.7.25
### 项目概述 Project Overview
应用测试认证服务，可以让用户选择应用包的测试场景，执行对应场景的测试用例。社区场景会对应用包进行安全性测试、遵从性测试（根据APPD、ETSI标准验证包结构）以及沙箱测试（应用包生命周期测试）， 对于测试通过的应用包，才可以发布到应用商店。


### 从上一个版本开始架构的变化 Architecture changes from last release
无架构变化

数据库变更：  
1. 新增config_table表，用于存储配置项信息。   
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/151614_f2b25411_7854857.png "屏幕截图.png")
2. test_case_table 表新增configIdList字段。    
   alter table test_case_table add column configIdList varchar(200) null;
3. task_table表需要新增字段reportPath，用于保存自测报告的路径。   
   alter table task_table add column reportpath varchar(200) null;



### v1.3 引入的新功能特性 New component capabilites for v1.3
1. 新增配置管理功能，支持测试用例关联配置项用于参数动态设置。
2. 支持用户上传自测报告，EG提供自测报告+平台报告的整合报告。

### 新增或者修改的接口 New or modified interfaces
* 新增配置项的创建、删除、更新、查询接口
* 新增自测报告上传接口
* 新增删除某个测试任务接口
* 测试用例的创建、更新参数新增configIdList字段

#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible
测试用例创建、更新接口需要传入configIdList字段

### 接口API 简述 interface naming
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/094417_891927ae_7854857.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/094507_33580385_7854857.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/094527_1b3fd229_7854857.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/094603_55370261_7854857.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/094633_a117b207_7854857.png "屏幕截图.png")

### 接口API参考文档 Reference to the interfaces
https://gitee.com/edgegallery/docs/blob/master/Projects/ATP/ATP_Interfaces.md

### 系统的限制目前有哪些 What are the system limits
None