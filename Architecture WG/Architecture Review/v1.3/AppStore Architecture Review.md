### 发布时间 
2021.8

### 项目概述 Project Overview
AppStore是开发者发布和上线App应用的市场，由Developer平台开发的边缘应用，经过测试可以直接分享到AppStore应用商店。AppStore分为前后台两个部分，appstore-be是后台部分，提供主要功能接口供前台或其他三方系统调用，appstore-fe是前台部分，提供界面展示。

### 从上一个版本开始架构的变化 Architecture changes from last release
无架构变化

### v1.3 引入的新功能特性 New component capabilites for v1.3(Chocolate)
* 应用包签名

  1. 公私钥文件做成可配置，用户可以使用自己的密钥，部署时指定为自己的公私钥文件，没有的话就用EG默认的密钥；

  2. 应用包中对于镜像文件、镜像描述文件、APPD文件进行hash计算，同时对Source、Alagorithm、Hash信息进行数字签名；

  3. 签名使用私钥加密；

  4. 使用者使用公钥进行解密，校验文件完整性。

  

* 虚机应用在线体验

  当应用支持在线体验时，需在应用描述中写明应用portal的URL，

  当应用实例化后，appstore会返回给用户当前应用部署所在的ip，用户可根据该ip和描述中的URL，前往应用portal进行体验。

  体验完成后释放资源；

  开始体验24h小时后，自动释放资源。

  数据库中需要新增字段startExpTime varchar(100) NULL，记录开始体验时间，定时器释放资源后需要将该字段置空。

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/171936_e5d2c5fc_8354563.png "vmexp.png")

  

* APPD工具集成到AppStore界面

  1. APPD前后台作为一个服务打包。

  2. APPD工具当前是作为一个独立工具存在，需要补充Dockerfile文件，支持容器部署。

  3. 补充appd工具部署helm-charts，集成在appstore中，当appstore.appPkgTransTool.enabled=true时，部署该工具。

  4. APPD工具界面嵌套在AppStore界面，用户可使用该工具进行应用包转换。
  
  

### 新增或者修改的接口 New or modified interfaces

| **API**                                | **方法** | API说明      | **变更类型** | **说明**                                                     |
| -------------------------------------- | -------- | ------------ | ------------ | ------------------------------------------------------------ |
| /mec/appstore/v1/system/hosts/{hostId} | GET      | 获取沙箱列表 | 修改         | 优化接口响应规范                                             |
| /mec/appstore/v1/system/hosts          | POST     | 新增沙箱     | 修改         | 优化接口响应规范，请求参数mepHost结构调整，新增parameter、status， status为必填参数，状态为NORMAL，BUSY,STOP状态 |
| /mec/appstore/v1/system/hosts/{hostId} | PUT      | 更改沙箱     | 修改         | 优化接口响应规范                                             |
| /mec/appstore/v1/system/hosts/{hostId} | DELETE   | 删除沙箱     | 修改         | 优化接口响应规范                                             |



## 数据库变更(Database Change)

| Table   Name          | Field Name   | Field Type | Field Length | Default | Value                                              | Change Type | Compatibility |
| --------------------- | ------------ | ---------- | ------------ | ------- | -------------------------------------------------- | ----------- | ------------- |
| Catalog_package_table | startExpTime | varchar    | 100          | NULL    | 开始体验的时间，定时释放资源使用。                 | Add         | Compatible    |
| Catalog_package_table | downloadTime | varchar    | 100          | NULL    | 开始下载应用包的时间，定时删除带镜像的应用包使用。 | Add         | Compatible    |
| tbl_service_host      | parameter    | text       |              | NULL    | 实例化参数类型从varchar(500)修改为text             | Modify      | Compatible    |



#### 如有，他们是否是后向兼容的 If modified, are the backwards compatible

兼容

### 接口API 简述 interface naming

https://gitee.com/edgegallery/docs/blob/master/Projects/APPSTORE/AppStore_Interfaces.md

### 系统的限制目前有哪些 What are the system limits