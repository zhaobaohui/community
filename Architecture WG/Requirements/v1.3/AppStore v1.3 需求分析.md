# **AppStore** **v1.3** **需求分析** 

## 需求列表

| **需求描述**               | **涉及模块**             | **依赖模块** | **工作量** | **交付计划** | **责任人**     |
| -------------------------- | ------------------------ | ------------ | ---------- | ------------ | -------------- |
| 删除应用时同步删除测试报告 | appstore-be              | atp-be       | 0.3k       | 迭代一       | 罗小云         |
| 接口响应规范化             | appstore-fe              |              | 0.5k       | 迭代一       | 赵龙飞         |
| 应用包签名                 | appstore-be              |              | 1.5k       | 迭代一       | 罗小云         |
| 虚机应用在线体验           | appstore-be  appstore-fe |              | 1.5k       | 迭代二       | 程润东         |
| 虚机应用镜像上传/下载      | appstore-be              |              | 2k         | 迭代二       | 罗小云、程润东 |
| 界面优化                   | appstore-fe              |              | 3.5k       | 迭代三       | 赵龙飞、陈凯   |
| APPD转换工具集成到AppStore | appstore-be  appstore-fe |              | 1.5k       | 迭代二       | 罗小云、赵龙飞 |

## **内部优化**

### **删除应用时同时删除测试报告**

当前删除应用，只删除了appstore的数据，atp测试报告数据还残留。

调用atp删除测试报告接口：DELETE edgegallery/atp/v1/tasks/{taskId}

可删除本用户应用的测试报告，admin角色可以删除所有测试报告，与删除应用权限一致。


另外：

删除应用时同时删除可推送应用列表中该应用的数据

### **应用包签名**

1.公私钥文件做成可配置，用户可以使用自己的密钥，部署时指定为自己的公私钥文件，没有的话就用EG默认的密钥；

2.应用包中对于镜像文件、镜像描述文件、APPD文件进行hash计算，同时对Source、Alagorithm、Hash信息进行数字签名；

3.签名使用私钥加密；

4.使用者使用公钥进行解密，校验文件完整性。

installer:
![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/171840_42b33b7b_8354563.png "installer.png")

helm-charts:

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/171851_ee713066_8354563.png "helm.png")

### **虚机应用在线体验**

当应用支持在线体验时，需在应用描述中写明应用portal的URL，

当应用实例化后，appstore会返回给用户当前应用部署所在的ip，用户可根据该ip和描述中的URL，前往应用portal进行体验。

体验完成后释放资源；

开始体验24h小时后，自动释放资源。

数据库中需要新增字段startExpTime varchar(100) NULL，记录开始体验时间，定时器释放资源后需要将该字段置空。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/171936_e5d2c5fc_8354563.png "vmexp.png")

### 虚机应用镜像上传/下载

1. Appstore部署时将应用包路径挂载到宿主机上

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/172309_f73943ba_8354563.png "imghelm.png")

2. 上传时如果上传了包含镜像的应用包，需将镜像分离上传文件系统， 并修改镜像描述文件。

   这里涉及两次分片上传：

   ![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/172321_56bd758c_8354563.png "imgupload.png")

   AppStore本地只保存不含镜像的应用包，包含镜像的应用包上传完成后删除。

   

   上传分两种场景：

   AppStore与FileSystem部署在一个节点：

   ①将文件拷贝到指定路径：/edgegallery/data/vmImage/{imageId}

   ②调用FileSystem接口添加该镜像信息到数据库：name、imageId

   AppStore与FileSystem部署在不同节点：采用分片上传方式上传镜像

   需要部署时配置AppStore所在节点IP和FileSystem所在节点IP，并设置到环境变量中，由此判断是否共部署。

2.  下载包含镜像的应用包：

   ![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/172336_cb15f43e_8354563.png "imgdownload.png")

   下载也分两种场景：

   AppStore与FileSystem部署在一个节点：

         直接从特定路径下拷贝镜像文件

   AppStore与FileSystem部署在不同节点：

         调用FileSystem的下载镜像接口下载，注意断点续传。

   数据库中需要新增字段downloadTime varchar(100) NULL，记录下载时间，定时器清理完应用包后需要将该字段置空。

### **APPD工具集成到AppStore**

1.APPD前后台作为一个服务打包，类似于user-mgmt。

2.APPD工具当前是作为一个独立工具存在，需要补充Dockerfile文件，支持容器部署。

3.补充appd工具部署helm-charts，集成在appstore中，当appstore.appPkgTransTool.enabled=true时，部署该工具。

4.APPD工具界面嵌套在AppStore界面。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/172352_f6536d3f_8354563.png "appdbutton.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/172405_be06d142_8354563.png "appdpage.png")

## **界面优化**

### **首页**

精选主题中对智慧园区、工业制造、交通物流行业领域进行介绍，筛选对应精品应用。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/172420_7e9ce268_8354563.png "home.png")

### **应用共享**

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/172434_56c046a2_8354563.png "share.png")


## 数据库变更(Database Change)

| Table   Name          | Field Name   | Field Type | Field Length | Default | Value                                              | Change Type | Compatibility |
| --------------------- | ------------ | ---------- | ------------ | ------- | -------------------------------------------------- | ----------- | ------------- |
| Catalog_package_table | startExpTime | varchar    | 100          | NULL    | 开始体验的时间，定时释放资源使用。                 | Add         | Compatible    |
| Catalog_package_table | downloadTime | varchar    | 100          | NULL    | 开始下载应用包的时间，定时删除带镜像的应用包使用。 | Add         | Compatible    |

