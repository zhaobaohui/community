## API List:

|   模块   |root   |URL   |Type   |Auth   |header   |body   |Rsp-Code   |Rsp-Body   |说明   |
|----|----|----|----|----|----|----|----|----|----|
|   resources   |/mobile/v1   |/resources/action/upload   |POST   |admin   |N/A   |Form: file   |201   |ResourcePo   |上传资源文件，将资源发布到web服务器上，通过返回的url可以浏览器直接访问   |
|      |/mobile/v1   |/resources/{resourceId}   |DELETE   |Admin   |N/A   |N/A   |200   |N/A   |通过资源ID删除资源   |
|      |/mobile/v1   |/resources/   |GET   |Admin   |N/A   |N/A   |   |List<ResourcePo>   |获取已经上传的所有资源数据   |
|   event   |/mobile/v1   |/events   |POST   |Admin   |N/A   |EventDto   |201   |EventDetailPo   |   |
|      |/mobile/v1   |/events/{eventId}   |PUT   |Admin   |N/A   |EventBasicPo   |200   |EventDetailPo   |更新文案内容或状态   |
|      |/mobile/v1   |/events/{eventId}   |DELETE   |Admin   |N/A   |N/A   |200   |N/A   |删除文案。删除后文案不可查看，收藏夹中也没有   |
|      |/mobile/v1   |/events/?limit=10;offset=0;type=news   |GET   |Allow All   |N/A   |N/A   |200   |PageResultDto   |只返回已发布状态的event，支持分页 和 过滤查找，不返回content内容，如果要查看详细内容，请单个查询。limit,offset 必须输入；admin访问时返回所有类型数据，其他账户登陆只返回publised数据；type=[news,activity,meeting,video]   |
|      |/mobile/v1   |/events/{eventId}   |GET   |Allow All   |   |   |   |EventDetailPo   |返回event的详细数据，包含content信息；含浏览+1功能   |
|   文章操作   |/mobile/v1   |/events/comments/?reply={commentId}   |POST   |Tenant   |N/A   |CommentEvent   |201   |CommentEvent   |非必须：reply=回复帖子ID  |
|      |/mobile/v1   |/events/{eventId}/favorite    |POST   |Tenant   |N/A   |N/A   |200   |N/A   |收藏+1，用户收藏文章，如果已经收藏过，则是取消收藏   |
|      |/mobile/v1   |/events/{eventId}/like   |POST   |Tenant   |N/A   |N/A   |200   |N/A   |点赞+1，用户点赞文章，不支持重复点赞   |
|      |/mobile/v1   |/events/{eventId}/share   |POST   |Tenant   |N/A   |N/A   |200   |N/A   |分享成功+1，分享的实现逻辑需要手机侧应用实现，后台只做+1   |
|   User   |   |/login   |POST   |Allow All   |N/A   |username:xxxx password:xxxx   |200   |UserPo   |通过用户名和密码登陆系统。同时支持密码或验证码登录两种方式，首先校验手机号是否已经注册，然后校验密码或者验证码   |
|      |/mobile/v1   |/users   |POST   |Allow All   |N/A   |RegisterUserDto   |201   |UserPo   |create new user   |
|      |/mobile/v1   |/users/sms/generate-code   |POST   |Allow All   |N/A   |{"phone": "138000011111"}   |200   |N/A   |生成手机验证码，直接发送到指定手机上   |
|      |/auth   |/login-info   |GET   |Allow All   |N/A   |N/A   |200   |N/A   |当前登陆的用户信息，也可以是匿名用户信息   |
|      |/auth   |/logout   |POST   |Allow All   |N/A   |N/A   |200   |N/A   |登出操作。登出后再次打开需要重新登陆   |
|      |/auth   |/sms   |POST   |Allow All   |N/A   | {"telephone": "13800001111"}   |200   |N/A   | 向手机号发送验证码。需要校验手机格式（11位手机号），只支持中国境内手机号码，验证码有效时长5分钟，3分钟后可以重复获取。失败返回错误信息 |
|      |/mobile/v1   |/user/{userId}   |PUT   |Tenant   |N/A   |UserDto   |200   |UserPo   |更新用户icon   |
|   App   |/mobile/v1   |/apps/   |POST   |Allow All   |N/A   |QueryAppReqDto   |200   |PageResultDto<AppDto>   |v1.3：后台需要判断当前显示的数据集flag是什么，根据flag过滤   |
|      |/mobile/v1   |/apps/{appId}   |GET   |Allow All   |N/A   |N/A   |200   |AppDto   |   |
|      |/mobile/v1   |/apps/{appId}   |PUT   |Allow All   |N/A   |AppDto   |200   |AppDto   |v1.3: 修改应用数据。例如设置是否热门应用/是否有在线体验链接/修改应用描述等   |
|      |/mobile/v1   |/apps/action/hotapps   |GET   |Allow All   |N/A   |N/A   |200   |List<AppDto>   |"获取热门应用列表 v1.3：后台需要判断当前显示的数据集flag是什么，根据flag过滤"   |
|      |/mobile/v1   |/apps/{appId}/lastpackage   |GET   |Allow All   |N/A   |N/A   |200   |   |获取AppPackage信息（目前没有用）   |
|      |/mobile/v1   |/apps/{appId}/favorite   |POST   |Tenant   |N/A   |N/A   |200   |N/A   |收藏+1，用户收藏文章，如果已经收藏过，则是取消收藏   |
|      |/mobile/v1   |/apps/{appId}/like   |POST   |Tenant   |N/A   |N/A   |200   |N/A   |点赞+1，用户点赞文章，不支持重复点赞   |
|      |/mobile/v1   |/apps/action/sync-data   |POST   |Admin   |N/A   |SyncDataDto   |200   |List<AppDto>   |根据输入的地址和类型同步获取App数据   |
|      |/mobile/v1   |/apps/action/set-show   |POST   |Admin   |N/A   |{"flag": "xxxxxx"}   |200   |N/A   |设置输入的数据作为   |
|   Capabilities   |/mobile/v1   |/capabilities   |GET   |Allow All   |N/A   |N/A   |200   |List<CapabilityGroup>   |   |
|      |/mobile/v1   |/capabilities/{groupId}   |GET   |Allow All   |N/A   |N/A   |200   |CapabilityGroup   |   |
|      |/mobile/v1   |/capabilities/{groupId}   |PUT   |Admin   |N/A   |CapabilityGroup   |200   |CapabilityGroup   |   |
|      |/mobile/v1   |/capabilities/action/sync-data   |POST   |Admin   |N/A   |SyncDataDto   |200   |List<CapabilityGroup>   |根据输入的地址和类型同步获取能力接口数据   |
|      |/mobile/v1   |/capabilities/action/set-show   |POST   |Admin   |N/A   |{"flag": "xxxxxx"}   |200   |N/A   |设置输入的数据作为   |
|   homapage   |/mobile/v1   |/homepage/banners   |GET   |Allow All   |N/A   |N/A   |200   |List<BannerPo>   |   |
|      |/mobile/v1   |/homepage/banners   |POST   |Admin   |N/A   |List<BannerPo>   |200   |List<BannerPo>   |批量替换所有的banner信息。该接口会先删除现有的banner，然后增加新的banner   |
|      |/mobile/v1   |/homepage/banners/{bannerId}   |PUT   |Admin   |N/A   |BannerPo   |200   |BannerPo   |编辑   |
|      |/mobile/v1   |/homepage/banners/{bannerId}   |DELETE   |Admin   |N/A   |N/A   |200   |N/A   |删除一个banner标签   |

## Object:

### ResourcePo
```java
public class ResourcePo {
    private String id;
    private String userId;
    private String userName;
    private String path;
    private String fileName;
    private long createTime;
    private long size;
    private String hashCode;
}
```

```json
{
    "id": "ae372716-d294-4b42-88fc-f470f53543b2",
    "userId": null,
    "userName": null,
    "path": "/2021/05/21/ae372716-d294-4b42-88fc-f470f53543b2",
    "fileName": "e35f4aae-29a1-4013-8fb8-cb7861cad8da.csar",
    "createTime": 1621591064878,
    "size": 9783,
    "hashCode": null
}
```

### EventDto:
```java
public class EventDto {
    private String name;
    private String introduction;
    private String iconUrl;
    private String content;
    private EventType type;
    // for meeting
    // date format: yyyy-MM-dd'T'HH:mm:ss e.g. 1979-10-11T14:45:00
    private Date startTime;
    private Date endTime;
    // for activity
    private boolean allowSignUp;
    private String signUpEndTime;
    // for video
    private String videoUrl;
    private String videoType;
}
```
```json
// To create a VIDEO event
{
  "name": "First-level name",
  "introduction": "Abstract, used as a short description in the list",
  "iconUrl": "An accessible picture link, recommended size xxx*xxx, for display on the homepage or list",
  "content": "Details in the event.",
  "type": "VIDEO",
  "videoUrl": "A link to the video",
  "videoType": "Enumerated value, TRAINING or LECTURE"
}

// To create a NEWS event:
{
  "name": "First-level name",
  "introduction": "Abstract, used as a short description in the list",
  "iconUrl": "An accessible picture link, recommended size xxx*xxx, for display on the homepage or list",
  "content": "Details in the event.",
  "type": "NEWS"
}

// To create a MEETING event:
{
  "name": "First-level name",
  "introduction": "Abstract, used as a short description in the list",
  "iconUrl": "An accessible picture link, recommended size xxx*xxx, for display on the homepage or list",
  "content": "Details in the event.",
  "type": "MEETING",
  "startTime": "date format: yyyy-MM-dd\u0027T\u0027HH:mm:ss e.g. 1979-10-11T14:45:00",
  "endTime": "date format: yyyy-MM-dd\u0027T\u0027HH:mm:ss e.g. 1979-10-11T14:45:00"
}

// To creat a ACTIVITY event:
{
  "name": "First-level name",
  "introduction": "Abstract, used as a short description in the list",
  "iconUrl": "An accessible picture link, recommended size xxx*xxx, for display on the homepage or list",
  "content": "Details in the event.",
  "type": "ACTIVITY",
  "startTime": "date format: yyyy-MM-dd\u0027T\u0027HH:mm:ss e.g. 1979-10-11T14:45:00",
  "endTime": "date format: yyyy-MM-dd\u0027T\u0027HH:mm:ss e.g. 1979-10-11T14:45:00"
  "allowSignUp": true,
  "signUpEndTime": "date format: yyyy-MM-dd\u0027T\u0027HH:mm:ss e.g. 1979-10-11T14:45:00"
}
```

### EventDetailPo:
```java
public class EventDetailPo extends EventPromote {
    private String name;
    private String introduction;
    private String iconUrl;
    private String content;
    private EventType type;

    private Date start;
    private Date end;

    private boolean allowSignUp;
    private Date signUpEndTime;
    private String videoUrl;
    private VideoEventType videoType;
}
```
```json
{
  "name": null,
  "introduction": null,
  "iconUrl": null,
  "content": null,
  "type": null,
  "startTime": null,
  "endTime": null,
  "isOver": false,
  "allowSignUp": false,
  "signUpEndTime": null,
  "videoUrl": null,
  "videoType": null,
  "views": 0,
  "likeCount": 0,
  "favoriteCount": 0,
  "forwardCount": 0,
  "commentCount": 0,
  "allowShare": true,
  "allowForward": true,
  "allowComment": true,
  "allowFavorite": true,
  "level": 9,
  "id": null,
  "createTime": null,
  "status": "DRAFT"
}
```

### EventPromote:
```java
public class EventPromote extends EventBasic {
    private int views = 0;
    private int shareCount = 0;
    private int favoriteCount = 0;
    private int forwardCount = 0;
    private int commentCount = 0;
    private boolean allowShare = true;
    private boolean allowForward = true;
    private boolean allowComment = true;
    private boolean allowFavorite = true;
    // 1: Essene; 2: Tops; 9(default): common
    private int level = 9;
}
```
### EventBasic:
```java
public class EventBasic {
    private String eventId;
    private Date createTime = new Date();
    private EventStatus status = EventStatus.DRAFT;
}
```
### PageResultDto<EventDetailPo>:
```java
public class PageResultDto<T> {
    private int count;
    private int offset;
    private int limit;
    
    // no need to response content in the list, 
    // if you want get the detail of one event, 
    // please to use the API of /mobile/v1/event/{eventId}
    private List<T> details;
}
```
```json
{
  "count": 0,
  "offset": 0,
  "limit": 0,
  "details": [
    {
      "name": null,
      "introduction": null,
      "iconUrl": null,
      "content": null,
      "type": null,
      "startTime": null,
      "endTime": null,
      "isOver": false,
      "allowSignUp": false,
      "signUpEndTime": null,
      "videoUrl": null,
      "videoType": null,
      "views": 0,
      "likeCount": 0,
      "favoriteCount": 0,
      "forwardCount": 0,
      "commentCount": 0,
      "allowShare": true,
      "allowForward": true,
      "allowComment": true,
      "allowFavorite": true,
      "level": 9,
      "id": null,
      "createTime": null,
      "status": "DRAFT"
    }
  ]
}
```
### CommentEvent:
```Java
public class CommentEvent {
    private String id;
    private String eventId;
    private Date createTime;
    private String userId;
    private String userName;
    private String content;
    private String replayTo;
}
```
### QueryAppReqDto:
```Java
public class QueryAppReqDto {

    // values: Video Application; Game, Video Surveillance, Safety, Blockchain,
    // Smart Device, Internet of Things, Big Data, AR/VR, API, SDK, MEP
    private List types;

    // values: private; public ;inner-public
    private String showType;

    // values: X86, ARM64, ARM32
    private List affinity;

    // values: Smart Park, Smart Supermarket, Industrial Manufacturing, Transportation Logistics, Water Conservancy,
    // Game Competition, Open Source, Other
    private List industry;

    // values: container, vm
    private List workloadType;

    private String userId;

    private String status;

    private String appName;

    @NotNull
    @Valid
    private QueryAppCtrlDto queryCtrl;
}

public class QueryAppCtrlDto {
    private static final String DEFAULT_SORTBY = "createTime";

    private static final String DEFAULT_SORTORDER = "ASC";

    private String createTime;

    @Min(value = 0)
    private int offset;

    @Min(value = 1)
    @Max(value = 500)
    private int limit;

    private String sortItem;

    // values: Most, Name, Score, UploadTime
    @Pattern(regexp = "(?i)DESC|(?i)ASC")
    private String sortType;
}
```
```Json
{
  "types": [],
  "showType": null,
  "affinity": [],
  "industry": [],
  "workloadType": [],
  "userId": null,
  "status": null,
  "appName": null,
  "queryCtrl": {
    "createTime": null,
    "offset": 0,
    "limit": 0,
    "sortItem": null,
    "sortType": null
  }
}
```

### PageResultDto<AppDto>:
```Json
{
  "limit": 0,
  "offset": 0,
  "total": 0,
  "results": [
    {
      "appId": null,
      "iconUrl": null,
      "name": null,
      "provider": null,
      "type": null,
      "shortDesc": null,
      "showType": null,
      "createTime": null,
      "details": null,
      "downloadCount": 0,
      "affinity": null,
      "industry": null,
      "contact": null,
      "score": 0.0,
      "userId": null,
      "userName": null,
      "status": null,
      "deployMode": null
    }
  ]
}
```

### SyncDataDto:
```Java
public class SyncDataDto {
    private String address;

    private String version;

    private String flag;
}
```
### UserPo:
```Java
public class UserPo {
    private String id;

    private String userName;

    private String password;

    private String alias;

    private String phone;

    private String type;

    private String iconUrl;

    private boolean isLocked;

    private String role;

    private Date lastLogInTime;

    private List<Favorites> favorites;
}
```

### RegisterUserDto :
```Java
public class RegisterUserDto {
    private String userName;
    private String password;
}
```