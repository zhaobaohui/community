### 统计MEP接口调用方数据，供审计使用

目前使用k8s方式安装EdgeGallery，request请求中的client_ip不是真实的请求方ip，所以无法使用client_ip定位请求方数据。

MEP的mp1接口都有token信息，根据token信息可以解析出appInstanceId，根据appInstanceId就能够得到appInstance的相关内容。在存储请求数据到ES之前，添加APP相关数据，查询时可以根据appInstanceId查询相关接口的次数调用数据。

 **请求日志记录时序图** 

![请求日志记录时序图](https://images.gitee.com/uploads/images/2021/0721/103757_1c13498b_4991354.png "MEP接口调用方数据统计.png")

 **JWT token解析** 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/141321_9e18f95a_4991354.png "jwt token解析.png")

### Service支持多endPointInfo

ETSI标准Service支持多endPointInfo，目前MEP服务注册相关接口只支持单个endPointInfo，需要修改服务注册、修改、删除相关接口，mep和mep-agent都要同步改动

![EndPointInfo](https://images.gitee.com/uploads/images/2021/0721/113633_456d0dd8_4991354.png "EndPointInfo.png")

### MEC application graceful termination/stop
Resource URI: {apiRoot}/mec_app_support/v1/applications/{appInstanceId}/confirm_termination

Method: POST

![graceful termination](https://images.gitee.com/uploads/images/2021/0721/114955_a91dd4ab_4991354.png "graceful termination.png")

### Confirm ready task

Resource URI: {apiRoot}/mec_app_support/v1/applications/{appInstanceId}/confirm_ready

Method: POST

![MEC app start up](https://images.gitee.com/uploads/images/2021/0721/115236_4c19da2e_4991354.png "MEC app start up.png")
