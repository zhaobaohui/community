# 改造晟腾等平台应用为WebAssembly形态，提供更多形态workload开放能力

当前WebAssembly支持最完善的语言为Rust与C/C++，开发人员多、发展快、支持组件多。
所以本需求对采用这两种语言的平台APP进行改造，能避免很多因为不支持导致的未知问题，
确保最大程度支持将平台APP进行WebAssembly改造。

## 1. 改造TensorFlow应用

TensorFlow应用多为python语言实现，但是Second State组织提供一组将基于Rust语言的
TensorFlow推理程序编译成Wasm的工具链，能够尽可能的保持原有TF的速度，同时适用GPU等TF特性。

所以可以将原TF推理程序使用Rust进行改写（推理程序的改写比较简单，100行左右），再使用Wasm编译工具进行编译。
具体过程如下图所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/105757_f445bb2c_7634758.png "屏幕截图.png")

1. 首先使用rustwasmc编译工具，结合ssvm-extensions针对TF的扩展功能，对rust语言编写的TF推理程序进行编译
2. 对需要运行的服务器端的APP，编写js，定义对外提供服务的API，将此TF APP应用在服务器端
3. 对需要运行在浏览器端的APP，编写js胶水文件和html网页文件，将此TF APP运行在服务器端

**风险点：**
1. **某些APP可能使用了TF的比较高级或者新的功能，ssvm-extension还未实现并支持** 

## 2. 改造晟腾应用

晟腾APP主要是C/C++语言实现，可以采用emscripten编译工具进行编译，
将编译后的wasm文件运行在服务器端或者浏览器端。
具体过程如下图所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/112743_43f4417c_7634758.png "屏幕截图.png")

**风险点：**
1. **晟腾应用依赖晟腾API，wasm能否支持此种场景**
2. **晟腾应用需要在arm机器上编译和运行，但webassembly编译工具当前仅支持运行在x86机器，晟腾应用在x86机器上使用emscripten进行交叉编译是否能支持** 