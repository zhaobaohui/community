# **AppStore** **v1.2** **需求分析** 

## 需求列表

| **需求描述**               | **涉及模块**             | **工作量**                          | **交付计划** | **优先级** | 责任人                 |
| -------------------------- | ------------------------ | ----------------------------------- | ------------ | ---------- | ---------------------- |
| 界面优化                   | appstore-be  appstore-fe | 1k                                  | 迭代一       | 高         | 罗小云、程润东、赵龙飞 |
| 虚机应用展示               | appstore-be  appstore-fe | 0.5k                                | 迭代一       | 高         | 罗小云、邹玲莉、程润东 |
| 上传应用时设置应用展示模式 | appstore-be  appstore-fe | 0.3k                                | 迭代一       | 高         | 罗小云、赵龙飞         |
| 应用支持修改               | appstore-be  appstore-fe | 0.7k                                | 迭代一       | 中         | 赵龙飞                 |
| 应用管理                   | appstore-be  appstore-fe | 1.5k                                | 迭代二       | 高         | 赵龙飞                 |
| 接口响应规范化             | all                      | 后台100/接口+0.3k前台+0.5k  gateway | 迭代三       | 高         | PTLs                   |
| 应用在线体验               | appstore-be  appstore-fe | 3.5k                                | 迭代三       | 中         | 罗小云、程润东         |
| 关键数据持久化/导出/备份   | all                      | 1k                                  | 迭代三       | 中         | PTLs                   |
| APPD转换工具               | appstore-be  appstore-fe | 3k                                  | 迭代三       | 高         | 罗小云、程润东         |

## 界面优化

### 应用详情显示csar包大小

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144402_5c9b1a1e_8354563.png "appdetail.png")

### 应用共享

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144432_d8494ae3_8354563.png "operation.png")

### 界面显示应用负载类型

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144446_783c97dd_8354563.png "appstore.png")

1.appstore-be解析csar包中app-class字段，获取负载类型：容器应用或虚拟机应用；

2.appstore-be将负载类型存储数据库，app_table表和catalog_package_table表增加字段deployMode varchar(100)；

3.appstore-be查询app列表或者apppackage列表时返回deployMode字段，对于历史数据多为容器应用，所以默认为container；

4.应用仓库支持“负载类型” 过滤，应用仓库卡片模式添加标签“容器”/“虚拟机”，表格显示中增加“负载类型” （workload type）列。

5.“我的应用”、“应用推送”、“应用拉取”等表格均展示“负载类型”。

## 应用权限管理

### 上传应用时可设置应用展示模式

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144503_c72c1d41_8354563.png "uploadapp.png")

1.应用展示模式有：应用发布后不在本应用仓库展示、在本仓库展示、可推广到其他应用仓库；

2.可推广到其他应用仓库的前提是在本仓库可见，默认为public；

3.应用展示在数据库增加字段showType varchar(100) default public，取值为public、inner-public、private

### 修改应用

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144516_e8516589_8354563.png "modapp.png")

1.在“我的应用”中增加操作“修改”，可以修改应用属性；

2.可以修改的属性包括：行业、类型、图标、视频、架构、描述、应用展示

### 应用管理

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144535_f1db8e5a_8354563.png "appmanager.png")

1.Admin角色增加一级菜单“应用管理”；

2.管理包括：近期热门应用管理、应用仓库显示应用列表管理、可推广应用列表管理；

3.三个页签统一格式，近期热门应用有且只能选择6个应用，且为应用仓库展示列表的子集。

## 数据库变更(Database Change)

| Table   Name          | Field Name | Field Type | Field Length | Default  | Value                       | Change Type | Compatibility |
| --------------------- | ---------- | ---------- | ------------ | -------- | --------------------------- | ----------- | ------------- |
| Catalog_package_table | deployMode | varchar    | 100          | NULL     | container/vm                | Add         | Compatible    |
| app_table             | deployMode | varchar    | 100          | NULL     | container/vm                | Add         | Compatible    |
| Catalog_package_table | showType   | varchar    | 100          | public   | public/inner-public/private | Add         | Compatible    |
| app_table             | showType   | varchar    | 100          | public   | public/inner-public/private | Add         | Compatible    |
| hot_app_table         | APPID      | varchar    | 200          | NOT NULL | Recent hot app id           | Add         | Compatible    |

## 接口变更(Interface Change)

| URL                                                          | Method | Change Type | Change Description                                           | Compatibility |
| ------------------------------------------------------------ | ------ | ----------- | ------------------------------------------------------------ | ------------- |
| /mec/appstore/v1/apps  /mec/appstore/v1/apps/{appId}  /mec/appstore/v1/apps/{appId}/packages  /mec/appstore/v1/apps/{appId}/packages/{packageId}  /mec/appstore/v1/packages | GET    | Modified    | 1、响应增加deployMode字段，标识是虚机应用还是容器应用。  2、userId请求参数为空时，只返回已发布showType非private的应用（包）；非空时，返回当前用户所有的应用（包） | Compatible    |
| /mec/appstore/v1/apps/hotapps                                | POST   | Add         | 添加近期热门应用，有且仅能添加6个已存在的热门应用，请求参数为应用ID列表。 | Compatible    |
| /mec/appstore/v1/apps/hotapps                                | GET    | Add         | 查询近期热门应用，直接返回应用appId列表。                    | Compatible    |
| /mec/appstore/v1/packages/pushable  /mec/appstore/v1/packages/{packages}/pushable  /mec/appstore/v1/packages/pullable | GET    | Modified    | 只返回已发布且showType为public属性的应用包                   | Compatible    |
| /mec/appstore/v1/apps/{appId}/package/{packageId}            | PUT    | Add         | 修改应用属性，包括展示类型。                                 | Compatible    |

## 接口响应规范优化

**优势**：

  接口响应规范化，错误信息细化，界面更好展示失败原因，有助于后期记录操作日志

**状态码：**

  现在我们代码中返回状态码不规范，业务处理异常场景时，返回的状态码有400、403、500等。

  各个模块需要分析业务场景，具体错误信息由响应内容给出，内部逻辑错误和访问外部系统（如文件系统、数据库）错误是500，部分成功206；

  部分请求参数错误、没有权限等可以返回400、403等状态码。

**建议**：

  此次整改影响接口很多，很难做到与V1接口兼容，要梳理外部接口，整理好接口变更。



**批量查询GET接口：Page<T>**

```
{
  "results" : [] --- 原响应结构,
  "total" : int,  ---- 用于调用方判断分页查询是否继续
  "limit" : int,
  "offset" : int
}
```


**方案一：**

   后台直接给出errMsg的中英文信息，当errCode不为0（失败）时前台直接根据当前语言解析errMsg。

 **优点：** 前台不用做错误码转义，不用解析资源文件，改动比较集中。

 **缺点：** 响应体内容增大，尤其对于批量接口，这样会导致批处理能力降低。

**单接口：**
```
{
  "errCode" : int,  --- 响应错误码: 0 --- 成功； 其他 --- 失败
  "errMsg" : {
      "en_US" : string,  --- 英文错误描述信息
      "zh_CN" : string, --- 中文错误描述信息  
  }  
  "data" : 原响应结构
}
```

**POST、PUT、DELETE接口：**
```
{
  "errCode" : int,  --- 大错误码：0 --- 成功; 1 --- 失败; 5000 --- 部分成功
  "successes" : [{
       "appId" : string,
       "appName" : string,
     }
  ],
  "failures" : [{
       "appId" : string,
       "appName" : string,
       "errMsg" : {
          "en_US" : string,  --- 英文错误描述信息
          "zh_CN" : string,  --- 中文错误描述信息  
       }  
     }
  ]
}
```


**方案二：（选用此方案）**

  Java语言类后台返回具体错误码，将错误码的中英文资源描述统一放在website-gateway仓库中，由website-gateway提供公共接口获取错误码资源信息，各个项目前台前台自行调用解析接口中的错误码信息。

  Go语言类项目自行规划错误码资源信息解析。

 **优点：** 响应结构简单，错误码在后续操作日志记录也可通用，错误信息解析可以复用，易扩展。

 **缺点：** 需要前台、gateway同步修改，资源化错误码信息。



**单接口：**
```
{
  "data" : 原响应结构,
  "retCode" : int,  --- 响应错误码 
  "message": string, --- 错误信息，各业务前台可根据此字段展示详细错误信息，也可只使用retCode
  "params": [string] --- 资源化信息中带的参数信息
}
```

**POST、PUT、DELETE接口：**

```
{
  "retCode" : int,  --- 大错误码：0 --- 成功; 1 --- 失败; 5000 --- 部分成功
  "successes" : [{
       "XXX" : string,  --- 业务需要返回的字段
       "YYY" : string   --- 业务需要返回的字段
       ......
     }
  ],
  "failures" : [{
       "XXX" : string,  --- 业务需要返回的字段
       "YYY" : string,  --- 业务需要返回的字段
       ......
       "errCode": int  --- 小错误码,
       "message": string --- 错误信息，各业务前台可根据此字段展示详细错误信息，也可只使用errCode
       "params": [string] --- 资源化信息中带的参数信息
     }
  ]
}
```

**通用错误码：**
0：成功，1：失败，5000：部分成功

**各个仓库错误码范围规划：**

```
appstore: 10000 ~ 19999
developer: 20000 ~ 29999
mecm: 30000 ~ 39999
atp: 40000 ~ 49999
mep: 50000 ~ 59999
lab: 60000 ~ 69999
usrmgmt: 70000 ~79999
```

**错误码分配原则：**按照业务场景，分析应该返回的错误码，预留错误码空间，稳定的错误码建议分配在前面，方便后面错误码的增删。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144605_cceaebfb_8354563.png "errcode.png")


## 接口定义规范

•接口路径能方便清晰的区分来源项目，可以采用不同项目作为接口前缀。如：

        /mec/appstore
    
        /mec/developer

•版本控制规范，为了方便后期接口的升级和维护，建议在接口路径中加入版本号，便于管理。如：

        /mec/appstore/v1
    
        /mec/developer/v1

•为了明确接口的功能，可以将功能模块也加入接口路径中。如：

        /mec/appstore/v1/packages
    
        /mec/developer/v1/projects

•为了明确接口的具体业务，可以外加具体接口名，命名规范统一使用小写字符，多个词使用短横线连接符，如：

        /mec/appstore/v1/packages/download-packages
    
        /mec/developer/v1/projects/vm-test-config

•URL路径不宜过长：产品名/版本形式/业务模型/子模型

•HTTP请求方式：

​        GET     ---  查询资源

​        POST    ---  增加资源

​        PUT     ---  修改资源

​        DELETE  ---  删除资源

​    对于查询条件负责或条件带有敏感信息的，采用POST方法替代GET

## 分页查询接口变更

| **API**                                | **方法** | API说明             | **变更类型** | **说明**                                                     |
| -------------------------------------- | -------- | ------------------- | ------------ | ------------------------------------------------------------ |
| /mec/appstore/v1/apps                  | GET      | 获取应用            | 修改         | 请求参数增加字段 appName、limit、offset;  后台进行分页查询、支持模糊查询 ，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/packages              | GET      | 获取app应用包       | 新增         | 请求参数在v1接口基础上增加字段  appName、limit、offset;  后台进行分页查询、支持模糊查询 ，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v1/appstores             | GET      | 获取appStore        | 修改         | 请求参数增加字段 appStoreName、limit、offset;  后台进行分页查询、支持模糊查询 ，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/packages/pushable     | GET      | 获取应用推送app列表 | 新增         | 请求参数在v1接口基础上增加字段  appName、limit、offset;  后台进行分页查询、支持模糊查询 ，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/packages/pullable     | GET      | 获取应用拉取app列表 | 新增         | 请求参数在v1接口基础上增加字段  appName、limit、offset;  后台进行分页查询、支持模糊查询 ，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/messages              | GET      | 操作分析消息列表    | 新增         | 请求参数在v1接口基础上增加字段  appName、limit、offset;  后台进行分页查询、支持模糊查询 ，响应参数增加总数量total、limit、offset。 |
| /mec/appstore/v2/apps/{appId}/comments | GET      | 获取应用评论信息    | 新增         | 请求参数在v1接口基础上增加字段  limit、offset;  后台进行分页查询、支持模糊查询 ，响应参数增加总数量total、limit、offset。 |

## 应用在线体验

管理员可以新增、删除和修改沙箱环境，沙箱环境配置好后方可进行在线体验。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144654_339de792_8354563.png "sandbox.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/160347_ffcb1656_8354563.png "experience.png")
## 关键数据持久化、备份、恢复

 **持久化：** Pod使用Deployment方式创建，而非StatefulSet，使用persistentVolumeClaim来持久化数据。 https://www.kubernets.org.cn/docs

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144723_269a17af_8354563.png "persistent.png")

 **备份/恢复：** 持久化使用nfs存储，可以提供backup.sh将nfs目录下需要备份的数据保存（数据库、业务重要文件），restore.sh脚本将备份的数据恢复到指定环境下。

其实我们数据已经持久化了，只要各个模块提供关键数据列表，执行拷贝命令直接拷贝到目标环境即可。

## APPD转换器

**实现方案：**

   分析各个标准的csar包，将各个标准的csar包转换关系输出到关系文件中，如联通标准转换成社区标准的关系文件Unicom2EG.properties，根据转换的源与目标解析对应文件，生成目标标准的csar包。

   对于应用镜像没有放置在csar包中的，需要用户手动上传。

   对于源应用包中需要输入的外部参数，通过解析源的values.yaml（每个标准的路径不同，需要配置好），提示用户手动输入参数。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144744_836946e6_8354563.png "appdtrans.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/144754_4fc1c396_8354563.png "inputs.png")

**接口变更：**

| URL                          | Method | Change Type | request                                                      | response                                                     |
| ---------------------------- | ------ | ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| /mec/appstore/v1/appd/upload | POST   | Add         | {    "appFile" : file,    "appFilePath" : string,    "appImagePath" : string,    "sourceAppd" : string,    "destAppd" : string,    "appDocs" : file,    "netType" : string  }    如果csar包与应用镜像分离，镜像单独上传，则填写字段“appFile”与“appImagePath”；如果镜像放在csar包中，且整体包大小不超过10M，则填写“appFile”字段；如果镜像放在csar包中，且整体包大小超过10M，则填写“appFilePath”字段（采用分片上传应用）。 | {    "param1" : {      "type" :  string,      "dftValue" : string,      "desc" : string    },    "param2" : {      "type" :  string,      "dftValue" : string,      "desc" : string    }  } |
| /mec/appstore/v1/appd/trans  | POST   | Add         | {      "param1" : string,      "param2" : string  }          | application/octet-stream  {      binary output.  }           |

