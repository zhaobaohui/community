# AppStore v1.6需求分析

## 需求列表

| **需求描述**             | **涉及模块**             | **依赖模块** | **工作量** | **交付计划** | **责任人**     |
| ------------------------ | ------------------------ | ------------ | ---------- | ------------ | -------------- |
| MEAO同步界面优化 | appstore-fe          |              | 0.5K   | 迭代一   | 程润东     |
| 应用界面优化             | appstore-fe              |              | 3k         | 迭代一       | 陈凯           |
| 增加应用下架功能         | appstore-be  appstore-fe |              | 0.5k       | 迭代一       | 罗小云、程润东 |
| 支持大应用下载           | appstore-be  appstore-fe | webesite-gateway | 0.5K      | 迭代一       | 程润东、罗小云 |
| 菜单栏公共组件替换优化           | appstore-fe |              | 0.3k         | 迭代一       | 程润东 |

## 需求分析

### MEAO同步界面入口优化

方案：

•    页签修改为“应用同步”，并放在应用详情的最后一个页签；

•    按钮修改为“创建同步任务”；

•    在表格上方增加描述说明。

![输入图片说明](../../../%E5%BA%94%E7%94%A8%E5%90%8C%E6%AD%A5.png)


### 界面优化

Dev平台已使用新界面，appstore风格与之保持一致，对界面进行优化：：

![输入图片说明](../../../%E9%A6%96%E9%A1%B5.png)
![输入图片说明](../../../%E5%BA%94%E7%94%A8%E4%BB%93%E5%BA%93.png)
![输入图片说明](../../../%E5%BA%94%E7%94%A8%E8%AF%A6%E6%83%85.png)

### 增加应用下架功能

方案：

•    应用仓库页面表格模式的“删除”操作修改为“下架”；

•    我的应用中操作列“更多”中添加“下架”；

•    只有ADMIN角色和应用生产者可以下架应用；

•    只有已发布的应用可以下架；

•    应用下架后状态修改为“Unpublished”；

•    状态为“Unpublished”的应用可以重新发布上架。

![输入图片说明](../../../%E5%BA%94%E7%94%A8%E4%BB%93%E5%BA%93%E5%88%97%E8%A1%A8.png)
![输入图片说明](../../../%E6%88%91%E7%9A%84%E5%BA%94%E7%94%A8.png)

### 支持大应用下载 
方案：

•    日志过滤器将response转成ContentCachingResponseWrapper类型时，会将response结果被全部读到内存里，需优化日志过滤器，不再记录response的body（appstore-be和website-gateway都涉及）；
![输入图片说明](../../../%E6%97%A5%E5%BF%97%E8%BF%87%E6%BB%A4%E5%99%A8.png)

•    修改从filesystem下载镜像的方法，不适用byte[]将文件一下返回，使用RequestCallback结合Files.copy()方法，保证了接受到一部分文件内容，就向磁盘写入一部分内容，而不是去不加载到内存，最后再写入到磁盘文件；同时指定请求头为APPLICATION_OCTET_STREAM，以流的形式进行数据加载。
![输入图片说明](../../../%E9%95%9C%E5%83%8F%E4%B8%8B%E8%BD%BD.png)


## 接口变更(Interface Change)

| URL                                                          | Method | Change Type | Request                                                      | Response             |
| ------------------------------------------------------------ | ------ | ----------- | ------------------------------------------------------------ | -------------------- |
| /mec/appstore/v1//apps/{appId}/packages/{packageId}/action/unpublish?userId={userId}&userName={userName} | POST   | Add         | Path Param:   appId, packageId  Requrest param:   userId,  username | “Unpublish  Success” |
