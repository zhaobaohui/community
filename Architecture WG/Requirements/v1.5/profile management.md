# profile 定位
1. 应用打包部署+应用使用前自动化配置（Day-0/Day-1 Configuration）
2. 为最终用户提供便捷性的使用，给出开箱即用的安装
3. 目前仅考虑容器场景
![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/143923_50b0e1af_7854857.png "屏幕截图.png")

# profile包设计
![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/144019_5bf10d01_7854857.png "屏幕截图.png")

# 使用场景
开发者平台使用场景：
1. 选择profile->获取profile的应用描述文件，创建一个项目 ->创建项目中多个profileId字段->正常部署流程.（配置脚本放到csar包中）
![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/144245_f54a0d19_7854857.png "屏幕截图.png")
2. 创建项目的时候，在填写基本信息里面，添加一个选择profile->到部署调测的流程，会自动导入profile存在的helm-charts，用户可以继续导入自己的应用helm模板，然后整体打包部署。
![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/144304_dcc6e731_7854857.png "屏幕截图.png")

mecm使用场景：
mecm界面部署成功后，如果csar包Scripts包里面包含sh文件，再进行个配置的操作，后台直接执行csar包中的sh脚本即可。
![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/144558_3a91033c_7854857.png "屏幕截图.png")

# 开发者平台适配修改：
1.Dev生成csar包的时候，要将脚本文件放到包里面 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/144742_070d027a_7854857.png "屏幕截图.png")   
2.创建项目之后，创建helm模板，传入部署文件。   
3.project模型要多一个profileId，根据profileId创建项目的时候，传入profileId。   
4.系统下面新增profile管理，支持profile的增删改查，新增和修改的时候上传zip包即可。   

# profile模型设计
![输入图片说明](https://images.gitee.com/uploads/images/2021/1108/163033_b9d1af39_7854857.png "屏幕截图.png")

# 接口设计
![输入图片说明](https://images.gitee.com/uploads/images/2021/1112/153653_b6a22ef2_7854857.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1109/155354_a3caf174_7854857.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1109/155414_86a97396_7854857.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/171752_ecacecc9_7854857.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/145118_8308cc2e_7854857.png "屏幕截图.png")

 


