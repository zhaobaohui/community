## 现状
- 1、上传部署yaml文件 / 通过界面编辑生成部署yaml
- 2、后台自动生成chart.yaml 和 values.yaml，组装成tgz包
- 3、修改部署yaml中的镜像路径（先下载到本地habor，然后修改镜像url）

## 修改方案：
- 1、上传tgz文件包，包含完整的helm charts结构
- 2、解析tgz包，将内容展示到界面，支持修改
    - 2.1、如果没有tgz包，通过界面配置生成
    - 2.2、内置一套完整的helm charts模板文件（chart.yaml/values.yaml/templates）
    - 2.3、抽象完整的helmcharts对象模型，支撑前后台数据交互
        - 2.3.1、方式1：三方件：microBean Helm（最后更新2018年），验证中
        - 2.3.2、方式2：自己开发模型，参考helm规范（https://helm.sh/zh/docs/topics/charts/）
        - 2.3.3、k8s模板没有对应的三方件解析，需要自己写，参考规范：https://blog.csdn.net/weixin_46991815/article/details/106066426
https://kubernetes.io/zh/docs/concepts/workloads/controllers/deployment/

    - 2.4、后台模型映射到前台界面，通过界面增删改
- 3、通过界面选择增加mep 和 mep-agent的配置（自动化）
- 4、修改部署yaml配置，绑定mep 和 mep-agent（自动化）
- 5、后台解析

## 流程设计
![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/164306_a02a70d7_5659718.png "屏幕截图.png")

- 决策点1：只支持上传完整的helmchart包，tgz文件
- 决策点2：只支持包内容回显和自动增加mep配置（高亮显示），不支持在线编辑

### 基于模板的界面编辑

### 后台模型解析（v1.5只做回显，不做模板解析）
后台定义文件类型的模板：当前需要支持 charts.yaml、values.yaml 和 deployment.yaml。以下是deployment.yaml举例：

模板中需要包含以下内容：
- 1、所有的变量定义和注释
- 2、变量类型：string/number/map/array
- 3、必要情况下需要提供输入备选项，默认不提供
- 4、需要指定字段是否必填

```yaml
# yaml格式的pod定义文件完整内容：
apiVersion: v1       #必选，版本号，例如v1
kind: Pod       #必选，Pod
metadata:       #必选，元数据
  name: string       #必选，Pod名称
  namespace: string    #必选，Pod所属的命名空间
  labels:      #自定义标签
    - name: string     #自定义标签名字
  annotations:       #自定义注释列表
    - name: string
spec:         #必选，Pod中容器的详细定义
  containers:      #必选，Pod中容器列表
  - name: string     #必选，容器名称
    image: string    #必选，容器的镜像名称
    imagePullPolicy: [Always | Never | IfNotPresent] #获取镜像的策略 Alawys表示下载镜像 IfnotPresent表示优先使用本地镜像，否则下载镜像，Nerver表示仅使用本地镜像
    command: [string]    #容器的启动命令列表，如不指定，使用打包时使用的启动命令
    args: [string]     #容器的启动命令参数列表
    workingDir: string     #容器的工作目录
    volumeMounts:    #挂载到容器内部的存储卷配置
    - name: string     #引用pod定义的共享存储卷的名称，需用volumes[]部分定义的的卷名
      mountPath: string    #存储卷在容器内mount的绝对路径，应少于512字符
      readOnly: boolean    #是否为只读模式
    ports:       #需要暴露的端口库号列表
    - name: string     #端口号名称
      containerPort: int   #容器需要监听的端口号
      hostPort: int    #容器所在主机需要监听的端口号，默认与Container相同
      protocol: string     #端口协议，支持TCP和UDP，默认TCP
    env:       #容器运行前需设置的环境变量列表
    - name: string     #环境变量名称
      value: string    #环境变量的值
    resources:       #资源限制和请求的设置
      limits:      #资源限制的设置
        cpu: string    #Cpu的限制，单位为core数，将用于docker run --cpu-shares参数
        memory: string     #内存限制，单位可以为Mib/Gib，将用于docker run --memory参数
      requests:      #资源请求的设置
        cpu: string    #Cpu请求，容器启动的初始可用数量
        memory: string     #内存清楚，容器启动的初始可用数量
    livenessProbe:     #对Pod内个容器健康检查的设置，当探测无响应几次后将自动重启该容器，检查方法有exec、httpGet和tcpSocket，对一个容器只需设置其中一种方法即可
      exec:      #对Pod容器内检查方式设置为exec方式
        command: [string]  #exec方式需要制定的命令或脚本
      httpGet:       #对Pod内个容器健康检查方法设置为HttpGet，需要制定Path、port
        path: string
        port: number
        host: string
        scheme: string
        HttpHeaders:
        - name: string
          value: string
      tcpSocket:     #对Pod内个容器健康检查方式设置为tcpSocket方式
         port: number
       initialDelaySeconds: 0  #容器启动完成后首次探测的时间，单位为秒
       timeoutSeconds: 0   #对容器健康检查探测等待响应的超时时间，单位秒，默认1秒
       periodSeconds: 0    #对容器监控检查的定期探测时间设置，单位秒，默认10秒一次
       successThreshold: 0
       failureThreshold: 0
       securityContext:
         privileged:false
    restartPolicy: [Always | Never | OnFailure]#Pod的重启策略，Always表示一旦不管以何种方式终止运行，kubelet都将重启，OnFailure表示只有Pod以非0退出码退出才重启，Nerver表示不再重启该Pod
    nodeSelector: obeject  #设置NodeSelector表示将该Pod调度到包含这个label的node上，以key：value的格式指定
    imagePullSecrets:    #Pull镜像时使用的secret名称，以key：secretkey格式指定
    - name: string
    hostNetwork:false      #是否使用主机网络模式，默认为false，如果设置为true，表示使用宿主机网络
    volumes:       #在该pod上定义共享存储卷列表
    - name: string     #共享存储卷名称 （volumes类型有很多种）
      emptyDir: {}     #类型为emtyDir的存储卷，与Pod同生命周期的一个临时目录。为空值
      hostPath: string     #类型为hostPath的存储卷，表示挂载Pod所在宿主机的目录
        path: string     #Pod所在宿主机的目录，将被用于同期中mount的目录
      secret:      #类型为secret的存储卷，挂载集群与定义的secre对象到容器内部
        scretname: string  
        items:     
        - key: string
          path: string
      configMap:     #类型为configMap的存储卷，挂载预定义的configMap对象到容器内部
        name: string
        items:
        - key: string
          path: string
```

### 前台回显数据和编辑（v1.5只做回显，不做编辑）
前台先根据支持的数据类型生成对应的小组件：String/number/map/array，根据模板定义的每个字段，自动生成界面展示，支持数组的收缩和展开

![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/165911_8ecfa73b_5659718.png "屏幕截图.png")

### 界面显示效果

![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/170150_c8e06e0b_5659718.png "屏幕截图.png")

## 容器镜像的流转过程（现状）

![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/165657_87d76ea9_5659718.png "屏幕截图.png")

决策点：
- 决策点1：中心侧harbor是否可以归一，dev/appstore/mecm 三个平台流转的appd包统一，不修改
- 决策点2：中心侧 和 边缘侧是否可以公用harbor，中心分发到边缘的appd包不再修改镜像地址
