# **develoepr虚机流程** **v1.5** **需求分析** 

## 需求列表

| **需求描述**                        | **涉及模块**                    | **依赖模块** | **工作量** | **交付计划** |
| ----------------------------------- | ------------------------------- | ------------ | ---------- | ------------ | 
| 前台界面操作流程优化，沙箱可显式选择，显示沙箱特征信息，显示应用孵化沙箱环境拓扑，统一操作台界面进行应用孵化          | developer-fe        |              | 10k       |        |                  
| 虚拟机应用数据模型重构(5K)                        | developer-be        |              | 5k       | 迭代一       | 
| 接口优化为标准restful接口，以对象方式操作，避免前台页面变更引起接口变更(2K)                | developer-be       |          | 2k         | 迭代一       | 
| 应用拉起，镜像制作等流程优化，规范lcm接口响应 | developer-be |      lcm        | 1k         | 迭代一       | 
| 支持APP线上信息收集，提供给伙伴支持在线提交诉求，由运营商进行应用孵化             | developer-be                         |              | 2k         | 迭代二       | 
| 支持虚机拉起后远程VNC（当前为SSH无法满足windows及桌面版linux场景）             | developer-be                         |              | 2k         | 迭代一       | 
| 支持fusionshpere场景的qcow2导出             | developer-be                         |      lcm         | 2k         | 迭代三       | 
| 支持APPD细节参数的在线修改             | developer-be                         |              | 2k         | 迭代一       | 
| 支持应用多虚拟机场景(2K)             | developer-be                         |      lcm         | 2k         | 迭代三      | 
| 支持灵活打包能力，应用包灵活二次修改能力             | developer-be                         |              | 1k         | 迭代一       | 
| 支持镜像描述文件在线修改能力             | developer-be                         |              | 0.5k         | 迭代一       | 
| 支持自定义VM的Flavor（当前为固定规格列表）            | developer-be                         |              | 0.5k         | 迭代一       | 
| 支持网络自定义            | developer-be                         |       lcm        | 0.5k         | 迭代三       | 
| APPD模板支持将边缘节点相关参数作为input            | developer-be                         |              | 0.5k         | 迭代二       | 

### **EdgeGallery虚机孵化主体流程**
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/095816_593346d5_7625288.png "屏幕截图.png")
### **虚拟机应用集成场景划分**
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/095842_564a2eb2_7625288.png "屏幕截图.png")
### **应用生命周期流程**
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/095918_305799b6_7625288.png "屏幕截图.png")
### **通过拓扑直观方式配置应用**
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/095935_aecaf6e8_7625288.png "屏幕截图.png")
### **应用打包预览，支持应用appd修改**
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/100018_5dc5d89d_7625288.png "屏幕截图.png")
### **后台重构**
•    原模型
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/100058_45b97495_7625288.png "屏幕截图.png")
•    新模型
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/100117_a4c7830e_7625288.png "屏幕截图.png")
## 接口变更(Interface Change)
https://gitee.com/edgegallery/docs/blob/master/Projects/Developer/Developer_Interfaces.md
## 数据库变更(Database Change)
https://gitee.com/edgegallery/docs/blob/master/Projects/Developer/Developer_DataBase_Design.md