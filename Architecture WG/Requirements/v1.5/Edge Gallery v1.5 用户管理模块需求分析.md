# EdgeGallery v1.5 用户管理模块需求分析

## 需求列表

| **需求编号**                        | **需求标题**                    | **需求描述** | **工作量** | **交付计划** | **责任人**             |
| ----------------------------------- | ------------------------------- | ------------ | ---------- | ------------ | ---------------------- |
| I4FU6R | EdgeGallery支持北向接口 | UserManagement提供获取AccessToken的北向认证API，第三方系统可以通过Token调用业务后端接口 | 1k | 迭代2 | 周艳兵 |
| I4FU7G | 支持与第三方IAM系统集成 | UserManagement支持与第三方IAM系统的集成，使用第三方IAM完成EG的认证鉴权 | 2k | 迭代2 | 周艳兵 |
| I4FU8F | 支持通过proxy访问EdgeGallery | 支持通过proxy访问EdgeGallery | 1k | 迭代1 | 周艳兵 |

## EdgeGallery支持北向接口

### 整体方案

提供给第三方系统调用的北向接口，通过一个北向网关来转发，屏蔽UserMgmt和各业务模块。

![输入图片说明](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA8UAAAG1CAYAAADQj1aEAAAAAXNSR0IArs4c%0A6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAFOwSURB%0AVHhe7d0J9CVlfef/nDPnf86cmXPmTE6SURk7mcSJCj1uoAGCBqENxAUURpy/%0ACyI0tD/XKCatGcdETFrRSHM0rRnFuBGXmNZ/bJqGZjOAGBYFVFAaBZVFofd9%0A737+z/epp6qeqnqeqrp163fr3qr365zv6b61PrXe+vyq7r2/pgAAAAAAGChC%0AMQAAAABgsAjFAAAAAIDBIhQDAAAAAAaLUAwAAAAAGCxCMQAAAABgsAjFAAAA%0AAIDBIhQDAAAAAAaLUAwAAAAAGCxCMQAAAABgsAjFAAAAAIDBIhQDAAAAAAaL%0AUAwAAAAAGCxCMQAAAABgsAjFAAAAAIDBIhQDAAAAAAaLUAwAAAAAGCxCMQAA%0AAABgsAjFAAAAAIDBIhQDAAAAAAaLUAwAHVqyZAlFzUQBANBXhGIA6JCEjZtu%0AuomiproIxQCAPiMUA0CHCMXULBShGADQZ4RiAOgQoZiahSIUAwD6jFAMAB0i%0AFFOzUIRiAECfEYoBoEOEYmoWilAMAOgzQjEAdIhQTM1CEYoBAH1GKAaADhGK%0AqVkoQjEAoM8IxQDQIUIxNQtFKAYA9BmhGAA6RCimZqEIxQCAPiMUA0CHCMXU%0ALBShGADQZ4RiAOgQoZiahSIUAwD6jFAMAB0iFFOzUIRiAECfEYoBoEOEYmoW%0AilAMAOgzQjEAdGiYofhytWzpUrVila8fNY1FKAYA9BmhGAA6NF4ojsLlshWX%0Aq1Xe/qvUiqVL1NJlKwL9u6pV6vJlS82yL12xytO/Sel1IdNbZtfFqhXRurm8%0A+fRXrdBtXLpMXT6p8L5qlVp1+Qq1Qq8bWQ7vMKZyy5qvVbJftNtuQjEAoM8I%0AxQDQoXFCsQltS3T48fQzpYPh0iX178iuklDWsHzTq6pVly/T7ZNgXBYA61b0%0AB4Bll9vXIy57sSR4Sqj29atXqy6/XF0eqBUrlqlly3Tp4L5Ut1v2g6jktZ6v%0A7hdsu15vMlzZskV/dCjZN0YsQjEAoM8IxQDQocah2IQ+N0y5FYWhy5f5+tla%0Amrt7bIKWZ7haNUb4XKVDYsm49e9wtxmKo2n5l9Wt8ulHf7TQw5ngm9Yye5dc%0Axjd3+eUPC57x/WXv/te4wx5t/3aCsbQXAIC+IhQDQIckbPhCSGnZQBwHIxN+%0AciE3cxe5xp3F1muskO1U6WPEbrUViuXRbj1fWZ+BsBqH3WZ3keXRZpm+3jb5%0AtskfCKoe9zbLlQbdVSuWqRVmHN94ellWFB+drx/A05LlBQCgrwjFANAhCRu+%0AEBIq+cypuUOcCYv2M6Y2JMtjyfnQ5gvOndRYd3Dj0gF4Wf7R5hqhWIfOFcHP%0AX0vFd4gldNpwnPvcbhSIGz5Wbf+Y4d0Ocb+KdSNtSu8SR9tdprdC2mrGr1G1%0A/9CQlowHAEBfEYoBoEMSNnwhJFQSipf5vjgrc1dTB7rCHcfmn/1ttdoKxeZu%0Aq7seaoRi+8cC76PH8uVUJjS6d3Dju7ordLf4y8GatF3u2EZ3l33zTj5bXfGF%0AaNEfO9K7xNHyOK+Tsn8kaRB+Q0UoBgD0GaEYADo0aihOyga8UavRHc42q5VQ%0ArMssvzud6lBcebfc+/nm+O6xVIN2J3eAozKfK3Zex9Ot/pbsOLRnyxeyC8tZ%0A8bntOiXzAgCgrwjFANAhCRu+EDJOZR+xjUtCVQthdNxqKxTHd0OT5awKxfnh%0Aa5Q8bm2/FCsJs/JFWSP9zFP0uV75xun4m7pNWJU70+YutHNnOnO331Pu+KG7%0AxKE/BnjvKNcvQjEAoM8IxQDQoakLxXHwqlu+aZRVrTvc9QJc9iepykNxdtjy%0AWmUCaxSGs1+IJQE3etRZ+snjzvIlV6uq1mmm0sews+E6Cu2ld7KTyi1rUtE0%0AfE8DVN4lryhZXgAA+opQDAAdkrDhCyHVZUPUiOULTGm5jwrXrFE/txr8HKwt%0A6V83vNlHk6NlKgvFdrkCbZVwf/nl8mVVNgjrWpqEYR16zV1edxzptiK60xuv%0ABynfN0o748SBWj7r618+u00r1qkJ+Ll1lLRHt2GF/BbyCvnsefRbyNFPQdk2%0ANvycsYwLAEBfEYoBoEMSNnwhpLokQPnv/PrvFE9HVd2x9QW+cLlhtyQUFz5/%0AnK3ozq2ExqXR7wZn+qfz8LcpCsgrdPgsTN88fi2h1AZSXUttUJXfKo5/tziu%0AeBgzXGj7eR6PNnfs9TJK92ReNhyboC/r09zZd/+IMFrJNAEA6CtCMQB0SMKG%0AL4RUliccxTW9odgGzJLQO1ooto8Fm5AdCsVucM6O61b4EehofP/6rFrHelwT%0AfqMgLD8HJZ8tjkt+RmmphHATWJ3x4rvpuTbF31JtPtdspivLnl+f2Ueozfp0%0All3mFf9/lCIUAwD6jFAMAB1qHIpNcLKhaMTqLDDbO5Vl88+HuMpK7gL7Q7GE%0Ax7K7xG5Fd7H96yxUo9x1NV+25b52/ngh867cLnaZ5G6zeUTaG3CzTxCMvD4D%0AJcsKAEBfEYoBoEMSNnwhZPRKv8BJ7iD6A1b2LuKkK72r6+8vZYZpFOJCd4pH%0AuTu6qniHOr5rm+/uTD/TPVTxHzH0ssXzyITi5C5w/bvk/iIUAwAwKkIxAHSo%0AjVDsfsmSPHLrhq1M2Tu1nYRiO++qu6HBtldWOBQXh61b0TS9oTIUln0VWPbC%0Assq3XuvhRgvG0Weak0etc+0qhOL8o9o1i1AMAOgzQjEAdGicUBz9dJAOUTr8%0AuV8QlQlbEpjNZ1njb0v23fWUoNRC5acblw2F1WEvCqHTEorjO+++aZiwWSe8%0AxsvuCdb+PwDE2zT6A0emn1nPeluab5Zeatdpcdu78yqG4mbrhVAMAOgzQjEA%0AdGjkUKxDTfQ5WQlOOlQtc+4S2sqErSSQymdRi8Mmd0Pt9MYqT/BLHguudVd1%0AnMe7WwzF8q3RZp2Ex8+Hz2Jlf4LJN4w/FEs5wdjpbuap2xT/PnJ2HJlfMcSb%0AUKynE/2ecvyIfZ1tkS3ZvgAA9BWhGAA6JGHDF0LKSoKN+a3bksDW7G5riyXh%0APQ7bpb/fG/1skfmZooogKhUFwxYqH1TNHVgdYm2bC39AkLvy8U8pmWksrQjv%0AUShelt8OErjlzn0cmIPbSUKsr3u+0vDtbZO7HWq1218yLgAAfUUoBoAOSdjw%0AhZDmNc4jyG1WdFfShHdv/7jiO9V6WPPbul21W+5SR3dhQwHehFz7zc+Vj00H%0Ay1lePa/m04krXs/hdrdRhGIAQJ8RigGgQ+2HYopqvwjFAIA+IxQDQIcIxdQs%0AFKEYANBnhGIA6BChmJqFIhQDAPqMUAwAHSIUU7NQhGIAQJ8RigGgQ4RiahaK%0AUAwA6DNCMQB0iFBMzUIRigEAfUYoBoAOEYqpWShCMQCgzwjFANAhQjE1C0Uo%0ABgD0GaEYADpEKKZmoQjFAIA+IxQDQIcIxdQsFKEYANBnhGIA6BChmJqFIhQD%0AAPqMUAwAHSIUU7NQhGIAQJ8RigGgQ4RiahaKUAwA6DNCMQB0iFBMzUIRigEA%0AfUYoBoAOEYqpWShCMQCgzwjFANAhQjE1C0UoBgD0GaEYADpEKKZmoQjFAIA+%0AIxQDQIcIxdQsFKEYANBnhGIA6JCEDYqahQIAoK8IxQCAXrj55pvVF77wBfsK%0AAACgHkIxAKAXCMUAAKAJQjEAoBcIxQAAoAlCMQCgFwjFAACgCUIxAKAXCMUA%0AAKAJQjEAoBcIxQAAoAlCMQCgFwjFAACgCUIxAKAXCMUAAKAJQjEAoBcIxQAA%0AoAlCMQCgFwjFAACgCUIxAKAXCMUAAKAJQjEAoBcIxQAAoAlCMQCgFwjFAACg%0ACUIxAKAXCMUAAKAJQjEAoBcIxQAAoAlCMQCgFwjFAACgCUIxAKAXCMUAAKAJ%0AQjEAoBcIxQAAoAlCMQCgFwjFAACgCUIxAKAXCMUAAKAJQjEAoBcIxQAAoAlC%0AMQCgFwjFAACgCUIxAKAXCMUAAKAJQjEAoBcIxQAAoAlCMQCgFyYZilfPHaHm%0AVtsXrtVL1IK5K+wLx7pL1KKk+xVqbsERakFZmWHvU8sXnaSWr4vGWrf8JLVo%0A+X3Ri4RMa4nyNcWQ+S66RNlJaNlpxmR5wu0AAKDfCMUAgF5oHIoLwbEOCZee%0AYBwKxZqEWn8/f1AtdvcNVwzFwYAbKtumTNC3y2HaXBjH01YZXvfz/qGgY2Xr%0Ao157A9saANAbhGIAQC80CcVJ6Bs5FAsdSPPjlYTi+uE35ulemH47d4qFLxQX%0Apy/jF+cn4y7S0w0v+zSQtjcJt03HAwDMCkIxAKAXRg3F5g6ihDgJgI1CsUdp%0AKA4JBVVf93w3TzCXblVtkHZW3WE205D5OcN576rb4Cz9fHeRp0bTcNt0PADA%0ArCAUAwB6ofHj0yOF4igghe4u+x+RdsZJ+km3OEC6/3eFujtCj37rZTKfPy6E%0AX+fOb25c/51i6e60wekeS5c5Ws7i556jacdhO98/3M9Zb1LufM1yjdDdiKbn%0AD7e5eWXCfW48E/7z0w+1NR7X7V+xTQEAE0coBgD0wmRCsRUIo1HAy96BTWQC%0ApYSkOBzlAlUSmtxhAjwh1XCDW/z/QMVB1H+nOAq97jDZUBmHPvuysC7tsjnd%0A1i2/xK6fGv1y4dK0wyyPs15WXxL9P9Q9kWtrIj8vzYTr7PYx49l16VsH3rbG%0A/Zx2mfU86v4GAJhXhGIAQC90H4olAJ2k5uaW5MKYVRqKnTCXCHVPuYE1YkOY%0AaZvn0ep8cHRkAq/b1mRZnbvMMTO9/J1oZzqF/o6yfiaUZvsld6TNeJ5wG+qe%0AiNZNob9nXkLWhxts51ZHy5Zd31pZW+24mXHKlhsA0AlCMQCgF7oKxeuW2xAc%0Ad3MDpSvT3Q287v9doe4xCWnh/iaY5fvrNsgXYhWCnRYMxXE79HLmlyuah9wJ%0AzVU8XNm6rernm248vNM/syyh7kYUUL2h2NMOc0fXLEc0Xmb+rtK2euZJKAaA%0AqUMoBgD0QheheHkuOEXhx4bIQsrS85Fh9b/RZ0zjYQLD57rnH7s1ryV85YKq%0AiANd9k5yPD3PHWQtCsUyjJ2uKTt/b5DL3RWOmZBoh3X/n9e0n8u0y9MGb/do%0A2Urb65D1Ea27eDy7bvLru7StnnkSigFg6hCKAQC90GooDoWtmO0fByRzx9Sd%0Ahm+aJjxJ0JRAJGGpTiiOw6mufCC2r9PwFnFfZ0Kxnr+3u2amZ+Zj2yFtTcKf%0AfWxYtzETCM3y+MKd+5hxMUiGPzfs9nOnEZE2m+2h55tul2gapd0Tvm6i2I7s%0AsrnjRe3KDFvWVt88CcUAMHUIxQCAXphoKJZxbDAygdgTckzQzAQ+N1RKWKoI%0Axb425MJ4JAplvrYmAbmwjJ6w5oqXz4TDdDh3mfLL5zL9kvlF84rDvRsey/vZ%0AAFrolx0nu0593WNly5wbN7M98+PZdmXWZ3lbM/MkFAPA1CEUAwB6oXEoHkdF%0AwPEFxyhEpwHKW/mQrkXj+e4oa9IObwhdouaC4dUO5+tnw7BvfmVhGACAWUQo%0ABgD0QiehGAAAzDxCMQCgFwjFAACgCUIxAKAXCMUAAKAJQjEAoBcIxQAAoAlC%0AMQCgFyYXirPfKGy+eMr9kqxMOV9UlXx5VVX5v7hL5pN8i3H87dAVZJzsNzqX%0AucL7+8XZL/HKqvWlYbr83/gMAMB0IBQDAHph5FAc/7xRXCN9o3IajDNhNUOG%0AyYXizDxy/Q35aZ92QnF1YM3PJxCKc38EcMk8qgJvnWEAAOgSoRgA0AujhWIJ%0Aem4ojH5ntv5d1VQmrBZ+GqmbUBz/fNMib5gNLasTimX6mQCdrXhcQjEAoA8I%0AxQCAXhj38WkTJCvvFkd3Td1gGIXVKGhmg+MVxVCcG8ZfNhTnfgM5HIqzd3Lz%0AyyHjxa/N/z2hO+qellk2z2PTMpwbpgnFAIA+IBQDAHph3FDshsdKOpRmQ7H5%0Aby5IjnunOBt2M/Nxp5ULz1lpiE8rNKxzpzjmLKeEWzcQC0IxAKAPCMUAgF4Y%0AKxRLyHS/FKtKPhTPee4CL1qiQ+Y4oVhzxgmF4mxYzYdg3zLl7monbYpDsS9I%0A58qGZ0IxAKAPCMUAgF5oGooltEnQGym45UOxd9xc6JUgmw+X3nLv5KYh2R+K%0AfcE6Lxe0g+KwnJ9/eN0QigEAfUAoBgD0wuihOL4jWicw5thQvHpuiZqzYVVC%0AaxpsJahmA6uEw+zjx75AWwywcRj2hmL5N//Ic507vVKZx7zdbnE4LinnLjWh%0AGAAw6wjFAIBeGDUUmxCbPDo8Ggl6Eg7dL9NKQ2scdouhOBsO64XiOAD7QnGm%0AW0AxjAs9n0KYznWLg3ei2DZCMQCgDwjFAIBeGCkUmy+nygdSh+kfDpxJGHW+%0AWMuE7ORuaj4U+wJwzVBs+UJxpdCXcDntTrmhWNrmLou0q7g+CMUAgD4gFAMA%0AemH0UOyG2LhsgKwIxTE3qBbv2jqh1xtC5zcURyHdPy3/+G4oTkXTsZUbRwJv%0Adv35i1AMAJhmhGIAQC80/aKtxiTo+oJpJnBHodR/tzQNxZlwaabp3qmtWXGg%0AlcCrX+fnlwm3ubvkST+Zd+4PBpnp2GnHj2NzpxgA0AeEYgBAL0w8FAMAgF4g%0AFAMAeoFQDAAAmiAUAwB6gVAMAACaIBQDAHqBUAwAAJogFAMAeoFQDAAAmiAU%0AAwB6gVAMAACaIBQDAHqBUAwAAJogFAMAeoFQDAAAmiAUAwB6gVAMAACaIBQD%0AAHqBUAwAAJogFAMAeoFQDAAAmiAUAwB6gVAMAACaIBQDAHqBUAwAAJogFAMA%0AeoFQDAAAmiAUAwB6gVAMAACaIBQDAGbSI488Yv8X8YXixx57TB04cMC+AgAA%0AKCIUAwBmzuHDh9XSpUvVAw88YLsUQ/HOnTvVu971LrVhwwbbBQAAoIhQDACY%0ASf/2b/+mPvjBD5qALPKh+Etf+pL6yle+Yl8BAAD4EYoBADNJwvCHPvQhdcst%0At5jXbih++OGHzV3iXbt2mdcAAAAhhGIAwMz62c9+pv78z/9c7dmzJwnFEpY/%0A+tGPqhtvvNEOBQAAEEYoBgDMNAnCK1euTELxHXfcoT7wgQ+oQ4cO2SEAAADC%0ACMUAgJm2bds2deGFF5pgfNlll6n3vOc96v7777d9AQAAyhGKAQAz79prr1VL%0Aliwx9elPf9p2BQAAqEYoBgDMPPkt4jgUb9682XYFAACoRigGAI9Du3ernd+9%0AU21edaXa+JWvUTNQN3zs79Rl77/I24+awvra19W2G29W+x79pT3qAADoBqEY%0AABwShO8/69Xqjv/8RPWDZx+t7jv1Rer+M15GzUitO/M0b3dq+mrdaS9R9/7h%0ACeq7T/ht9YOjj1OPffJT6vD+/fZIBABgcgjFAKAd2rNHPXDeEnXXU45U6z/5%0AN+rgz/9VqU23UxQ137XhVrXj2i+o+898ubrr95+hdn7vLntUAgAwGYRiAIN3%0AcNt29cNjX6B+tuT16vCvbvFfuFMUNe+17ZufVncueIractVae3QCADD/CMUA%0ABu3wwYPqvpe+Qj387rd4L9Ipipps7b7t6yYY7/rBPfYoBQBgfhGKAQza4//w%0AefXjU16k1MbbvBfoFEVNvjZ/6VL1g2OOU+rQIXukAgAwfwjFAAbr8L596ntP%0A/u9q93f+2XthTlFUR7XxNnXvCSeoTf/8dXu0AgAwfwjFAAZr6zXXq3uf/0f+%0Ai3KKojqtDZ/+oFr38jPs0QoAwPwhFAMYrJ//2V+oR9//Z94Lcoqiuq0DD96g%0AbvsPv6HU4cP2iAUAYH4QigEM1rpXvlZt/OxHvBfkFEV1X3f85yeoAxs32SMW%0AAID5QSgGMFj3vvBUte1fPuW9GKcoqvu66/eeqnbfd789YgEAmB+EYgCDde+J%0AOhSv+rT3YpyiqO7r7v/+NLX7x+vsEQsAwPwgFAMYLEIxRU13EYoBAJNAKAYw%0AWIRiipruIhQDACaBUAxgsAjFFDXdRSgGAEwCoRjAYBGKKWq6i1AMAJgEQjGA%0AwSIUU9R0F6EYADAJhGIAg9XHULzuqxequfPOUsu/ujLXb6Vavews3e9CtS7T%0AvWldquZeeJxadN6lzaf31bPUIj2Nua96+mVK2q7n9cK22j5btfo8WUf57emv%0AdXobj7VNpqwIxQCASSAUAxisXoZiHR4XLDhCLVpWDMXLX3iE7neWWp3p3rB0%0AoJX5LBgnqNppVIfidLnqDDv/pdflefIHhraqZB3efqFaJOvZu03zdamaqz3s%0AbBShGAAwCYRiAINFKG5eq8+LwtdYIdUbileqdbf7yoZDCeHe/u40dOnh/QF0%0A9Fqen7YTPtupim1SMxjH22TBeZd6+89iEYoBAJNAKAYwWITistJB86uXqtXe%0A0oHThDR5rNfX31eeMOcJxXH7R6/ccsV3sluoYvC3oVgH9NXegF63Lq2/TfTy%0AxMHY+4eIZHnb+aPHtBShGAAwCYRiAINFKC6rlu+Gxo9Z6zCYBOWkrWlwlqCY%0A9r9QLY//76nlun/6Wo/rtj8OifL52kIYrVfhu+FpKB7vs7tl28TTJvm8sF5X%0Ahe7JneTos8fF/vlpz04RigEAk0AoBjBYhOKo1tm7kNlx4uAnX9qVDaOjlfPY%0As0y39A6u07Y46IUeBXYfp/b1d0JxoV/NqhuKi6FTr+vz5C5yvrstE/xlXYe3%0ASfIodBs1dnjvrgjFAIBJIBQDGKzBh2J5fPe89HHlRZkvfGrrbmhuOrdX3Cl2%0Axo2DYXhZAo8SS00kFJ+l5nyh1s672O6o4m0kd3WDf6gw3xReVvF2O87TL1dy%0Adzk3/VkpQjEAYBIIxQAGa2ZDsfPFS6EqD8XRzzMl0zB3g/PDz1ModsuGx2Cw%0AjceVR6Cd7uGw7NSk7hR75hONd5xafrseTtZ5pg12O5j1EQ7FlVV1p7wnRSgG%0AAEwCoRjAYM1uKJY7vJ47glImZJXfXU3LfgY1M1xcgTBb+Lyqr5zhRwjF8W8s%0Al1f4Dmkm2E8qFCfDSQh2+tn5pneF7bi2XdH2IRRXFaEYADAJhGIAgzXMx6ej%0AAFf9SK0vzPqCtacy44wQim3bm1ZmmScYit2AGj8Sno5jh7V36LMhmFBcVYRi%0AAMAkEIoBDBZftFVWJaE4+OVbvqBWMxTfvtL8vFGmfzKcnnaoX6gmGYp1ZQJ9%0Aflnju8MvzG8bQnFVEYoBAJNAKAYwWITisioLxaEg5hunOhTHVWyzVNzu+PHk%0AmjXhUJx08w4fL4OUu/5920RPR4dnCdDlFU8vCtvVddZo629KilAMAJgEQjGA%0AwSIUl5Uv+I0fis3nhnVIiwNdEuzOu1CHNk8obhpuOwzF3nDvbc8YodjOy5Sv%0Af6EIxQAAhBCK0alvfUupa6+lqG7qa0e/lVAcLF/wayEUO48ZS1iTf+PQmXkE%0AuUFlwuuEQ3G27fm72vG6z09v1G2SVtw2Kf8d9n4UoRgAMAmEYnTq1/QeeMIJ%0ASi1aRFGTr3Oe/HVCcbDmJxTL54OTb4m2wTUJxblvn47vhi5yuiX9bMh0+2WC%0A6CRDsfv5Xs98k20i7XbHaxqK4/np8B39O3qonpUiFAMAJoFQjE79x/+o1I4d%0A9gUwYTw+7ZQOc/II8+okWJaEYpl+4fFcqahfdhzfdGzlQrGvnz/UlkwzrgmG%0A4mi49O5wZrzcF2Jlp9ksFMfTkG2c3KEuWxczXIRiAMAkEIrRKUIxutSvULxS%0ArV7m3F0dMRQn4SoJkeWhuLQy4zQJxXacwJdrhcOqU5MKxb752CC8SHdbXphG%0AvGyyHUYPxcl2SsZxtskYyzqtRSgGAEwCoRidIhSjS30Ixetu1yHrhXFQSqtp%0AKE7H84VZOw1fwA2Wbzq2gqFYB/zzjksCfvQTUFG7iuE9UBMJxXEbi+t03e0r%0A/YFZ+iXrWofmEUJxMj1d2TbFQTuaV/1tM/1FKAYATAKhGJ0iFKNLMxuKdRBe%0A7oZGKR3Q5nRwXF0It3GVh+JiAPSHYrkbPafD3PyG4riiYOn+/JCpOqHcCZD+%0AR72rKx4/HIovVKv1fJYH2h+13be+5TedZfvUvVMc/ZEgWZ7CttWVfM5Y2qWn%0A57nDPotFKAYATAKhGJ0iFKNLsxqK00doJfBJAEpDUvGOb1xxAPM9kuzrVxJm%0AR6pxQrFU9rHwpHLLXagJheLx1k11KI5+wipqh2wf+cOHbzhT5qmBeFi93JnP%0AiM9mEYoBAJNAKEanCMXo0uw+Pn2pWh64WxsOxendYHNX2fnW5iRIZUJeS8Ev%0A90VTbr+4rcXQKXeI879nbAOhvUue6e5bF3EontfHp8ddN3Y63sevswG3/t3f%0A7F1lKfMbxWVheoqLUAwAmARCMTpFKEaX+vVFW1GVhWIT5tyglan8HeRmwc+9%0Ai52pOJw6d3CjiucrbfOMKwFeB7pCGzzhONP+KQzFletGyn0M2lTF3eFAZe8w%0AS9UN1dNVhGIAwCQQitEpQjG61M9QLJ9jlbunJUFKvgQqU55hbEiVR3BHuhuq%0Aw2j+MWT5FuZkGjr0menacgNfHELlzqa5+1snxNlwLPPI9zPLlus2WkXrp9i9%0A4brJLbtZfs9d7mg9+PuNWvEd9+Ij87NRhGIAwCQQitEpQjG61MdQTFF9KkIx%0AAGASCMXoFKEYXSIUU9R0F6EYADAJhGJ0ilCMLhGKKWq6i1AMAJgEQjE6RShG%0AlwjFFDXdRSgGAEwCoRidIhSjS4RiipruIhQDACaBUIxOEYrRJUIxRU13EYoB%0AAJNAKEanCMXoEqGYoqa7CMUAgEkgFKNThGJ06ccvPVNt/vLHvBfjFEV1X997%0A4m+rfQ8/Yo9YAADmB6EYnSIUo0sPLHmrevzS93svximK6rYOr79V3fr//Cd1%0AeP9+e8QCADA/CMXoFKEYXVr/2S+qn/yvV3ovyCmK6ra2X/kZ9YOjj7VHKwAA%0A84dQjE4RitGl/es3qO/+5pPVwV/c6L0opyiqu3pw8WvVox/8sD1aAQCYP4Ri%0AdIpQjK49+OY/VQ+/523ei3KKorqpvd+/Qn33ib+jDmzeYo9UAADmD6EYnSIU%0Ao2tyt/jO33ma2n7VZ70X5xRFTbYO/+oWdc9xx6vH/+9n7FEKAMD8IhSjU4Ri%0ATIMdt92hvvfkp6jtq/VFuOcinaKoyZR8lOG+l75YPfimt9ujEwCA+UcoRqcI%0AxZgWO27/rrlj/NA736gO/uxb3gt2iqLmqTbepjZ/6WPq7qcuVA+97yKlDh2y%0ARyYAAPOPUIxOEYoxTQ5s2qx+/q53qzt+47+qdae9TP3yA0vVxs982PyWMUVR%0A7damy5erx5b/lfrZm85Xdz3lSHXPCSer7TffYo9GAAAmh1CMThGKMY0Obtuu%0ANn7la+rn73y3uv9VZ6sfv/RMagbqn95wpnr1B8/w9svXVa85U739/9Qblpqf%0Auu/0s8xvhT/6kUvV7vvut0cfAACTRyhGpwjFANpy7fq71aJb/tK+Knfzxh+p%0AE256j30FAACGjFCMThGKAbSFUAwAAJogFKNThGIAbSEUAwCAJgjF6BShGEBb%0ACMUAAKAJQjE6RSgG0BZCMQAAaIJQjE4RigG0hVAMAACaIBSjU4RiAG0hFAMA%0AgCYIxegUoRhAWwjFAACgCUIxOkUoBtAWQjEAAGiCUIxOEYoBtIVQDAAAmiAU%0Ao1OEYgBtIRQDAIAmCMXoFKEYQFsIxQAAoAlCMTpFKAbQFkIxAABoglCMThGK%0AAbSFUAwAAJogFKNThGIAbSEUAwCAJgjF6BShGEBbCMUAAKAJQjE6RSgG0BZC%0AMQAAaIJQjE4RigG0hVAMAACaIBSjU4RiAG0hFAMAgCYIxegUoRhAWwjFAACg%0ACUIxOkUoBtAWQjEAAGiCUIxOEYoBtIVQDAAAmiAUo1OEYgBtIRQDAIAmCMXo%0AFKEYQFsIxQAAoAlCMTpFKAbQFkIxAABoglCMThGKAbSFUAwAAJogFKNThGIA%0AbSEUAwCAJgjF6BShGEBbCMUAAKAJQjE6RSgG0BZCMQAAaIJQjE4RigG0hVAM%0AAACaIBSjU4RiAG0hFAMAgCYIxfNs9dwRam61feFavUQtmLvCvnCsu0QtSrpf%0AoeYWHKEWlJUZ9j61fNFJavm6aKx1y09Si5bfF71IyLSWKF9TDJnvokuUnYSW%0AnWZMlifcjtERisck+5FvexSqZNuH9sWY7Bv56cm+4o5nhvHMo077MvtdzL//%0AucdEclw1OmbGYJbV17aIHH/5ec4t96xDKe+yo47MudXui2WhWIZ3z4vVoVjv%0AS77tUzhXpnzb3lfe94TaAu0SVcdygRxn0XFr2h4aN7/MJevAyPdP2iXzK1n+%0AzHjue1bofAAAQDsIxaOouhDwClwElFy8hC9OQhcG+e6+4dwLjEgw4IbKtsl3%0AMeq/GPS0VYbX/eLxpykUl62P+hexge3dFtkH3fVa2I/qbfusqM3FP6T4peEi%0AmtciPW5p6C4TOKZkf5J5ePerwPCjHTM12f21bmW2ux63dJ3qZZ9z+pv9z7sM%0APWP2YWe9NVxm33koFIr95ye3fPtvKHyGj3GZT9WxX2eYSvFxX2P/LD+uZVmi%0AZV+37r7wPqj7LbfnR9P2qvfCTP9ofeXbldTcp8r7+8rXRrsuxl6382D895bw%0APgcAaAehuKbkoqrsQiDIc3EVX9R4yRug70J+hO6F6VcEo8JFTmhe0Rt88uac%0AzCc/fRm/OD8Zd5Gebty26bxTLG1vegEyzrhV6mxn33bLbRt78VhZ8XTN8NE0%0AzcWdu59Iv/i1CTu+eeemm6/8sWGmU9x35BgMX+D7lluEutdUWL/SjiXO9NLt%0AnQ87meNEy/cvhqN0Wv0ly+hu22j/KA9ufr7zkC8UR+fu6I837rqN7hS/umT+%0Aum3xvllxzMTjF7dpUZ1h6lg959mvPftrLFoP/vZ7y07HjOeeC2SdlIRic45I%0ApqO3dTyO7S/TCG7vzHTd81b1cZx/b5lOTY/xIZwbAKBbhOIazJu8vNHm39zH%0AUXLxEha6MPB1z3dzLvASultVG6SduYCSveix68bMzxnOe9FkL3Kc8EQoHoFv%0AnzHbJ7c9vOVsmzr7XmEY2XYynfjOsK9kHvfpC2Y7Sh35/cTsG3ZadZatajkK%0Ax4FfJmDlSTukjck6iaY5p0OJtEHGK4xvlsNZ51o2xATaVWfb9EwmdJXJrdPM%0AOrfrLQrFf5Ycg/lpJ+dy7aPnyT50fGYbxfLnOBPi8vuqJsO5Aa9O4K0zTH3R%0A+cZta7aq9n0ZP7uf5kkAjo/9ZFqedZGR9K9qX8lxN5Lie8t0itbH6MvcdDwA%0AQF2E4lHEF8f2ZTnnYsAzjv9C0Bkn6Sfd4jd59/+uUHdH6CJGL5O5qJNly1zE%0A24sM+W9uXN/FqPmve9HvdI+lyxwtp8w3H4rdi9H83YRwP2e9SbnzNcs1Qncj%0Amp7/AiQ3r8IFWG5cc5HmziPU1ng8t3922pn1HiusZxk/3yZnWwrPtinIDGPb%0AVLLvm22b2X+i9qbrqUbp6X/aLKPMLzstIfPI7xOZ9Zlpb7wOfOujyLtuDVl3%0Advx4ndh9J2pL/EeA7HzMshe2S+748q7P3LYagOK6Com2dbydMtvMbhsTiq99%0AQ+EPEpFo/Mw+FwjFEc820vOJ90Hf/ijd/PtRyjeM7/jxc5bBt/9kjlsPs++6%0A84mml1mO0DSkezxP9/8+8XuGHm5udW496m6F41iml9ku5eVdf6bNnuWx3PNR%0Avr+/X25/ya8Tt81uv1B3I5qmfx/JzS9z/s+NV/t9RcTjusNUnxMBYGgIxaOQ%0AN7uyCwGfQBiN3oQDF0Eyn8ybXfwGlnvjS97Y3GECMtN0uG+u8f8DFV8suBcQ%0Apux03YvEzAWrEb8x25d2Xf6nJBTbZXPW1brll9j1U6NfZn3ZtprlcdbL6kui%0A/4e6J3JtTeTnpZkLIHfdO+Pa9ZlOp6StcT9nWmY9J8ss/T3buLBdfcPlglZo%0AX3Alw8RtvMK2r6Tm9AWw/OvZ31N1Qp/MsziMu38VZJbJXQdR+9N2etahVtxf%0A85zpZNadszyyvaWf2SeceZr1kW9HWtllioYrb0uPmHXl3yZeznbObDPbXULx%0A8V95We6Yiis7H/fx6WSY/Lat2HZJ2X3eF3jzfMOYUFd5XDhkX9PzjD/nW1r5%0A49Gcl+J5xcsm6yb+v+6n/1+6SWR956frsue+aJnkGLFt8ZRZF852TfnPFXXf%0AW9K22eVyulW/t9juSZui18mxapbP2Z/m/b3FGc+u23QaFW2N+zvtyr63AAAE%0AoXgUVRcCPvIGVhhH3qT0hZEOEdk3TCtzgRANm74xOm+4iVD3VDFQ2DdK07b4%0A4s+Rf3N3+C5GjWRZPRczZnput+hC6Qm/bkNxob+jrJ+5cMj2MxeY0iYznucC%0AJNQ9Ea2bQn/PvISsj/wFiLk7oueRWedlbbXjZYbPLLf/AjGaplzwVJUzbt1x%0Akn1QVOxjMs3M8JF0+WLRenHXrX/fjNprLt7y7Uoqt0zJfNy2lrQ7uB6c4e2+%0AIt2zd1qcyi23PDEhj1ZHy6iXt+ScUbzA93frI7NvmPVqO9SSHge+89C1669Q%0Av7/iZeH91Bm/1rdPm23s7Gee/dcly1S1PHWGqST7ZbxfBY69YPcMe6zZYyF7%0AHKbbqHa5X5qV7Pe5Y0DPKz+fSLy+y8rdFlbmPCly26jQ3xHqZ9ZHtrtZF/H6%0AtOeFwnYMdU9E55BCf8/8RPreEo8XLVtm/VW11Y6bGadsnQDAQBGKRyFvPiUX%0AuF7y5mPHSb6gJ+4m06u8mJE3tKoL/FD3mLyRhvtHFz65/roN8qUlvosX38Vo%0AxLZDL2d+uYIXV0+4IgrFZeu2qp9vuvHwTv/ihYSnuxFdRHgvXDztkPWRvwDJ%0AtCFW2lbPPDMXLrINixcxsl6z7fftC7lxpR2+/c5VGMY3XUfJNM36SdZFdFEX%0Av472CzkuivvZSDLzd9ta0W4rs087Vi8vbu86ZLmi6enl9ewzMd98Q23pj2hf%0A9wacGuL1k1lPdvtfe8Ub1L9f8QZnfcfzqqjMNnLGMd3tPltWdt9Lt3tYnWEq%0AOe8rZtl9bZIKHJMiOvZkuGg7xMdinaaZYUumnbavxvpP1n3+HFf1OpIuR67i%0A9sn6yWxfR6hfaJ26wzrDzP97S36fdFS21TPPzHsLAEAQikcReOMqZS8OzGNu%0Azptb9AYl//dcsMt8ZFj9b3R3quoCP9vdvJE67TSv5U3ScxETv+nKhUX6Bh5P%0Az39BH12MyjB2uqbs/L1vttFFpfdCQA+7Mg7FoTfppv1cpl2eNni7R8sWam+h%0As14f2XXnrB93nZe21TPPzLqU/sVtL9ut2Pb8cNmLyey2rssuT7K9PeXZv2Km%0AnXN6+c0j1joExxeyJceT/2I3sP5k3Y50zGRF+7R9kRdPO1G8OM9vh/R1dAx5%0AH3O13Yvbr6QtPRCfcxqz2yOzzWy3v3nv09V//ab/d4qFu++nd4r957lC9wb7%0AgU+dYSrZ9xXTtkK7LG/39DiO1oO8dpZBxtH9Sttnz5npNDzc9hme+RTall+f%0AVa+FdPO01yyHHdb9f16oX9k4ed73EM3bPXB8B+aXvrfE49nt5667yrZ65mna%0AVnP5AGAgCMWjkDef/MVT6A0xZvvHb2JyQZSZhm+a5k1OLjrkTUve0Kou8O0b%0ApRlHlzM9cwFqX6dvsBH3tXuxKPP3dtfM9Mx8bDukrckbdHSBUvhZDLM8vjfg%0AK9QT9PAnXSzTL77Zhz837Paz83TamFx06vmm2yWaRmn3hK+bKLajuGzuuFHb%0ACuvH11bfPHMXLrLui+3M7w++bjLfeDq+/vUV2xAtQ2adFNj1YPbDuC3R8rrr%0AIi+/72WXQ3P3PbMd9DySacfLWG95fcuVcOdj5Nqhpdsxkr7Ww+aP70S0DrLz%0ALU67V8w+XbI9qs6njsw2s9so9DvFRu54SkKxdPduI3fbRdsqPffJdvIfC1Vt%0A9w0j3cqDTY5t82qZlvyxybTLU85+G5278+telis/X7usnmM6amc6Df80tfw6%0Azb2OppNvY37fr3qtFc6/sWj7ROeP4vJUv7e440cy203PN92G0TTM61D3hK+b%0AKLYju2zueFHbsuutpK2+eeaOBQAAoXg08iaVv3gyby6+NzlLxrFvXqELH3Nh%0AkXlTdkOGvKHFFx3u/x2+NthumTdZ++bpa6u0wbypFpbR84bqipfPvIGnw7nL%0AlF8+168/Qfc7yXfhmQ9MZf3sRUKhX3ac7Dr1dY+VLXNu3ML2zI9r25as0/K2%0AZuaZv3Bx9iVD+uf3RzOd/D4i87TTKWzfUWX3IbOvBqcXryu3PU5btOiiunhM%0ARPtTyXJoMu94/Y18zIzC7ttV5W676Fi3/YLrR5Yn17axt8+Ui89LhbLb1Xcu%0AC5B9JxnOHhuhUOzbz5JQbMct0tvHsy2iadnKjZfZ7iWVX75oPM9xEOKe30Pt%0ADy6XK3tMBcm0ZH6+fdNtS8w9N5n+gWNQTzc9B+bbEr+Wf+26y82/7L3F9EuG%0Aj89FUWXfP0L9nPlWjJO2IdQ9FvX379+5cTPrIj+ebVuyfNVtzczTbJMR9jcA%0AGABC8aRUvAn53txrXWB5LlKi8QIXIe7FSuZNeImaC15g2OF8/eKLJc/8yi5Y%0AYtP5O8XTSrZDup5lOxcvrtJhMvuP3Q6yTfwXZGFmO8bTqVPOvJKLs2Q/Sfun%0A7AWd7p602bvfuPurVHGfa3rMjCu/LdLXetly88usz8xyRss36vYZhvy299dv%0Avejp6t9/5OTotax3u9/l12l2nw6cu6SfbB9z7k6Hz0zLTj/ez/P7gU+dYarI%0ANDLHlrMfucuWDUeO0uPREQ9XNowVH+/uupN2FsJ+Zn0W131KzgsENwDAZBCK%0A0SlC8YjMBWXZhSRmlQkTNcIHwkofn86p/vZpAAAwFIRidIpQDKAthGIAANAE%0AoRidIhQDaAuhGAAANEEoRqcIxQDaQigGAABNEIrRKUIxgLYQigEAQBOEYnSK%0AUAygLYRiAADQBKEYnSIUA2gLoRgAADRBKEanCMUA2kIoBgAATRCK0SlCMYC2%0AEIoBAEAThGJ0ilAMoC2EYgAA0AShGJ0iFANoy6ih+Nf+5RXq3fd+kaIoajD1%0Av3/0j/YsCMBFKEanCMUA2jJKKH5490b1ofu/TlEUNZj663VfM38MBFBEKEan%0ACMUA2jJKKAaAoTlw+KD6d988074C4CIUo1OEYgBtIRQDQBihGAgjFKNThGIA%0AbSEUA0AYoRgIIxSjU4RiAG0hFANAGKEYCCMUo1OEYgBtIRQDQBihGAgjFKNT%0AhGIAbSEUA0AYoRgIIxSjU4RiAG0hFANAGKEYCCMUo1OEYgBtIRQDQBihGAgj%0AFKNThGIAbSEUA0AYoRgIIxSjU4RiAG0hFANAGKEYCCMUo1OEYgBtIRQDQBih%0AGAgjFKNThGIAbSEUA0AYoRgIIxSjU4RiAG0hFANAGKEYCCMUo1OEYgBtIRQD%0AQBihGAgjFKNThGIAbSEUA0AYoRgIIxSjU4RiAG0hFANAGKEYCCMUo1OEYgBt%0AIRQDQBihGAgjFKNThGIAbSEUA0AYoRgIIxSjU4RiAG0hFANAGKEYCCMUo1OE%0AYgBtIRQDQBihGAgjFKNThGIAbSEUA0AYoRgIIxSjU4RiAG0hFANAGKEYCCMU%0Ao1OEYgBtIRQDQBihGAgjFKNThGIAbSEUA0AYoRgIIxSjU4RiAG0hFANAGKEY%0ACCMUo1OEYgBtIRQDQBihGAgjFKNThGIAbSEUA0AYoRgIIxSjU4RiAG0hFANA%0AGKEYCCMUo1OEYgBtIRQDQBihGAgjFKNThGIAbSEUA0AYoRgIIxSjU4RiAG0h%0AFANAGKEYCCMUo1OEYgBtIRQDQBihGAgjFKNThGIAbSEUA0AYoRgIIxSjU4Ri%0AAG0hFANAGKEYCCMUo1OEYgBtIRQDQBihGAgjFKNThGIAbSEUA0AYoRgIIxSj%0AU4RiAG0hFANAGKEYCCMUo1OEYgBtIRQDQBihGAgjFKNThGIAbSEUA0AYoRgI%0AIxSjU4RiAG0hFANAGKEYCCMUo1OEYgBtIRQDQBihGAgjFKNThGIAbSEUA0AY%0AoRgIIxSjU4RiAG0hFANAGKEYCCMUo1OEYgBtIRQDQBihGAgjFGNili1T6qMf%0AtS+sfCj+4heVesc77AsAKHHit9+rfrjtF/aVPxS/6o6/Vdfo7gAwNB++/xvq%0AqsfvtK/8ofj/++Wt6uMPrLavgOEiFGNiHnxQqd/8TaUefdR20NxQvG2bUk96%0AklLf+170GgDKfPLBq9TJ336ffVUMxWsfv0s95do5tffQftsFAIZDzoG/d+0b%0Ak3NgPhTvPLhHPXntYnXLph/bLsBwEYoxUe99r1Kve519obmh+F3vUur886P/%0AA0CVg4cPqWfe8A618tHvmNduKJaLvyOvf6v65q9uM68BYIhOv/WDatm6fzb/%0Az4fi9/7oH9Vrv7vcvgKGjVCMidq5U6kFC5S65ZbodRyK77tPqd/6LaXWr4+6%0AA0Ad/7rhHvXfrlmidh/clwnFl/50lTr1OxeZ/wPAUD2w8zH1G2vOVo/s2ZgJ%0AxW53AIRidODLX1bqmGOUOnQoDcWnnqovYi+1AwDACORzw++/76tJKF6/d5v6%0Arater3684xE7BAAM11/ce7m5I+yG4lfc9qHkDjIAQjE68vznK3XZZVEolpB8%0A5JFKHThgewLACH6xe7254/EPv7jOhOLz7/qEuvCHn7V9AWDY4s8O37jxHhOK%0A5Q+I7meNARCK0ZE771TqiU/UO6DeA+XftWttDwBoQO4U//qVrzP1pKvPVdsO%0A7LJ9AABfevhG9axvvUP92r+8Qh11/dvMt04DSBGK0ZkLLohC8ckn2w4A0JB8%0Aplgu9qTkjjEAIOvYG5eac+Qf3/JXtguAGKF4gg4fVmrv3kOUrUceOWRC8fe/%0A7+8/1JL9BKiL80pa77/3a+aCb8/eg97+1HQW5zxgMu7Y8hNzjrx3+0O2C4AY%0AoXgeyRv9df+6UZ395u+ro573r+qI31urnvTfrlZP+l0qrv+y4EZv98GW3j+O%0A+L2r9f7yLfW6N92t1l6/gQtGZMj+cMPN69U5b7tVHXXsWnXEU67S+80avf9Q%0AUv/lmH/ydqemtPS+K/uw7MuyT8u+zTlveB782S614pMPqHMWf0+9+PTvqD9a%0AdBM1T/Wss7/i7U61Uye+6GZ1xqtuVW97x/fVv6z6pdqxgy/MmRWE4nly3/07%0A1KLT/k39j+ffrF72zp+oxX+/Qb39q9vVO7++i6JKS/aT8//vBvWyC3+invFH%0A31YvfMl31D0/2m73LAzZ/T/dqV70ipvUwhdcpV7y7ivVeV+4Ur31iivVn66l%0AqNkt2YdlX5Z9WvZt2cdlX0f/PfroHrXkrXeqI4+5Rr3sjder/3Xx1ersv1+j%0A3vDZKylqJuucf7hSvebja9SZf3m1euFZ16kjn32N+tRnHlQHDvDXvmlHKJ4H%0Aa69br572nOvV/3zfz9U7V/qDD0XVKr3/vPIvf2H2pyvWPGb3MAzRddc/rp76%0AnLXqFR/QQeLqYrCgqF6U3rdlH5d9/bobHrd7P/ro27dsVM943nXq5X9xtXrb%0AGs++QFE9qAu+cqU64ZVr1emvukVt3cq3fU8zQnHLbvvuZnXkMTeo8/9+oz/k%0AUFSDOv9TG9VRz/2W+vZ3Ntk9DUPyvTu3qKcfc4069/P+N12K6lvJvi77vOz7%0A6J/b7tis39OuVed8Zo13+1NUr+rqK9WLL7xKnfgnN6o9ew7aowDThlDcIvnc%0AwDN0cHn98se8wYaixqk3fOxxddTRN/CXxoHZufOgeuax16nXfdLzRktRPa7X%0AfnKN2fd37eIisk82btynnvkH16mzP+Xf7hTV11p0/tVq7h132iMB04ZQ3KIP%0AfmSdetE5P/AGGopqo04574fqr/7mx3aPwxBcfMn96uTz1nrfYCmq73XyuWvV%0Ah5ffb48G9MHS/3OPesmfck6jhlfyMYGj/mCtuvv7W+3RgGlCKG7JwYOH1dOe%0AcZ1642c3e8MMRbVRb/7CVvX7ej/bzxc2DMKhQ4fVkc+5Rl3wZf8bLEX1vWTf%0AP0ofA3IsYPZt3bZfPWXhWvXmb/q3N0X1vU5735Xq7CW32SMC04RQ3JJbb9us%0Ajjnp294gQ1Ft1rGn3GK+oAT9d9fdW9XRJ17rfWOlqKGUHANyLGD2rfzGo+qk%0AV13n3c4UNYR60zeuVL975NVq//5D9qjAtCAUt+Qfv/yQOunVd3lDDEW1WYvO%0Avlt97ou/sHse+uyfVj6iTnwtjxlSw64TX7NWfe3rj9ijArPs/X/9Y3X6n3FO%0Ao4ZdRx57tfoJPzs3dQjFLbn04z9Vf3z+Pd4QQ1Ft1qlv/JG6+G/X2T0PffZ3%0An3xAnfzGq7xvqhQ1lDp5yRpzLGD2vfntd6szPsA3TlPDrmefcpW66ds88Tdt%0ACMUt+dvl96sXXXCvN8RQVJt16pvvU3/9wfvsnoc+W/6xn6qT5/xvqhQ1lJJj%0AQI4FzL7Fb7xTnflB/3amqKHUc15ypbrmOn6HfdoQiltCKKYmVYTi4SAUUxSh%0AuE8IxRRFKJ5WhOKWEIqpSRWheDgIxRRFKO4TQjFFEYqnFaG4JYRialJFKB4O%0AQjFFEYr7hFBMUYTiaUUobgmhmJpUEYqHg1BMUYTiPiEUUxSheFoRiltCKKYm%0AVYTi4SAUUxShuE8IxRRFKJ5WhOKWEIqpSRWheDgIxRRFKO4TQvGY9bnL1Lm6%0AvP2omSlC8XQiFLek9VD8ibvVYilfv0LZYT/h6zcNFbev7vLkKh5Xl7f/wIpQ%0APBydh2J7AXaur99Ul27zsovUK50a5oWk3X758g47vUUo7o/5DcWefd2Ub9hZ%0ArIvUcQuOUAt0HbfM13+I5dveurzDTk8RiqcTobgl7Ybiu9Wpz4tOfAsvqA6C%0Aiy840Qy74HkXNwud81xnnRYtS6M2fuJitTAed8GJ6tSpDf6TK0LxcHQbitML%0AsAULTlev9A4zZfW5OXXcsXGbPXXs3AwG/Ob1yjM868CphWdcNBPrg1DcH/MZ%0Aiqv29wXHPlcd95bZ2Of9RSjO16ye4wjF04lQ3BJCcbgyoXjEYDvOuF3X4k+s%0AVKeedrE6y9NvnCIUD0eXofjctzzXOfZm4CJs2emZ9i489nR13BlRLYyDsicU%0An/u5i9QpZ8zNRugfsdwLxoU6ECTrIVPT/wcPQnF/dBqKk5JwPItPjhCK8zWr%0A5zhC8XQiFLeEUByubLDVddpK73CFytwllpqhUJy0fTGhGI11F4ovU6fYiwu5%0A0DDH3xkXeYablnLuagfvBl8WPUbtdvvcnD1OZ+RO+IiVXDAWtp1eF+4fPaZ6%0A2xKK+2Qiodi7P1+mzl3m/IFMl9xFLA43zUUoztesnuMIxdOJUNwSQnG44lC8%0A8LTF9gK0XrhNl+vEkcabiiIUowWdhWI3LCZ3YKc4ODZt42BDca7/lD9WTiju%0Aj+5CcVruUzALZ+qOMaE4X7N6jiMUTydCcUtmIRQnX3ZV8wurkuE9/UorN04S%0Aii9YOcJyrVTHyzJJEH5fHDBrhumm7Y6ravy4f1lbCMVoQVehOHuh4dw1bnQB%0AmX75ib9/WdUbN73InedQbNtiyte/qirGG2vanqq6YEzWW40LxrbbNkoRivtj%0AGkKxVHrOeK46peKLuCa171fPZ8RQHE+vcdvT8f39/TXWPEccb1bPcYTi6UQo%0Absn0hmI9rdNs/1z5p323DrGe4Z+32BNI4+B6hDr+ffp15nHnNAymoVjP732L%0AC/29FQ8ny5RMtywU+5dz4WmyTnLtTMYptv94u96z40fDL07CuVN6vZzltCnZ%0AFoHKzr9ZEYqHo5tQXLzwqndhkRvvc/q15/Nd/i8+GWdcXc7nietcLKYXxP7K%0ATkMewzu9eOxLHXt64II6vzxx+JbKB3A9/TM87QlOu35VXTBWh4j5a9soRSju%0Aj2kJxe4x6h++3r6fzLPs3Ogc/8Xz0yjHWO68kukXVxvnqxHPv6ZGPVeMco4M%0A16ye4wjF04lQ3JLpDMVp6JNAKY8vHy/1vGj44rT18E4ojIdf6HQLh0p3XlKB%0AUBwMqG6lyx+H1ehkGQrFuXnroJppd/L4dUn7L7DzyI8rddpKHYjjMH+iXYdO%0Af2dZCcVoUyeh2PcocnLBUnZXxb3ISf+/MP6yK/vaVOECcpxxs+ObL9FZVvfO%0Asr/SC049XffiUL691rQpO37xAtW/PFG5F3zZ6SfL63QLX/xWV+kFYbKdQ9t0%0Afts2ShGK+2N6QnFZoB1h3688jtzzTT7sjXqMpecS/7GXnd6456uRzr8jLYcd%0AJ+mX/j+qlkLxFJ/jCMXTiVDckmkMxWlA892VLT7+m3whlueOs39aaahcKEHb%0Ae6c6H4rLQryt/N3k0lCcriszfL5/5u51OBRL5dd1IeBKOHb6p+30hN1kvr51%0AP14Rioeji1Dsv8hIH6EOX3DmLmw8F09uGA1d7I0+ri19AZS9eNMXVlV/6a94%0AfDpZF96LKrnDEOqfLo/5ojLvhWTZRXnZhXT9cqef/maz/GxVPG35A0L1uPPR%0AtlGKUNwf0xSKQ/vxaPt+1cdLwv1HP8bS84rvuE2mN8b5ylRpe4rzHn05pOqd%0AI6vKnfcsneMIxdOJUNySaQzFScit823PI9yNTQOgGyrD4S8fiqvmNdLwZcHU%0Alhtug6HYG9Crls8J5Pl1TChGCyYeikvuCFdfJLgXVuELCf+FyDjjOuV77K8s%0AHJeF4uQuQ9ndgtAfC2osT+Xd93Qa4fmXV7K+vCV3kS5S5/rmPYG2jVKE4v6Y%0AplDsvZPYYN9Pzo2+81Joeo2OsZLjbr7PV7q859/G54p686yqWT3HEYqnE6G4%0AJdN9pzgUdNNKhi0J0IWw6obGkcYrCey+MFkSikPLni1foM9296/nktBra6Tl%0AaKkIxcMx6VBc7+IudJGQXkSUfiFXcvHmXgiNM26x8j+9EpxuskzF6ZWuC6f8%0AwzkXfIEL9GS8kgv4+IKvdJ2UVHoRGz9GaSu5i+Kf/iTaNkoRivtj2kNxo32/%0AJGCFptfsGAsHtWR6Y56vSo9nz/m32XJIVZ8j61SyzWfsHEconk6E4pZM/2eK%0A9bTMl0JVhD8dPOVRaF/F00kDYChsZssXikNB1xsyS0JxMJRmqjoU+9tfvR0I%0AxZhPkw3FVY8Buhcgvguv8AVbprwXkOOMGy4Tju10zXLlL4BKQnGyrFUXbN6g%0AXrU8zh0bvSzy+KCvov663DYkbfZUrq3ly5D9gpm0nWO0bZ6KUNwf0/34dNN9%0AP3TuTLtnzwNN5xM+r8zv+cpW4fw7zrmiZJ4DOMcRiqcTobgl0xmKdX1iZfFb%0AmXXAPf597nTdUFyj2gjF3mWMp5cLv8FQXHc9EYoxmyYaipOLpTrlC6XjBNtx%0Axq2q7AVQZrxkevlQnI5TeZdg7FBco9yLss9dpE5x74i4lWtr9YWy045kmDHa%0ANk9FKO6P6f6irTH2/fg84P7BsMb5pVYl8wmdV9Lpzc/5ylbh/DvOuaJkngM4%0AxxGKpxOhuCXzFYrL74BGVetu6Sein1qKTmhRpUHPCX96Gsnv/IYqme44oVhX%0A7gu1guGeUJwpQvFwTDIUJxcXNat48VXzwmqcizLvuHUqnX6m3VMQis3PnMS/%0Ajxmqwvj1qk5IKAsG89m2UYpQ3B9TE4qTY989xsfZ9+NjPv3DW3wnungOaTqf%0A0Hllvs9XtgrjtrG+KuZZUbN6jiMUTydCcUvaDcVO2Cr9rKxU3WAYlxO4ncBW%0AK1gXasxQnBl/ZdKuwrSCobhmu5PxCcWYLZMLxekFStVFVfECI6560xjnM23+%0AcetUehGUuXgKhuKy5cxW1fKELvhGuoBvWM0uGCfTtlGKUNwf0xGKnfNB7tgf%0AZ9+Px43OYfE5wP9US7P5hM8ryfTGPF+Nev5tvr6qz5F1qs78feumebvbKULx%0AdCIUt6TtUFznW5VNJcMVA2Owcndos91GmM7Yodi9O2zvYnsfAQ+H4mT8knan%0AwxCKMVsmFoqTOwA1HktOhs1fzKQXOeELM2eYzMXIOOPWqXTczEVfSSiut05C%0Ad2jS+QUv+EZZ5w2r8sLPe7dM1wTaNkoRivuj+1DsBmLP8TnOvh+PK+ew+P+h%0AtjSaT8l5pdb0qs9XI59/G6+vGufIGjWr5zhC8XQiFLek9VCcuaOb/wxwVIt1%0AkI0Pdt+d0rMuuLj4u7260pDoBrY0IEow9Y0nn08+/gJ3PuOH4sx8Q8OUhOJ8%0Au/OBWtZRPG2pyYfi8nXTpAjFwzGZUBy4ixqs0PDORZOu7EVXNF5yAVO4EBln%0A3OgOxkLz00v58aRKxnUumIoXZu7Fs0y72D+dbj5U17ngc5ZZX4h6fzZKfmLq%0ALXW2ib/CF4yX5b6ELNz++WrbKEUo7o9OQrF5DFY+p6rPE8k+H/r92nH2/fic%0AId+EHN1Vbff4T8eZr/OVVONz90jrq2xZ6tesnuMIxdOJUNyS9kOxLidYxVX4%0AJmgp+VZpz/hJYNP9jz/NljNuIei5IVv6O+Ml3TPhr41Q7LQzdFe1NBTrygXf%0AhXGbk0B7caCd8xiKc3/UiNokf9xwh2lWhOLhmEgoLg2G/koeo8tcaDgXOWek%0Af4WPviDFvRj1zWeccd32RJV+g2jazXT3XOylF5J6eDu/dB7OhZOUDt7FNvnu%0ANDjLU7ZOMxdt0u54+k73Wn+o8Fd6IVtWvvbrmue2jVKE4v6YSCiuqrLfLpca%0AY9/PnovyQSxXI8+n6rzSwvlKDx8PVxw3MN9G66vmObKiZvUcRyieToTilsxL%0AKJbyfXt0UhK2VgY/c+w+NpwpHYxP9dx5NiV3g5Mwl63oJ53c4dsJxXHoreof%0ADMW6MnfNk9Ih1Ewz1M75DMW6PH/UIBRjFJMIxf6AW1HeR9KyFzn5n0MypS86%0ATlnmu5s7zri65C/67k9o5Kt03HRZ4spepOngrC8M3f5xyUWU/+J6hAs+0/bi%0AtKXC069XpReMen3JXRD/o5K25rFtoxShuD86C8XyR7Iz5I5g4DyQr6b7fuhx%0A3VCNNJ8655Xxz1cjn3+lRl5fdZalumb1HEconk6E4pbMWyh2Kvst0P5hfJUZ%0Az9PfX844uvzDTGH52lwjVM9nJevR069JEYqHYyJ3ilsr/0VO+i2e7rD5Gmfc%0AXCXjROUdxlPJOJ5+cTWZbv1Kpz0/0x+num0bobg/5jMUz09Nat9vfz71p9fi%0A+Xeqz2NlNdl2E4qnE6G4JZMIxVTDSh6vbv9Lr7ooQvFw9CEU16txxqX6XoTi%0A/pi9UDyE4vw76SIUTydCcUsIxdNazmd7fY84z2ARioeDUExRhOI+IRRPY3H+%0AnXQRiqcTobglhOKuSofe0xb7PyMtn8d2vuyqi0en56MIxcNBKKYoQnGfEIqn%0AsTj/TroIxdOJUNwSQnFX5X7Lsy777dzZL4lo51ufp6UIxcNBKKYoQnGfEIqn%0AsTj/TroIxdOJUNwSQnF3tfh9F2d+aiot+XbuwG8uz3ARioeDUExRhOI+IRRP%0AY3H+nXQRiqcTobglhGJqUkUoHo7ZCsUUNT9FKO4PQjFFEYqnFaG4JYRialJF%0AKB4OQjFFEYr7hFBMUYTiaUUobgmhmJpUEYqHg1BMUYTiPiEUUxSheFoRiltC%0AKKYmVYTi4SAUUxShuE8IxRRFKJ5WhOKWXPrxn6oXLb7HG2Ioqs069Y0/Uh/+%0A6P12z0OfrfjkA+rkJWu8b6oUNZSSY0COBcy+8+fuVGf8jX87U9RQ6tkvXqOu%0Av2G9PSowLQjFLfnHLz+kTnr1Xd4QQ1Ft1ovOvlt9/vJf2D0Pffa1rz+iTnzN%0AWu+bKkUNpU589TXqn7/+qD0qMMve/d571Gnv4Q991LDrqOevUT/44TZ7VGBa%0AEIpbctsdm9XRJ37bG2Ioqs36g0W3qFu+s8nueeizu7+/VT37xGu8b6oUNZR6%0A9h9dY44FzL5PfupBtWgxf+ijhltvW3Ol+p0jr1Jbtuy3RwWmBaG4JQcPHlZP%0Ae+b16o2f2ewNMhTVRr3p81vU7z/jOnXgwGG756HPDh06rI56zjXq/C9xZ4Ua%0AZsm+v/Doa82xgNn305/uVEc+V4fiq/3bm6L6Xq/5+JXqBS/+lj0iME0IxS36%0A8CU/UYvO/r43zFBUG/XH5/xA/fWH1tk9DkNwycd+ok4652rvmytF9b1Oev1a%0AtfzjP7FHA/pg0ctuUmf9rX97U1Tf63kvv0pd9oUH7dGAaUIobtGuXQfVM//g%0ABvW6j/7SG2goapx6/fJfqf/x3G+pbdsP2D0OQ7B790H17OOvU6/5O/8bLEX1%0AtWSfl31fjgH0h3zcbOHxa9Vbvunf7hTV13rVJWvU0S/gab9pRShu2Z13bVVP%0AP/oGdd4nNniDDUU1qcV/v1EdecwN6rbvbrZ7Gobk+z/Q55VjrlHn/IP/jZai%0A+layr8s+z5fR9NMHLv6ROvYVa83nK33bn6L6Vud+/kr11KOvVj+8h3PatCIU%0Az4Nv3bjBBOMz/vfP1DtW7vSGHIqqU7L/nPnen6mnPed6de11fH3/kN307Q3q%0AaUevVS//qzXq7Xwej+ppyb4t+7gEYtnn0U/yGfG3vusudfQpV6sl/8R3JlD9%0Arld+JArE19zAbxNPM0LxPPnpgzvVS8+6XR11/I3qT966Tr3h4+vVWy7fpt65%0A0h9+KMqU3j9kPzn379arF79tnVr4hzepU15xq1r3kx12z8KQPfjzXeql/+/N%0A6sg/vEqdeuEadc5n1qg3fUO/6RKSqVktve/KPiz7suzTsm/LPi77Ovrt8GGl%0APvW5B9VTn7VWvfCCNer1l+lwzLmM6km9ZZUOwx++Uj3nT65Sxy66Qf3ox9wh%0AnnaE4nl2862b1fnv/KF65gk3qQVPu0Y9YcEaiiqt337qNeqZOgwvfscP1Y23%0AbDIXDoDrlts3qiV/frt61gvWqt8+0r8fUdSslOzDsi/LPi37NoZl06Z96qIP%0A36uee+L1asFTr1JPfd5V6unPX0NRs1knrFG/+6yr9L68Rr34VTepr37jIb49%0Af0YQigEAANC5nTsPmp9tkt+lpqhZLPkehF/+co/az5dpzRxCMQAAAABgsAjF%0AAAAAAIDBIhQDAAAAAAaLUAwAAAAAGCxCMQAAAABgsAjFAAAAAIDBIhQDAAAA%0AAAaLUAwAAAAAGCxCMQAAAABgsAjFAAAAAIDBIhQDAAAAAAaLUAwAAAAAGCxC%0AMQAAAABgsAjFAAAAAIDBIhQDAAAAAAaLUAwAAAAAGCxCMQAAAABgsAjFAAAA%0AAIDBIhQDAAAAAAaLUAwAAAAAGCxCMQAAAABgsAjFAAAAAIDBIhQDAAAAAAaL%0AUAwAAAAAGCxCMQAAAABgsAjFAAAAAIDBIhQDAAAAAAaLUAwAAAAAGCxCMQAA%0AAABgsAjFAAAAAIDBIhQDAAAAAAaLUAwAAAAAGCxCMQAAAABgsAjFAAAAAIDB%0AIhQDAAAAAAaLUAwAAAAAGCxCMQAAAABgsAjFAAAAAIDBIhQDAAAAAAaLUAwA%0AAAAAGCxCMQCgV/bt2aN27dyp9uzebbsAAACEEYoBAL1x+PBhtXPbNrVpw3q1%0AbcsW2xUAACCMUAwAAAAAGCxCMQAAAABgsAjFAICZcejgQXVw/37zr5DHpQ8e%0AOGBK/i8O6P579+xR+/ftM68BAADKEIoBADNj15bNattjj6ldmzebELx/7161%0A5fHH1WZd8n/ptmXTJrX+l79UmzZssGMBAACEEYoBADPj8KFD5i6x/BuT16ab%0AvVN8SPc7KN2cYQAAAEIIxQAAAACAwSIUAwBmhtwNlrvE8V1hIXeE3bvC0k9e%0Au8MAAACEEIoBADNDPlO8ff3jyWeKD+zbp7bq11t07bOfKd66ZYtaL58z3rTJ%0AjgUAABBGKAYAzIz9u3epPdu3q707d5rX8lniXfq1lHwDtdi9a5faId12RcMA%0AAACUIRQDAAAAAAaLUAwAAAAAGCxCMQBg4g4fPKj27tym9u3arg4dPGA+C7x/%0Azy61d/sW84h0yL6dO9WuLVuSx6flkekdW7eaOrB/v+m2S/eTzxXv2LHDvAYA%0AAChDKAYATNzBA/vVnm2b1J7tm9XB/ftMKJaQvGvLBrV3x7bgN0fv3qYD8IYN%0AJhgLCcJbN25QW3Tt3xdNZ5sOyBv1MBKMAQAAqhCKAQCdMD+tlPsppfzPLeX5%0AhuEnmQAAwDgIxQAAAACAwSIUAwDmnXxuWB6Tln+F3MU9eEC/PrBfXphu8TAH%0A7TA+8hli+W3i+OeXZDry2LQ8Rh3fGT6g++2TbnYYAACAMoRiAMC8k88Kb/3V%0AQ2r31o3m9YF9e8zrbesfNf+XR6J3bt6gNutuO/WwoUeft29YrzY+9JDa8vjj%0AZph9e/aoxx952NTePbtNt43r16tHHn5IPf74Y3YsAACAMEIxAGDeHT500Hy5%0AlvxrXuvwKq/jb54Whw5Gw8i/IWaY/fszd4rlLrF7p/igHma/dONOMQAAqIFQ%0ADAAAAAAYLEIxAKBVcsdW7ui6d3zl8Wh57X7bdH4Y8y3S0s0ZJs83jNwZlopJ%0AP3ldNh0AAIAYoRgA0KoDe/eonZvXq52bHo++SEvbvW2z2r7hMf1v9NvB8oVa%0A29Y/prZvfFwd2LfXBOldut8W3U3+jR+Fztu5dbPa8vhjasemTdGj0/v2qc36%0A9UZd+/ZG09mqh1mv57V5S/T5ZQAAgDKEYgBAq+Rzwnt3bFN7d25Th+0d3P27%0Ad6o927aqffpfIZ8J3q1f79mxXQfn6LO/+3bv0oF4q9prh/GRfjLMnp07zGuZ%0Azs5t29QOXfFniHfv2qm2bd+qdu2KhgEAAChDKAYAAAAADBahGAAAAAAwUEr9%0A/06OKbc36yTrAAAAAElFTkSuQmCC "支持北向接口.png")



### 北向网关选型

|                         | **方案标题**                    | **方案描述** | **优点** | **缺点** | **备注**             |
| ----------------------------------- | ------------------------------- | ------------ | ---------- | ------------ | ---------------------- |
| 备选1 | 复用现有websitegateway作为北向网关 | 复用现有websitegateway作为北向网关 | （1）采用微服务路由方式转发，后端微服务支持弹性扩缩容后无感<br>（2）不会增加代码仓和工程等引入的工作量 | （1）与Web页面网关混合在一起，不符合单一职责原则<br>（2）需要扩充支持北向接口转发| -- |
| 备选2 | 改造一个简易版northapi-gateway作为北向网关 | 基于当前websitegateway改造出来一个北向网关：northapi-gateway<br>只保留流量转发功能，删除web安全策略控制、单点登录等。 | （1）北向网关与web应用网关分开，更加清晰。<br>（2）采用微服务路由方式转发，后端微服务支持弹性扩缩容后无感 | （1）需增加一个代码仓，需额外增加ci构建配置 | -- |
| 备选3 | 利用nginx/kong等三方件作为北向网关 | 利用nginx/kong等三方件作为北向网关 | 北向网关与web应用网关分开，更加清晰。 | （1）只能支持IP+端口转发，后端微服务弹性扩缩容后要调整转发规则，实际上导致后端微服务弹性扩缩存在问题；如果要支持微服务路由转发，需要引入ServiceComb与ServiceCenter集成，工作量较大。<br>（2）需引入nginx/kong等三方件的配置和部署 | -- |

**经多次讨论与验证，结合考虑工作量大小，最终确定采用方案1，复用当前websitegateway作为北向网关。**

### 接口定义

需要在UserMgmt模块封装一个获取AccessToken的接口，接口定义如下:

| URL                                                          | Method | Request                                                      | Response                                                     | Response Status                                                    | 
| ------------------------------------------------------------ | ------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| /v1/accesstoken       | POST    | {<br>  "userFlag": "admin",//用户登录凭证（用户名/邮箱地址/手机号）<br>  "password": "xxx"//登录密码<br>} | {<br>  "userId": "39937079-99fe-4cd8-881f-04ca8c4fe09d",<br>  "accessToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hb.........."<br>} | 200：认证成功，返回accessToken<br>401：认证失败 |


## 支持与第三方IAM系统集成

### 需求背景

在一些合作场景下，需要EdgeGallery与第三方业务系统对接集成，EdgeGallery通常作为被集成方，第三方系统作为集成方。

第三方系统往往都有了自己一套IAM，在集成EdgeGallery后，不希望EdgeGallery仍然使用自己的帐号，而是能直接使用第三方系统的IAM帐号，达到统一一套帐号的诉求。

### 整体思路与约束

【思路】

1、通过UserMgmt屏蔽第三方IAM认证系统，让各业务平台不感知到第三方IAM的存在，避免引入业务平台的调整。

2、定义一套标准接口，由第三方IAM实现。

3、在部署阶段通过配置开关来控制是否与第三方IAM认证系统对接，并支持配置调用第三方IAM接口的endpoint等信息。

4、UserMgmt通过配置开关控制是走自身的用户管理流程 or 第三方对接流程。

【约束】

1、与第三方IAM系统对接场景下：

（1）只支持登录的第三方对接，以及退出登录、Web会话超时两个附场景的对接处理。

（2）不支持的场景：
- 不支持用户注册。（识别到是第三方对接场景，屏蔽对应入口。参考邮箱服务是否开启的做法，传参到界面做控制）
- 不支持第三方IAM用户首次登录后强制修改密码。（基于当前实现，当识别到是第三方IAM登录的用户后，不设置强制修改密码的标识）
- 不支持第三方IAM用户登录后在帐号中心修改个人信息、修改密码。（识别到是第三方对接场景且非EG内置的admin用户，屏蔽修改个人信息、修改密码入口）
- 不支持忘记密码后找回功能。（识别到是第三方对接场景，屏蔽对应入口）
- 不支持已登录第三方业务系统后跳转到EG界面自动登录。
- 不考虑集成使用第三方IAM的页面，如登录页面。

2、只支持同时对接一个第三方IAM系统。

3、不支持在已完成EdgeGallery部署后，切换是否对接第三方IAM的配置开关。


### 第三方IAM对接--登录、会话超时与退出登录


**一、需要第三方IAM系统实现的接口**

**1、用户登录**

（1）支持用户名和邮箱登录。如果不支持邮箱地址，直接认证失败即可。

（2）生成一个LoginToken来维护登录状态信息，并通过过期时间来控制该LoginToken的有效性。

（3）在登录成功后返回用户ID、用户名、邮箱地址（可选）、用户角色、LoginToken及其过期时间。其中用户角色按照EG定义的两种角色返回：ADMIN和TENANT。

**2、失效LoginToken**：清除LoginToken及对应的登录状态信息。

**3、延期LoginToken**：延长LoginToken的过期时间。


**二、EG侧对接处理**

**1、登录**：调用第三方IAM的登录接口，拿到LoginToken及过期时间，通过Session存放对应的第三方LoginToken及过期时间。

**2、会话超时**：拦截会话超时，获取该会话对应的LoginToken，通知第三方LoginToken失效。

**3、退出登录**：获取会话对应的LoginToken，通知第三方LoginToken失效。

**4、LoginToken延期**：在请求拦截器中判断当前Session对应的LoginToken是否快要过期，过期前1分钟请求第三方延期LoginToken。(业务平台的请求不经过UserMgmt，只经过WebsiteGateway，如果需要做延期处理则需要在WebsiteGateway层做)

**说明**：登录过程用到的图形验证码，生成与验证仍然在EdgeGallery侧，无需第三方系统支持图形验证码生成与验证。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/142241_b1e660e5_8279549.png "第三方IAM对接流程--登录.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/142256_754ed729_8279549.png "第三方IAM对接流程--延期LoginToken.png")

### 需要第三方IAM实现的接口定义

| 接口名称 | URL                                                          | Method | Request                                                      | Response                                                     | Response Status                                                    | 
| --------------- | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 登录 | /iam/users/login | POST | {<br>  "userFlag": "admin",//用户登录凭证（用户名/邮箱地址）<br>  "password": "xxx"//登录密码<br>} | {<br>  "userId": "",     //用户ID<br>  "userName": "",   //用户名<br>  "mailAddress": "",  //邮箱地址（可选）<br>  "userRole": "",  //用户角色（ADMIN：管理员 TENANT：租户）<br>  "loginToken": "E2MzU1Mzc5MzIsInNzb1Nlc3Npb25JZCI6IkE0RG"  //第三方IAM登录成功后返回的Token（可选）<br>  "expireTime": 1635734278147      //过期时间<br>} | 200：登录成功，返回用户信息<br>401：认证失败 |
| 延期LoginToken | iam/users/logintoken | PUT | {<br>  "loginToken": "E2MzU1Mzc5MzIsInNzb1Nlc3Npb25JZCI6IkE0RG"<br>} | {<br>  "expireTime": 1635734278147 //延期后新的过期时间<br>} | 200：成功 |
| 失效LoginToken | /iam/users/logintoken/invalidation | POST | {<br>  "loginToken": "E2MzU1Mzc5MzIsInNzb1Nlc3Npb25JZCI6IkE0RG"<br>} | {<br>} | 200：成功 |

### 编码实现的主要思路与定义

**1、通过简单工厂模式控制要采用内部认证实现 or 第三方IAM对接的实现**

UserMgmt后台针对登录的核心实现流程抽象成接口，
提供两套实现，一套为与第三方IAM对接的实现，一套为原有的自己的实现。
通过简单工厂模式控制使用哪套实现。


**2、对接第三方IAM的配置定义**

EXTERNAL_IAM_ENABLED：是否开启第三方外部IAM对接，默认false

EXTERNAL_IAM_ENDPOINT：第三方外部IAM对接的Endpoint，默认不配置

**3、UserMgmt监听会话销毁**

UserMgmt实现会话销毁的监听器，判断会话中如果有第三方LoginToken则调用第三方失效接口

**4、WebsiteGateway请求拦截器**

请求拦截中判断是第三方IAM对接场景，获取当前会话中的LoginToken及过期时间，判断快要过期则调用第三方接口进行LoginToken的延期处理。

### 实际落地方案的简化

考虑对接的复杂性，对上述方案进行简化：

不需要第三方IAM管理loginToken，也不存在有效期。

这样与loginToken及其有效性相关的接口与处理都裁剪。

最终只需要第三方IAM实现接口列表中的第一个接口即可，且响应中不需要loginToken和expireTime两个字段。

而EG侧只需要登录流程支持与第三方IAM对接即可。


## 支持通过proxy访问EdgeGallery

### 前端通过代理访问EG的转发配置样例

以ngxin为例说明代理的配置和方案。

由于EG的各大业务平台端口不同，如果只用一个代理进行转发，在配置转发规则时必然需要通过不同前缀来区分。如下配置中，usermgmt等作为代理到EG的地址前缀：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/142333_8deb6a54_8279549.png "代理配置样例.PNG")


如果使用多个代理，每个代理各自负责一个业务平台的转发，则可以不用前缀。

实际情况一般都会使用一个代理来完成转发，不会使用多个代理的方式。


### 通过代理访问存在的问题分析（一）: 页面登录重定向过程中redirect_uri不匹配的问题

【问题描述】当前经过几步骤的配置调整后，仍然存在redirect_uri参数不匹配的问题，见下面四个流程分析。

【解决方案】

以下面的Step2、Step3的调整为基础，解决redirect_uri参数不匹配的问题。

当前采取授权码方式授权，实现自定义授权模式，接管授权流程，去掉对redirect_uri的校验。

参考链接：https://blog.csdn.net/weixin_42271016/article/details/104212326

**重定向过程--不走前端代理正常访问**

EdgeGallery的IP：12.12.12.12   变量AUTH_SERVER_ADDRESS=https://12.12.12.12:30067

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/142357_c65f3f03_8279549.png "重定向过程--正常.png")

**通过代理访问的重定向过程--Step1 不做任何调整**

EdgeGallery的IP：12.12.12.12   前端代理地址：10.10.10.10    变量AUTH_SERVER_ADDRESS=https://12.12.12.12:30067

条件：EdgeGallery的IP无法通过外部访问，只能访问代理地址

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/142415_fd79d0f9_8279549.png "增加前端代理后访问的重定向过程--(Step1 不做任何调整).png")

**通过代理访问的重定向过程--Step2 AUTH_SERVER_ADDRESS调整为代理地址**

EdgeGallery的IP：12.12.12.12   前端代理地址：10.10.10.10    变量AUTH_SERVER_ADDRESS=https://10.10.10.10/usermgmt

条件：EdgeGallery的IP无法通过外部访问，只能访问代理地址

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/142406_b5072907_8279549.png "增加前端代理后访问的重定向过程--(Step2 AUTH_SERVER_ADDRESS调整为代理地址).png")

**通过代理访问的重定向过程--Step3 覆盖redirect_uri参数**

EdgeGallery的IP：12.12.12.12   前端代理地址：10.10.10.10    变量AUTH_SERVER_ADDRESS=https://10.10.10.10/usermgmt

条件：EdgeGallery的IP无法通过外部访问，只能访问代理地址

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/142423_ac474f4f_8279549.png "增加前端代理后访问的重定向过程--(Step3 覆盖redirect_uri参数).png")


### 通过代理访问存在的问题分析（二）: 前端调用后台（指前端所在website网关）接口的地址，需要增加代理前缀

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/153031_329815d0_8279549.png "问题二.png")


### 通过代理访问存在的问题分析（三）: 前端通过IFrame嵌入了其它业务平台的页面，地址不支持代理地址

【问题描述】前端代码中拼接了要嵌入页面的完整地址，拼接的地址不支持代理地址访问的场景

【解决方案】

前端调整拼接完整地址的实现方式，支持代理场景。

参考问题二来识别是否走了代理，以及确定代理前缀的方式。


涉及点：

1、老developer/appstore界面通过IFrame嵌入atp页面

2、融合界面（经典版）通过IFrame嵌入各业务平台的页面(edgegallery-fe\src\classic\pages\pageContainer.vue)


### 通过代理访问存在的问题分析（四）: 后台环境变量配置问题

【问题描述】

(1) 后台通过环境变量定义了要通过浏览器发起访问的地址，需要在部署时调整为代理地址。

(2) 第(1)中的此类环境变量对应的地址，同时又供后台调用，如果EG节点无法反向访问代理节点则导致后台无法调用。


【解决方案】对涉及的环境变量配置进行拆分，把浏览器访问的地址配置与后台访问的地址配置分开。在部署时把需要通过浏览器访问的地址设置为代理地址。

目前涉及如下两处：

(1)websitegateway中的AUTH_SERVER_ADDRESS变量拆分，各fe的部署文件相应调整配置

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/173548_fbf99f87_8279549.png "WebsiteGateway配置拆分.png")

(2)appstore-be如下配置对应的地址用来通过浏览器发起访问，且不涉及后台访问。只需要在部署时配置为代理地址：

ATP_REPORT_URL：获取ATP测试报告的地址，最终会由用户浏览器发起访问

### 通过代理访问存在的问题分析（五）: 用户管理模块前端调用后端的地址问题

与问题二一样，用户管理模块前端调用后端的地址需要支持代理地址
