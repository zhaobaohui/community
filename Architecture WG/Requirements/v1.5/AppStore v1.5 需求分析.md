# **AppStore** **v1.5** **需求分析** 

## 需求列表

| **需求描述**                        | **涉及模块**                    | **依赖模块** | **工作量** | **交付计划** | **责任人**             |
| ----------------------------------- | ------------------------------- | ------------ | ---------- | ------------ | ---------------------- |
| 应用上传做应用包完整性校验          | appstore-be  appstore-fe        |              | 0.5k       | 迭代一       | 罗小云                 |
| 接口规范优化                        | appstore-be  appstore-fe        |              | 0.5k       | 迭代一       | 罗小云、赵龙飞         |
| 应用在线体验流程优化                | appstore-be  appstore-fe        | lcm          | 1k         | 迭代一       | 程润东、陈凯           |
| APPD转换工具支持容器应用转换（PoC） | appdtranstool  appdtranstool-fe |              | 2k         | 迭代三       | 罗小云、陈凯           |
| 数据构造工具优化（PoC）             | datagen                         |              | 3k         | 迭代二       | 罗小云、李媛媛、张阿利 |

## **内部优化**

### **上传应用做完整性校验**

上传的应用包会解析manifest文件，进行完整性校验：

•    对source文件做hash校验，检查APPD文件、镜像文件等是否被改变；

•    对签名值进行校验，校验签名是否正确。
![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/173610_5d96b60f_8354563.png "mf.png")


### **接口规范优化**

AppStore存在部分混用部分权限不一致的接口，以及部分接口URI定义不规范：

•    查询应用包的接口“我的应用”和“应用管理”界面游客不可见，且支持传入多个查询条件进行过滤查询，增加“POST /mec/appstore/v2/packages/action/query”接口，前后台配套修改；

•    原有的接口“GET /mec/appstore/v2/packages”所有用户均可调用，获取应用仓库界面展示的应用包；

•    “POST /mec/appstore/v2/query/apps”接口URI定义不规范，修改为“POST /mec/appstore/v2/apps/action/query”接口，且排查使用场景是应用仓库调用，只需要展示应用最新版本的应用信息，故在响应中增加应用最新版本的packgeId和exprienceAble信息，减少前台调用，前后台配套修改。

### **应用在线体验流程优化**

•    查询应用包上传、分发、实例化状态时，解析响应中的status字段，解析当前具体的状态，并在客户端展示成功/失败；

•   释放资源失败，下次体验前需要先释放才能再次体验；

•   数据库catalog_package_table表增加字段experienceStatus字段，保存当前体验的状态；

•   新增接口供前台查询当前体验状态，刷新进展。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/173625_09906e11_8354563.png "在线体验流程.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/173641_4858c745_8354563.png "在线体验.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/173654_d3a5c333_8354563.png "在线体验UCD.png")

## **APPD转换工具支持容器应用转换（PoC）**

当前很多伙伴容器应用没有具体csar包，都是直接通过helm或者k8s原生方式部署的，这块待合作伙伴提供容器应用包后开始分析。

容器应用的镜像？



## **数据构造工具优化（PoC）**

•   应用镜像自动上传到环境；

•   为避免应用包格式差异，应用通过developer集成发布；

•   自动化获取边缘节点config文件，自动上传。

## 接口变更(Interface Change)

| URL                                                          | Method | Change Type | Request                                                      | Response                                                     |
| ------------------------------------------------------------ | ------ | ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| /mec/appstore/v1/experience/package/{packageId}/status       | GET    | Add         | packageId                                                    | {<br />     “data”: “experienceStatus”, <br />     “retCode”:  int,<br />     “message”: string, <br />     “params”:  []  <br />} |
| /mec/appstore/v2/packages/action/query                       | POST   | Add         | {<br />    “limit”: 10,<br />    “offset”: 0,<br />    “appName”: “”,<br />    “status”: “”, <br />   “sortItem”: “”, <br />   “sortType”: “desc” <br /> } | Page<PackageDto>                                             |
| /mec/appstore/v2/packages                                    | Get    | Modify      | 游客可以调用，资源变量传分页信息，支持按照应用上线时间区间查询。 | Page<PackageDto>                                             |
| /mec/appstore/v2/query/apps变更为：  /mec/appstore/v2/apps/action/query | POST   | Modify      | 不变                                                         | Page<AppDto>  AppDto中最新已发布应用包的packageId和exprienceAble信息，供首页、应用仓库、热门应用查询。 |




## 数据库变更(Database Change)

| Table   Name          | Field Name       | Field Type | Field Length | Default | Value        | Change Type | Compatibility |
| --------------------- | ---------------- | ---------- | ------------ | ------- | ------------ | ----------- | ------------- |
| Catalog_package_table | experienceStatus | varchar    | 100          | NULL    | 当前体验状态 | Add         | Compatible    |

