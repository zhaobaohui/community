# WASM运行态完整支持

当前WebAssembly支持最完善的语言为Rust与C/C++，开发人员多、发展快、支持组件多。
所以本需求对采用这两种语言的平台APP进行改造，能避免很多因为不支持导致的未知问题，
确保最大程度支持将TF模型与推理程序进行WebAssembly改造。

## 1. 改造TensorFlow应用

TensorFlow应用多为python语言实现，但是Second State组织提供一组将基于Rust语言的
TensorFlow推理程序编译成Wasm的工具链，能够尽可能的保持原有TF的速度，同时适用GPU等TF特性。

所以可以将原TF推理程序使用Rust进行改写（推理程序的改写比较简单，100行左右），再使用Wasm编译工具进行编译。
具体过程如下图所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/105757_f445bb2c_7634758.png "屏幕截图.png")

1. 首先使用rustwasmc编译工具，结合ssvm-extensions针对TF的扩展功能，对rust语言编写的TF推理程序进行编译
2. 对需要运行的服务器端的APP，编写js，定义对外提供服务的API，将此TF APP应用在服务器端
3. 对需要运行在浏览器端的APP，编写js胶水文件和html网页文件，将此TF APP运行在服务器端

**风险点：**
1. **某些APP可能使用了TF的比较高级或者新的功能，ssvm-extension还未实现并支持** 
2. 国内网络使用rustwasmc将TF模型和rust代码编译成wasm二进制会出现错误、超时、速度慢的结果，因为编译过程会去github下载一些文件

## 2. 补充EG能力中心的应用

针对当前EG能力中心不具备的能力，从TF官方模型库中选择相应有吸引力的应用场景的模型，进行改造，集成进能力中心。

## 3. 后续版本进一步规划

当前EG与webassembly集成还是基于Docker方式，使用k8s调度Docker容器应用。

等上游WasmEgde社区与k8s社区深度集成，k8s支持直接调度wasm二进制文件后，EG也可以进行改造，直接运行wasm二进制文件，提供TF能力。