# 怎么通过Gitee进行EdgeGallery开发工作

 **目录** 
- [1. Gitee贡献代码全流程](#1-Gitee贡献代码全流程)
    - [1.1 Fork项目代码](#11-Fork项目代码)
    - [1.2 编辑代码](#12-编辑代码)
        - [1.2.1 clone代码到本地](#121-clone代码到本地)
        - [1.2.2 修改代码](#122-修改代码)
        - [1.2.3 提交代码](#123-提交代码)
    - [1.3 提交PR](#13-提交PR)
        - [1.3.1 新建PR](#131-新建PR)
        - [1.3.2 填写PR详细说明信息](#132-填写PR详细说明信息)
    - [1.4 查看PR](#14-查看PR)
        - [1.4.1 查看PR提交的代码](#141-查看PR提交的代码)
        - [1.4.2 查看PR自动化检查结果](#142-查看PR自动化检查结果)
    - [1.5 更新PR](#15-更新PR)
        - [1.5.1 更新PR代码](#151-更新PR代码)
        - [1.5.2 更新PR描述信息](#152-更新PR描述信息)
- [2. Gitee评审代码全流程](#2-Gitee评审代码全流程)
- [3. Gitee合入代码全流程](#3-Gitee合入代码全流程)
- [4. 修改非master分支代码](#4-修改非master分支代码)
    - [4.1 修改稳定分支代码](#41-修改稳定分支代码)
        - [4.1.1 clone代码到本地](#411-clone代码到本地)
        - [4.1.2 修改代码](#412-修改代码)
        - [4.1.3 提交代码](#413-提交代码)
    - [4.2 提交PR到稳定分支](#42-提交PR到稳定分支)
    - [4.3 审查与合入PR](#43-审查与合入PR)
- [5. 关键点总结](#5-关键点总结)
- [6. 各项目负责人](#6-各项目负责人)
- [7. FAQ](#7-FAQ)

本指导文档是基于EdgeGallery社区开发人员，尤其是刚刚加入社区的新成员，在使用Gitee开发过程中遇到的问题或者不规范操作进行指导与说明。
本文档目的是帮助新成员快速上手，熟悉通过Gitee贡献代码的操作，并为开发过程提供规范指导。

同时也可以访问[Gitee帮助页面](https://gitee.com/help)查看更多Gitee的操作指导。

如发现错误之处或意见，可直接编辑更新此文档。如果有其他问题或困惑，欢迎在文档下方留言评论。

## 1. Gitee贡献代码全流程

EdgeGallery所有项目代码均在EdgeGallery企业里，可通过如下链接查看：
- [https://gitee.com/organizations/edgegallery/projects](https://gitee.com/organizations/edgegallery/projects)

当前EG所有项目分三种类型，公开仓库、私有仓库和企业内部公开仓库。
- 公开仓库对所有人可见，包括未登录用户
- 私有仓库仅被授权人可见
- 企业内部公开仓库仅对EdgeGallery企业内所有成员可见

以下所有操作说明均假设拥有该项目操作权限，若无权限，可联系对应项目仓库管理员进行权限配置，添加相应权限。

### 1.1 Fork项目代码

理论上所有项目的更改都应该通过Pull Request (PR)进行，便于代码自动化检查、review、修改、回滚等。
提交PR的前提就是先fork社区项目到自己的私人仓库，然后在私人仓库进行代码修改，最后以PR方式提交到社区项目里，
经过相关人员review通过之后，正式合入社区项目代码中。

通过点击项目页面右上角的“Fork”按钮进行fork操作，之前已fork过该项目的用户可以通过点击“Forked”按钮跳转到自己私人仓库。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0716/165433_5164d699_7634758.png"></div>

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0716/165448_3eaf73fc_7634758.png"></div>

对于此前已fork过该项目的用户，需要在编辑之前同步社区项目最新更改到自己fork的私人仓库，以使自己的私人仓库代码保持最新状态。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0716/165556_e6eba65c_7634758.png"></div>

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0716/165609_c701f8d2_7634758.png"></div>

 **注意：此项同步操作非常重要，能够在一定程度上避免因为未同步最新代码造成对他人已合入代码的覆盖，导致出错。** 

到目前为止，我们已经fork社区项目到自己的私人项目，并且已经同步到最新的代码了。可以进行后续代码开发工作。

### 1.2 编辑代码

以下代码编辑过程是以Linux操作为例，对windows操作比较熟悉的同学可以帮助在此处补充完整。
由于所有项目最终都是以容器方式运行在Linux服务器上，所以推荐使用Linux或windows上的专业代码编辑器进行编写。
开发过程应避免直接通过Gitee在网页上进行编写操作（文档编辑除外），因为这会导致代码运行过程中出现不可预期的错误，
并且这些错误在代码review时不容易被发现，且定位很难。

目前所遇到的由该操作引起的问题如下：
- Tab键输入，无法转化成空格，导致错误
-  **换行符Linux、Windows不兼容，导致错误** 

#### 1.2.1 clone代码到本地

此处以installer项目为例，其他项目操作过程完全一致。

从fork的私人项目页面获取下载链接，如下图所示：

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0716/165804_6559d89a_7634758.png"></div>

在Linux命令行输入复制的命令获取代码，其中`<gitee-username>`每个人不同，以拷贝的值为准

```
git clone https://gitee.com/<gitee-username>/installer.git
```

#### 1.2.2 修改代码

根据本次的目标修改代码，此处的原则是尽可能将大任务拆分成小的独立任务，单个PR提交完成一个小任务，每个PR的代码行数不应过多，
个人建议200行之内最佳，否则不利于代码review，问题定位等。

#### 1.2.3 提交代码

本地代码修改完成后，将代码上传至Gitee的私人项目。

```
git add .
git commit -m "a brief description about what this submit do"
git push
```

- 上述第一个命令是将本地未跟踪或已修改的文件添加到本地暂存区，该命令可多次执行，不断将本地的修改添加到暂存区
- 第二个命令是将暂存区里的改动提交到本地的版本库中，引号内为本次提交的一些说明，主要是改了什么，修复了什么问题，实现了什么功能等，第一个命令执行一次或多次后，可执行一次本命令，完成一次本地代码提交
- 第三个命令是将本地版本库中的代码推送到远端的Gitee的私人仓库中，第二个命令执行一次或多次后，可执行一次本命令，完成一次代码推送

至此本地的所有改动都已经提交到Gitee的私人项目，可以在网页上查看刚刚的提交记录与修改后的文件。

### 1.3 提交PR

关于本次独立任务的代码修改完成后，在Gitee的私人项目页面进行提交PR操作。

#### 1.3.1 新建PR

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0716/171613_0ee99faa_7634758.png"></div>

#### 1.3.2 填写PR详细说明信息

之后会跳转到该项目的社区仓库，需要填写PR的 **“标题”** 和 **“说明”** 。此处 **“标题”** 需要一句话简洁清晰的描述PR做的事情，
**“说明”** 可以详细列出为什么要做，怎么做的，达到什么效果等。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0719/093738_c4084b53_7634758.png"></div>

创建成功后会自动跳转到PR页面，可在页面中查看或修改此次提交的详细信息。

### 1.4 查看PR

PR提交后应重点查看本次修改的代码和PR自动化检查结果。

#### 1.4.1 查看PR提交的代码

每次提交PR之后， **提交者应该首先自行检查所提交的代码及其范围，确保所有的修改均正确** ，没有如下错误：
- 因为未同步社区仓库的最新代码，导致的对别人已合入代码的覆盖，具体体现为自己未修改的代码行在PR中显示为修改了
- 因为误操作等原因造成的，对不需要修改的代码进行的修改，具体体现为误删除、误添加等
- 因为格式问题造成的输入错误，具体体现为tab键造成的不对齐、错误使用中文符号等

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0719/095053_425d6851_7634758.png"></div>

#### 1.4.2 查看PR自动化检查结果

大部分项目都配置了PR提交后的自动化检查，该检查作为门禁检查，是PR合入的必要条件。因为installer项目没有PR检查任务，此处以MEP项目为例进行说明。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0719/095329_27326d96_7634758.png"></div>

如上图所示，mep项目提交或更新PR后，会自动触发jenkins的检查任务，当前为两个任务，分别是三方件检查和代码基础检查。
只有所有任务显示成功，该PR才具备最基础的准许合入条件。

 **注意：PR自动化检查未通过的PR禁止合入。** 

### 1.5 更新PR

PR提交后支持更新操作，直到该PR合入之前，都可以不断地进行更新。

PR的更新分两部分，一部分是代码的更新，另一部分是PR描述信息的更新。

#### 1.5.1 更新PR代码

PR代码的更新与[1.2.2小节](#122-修改代码)一致，直接在上次修改的基础上进行更新，无需重新拉取代码。
修改完成后直接按照[1.2.3小节](#123-提交代码)操作指导推送本地代码到社区仓库即可完成PR更新。
之后可直接在PR页面刷新查看更新情况，无需额外的创建或更新PR的操作。

下图是PR代码更新之后，刷新页面查看到的更新信息。可以看出提交次数增加，以及新增一个修改文件。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0805/192125_43be3222_7634758.png"></div>

#### 1.5.2 更新PR描述信息

除了代码之外，PR的描述信息也可以更新，如“标题”，“说明”等文字信息，以及审核人等。

 **注意：应该将与此PR相关的人员加入审查人员中，已达到通知相关人员审查此PR，并在其他关联项目做出对应修改的目的。** 

可以查看[第六节各项目负责人](#6-各项目负责人)，添加相关项目负责人。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0805/192442_d1f75975_7634758.png"></div>

## 2. Gitee评审代码全流程

用户可以通过[EdgeGallery个人工作台](https://e.gitee.com/OSDT/dashboard)查看指派给自己评审的PR。建议按照以下原则进行PR评审：

- 每天习惯性查看指派给自己评审的PR
- 确保查看PR中所有的修改，帮助提交者发现问题、提出意见、或给出更好的解决方案等
- 确保所有的门禁检查都成功
- 确认PR中与其他关联项目的关系，是否需要一起改动

用户可以将鼠标移动到对应代码处，通过点击出现在代码行边的加号进行评论。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0805/193442_87593f42_7634758.png"></div>

## 3. Gitee合入代码全流程

评审通过后，有合入权限的用户会出现合并按钮，可以通过页面操作进行PR的合入。

合并方式需要选择 **“扁平化分支”** 进行合并，此方式会将本次PR的所有提交记录合并成一次提交，记入仓库的git log中，
避免一个PR中存在的多次提交记录全部并入仓库的git log中，造成log的混乱。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0805/193908_7554447d_7634758.png"></div>

至此，一次完整的PR新建、提交、修改、评审、合入已全部结束。

## 4. 修改非master分支代码

上述所有介绍均是假设对master分支进行修改，也是日常开发工作中最常见、最基本的场景。

有些情况下开发者也需要对某个已发布的稳定分支进行修改，比如将一个bug修复相关的代码合入Release v1.2分支。
此处对这种场景进行说明。

### 4.1 修改稳定分支代码

#### 4.1.1 clone代码到本地

此处以installer项目为例，其他项目操作过程完全一致。

从fork的私人项目页面获取下载链接，如下图所示：

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0716/165804_6559d89a_7634758.png"></div>

在Linux命令行输入复制的命令获取代码，其中`<gitee-username>`每个人不同，以拷贝的值为准

```
# 下载个人仓库代码到本地
git clone https://gitee.com/<gitee-username>/installer.git

# 查看仓库所有分支
git branch --all

# 切换到想要修改的分支
git checkout remotes/origin/Release-v1.2

# 给本地1.2分支起名，名字可以任意，此处以local-v1.2为例
git checkout -b local-v1.2
```

#### 4.1.2 修改代码

此处修改代码操作，与之前描述一致。

#### 4.1.3 提交代码

本地代码修改完成后，将代码上传至Gitee的私人项目。

其中`git push`命令与之前不同

- origin表示将分支推送到原先的仓库
- local-v1.2:Release-v1.2表示将本地的名为local-v1.2的分支推送到gitee私人项目仓库，作为新的Release-v1.2分支

```
git add .
git commit -m "a brief description about what this submit do"
git push origin local-v1.2:Release-v1.2
```

此时可以通过刷新Gitee私人项目页面查看上传的代码。同样该稳定分支的修改需要切换到相应分支进行查看，master分支无改动。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0805/201045_ad4111b3_7634758.png"></div>

### 4.2 提交PR到稳定分支

提交PR到社区项目仓库的对应分支的操作与master类似，唯一的区别是创建PR时需要切换分支。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0805/201151_0d068e7b_7634758.png"></div>

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0805/201308_6f588db2_7634758.png"></div>

### 4.3 审查与合入PR

稳定分支PR的审查与合入与之前master分支的过程完全一致。

## 5. 关键点总结

- Fork社区项目代码仓库到私人项目后，一定要同步社区项目代码，否则极易覆盖他人已合入代码，造成未知错误
- 避免直接通过网页修改代码（文档编译可直接网页编辑），特别是.sh脚本文件，极易因为Linux、Windows不兼容，导致错误
- PR规模不应太大，200行之内便于review，涉及文件不应太多，否则容易产生冲突
- 开源社区开发者需要对自己的代码负责，提交PR后先自行review自己的代码，确保所有修改均正确、并且范围准确无误，门禁检查通过
- 开发者需要将项目负责人，其他与PR修改内容有关联的人员加入审查列表，共同评审
- 评审人员需要核对代码修改范围、有无错误、门禁检查结果等，最好能对代码规范、解决方法等进行审查，提出必要的意见
- 由于Gitee不支持PR提交后的Rebase操作，所以PR合入后也有可能因为存在冲突而覆盖之前已合入的代码，这在当前多人合作情况下仍无法避免
- 避免冲突的有效办法是尽量避免多人同时修改相同文件，同一仓库不同文件的修改不会造成冲突

## 6. 各项目负责人

| Project Name        | Contacts             | Email                           |
|---------------------|----------------------|---------------------------------|
| api-emulator        | zhanghailong         | zhanghailong22@huawei.com       |
| appstore-be         | sunjinghan           | hw.luoxiaoyun@huawei.com        |
| appstore-fe         | sunjinghan           | hw.luoxiaoyun@huawei.com        |
| atp                 | liuhuiling           | liuhuiling4@huawei.com          |
| atp-fe              | liuhuiling           | liuhuiling4@huawei.com          |
| ci-management       | xudan                | xudan16@huawei.com              |
| crane-framework     | Kanag                | kanagaraj.manickam@huawei.com   |
| developer-be        | zhanghailong         | zhanghailong22@huawei.com       |
| developer-fe        | zhanghailong         | zhanghailong22@huawei.com       |
| docs                | Kanag                | kanagaraj.manickam@huawei.com   |
| edgeT               | Kanag                | kanagaraj.manickam@huawei.com   |
| eg-view             | Zhou Wenjing         | zhouwenjing@huawei.com          |
| example-apps        | Chen Chuanyu         | chenchuanyu@huawei.com          |
| file-system         | Gao Guozhen          | gaoguozhen@huawei.com           |
| helm-chart          | lizhiqian            | lizhiqian1@huawei.com           |
| installer           | xudan                | xudan16@huawei.com              |
| mecm-apm            | Vidyashree Rama      | Vidyashree.Rama@huawei.com      |
| mecm-applcm         | Rama Subba Reddy S   | Rama.Subba.Reddy.S@huawei.com   |
| mecm-appo           | Shashikanth V H      | shashikanth.vh@huawei.com       |
| mecm-apprulemgr     | Gaurav               | gaurav.agrawal@huawei.com       |
| mecm-fe             | yangyang             | yangyang263@huawei.com          |
| mecm-inventory      | Vidyashree Rama      | Vidyashree.Rama@huawei.com      |
| mecm-mepm-fe        | Zhou Wenjing         | zhouwenjing@huawei.com          |
| mep                 | luxin                | luxin7@huawei.com               |
| mep-agent           | luxin                | luxin7@huawei.com               |
| plugins             | swarup               | swarup.nayak1@huawei.com        |
| toolchain           | helongfei            | helongfei6@huawei.com           |
| user-mgmt           | zhangbeiyuan         | zhangbeiyuan@huawei.com         |
| user-mgmt-fe        | zhangbeiyuan         | zhangbeiyuan@huawei.com         |
| website-gateway     | zhangbeiyuan         | zhangbeiyuan@huawei.com         |

## 7. FAQ