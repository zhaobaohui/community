/*
 * Copyright 2020 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#!/bin/bash

git clone https://gitee.com/edgegallery/appstore-be.git --branch master

cd appstore-be

for filename in `git ls-tree -r --name-only HEAD`
do
    addYear=$(git log --pretty=format:"%ad %H %an" -- $filename | tail -1 | awk '{print $5}')
    modYear=$(git log -1 --pretty=format:"%ad %H %an" -- $filename | awk '{print $5}')
    if [ $modYear -gt $addYear ]; then
        sed -i "s/Copyright \([0-9]\{4\}\(-[0-9]\{4\}\)*\)/Copyright $addYear-$modYear/" $filename
    elif [ $modYear -eq $addYear ]; then
        sed -i "s/Copyright \([0-9]\{4\}\(-[0-9]\{4\}\)*\)/Copyright $addYear/" $filename
    fi
done
