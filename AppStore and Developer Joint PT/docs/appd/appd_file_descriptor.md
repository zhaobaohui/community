## TOSCA.meta 文件定义

#### 样例：  
```
TOSCA-Meta-File-Version: 1.0
CSAR-Version: 3.25
Created-by: EdgeGallery
Entry-Definitions: APPD/loactionAppTest.zip

Name: Image/ubuntu_test.zip
Content-Type: image

Name: APPD/loactionAppTest.zip
Content-Type: appd

Name: Image/SwImageDesc.json
Content-Type: SwImageDesc
```

#### 格式说明
- 不支持注释符号
- 起始没有空行
- 所有行数据格式固定： {key}: {value}，中间以": "分割
- TOSCA-Meta-File-Version/CSAR-Version/Created-by/Entry-Definitions 为第一部分内容，中间无空行，顺序可变，并且只解析定义的key值内容
- Name/Content-Type 为第二部分内容，Name/Content-Type中间不能有空行，不同的Name/Content-Type之间以空行分割，**至少存在一个空行（默认一个）**
- 第一部分内容 和 第二部分内容以空行分割，**至少存在一个空行（默认一个）**
- 以上所有定义的value都是String类型，特殊说明的字段以自动备注为主。

## MF文件定义

#### 样例
```
metadata:
app_product_name: loactionAppTest
app_provider_id: HUAWEI
app_package_version: v1.2
app_release_data_time: 2021-06-24 20:21
app_type: edgegallery_vm_package
app_class: vm
app_package_description: dadad

Source: APPD/loactionAppTest.zip
Algorithm: SHA-256
Hash: 556a62edb8a15a457152c6c9d02607c28ebc69d9bdcab9e9f8c411eac75b3924

Source: Image/SwImageDesc.json
Algorithm: SHA-256
Hash: 31c7e3b82418f736fb2ff34057dbdf8a2de9136bf9e60a178399346fb1dd7f5d
```

#### 格式说明
- 不支持注释符号
- 起始没有空行
- 所有行数据格式固定： {key}: {value}，中间以": "分割
- 第一部分内容是对metadata的描述，起始行必须是“metadata:”
- app_product_name/app_provider_id/app_package_version/app_release_data_time/app_type/app_class/app_package_description 是对metadata的属性定义，之解析已定义的内容，中间无空行分割
- 第二部分是对source文件的定义，支持多个文件定义，中间已空行分割，定义属性有：Source/Algorithm/Hash
- 以上所有定义的value都是String类型，特殊说明的字段以自动备注为主。