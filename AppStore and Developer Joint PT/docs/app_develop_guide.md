### 01 进入应用孵化界面
![输入图片说明](%20app_develop_guide_image/01.png)

### 02 新建一个名称为位置服务的应用
![输入图片说明](%20app_develop_guide_image/02%20%E6%96%B0%E5%BB%BA%E5%BA%94%E7%94%A8.png)

### 03 在对话框界面输入必要的应用信息，其中负载类型选择“虚拟机”
![输入图片说明](%20app_develop_guide_image/03.png)

### 04 进入能力中心选择应用需要依赖的能力
![输入图片说明](%20app_develop_guide_image/04.1.png)
![输入图片说明](%20app_develop_guide_image/04.2.png)

### 05 平台提供了沙箱环境供应用调测，我们为应用选择沙箱并进入沙箱结构展示界面
![输入图片说明](%20app_develop_guide_image/05.1.png)
![输入图片说明](%20app_develop_guide_image/05.2.png)
![输入图片说明](%20app_develop_guide_image/05.3.png)

### 06 配置网络类型和虚拟机规格，选择镜像，配置边缘UPF的应用规则（包括流量规则和DNS规则），配置完成后点击启动按钮即可成功拉起应用
![输入图片说明](%20app_develop_guide_image/06.1.png)
![输入图片说明](%20app_develop_guide_image/06.2.png)
![输入图片说明](%20app_develop_guide_image/06.3.png)
### 07 点击登录按钮，进行VNC远程登录，上传应用代码包
![输入图片说明](%20app_develop_guide_image/07.1.png)
![输入图片说明](%20app_develop_guide_image/07.2.png)

### 08 进入应用的镜像界面，完成应用镜像的打包，同时支持对镜像包进行二次编辑
![输入图片说明](%20app_develop_guide_image/08.1.png)
![输入图片说明](%20app_develop_guide_image/08.2.png)
![输入图片说明](%20app_develop_guide_image/08.3.png)
### 09 进入应用的测试界面，对应用进行测试认证，测试完成后可同步查看测试报告
![输入图片说明](%20app_develop_guide_image/09.1.png)
![输入图片说明](%20app_develop_guide_image/09.2.png)
### 10 应用测试结果通过之后即可把应用发布到应用商店，可以进入应用商店查看发布成功的应用
![输入图片说明](%20app_develop_guide_image/10.1.png)
### 11 平台还支持对应用进行分发和部署。进入MEAO，点击分发，选择对应节点进行分发操作
![输入图片说明](%20app_develop_guide_image/11.1.png)
![输入图片说明](%20app_develop_guide_image/11.2.png)
### 12 点击部署，填写部署配置信息，确认后完成应用部署
![输入图片说明](%20app_develop_guide_image/12.1.png)
![输入图片说明](%20app_develop_guide_image/12.2.png)
![输入图片说明](%20app_develop_guide_image/12.3.png)