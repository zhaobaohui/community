
## 通过IDEA添加Copyright

- 1 创建一个Copyright模板
    - 打开Setting配置，定位到Copyright，创建模板，复制以下内容到“CopyRight text”
```
Copyright $today.year Huawei Technologies Co., Ltd.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied. See the License for the specific language governing permissions and limitations under
the License.
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/144839_9296db7b_5659718.png "屏幕截图.png")

- 应用新建的模板
![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/144953_3ffbc11b_5659718.png "屏幕截图.png")

- 选择代码进行刷新（注意：不要选择“Update existing copyright”）
![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/145259_1c88d6c9_5659718.png "屏幕截图.png")