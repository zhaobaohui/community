## 项目目标
* 拉通各开发项目组，对需求进行详细方案设计，确保规划需求可以有效的落地
* 通过定期的技术方案讨论 和 网络安全研讨，打造安全可靠的EdgeGallery平台

## 项目范围
* 拉通Developer、App Store、User-mgmt的需求设计，保障版本的落地开发
* 遗留问题跟踪和闭环
* 提供技术方案求助通道，解决开发过程中遇到的问题

## 项目成员
| **Name**  | **Affiliation** |  
|-----------|-----------------|                          
| [张倍源](https://gitee.com/zhang_bi_yuan)      | 华为（主席）           | 
| [张海龙]()   | 华为             |  
| 刘慧玲    | 华为             | 
| 李治谦    | 华为             | 
| 杨阳    | 华为             |
| [贺龙飞](https://gitee.com/helongfei_it)    | 华为             | 
| 白针针    | 华为             | 
| 邹玲莉    | 华为             | 
| 陈辉    | 九州云             | 
| 孙友伟    | 九州云             |
| [罗小云](https://gitee.com/lxyhiuwan) | 华为  |
| [鹿鑫](https://gitee.com/deernewman) | 华为  |

## PTL
| **Component**  | **PTL** |   **Contributors** |
|-----------|-----------------|----------------------|
| Developer  | 张海龙  |   陈传雨，周艳兵，张海龙，孙友伟，陈辉，贺龙飞，邹玲莉  |
| AppStore  | 罗小云  |    陈传雨，张倍源，罗小云，李治谦，白针针，赵龙飞，程润东，Janani，Khemendra |
| MECM  | 李治谦  |  Seshu Kumar M，张超，邹玲莉，周艳兵，崔敬潮，李治谦、陈传雨，杨阳，Gaurav Agrawal，Shashikanth|
| User-mgmt  | 周艳兵  |  周艳兵，张倍源  |
| ATP  | 刘慧玲  |   陈传雨，刘慧玲，白针针，杨阳  |
| MEP  | 鹿鑫  |   Libu Jacob Varghese, Vineesh V, Swarup Nayak, Bhanu Soni |


## Committer
Repo    |     Status    |     committer1    |     committer2    |     committer3 and Admin（PTL or Owner）
--------|-------|--------|-------|--------|
[appstore-be](https://gitee.com/edgegallery/appstore-be)    |     public    |     zhangbeiyuan    |     chenchuanyu    |     luoxiaoyun
[appstore-fe](https://gitee.com/edgegallery/appstore-fe)    |     public    |     zhangbeiyuan    |     zhouyanbing    |     luoxiaoyun
[atp](https://gitee.com/edgegallery/atp)    |     public    |     chenchuanyu    |     zhangbeiyuan    |     liuhuiling
[atp-fe](https://gitee.com/edgegallery/atp-fe)    |     public    |     yangyang    |     zhouyanbing    |     liuhuiling
[installer](https://gitee.com/edgegallery/installer)    |     public    |     xudan    |     kanag    |     xudan
[helm-charts](https://gitee.com/edgegallery/helm-charts)    |     public    |     lizhiqian    |     kanag    |     lizhiqian
[developer-be](https://gitee.com/edgegallery/developer-be)    |     public    |     chenchuanyu    |     zhouyanbing    |     zhanghailong
[developer-fe](https://gitee.com/edgegallery/developer-fe)    |     public    |     zhouyanbing    |     yangyang    |     zhanghailong
[edgeT](https://gitee.com/edgegallery/edgeT)    |     public    |     Kanagaraj Manickam |         |     Kanagaraj Manickam
[file-system](https://gitee.com/edgegallery/file-system)    |     public    |     zhangbeiyuan    |     zhangchao    |     gaoguozhen
[mecm-apm](https://gitee.com/edgegallery/mecm-apm)    |     public    |     Seshu Kumar M |     chenchuanyu    |     lizhiqian
[mecm-applcm](https://gitee.com/edgegallery/mecm-applcm)    |     public    |     zhangchao、cuijingchao    |     Seshu Kumar M |     lizhiqian
[mecm-appo](https://gitee.com/edgegallery/mecm-appo)    |     public    |    Seshu Kumar M |     chenchuanyu    |     lizhiqian
[mecm-apprulemgr](https://gitee.com/edgegallery/mecm-apprulemgr)    |     public    |     zhangchao    |     Seshu Kumar M    |     lizhiqian
[mecm-fe](https://gitee.com/edgegallery/mecm-fe)    |     public    |     zoulingli    |     zhouyanbing    |     yangyang
[mecm-mepm-fe](https://gitee.com/edgegallery/mecm-mepm-fe)    |     public    |     zoulingli    |     zhouyanbing    |     yangyang
[mecm-inventory](https://gitee.com/edgegallery/mecm-inventory)    |     public    |     Seshu Kumar M |     chenchuanyu    |     lizhiqian
[mep](https://gitee.com/edgegallery/mep)    |     public    |     zhangchao    |     chenchuanyu    |     luxin
[mep-agent](https://gitee.com/edgegallery/mep-agent)    |     public    |     zhangchao    |     chenchuanyu    |     luxin
[user-mgmt](https://gitee.com/edgegallery/user-mgmt)    |     public    |     zhangbeiyuan    |         |     zhouyanbing
[user-mgmt-fe](https://gitee.com/edgegallery/user-mgmt-fe)    |     public    |     zhangbeiyuan    |         |     zhouyanbing
[website-gateway](https://gitee.com/edgegallery/website-gateway)    |     public    |     zhouyanbing    |         |     zhangbeiyuan

代码检视要求请参考[Code Review Practice.md](https://gitee.com/edgegallery/community/blob/master/Best%20Practices/Code%20Review%20Practice.md)

## 项目会议
* 每周一 16:30-17:30
* 会议链接：每周一早上通过main@edgegallery.groups.io邮件发送
* 历史会议纪要：[历史会议纪要](https://gitee.com/edgegallery/community/tree/master/AppStore%20and%20Developer%20Joint%20PT/Meetings)

## 会议纪律
* 1 会前需要做充足的准备，输出方案的详细设计，线下已经沟通过，线上更加关注方案的可行性和实现落地；
* 2 会议时间需要严格守时，开一个高效的会议

## 项目联系方式
* 邮件列表：使用Edge Gallery邮件列表： main@edgegallery.groups.io。
* 其他： 请在邮件标题添加【#app-dev】用以区分
** 