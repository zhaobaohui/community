|Requirements description | responsible person | priority | workload estimation (k) | design link|
|---|---|---|---| -----|
|[OS Plugin]针对iso镜像加载虚机使用卷方式. |   崔敬潮 | High |  0.5  |  100% |
[mecm-fe] I4QI76【mepm】登录背景顶部出现空白，需自适应屏幕大小 |  赵龙飞 | High  |  100% |
[mecm-fe] I4PVOT【mepm-fe】mepm bug+功能优化-界面中英文资源修改 |  赵龙飞  | High  |  100% | 
[mecm-fe] I4LTV1【mepm】应用详情按钮功能调整+分流规则代码优化 |  赵龙飞 | High   |  100% |
[mecm-fe] I4LTV1【mepm】分流规则代码优化 |  赵龙飞 | High  |  100% |



