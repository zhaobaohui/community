
### Create server on mechost

Create servers on mechost
```
Resource URI: /rescontroller/v1/tenants/:tenantId/hosts/:hostIp/servers
Method: Post
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|access_token |access token|header |yes|Jwt Token|

Example request:
```
{
    "server" : {
        "name": "vmTest",
        "flavor": "0e12087a-7c87-476a-8f84-7398e991cecc",
        "image" : "cec3aab9-5991-4893-befe-4775ddf79de6",
        "imageRef" : "70a599e0-31e7-49b7-b260-868f441e862b",
        "availabilityZone": "us-west",
        "user_data" : "IyEvYmluL2Jhc2gKL2Jpbi9zdQplY2hvICJJIGFtIGluIHlvdSEiCg==",
        "configDrive": "true",
        "securityGroups": [
            {
                "name": "default"
            }
        ],
		"netWorks": [
            {
                "network": "0884b5fb-ea5c-4951-9c54-256f4fc38991",
				"fixedIp": "192.168.xx.19"
            },
			{
                "network": "241aee72-de67-4c95-bdaa-e63f12fbd183",
				"fixedIp": "192.168.xx.19"
            },
			{
                "network": "446aa035-f737-45db-9834-83c7a5f94046",
				"fixedIp": "192.168.xxx.19"
            }
        ]
    }
}
```

Example response:
```
200 OK
{
    "data": null,
    "retCode": 0,
    "message": "Create server success",
    "params": null
}
```


### Query serverson mechost

Query servers on mechost

```
Resource URI: /rescontroller/v1/tenants/:tenantId/hosts/:hostIp/servers
Method: Get
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|access_token |access token|header |yes|Jwt Token|

Example response:
```
200 OK
{
    "data": {
	"servers": [
        {
            "OS-DCF:diskConfig": "AUTO",
            "OS-EXT-AZ:availability_zone": "nova",
            "OS-EXT-SRV-ATTR:host": "compute",
            "OS-EXT-SRV-ATTR:hostname": "new-server-test",
            "OS-EXT-SRV-ATTR:hypervisor_hostname": "fake-mini",
            "OS-EXT-SRV-ATTR:instance_name": "instance-00000001",
            "OS-EXT-SRV-ATTR:kernel_id": "",
            "OS-EXT-SRV-ATTR:launch_index": 0,
            "OS-EXT-SRV-ATTR:ramdisk_id": "",
            "OS-EXT-SRV-ATTR:reservation_id": "r-l0i0clt2",
            "OS-EXT-SRV-ATTR:root_device_name": "/dev/sda",
            "OS-EXT-SRV-ATTR:user_data": "IyEvYmluL2Jhc2gKL2Jpbi9zdQplY2hvICJJIGFtIGluIHlvdSEiCg==",
            "OS-EXT-STS:power_state": 1,
            "OS-EXT-STS:task_state": null,
            "OS-EXT-STS:vm_state": "active",
            "OS-SRV-USG:launched_at": "2019-04-23T15:19:15.317839",
            "OS-SRV-USG:terminated_at": null,
            "accessIPv4": "1.2.3.4",
            "accessIPv6": "80fe::",
            "addresses": {
                "private": [
                    {
                        "OS-EXT-IPS-MAC:mac_addr": "00:0c:29:0d:11:74",
                        "OS-EXT-IPS:type": "fixed",
                        "addr": "192.168.1.30",
                        "version": 4
                    }
                ]
            },
            "config_drive": "",
            "created": "2019-04-23T15:19:14Z",
            "description": null,
            "flavor": {
                "disk": 1,
                "ephemeral": 0,
                "extra_specs": {},
                "original_name": "m1.tiny",
                "ram": 512,
                "swap": 0,
                "vcpus": 1
            },
            "hostId": "2091634baaccdc4c5a1d57069c833e402921df696b7f970791b12ec6",
            "host_status": "UP",
            "id": "2ce4c5b3-2866-4972-93ce-77a2ea46a7f9",
            "image": {
                "id": "70a599e0-31e7-49b7-b260-868f441e862b",
            },
            "key_name": null,
            
            "locked": true,
            "locked_reason": "I don't want to work",
            "metadata": {
                "My Server Name": "Apache1"
            },
            "name": "new-server-test",
            "os-extended-volumes:volumes_attached": [],
            "progress": 0,
            "security_groups": [
                {
                    "name": "default"
                }
            ],
            "status": "ACTIVE",
            "tags": [],
            "tenant_id": "6f70656e737461636b20342065766572",
            "trusted_image_certificates": null,
            "updated": "2019-04-23T15:19:15Z",
            "user_id": "admin"
        }
    ]},
    "retCode": 0,
    "message": "Query servers success",
    "params": null
}
```
### Query vm on mechost by vmId

Query vm on mechost by vmId
```
Resource URI: /rescontroller/v1/tenants/:tenantId/hosts/:hostIp/servers/:serverId
Method: Get
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|access_token |access token|header |yes|Jwt Token|

Example response:
```
200 OK
{
    "data": {"server": {
        "OS-DCF:diskConfig": "AUTO",
        "OS-EXT-AZ:availability_zone": "nova",
        "OS-EXT-SRV-ATTR:host": "compute",
        "OS-EXT-SRV-ATTR:hostname": "new-server-test",
        "OS-EXT-SRV-ATTR:hypervisor_hostname": "fake-mini",
        "OS-EXT-SRV-ATTR:instance_name": "instance-00000001",
        "OS-EXT-SRV-ATTR:kernel_id": "",
        "OS-EXT-SRV-ATTR:launch_index": 0,
        "OS-EXT-SRV-ATTR:ramdisk_id": "",
        "OS-EXT-SRV-ATTR:reservation_id": "r-t61j9da6",
        "OS-EXT-SRV-ATTR:root_device_name": "/dev/sda",
        "OS-EXT-SRV-ATTR:user_data": "IyEvYmluL2Jhc2gKL2Jpbi9zdQplY2hvICJJIGFtIGluIHlvdSEiCg==",
        "OS-EXT-STS:power_state": 1,
        "OS-EXT-STS:task_state": null,
        "OS-EXT-STS:vm_state": "active",
        "OS-SRV-USG:launched_at": "2019-04-23T15:19:10.855016",
        "OS-SRV-USG:terminated_at": null,
        "accessIPv4": "1.2.3.4",
        "accessIPv6": "80fe::",
        "addresses": {
            "private": [
                {
                    "OS-EXT-IPS-MAC:mac_addr": "00:0c:29:0d:11:74",
                    "OS-EXT-IPS:type": "fixed",
                    "addr": "192.168.1.30",
                    "version": 4
                }
            ]
        },
        "config_drive": "",
        "created": "2019-04-23T15:19:09Z",
        "description": null,
        "flavor": {
            "disk": 1,
            "ephemeral": 0,
            "extra_specs": {},
            "original_name": "m1.tiny",
            "ram": 512,
            "swap": 0,
            "vcpus": 1
        },
        "hostId": "2091634baaccdc4c5a1d57069c833e402921df696b7f970791b12ec6",
        "host_status": "UP",
        "id": "0e12087a-7c87-476a-8f84-7398e991cecc",
        "image": {
            "id": "70a599e0-31e7-49b7-b260-868f441e862b",
        },
        "key_name": null,
        
        "locked": true,
        "locked_reason": "I don't want to work",
        "metadata": {
            "My Server Name": "Apache1"
        },
        "name": "new-server-test",
        "os-extended-volumes:volumes_attached": [],
        "progress": 0,
        "security_groups": [
            {
                "name": "default"
            }
        ],
        "server_groups": [],
        "status": "ACTIVE",
        "tags": [],
        "tenant_id": "6f70656e737461636b20342065766572",
        "trusted_image_certificates": null,
        "updated": "2019-04-23T15:19:11Z",
        "user_id": "admin"
    }},
    "retCode": 0,
    "message": "Query servers success",
    "params": null
}
```

### Operate vm server

Operate vm server
```
Resource URI: /rescontroller/v1/tenants/:tenantId/hosts/:hostIp/servers/:serverId
Method: POST
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|access_token |access token|header |yes|Jwt Token|

Example request:
```
{
    "action" : "" ### reboot/createImage/pause(数据保留到内存)/unpause(after pause)/suspend(数据保留到磁盘)/resume(after suspend)/stop(关机)/start(after stop)/createConsole
    ### if action reboot, need add params like "HARD" or "SOFT" , if action createImage need add params name and metadata
	"reboot" : " " 
	"createImage : { 
	    "name": "vmsnap",
		"metadata": { }
	} 
}
```

Example response:
```
200 OK
{
    "data": null,
    "retCode": 0,
    "message": "Operate servers success",
    "params": null
}
```

### Delete servers on mechost by serverId

Delete servers on mechost by serverId
```
Resource URI: /rescontroller/v1/tenants/:tenantId/hosts/:hostIp/servers/:serverId
Method: Delete
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|access_token |access token|header |yes|Jwt Token|

Example response:
```
200 OK
{
    "data": null,
    "retCode": 0,
    "message": "delete servers success",
    "params": null
}
```