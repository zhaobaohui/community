|Requirements description | responsible person | priority | workload estimation (k) | design link|
|---|---|---|---| -----|
|[Resource Controller]Add Resource Controller module on edge side, that provide CURD apis for network, images, security group, SecurityGroup rule, flavor and vm. |   Rama Subba Reddy/李治谦 | High|  2 | 90% |
|[LCM Controller]Add Tenant isolation for Add/Upload config/ for mechost apis. |  李治谦 | High |  0.5 |  100% |
|[MECM]Add Tenant isolation for Add/Upload config/ for mechost apis. |  Shashi | High |  1k |   |
|[OS Plugin]Add Resource Controller module on edge side, that provide CURD apis for network, images, security group, SecurityGroup rule, flavor and vm. | 崔敬潮 | High| 1.5  | 100% |
|[OS Plugin]Add Tenant isolation for Add/Upload config/ for mechost apis. |   崔敬潮 | High |  0.5  |  100% |
|[OS Plugin]APP instantiation add multi vm support. |   崔敬潮 | High |  0.5  |  100% |
|[OS Plugin]KPI of edge host support. |   崔敬潮 | High |  1  |  90% |
|[appo]Add Resource Controller module on center side, that provide CURD apis for network, images, security group, SecurityGroup rule, flavor and vm. |   Shashikanth v.h. | High |  1.5 |  80% |
|[mecm-fe]Add Page for resource api use, implement lightweight cloud management features. |   唐浩天/樊正伟/赵龙飞  | High |  3 |  80%  |
|[Edge autonomy Portal]Add Page for resource api use, implement lightweight cloud management features. |   唐浩天/樊正伟/高国桢  | High |  3 |  80% |
|[LCM Controller] 增加profile脚本执行接口. |   李治谦  | Middle |  0.5 | 10%  |
|[LCM Controller] MECM支持zip格式应用包. |   李治谦  | Middle |  0.1 | 100%  |
|[MEO] MECM support zip package. |   Shashikanth v.h.  | Middle |  0.1 | 100%  |



# Requirements Collection:


Architecture change: 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/164622_c43de297_7786397.png "屏幕截图.png")

Sequence Diagram:
![输入图片说明](https://images.gitee.com/uploads/images/2021/1102/170635_d9828b6e_7786397.png "屏幕截图.png")