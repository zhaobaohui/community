## 需求名称：支持用户注册节点时可以自定义位置
### 方案设计：
#### 方案一：   
###### 1.注册节点界面增加新增地址配置按钮，用户点击自定义按钮，选择省市，以及输入地理位置坐标，完成自定义地址配置。  
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0420/144121_48263677_7625361.png "屏幕截图.png")   
###### 2.后台需要提供地址新增、删除、编辑和查询功能，根据业务场景仅提供增加和删除功能即可。
|  Api   | 方法  |
|  ----  | ----  |
| /inventory/v1/location  | POST |
| /inventory/v1/location  | GET |
| /inventory/v1/location  | DELETE |
| /inventory/v1/location  | PUT |
#### 方案二：  
###### 注册节点是直接让用户填写省市一级具体坐标信息  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0425/165003_05407505_7625361.png "屏幕截图.png")