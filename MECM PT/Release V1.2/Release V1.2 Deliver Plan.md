# MECM Sprint Plan
Sprint1: 2021-5-6 ~ 2021-5-21  
Sprint2: 2021-5-24 ~ 2021-6-4  
Sprint3: 2021-6-7 ~ 2021-6-18

## MECM V1.0 Requirtments
| Story Name | Target Sprint | Owner | 2021-5-10 ~ 2021-5-14 | 2021-5-17 ~ 2021-5-21 | 2021-5-24 ~ 2021-5-28 | 2021-5-31 ~ 2021-6-4 | 2021-6-7 ~ 2021-6-11 | 2021-6-14 ~ 2021-6-18 |2021-6-21 ~ 2021-6-25 | 2021-6-28~ 2021-7-2|
|-----------|------|-------|----------|-----------|-------------|----------|----------|----------|----------|----------|
| [【MECM】统一MECM组件风格](https://gitee.com/OSDT/dashboard/issues?id=I2P3CD)| Sprint3   |  杨阳  |  高保真正在输出，目前还未开始  | 设计已完成，编码中  | 优先级降低   |  80%完成 | Delay  |  |  |
| [【HealthCheck】MECM层调用HealthCheck](https://gitee.com/OSDT/dashboard/issues?id=I3PRZ3)| Sprint3   |  高国桢  |     |  设计完成，暂未开始 |    |  延迟 | finish |  |  |
| [【MECM】MECM支持前台节点位置可自定义](https://gitee.com/OSDT/dashboard/issues?id=I3PVE0)| Sprint1   |  杨阳  |  代码已经开发完成 |  正常交付   |  NA | NA | NA | NA |NA | NA |
| [【MECM】MECM支持applcm和apprulemgr注册统一](https://gitee.com/OSDT/dashboard/issues?id=I3PVDV)| Sprint2   |  rama & shashi& 杨阳  | technical testing. using ingress solution.   |  开始联调  | finished  | test  | NA  | NA | | |
| [【MECM】MECM后台统一servicecomb](https://gitee.com/OSDT/dashboard/issues?id=I3PVD8)| Sprint1   |  李治谦  |  编码完成，相关issue已经修改完成 |  正常交付  |   Test | NA | NA | NA |NA | NA |
| [【MECM】边缘自治增强](https://gitee.com/OSDT/dashboard/issues?id=I3PV9W)| Sprint2   |  周文敬  | 部分交付 |    | 正常  | done | NA | NA |NA | NA |
| [【MECM】MECM支持虚机APP包同步和分发，MECM support app package management of VM app](https://gitee.com/OSDT/dashboard/issues?id=I3PVBT)| Sprint3   |  shashi, yangyang  |  Design flow is updated |  In progress  |  code finished | test |  |  | | |
| [【MECM】mecm支持虚机APP的实例化和终止 VM APP instantiation and termination support.--OpenStack](https://gitee.com/OSDT/dashboard/issues?id=I3PV4Y)| Sprint3   |  崔敬潮 & shashi & 白针针 | osplugin的实例化和上传流程已经设计。  |    |  5.28 start test | part success, do test next week. | fe code finished test together next week. | finished |Test| |
| [【MECM】mecm支持虚机APP的实例化和终止 VM APP instantiation and termination support.--FusionSphere](https://gitee.com/OSDT/dashboard/issues?id=I3PV4Y)| Sprint3   |   李治谦 |   |  test start 5.24  |   | Self-verification  | finished | NA |Test| |
| [【MECM】mecm支持VIM预配置 MECM support the preconfig of OpenStack](https://gitee.com/OSDT/dashboard/issues?id=I3PV5W)| Sprint3   |  Rama/李治谦/崔敬潮  |   |  优先级降低，先完成编码。  |   | doc start |  |  |   |  |
| [【MECM】接口响应规范优化(Interface response specification optimization)](https://gitee.com/OSDT/dashboard/issues?id=I3OYB2)| Sprint3   |  Rama  |   |  优先级降低，先完成编码。  |   |  | Delay |  |NA | NA  |
| [【MECM】关键数据持久化(Key data persistence)](https://gitee.com/OSDT/dashboard/issues?id=I3OY79)| Sprint2   |  Rama  |   |  code finised  |  NA | NA | NA | NA  |NA | NA  |
| [【Example-apps】Example-apps enhancement] (https://gitee.com/OSDT/dashboard/issues?id=I3PVF2)| Sprint3   |  Rama  |  Now are working on this. launch standealone is ok now. Now have to embed to the deployment. |  low Priority  | processing  |  processing | finished | NA | NA| NA|
| Dynamic NameSpace support | Sprint3   |  Rama  |  processing |  processing  |  processing  |  finished |  NA | NA | Test| |
| Enhancement of app Query API | Sprint3   |  Rama  | processing  |  processing  | finished  |  NA | NA | NA |  Test| |
| add app info ak/sk config interface | Sprint3   |  Rama  | processing  | processing   |  finished |  NA | NA | NA | Test| |