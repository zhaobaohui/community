# Overview
![alt Overview](img/overview.png)

## 说明(notes)：
1. 点击 AppDetails，跳转到MEP首页。不符合用户预期。Overview页面删除“AppDetails”。
2. 点击Node Details，跳转到Node Details页面

# Node Details
![alt Node Details](img/nodeDetails.png)

## 说明(notes)：
1. 节点详情页面，删除按钮“Manage”和“Operation”
2. 增加按钮“More Information”跳转到MEP详情页面

# MEP界面展示的特性(the features shown on MEP portal)
* 应用数量统计
* 服务数量统计
* 被订阅方服务统计
* 订阅方应用统计
* 应用列表
* 服务列表
* 平台能力和三方能力，各能力近7天的调用次数统计
* 服务和应用间的所属关系和订阅关系

接下来将考虑如何将这些特性整合到MEPM的MEP Details页面中。

# MEP Details
![alt MEP Details](img/mepDetails.png)

# 数据交互(Data Interaction)
![alt MEP Details](img/dataInteraction.png)

# 接口变更
无

# 工作量
|  模块   | 工作量  |
|  ----  | ----  |
| MEPM-FE  | 0.5人/月 |

