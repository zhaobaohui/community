|  需求描述 | 责任人  | 优先级  | 工作量预估 | 设计链接|
|---|---|---|---| -----|
| 完成 MECM各模块API返回值归一化管理(Interface response specification optimization)  | 李治谦| 高  | 2k  |    [V2InterfaceOptimization](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.3/V2InterfaceOptimization%2Emd)   |
| 完成 MECM各模块2.0版本接口改造（Modification of the interface of each module of MECM 2.0 version）  | 李治谦| 高  |  0.5k |    [V2InterfaceOptimization](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.3/V2InterfaceOptimization%2Emd)   |
| 边缘自治Portal支持帐号登录访问控制（Edge Autonomous Portal supports account login access control）  | 鹿鑫/李治谦/周文敬 | 高  |  1.5k |    [EdgeAuthDesign](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.3/EdgeAuthDesign%2Emd)  | 
| 支持租户查看APP虚机，登录虚机等自运维功能，分析中 （Support tenants to view APP virtual machine, log in to the virtual machine and other self-operation and maintenance functions） | 李治谦/杨阳 | 低  | 延后  |      |
| 支持FS管理应用生命周期（Support docking FS management application life cycle）  | 崔敬潮/李治谦 | 高  |  1k |      |
| MECM支持Metric数据显示与采集，替换Prometheus组件临时方案（MECM supports Metric data display and collection, and a temporary solution to replace Prometheus components）  | Rama/杨阳 | 高  | 2k  |   [ResourceMonitorDesign](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.3/ResourceMonitorDesign%2Emd)   |
| 虚机APP支持能力调用（Support ability call for virtual APP）  | 陈传雨| 高  | 测试验证  |      |
| 虚机APP支持集成mep-agent方案（Support integrated mep-agent solution for virtual APP）  | 陈传雨| 高  | 测试验证  |      |
| MEP支持虚机的方式部署在边缘节点上（MEP supports deployment on Vm based edge nodes.）  | 陈传雨| 中  |  测试验证 |  


# Requirements Collection:

## 虚机支持增强
1.支持虚机集成mep-agent，调用平台能力

2.poc：对接FS时osplugin完成镜像下载与上传的过程，完成部署流程可用

## 边缘资源管理
3.使用metrics-server替换Prometheus提供边缘节点资源状态查询（CPU、MEMORY）
4.边缘节点状态显示增强，定时查询边缘节点健康状态，页面上实时显示节点健康情况
![输入图片说明](https://images.gitee.com/uploads/images/2021/0716/153016_53677821_7786397.png "屏幕截图.png")
5.应用资源状态监控，新增选择应用示例后，显示资源占用情况显示区
![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/153146_b00a511e_7786397.png "屏幕截图.png")

## 边缘自治需求增强
*边缘MEPM认证
MEPM Portal组件归一化后优化


## 租户自运维界面
分析OpenStack运维接口，制定运维范围及界面设计（采用Gnocchi、Ceilometer、Aodh）
针对k8s，使用prometheus-operator

