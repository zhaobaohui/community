### 需求详情
接口响应规范化
#### 状态码：
现在我们代码中返回状态码不规范，业务处理异常场景时，返回的状态码有400、403、500等。
各个模块需要分析业务场景，具体错误信息由响应内容给出，内部逻辑错误和访问外部系统（如文件系统、数据库）错误是500，部分成功206；
部分请求参数错误、没有权限等可以返回400、403等状态码。
#### 方案
Java语言类后台返回具体错误码，将错误码的中英文资源描述统一放在website-gateway仓库中，由website-gateway提供公共接口获取错误码资源信息，各个项目前台前台自行调用解析接口中的错误码信息。
 **单接口：** 
```
{
  "data" : 原响应结构,
  "retCode" : int,  --- 响应错误码 
  "message": string, --- 错误信息，各业务前台可根据此字段展示详细错误信息，也可只使用retCode
  "params": [string] --- 资源化信息中带的参数信息
}
```
**批量查询GET接口：Page<T>**
```
{
  "results" : [] --- 原响应结构,
  "total" : int,  ---- 用于调用方判断分页查询是否继续
  "limit" : int,
  "offset" : int
}
```

APPLCM v2接口规范

接口规范性整改详见接口[设计文档](https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.3/V2InterfaceDesign%2Emd)