# MECM Sprint Plan
Sprint1 2021-7-26 ~ 2021-8-15

Sprint2 2021-8-16 ~ 2021-8-29

Sprint3 2021-8-30 ~ 2021-9-12

## MECM V1.0 Requirtments
| Story Name | Target Sprint | Owner | 2021-7-26 ~ 2021-8-15 | 2021-8-16 ~ 2021-8-29 | 2021-8-30 ~ 2021-9-12 | 2021-9-13 ~ 2021-9-26 |
|-----------|------|-------|----------|-----------|-------------|----------|
|【MECM】界面优化 | Sprint3   |  杨阳 |    |    | 70%  |    |
| Interface response specification optimization | Sprint1   |  李治谦  |  100%  |   |   |   |
| Add version v2 interface for response specification | Sprint1   |  李治谦  |  100%  |   |   |   |
| 【MECM】 switch interface calling from v1 to v2 | Sprint2  |  Shashi |  100%  |   |   |   |
| 【MECM-FE】 switch interface calling from v1 to v2 | Sprint2  | Mukesh  |    |   | Delay to v1.4  |   |
| 【MEPM-FE】 switch interface calling from v1 to v2 | Sprint2  | 周文敬|    |   | Delay to v1.4  |   |
| 【APPLCM】Edge autonomy Portal support login/ 边缘自治支持帐号登录访问控制 | Sprint2   |  Rama  |  100%  |   |   |   |
| 【MEPM-FE】Edge autonomy Portal support login/ 边缘自治支持帐号登录访问控制 | Sprint2   |  杨阳 |  100%  |   |   |   |
| Improve code coverage for modules of mecm Coverage up to 70% | Sprint2   |  75% |    |  66% of lcm  |   |   |
| Support FS app life cycle management | Sprint2   |  崔敬潮/李治谦  |   Test for community |   |  100% |   |
| 【APPLCM】MECM支持资源统计，资源分配等资源管理<br>支持Metric数据显示与采集MECM supports Metric data display and collection, and a temporary solution to replace Prometheus components | Sprint2   |  Rama  |    |   |  100%  |   |
| 【MECM-FE】MECM支持资源统计，资源分配等资源管理<br>支持Metric数据显示与采集MECM supports Metric data display and collection, and a temporary solution to replace Prometheus components | Sprint2   |  Mukesh/杨阳   |    |   |  100% |   |
|【MECM】【LCM】Query status interface responds according to the status of the plugin | Sprint2   |  Rama/李治谦   |    |   | 100%  |   |
|【MECM】【OSplugin】Query status interface responds according to the status of the plugin | Sprint2   |  崔敬潮 |    |   | 100%   |   |
|【MECM】【OSplugin】v1.2 support FS APP Instantiate | Sprint2   |  崔敬潮/李治谦 |    |   |  100%  |   |
|【ExampleAPP】support mep api calling | Sprint3   |  Rama |    |   |  90% not test  |   |
|【MECM】Code quality improvement | Sprint3   |  Shashi/Rama |    |   |    |   |
| 虚机APP支持能力调用 | Sprint2   |  陈传雨   |  50%  |   |   |   |
| 虚机APP支持集成mep-agent方案 | Sprint2   |  陈传雨   |  50%    |   |   |   |
| MEP支持虚机的方式部署在边缘节点上 | Sprint2   |  陈传雨   |  50%    |   |   |   |