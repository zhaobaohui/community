## 边缘自治认证方案设计

边缘自认证，增加边缘用户登录认证功能，预置边缘登录默认用户，和边缘机机认证用户。

### 需求分解：
#### 1.MEPM提供界面登录、首次登录密码修改功能。
#### 2.LCM操作接口增加机机账号认证的可选性判定。


![新增管理表](https://images.gitee.com/uploads/images/2021/0726/154824_9d900219_7786397.png "屏幕截图.png")


### 时序图
#### 1.MEPM界面增加登录控制，首次登录需要修改用户密码

![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/151219_a2fa6173_7786397.png "屏幕截图.png")



## 接口变更
### 新增接口
### login

login

```
Resource URI: /lcmcontroller/v1/login
Method: POST
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|name|certification name|formData|yes|Valid Name|15:default value "admin"|
|key|certification passkey|formData|yes|

Example response:
```
200 OK
{
    "data": {
        "username": "admin",
        "auth_time":"2021-02-24 04:39:23",
        "allowed": true,
        "authenticate_id": "37423702-051a-46b4-bf2b-f190759cc0b8",
        "session": "38423702-051a-46b4-bf2b-f190759cc0b8"
    },
    "retCode": 0,
    "params": "[string]",
    "message": "string"
  }

response 400 Bad Request
{
  "retCode": 0,
  "params": "[string]",
  "message": "string"
}

response 401 
{
  "retCode": 0,
  "params": "[string]",
  "message": "string"
}

response 403 FORBIDDEN
{
  "retCode": 0,
  "params": "[string]",
  "message": "string"
}

response 500 INTERNAL ERROR
{
  "retCode": 0,
  "params": "[string]",
  "message": "string"
}
```

### Change password

Change password

```
Resource URI: /lcmcontroller/v1/password
Method: POST
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|name|certification name|formData|yes|Valid Name|15:default value "admin"|
|key|old certification passkey|formData|yes|
|newkey|Certification passkey|formData|yes|

Example response:
```
200 OK
{
    "retCode": 0,
    "params": "[string]",
    "message": "string"
  }

response 400 Bad Request
{
  "retCode": 0,
  "params": "[string]",
  "message": "string"
}

response 401 
{
  "retCode": 0,
  "params": "[string]",
  "message": "string"
}

response 403 FORBIDDEN
{
  "retCode": 0,
  "params": "[string]",
  "message": "string"
}

response 500 INTERNAL ERROR
{
  "retCode": 0,
  "params": "[string]",
  "message": "string"
}
```

### Operation API add Certificate params（Non-essential）

For Add MEC host/ Update MEC host/Delete MEC host/Upload Config File/Delete Config File/Instantiate Application/Terminates Application/Upload package/Delete package/Delete application package on host/Distribute package Add 2 params(Non-essential)

|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|name|certification name|formData|yes|Valid Name|15:default value "admin"|
|key|certification passkey|formData|yes|
