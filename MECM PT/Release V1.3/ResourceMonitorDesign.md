
首页边缘节点资源使用情况监控，首页左侧列表中选择边缘节点时，展示其CPU、Memory使用情况
![输入图片说明](https://images.gitee.com/uploads/images/2021/0716/153016_53677821_7786397.png "屏幕截图.png")

修改接口：
### Query Kpi
Get cpu , memory and file system size
```
Resource URI: /lcmcontroller/v1/tenants/{tenantId}/hosts/{hostIp}/kpi
Method: GET
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|access_token |access token|header |yes|Jwt Token|
|hostIp|host Ip|path |yes|Valid IP Address|15|
|tenantId |tenant Id|path |yes|Valid UUID|64|

Example response:
```
200 OK
  {
    {}
  }
```

应用资源状态监控，新增选择应用实例后，显示资源占用情况显示区
![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/153146_b00a511e_7786397.png "屏幕截图.png")
 调用接口如下
### Query
Get the statistics information
```
Resource URI: /lcmcontroller/v1/tenants/{tenantId}/app_instances/{appInstanceId}
Method: GET
```
|Name|Definition|Type|Required|Allowed|Max Length|
|---|---|---|---|---|---|
|access_token |access token|header |yes|Jwt Token|
|appInstanceId|appInstance Id|path|yes|Valid UUID|64|
|tenantId|tenant Id|path|yes|Valid UUID|64|

Example response:
```
200 OK
 {
    "pods": [
        {
            "podstatus": "Running",
            "podname": "srs-6c79bfbb85-4s8x4",
            "containers": [
                {
                    "containername": "srs",
                    "metricsusage": {
                        "cpuusage": "1/16000",
                        "memusage": "8228864/33623261184",
                        "diskusage": "0/151949750840"
                    }
                }
            ]
        }
    ]
}
```