|Requirements description | responsible person | priority | workload estimation (k) | design link|
|---|---|---|---| -----|
|[mepm-fe]虚机接口增加操作项的实现Improve the virtual machine operation function. |   赵龙飞/李治谦 | High|  2 |  |
|[OS Plugin]接口增加返回失败信息. Interface addition returns failure information | 崔敬潮 | 取消| 1  |  |
|[mepm-fe]接口增加返回失败信息. Interface addition returns failure information | 赵龙飞 | 取消| 1  |  |
|[mecm-fe]增加文件管理界面，后台接口为filesystem提供，并且可将文件管理中的镜像导入到resource mgr中的节点。Add Page for filesystem, implement img import to edge node by cresource mgr. |  赵龙飞  | High |  3 |   |
|[resource-mgr]资源管理，用户选择包+规格计算出边缘节点可创建的应用数量。In resource management, the user selects the package + specification to calculate the number of applications that can be created by the edge node. |   | High |  3 |   |
|[mepm-fe]资源管理，用户选择包+规格计算出边缘节点可创建的应用数量。In resource management, the user selects the package + specification to calculate the number of applications that can be created by the edge node. |    | High |  3 |   |
|[mepm-fe]坐标组件优化，支持用户点击选择坐标。Coordinate component optimization, support users to click to select coordinates. |  杨阳  | High |  3 |   |
|[mepm-fe]地区选择优化，支持国际化。Region selection optimization to support internationalization.|  杨阳  | High |  3 |   |
|[mecm-north]增加host健康检查接口。add API for 后三天heleath check .|  李治谦  | High |  0.5 |   |
