# MECM Sprint Plan
Sprint1 2022-2-7 ~ 2022-2-18

Sprint2 2022-2-21 ~ 2022-3-4

Sprint3 2022-3-7 ~ 2022-3-18

## MECM V1.6 Requirtments
| Story Name | Target Sprint | Owner | 2022-2-7 ~ 2022-2-18 | 2022-2-21 ~ 2022-3-4 | 2022-3-7 ~ 2022-3-18 | 2022-3-21 ~ 2022-3-31 |
|-----------|------|-------|----------|-----------|-------------|----------|
| [mepm-fe]虚机接口增加操作项的实现Improve the virtual machine operation function. | Sprint1   |  赵龙飞 |    |    | 100%  |    |
| [mecm-fe][mecm-north]增加文件管理界面，后台接口为filesystem提供，并且可将文件管理中的镜像导入到resource mgr中的节点。Add Page for filesystem, implement img import to edge node by cresource mgr.  | Sprint1  |  赵龙飞 |    | 100%  |   |   |
| [mepm-fe]坐标组件优化，支持用户点击选择坐标。Coordinate component optimization, support users to click to select coordinates. | Sprint1  | Mukesh/杨阳  |  100%  |   |   |   |
| [mepm-fe]地区选择优化，支持国际化。Region selection optimization to support internationalization. | Sprint1  | 杨阳 |    | 100%  |   |   |
| [mecm] MECM Intent driven. | Sprint3   |  Shashi  |  NA  |   |   |   |
| [resource-mgr]资源管理，用户选择包+规格计算出边缘节点可创建的应用数量。In resource management, the user selects the package + specification to calculate the number of applications that can be created by the edge node. | Sprint2   |  Shashi |  NA  |   | 80%  |   |
|[mecm-north]add api for host health check.  | Sprint1  | 卿舒婷/杨阳 |    |  100%  |   |   |
|[mepm] kubernetes network policy for mepm modules  | Sprint2  | Rama |    |   | 100%   |   |
|[mepm] enhence query kpi to support port name and protocol.  | Sprint1  | Rama |    | 100%  |    |   |