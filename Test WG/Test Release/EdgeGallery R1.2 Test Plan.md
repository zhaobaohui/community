**1. Prerequisites** 

- [release milestone]()
- [requirement list](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&milestone_id=101482)
- [requirement list-story](https://gitee.com/OSDT/dashboard/issues?issue_type_id=199539&milestone_id=101482)

**2. Test target** 
    
     - Quality Index for R1.2

|指标|目标值|备注|
|---|---|---|
|需求完成率|90%|实际完成的需求个数/计划完成的需求个数|
|遗留DI|10|1、本迭代发现的不影响本迭代只影响下个迭代的问题，也要计入本迭代的遗留DI中；2、本迭代发布时说明遗留问题的影响及对策，版本经理同意后可特例发布；"	∑(提示问题个数*0.1+一般问题个数*1+严重问题个数*3+致命问题个数*10)
|严重及以上问题数|≤1	|严重≤1，致命0;∑(严重问题个数+致命问题个数)|
|开源软件禁选|0|开源软件禁选个数为0|
|代码静态检查告警|0|代码静态检查告警个数（findbugs、stylecheck、dependency check、PWD）|
|测试用例执行率|90%|计划执行的测试用例数/实际执行的测试用例数|
|单元测试代码覆盖率|	>=60%|


**3. Test Scope** 


 **3.1 第一次迭代需求** 

- 【developer】支持input参数补齐  ——李媛媛 done
- 【developer】虚机镜像管理（用户，管理员权限）  ——李媛媛 done
- 【Developer】首页界面优化重构  ——李媛媛 done
- 【developer】虚机资源配置可编辑  ——李媛媛 done
- 【UserMgmt】登录的前端滑块验证方式优化为后台验证码   ——李强 done
- 【UserMgmt】发送验证码到用户手机或邮箱的前端滑块验证方式优化为后台验证码  ——李强  done
- 【UserMgmt】用户注册支持与后台交互的验证  ——李强 done
- 【ATP】ATP首页优化  ——李媛媛 done
- 【ATP】后台新增批量导入包解析接口  ——李媛媛 done
- 【ATP】前台新增批量导入入口  ——李媛媛 done
- 【ATP】后台新增贡献批量删除、脚本类型贡献下载接口  ——李媛媛 done
- 【ATP】前台新增贡献管理页面  ——李媛媛 done
- 【ATP】关键数据持久化——李媛媛 done
- 【ATP】测试任务页面优化——李媛媛 done
- 【AppStore】关键数据持久化  ——张阿利 done
- 【AppStore】操作分析界面优化  ——张阿利 done
- 【AppStore】文档界面优化  ——张阿利 done
- 【AppStore】上传应用时可设置应用展示模式  ——张阿利 done
- 【AppStore】支持修改应用属性  ——张阿利 done
- 【AppStore】支持显示应用部署类型虚机还是容器  ——张阿利 done
- 【MECM】边缘自治增强--mep合并到mepm  ——张阿利 done
- 【MECM】MECM后台统一servicecomb  ——张阿利 done
- 【MECM】MECM支持前台节点位置可自定义  ——张阿利 done


 **3.2 第二次迭代需求** 

- 【Developer】统一Develop组件风格【poc】--李媛媛 done
- 【developer】支持配置app_configuration, 支持定义AK/SK, APP分流规则，DNS规则等--李媛媛 done
- 【developer】虚机资源配置可编辑--李媛媛 done
- 【developer】虚机镜像管理（用户，管理员权限）--李媛媛 done
- 【Developer】能力中心界面优化重构--李媛媛 done
- 【AppStore】应用管理界面  ——张阿利 done
- 【appstore】appstore支持虚机镜像的管理以及上传下载  ——张阿利 done
- 【ATP】用户面页面优化--李媛媛 done
- 【ATP】测试用例增强--李媛媛 done
- 【ATP】测试模型页面优化--李媛媛 done
- 【UserMgmt】个人帐号中心展示我的权限(角色+可访问平台)    ——yxl done
- 【UserMgmt】支持非guest用户90天后必须修改密码    ——yxl done
- 【UserMgmt】支持admin首次登录成功后强制修改密码     ——yxl done
- 【MECM】首页展示优化  ——张阿利 done
- 【MECM】MECM支持applcm和apprulemgr注册统一--lh done
- 【installer】支持docker-compose直接从镜像部署EG，不依赖k8s--xd、张阿利 done
- 【installer】ansible安装脚本强化与问题解决--xd（自动化测试）done


 **3.3 第三次迭代需求** 

- 【AppStore】APPD转换器【poc】 --阿利 done
- 【AppStore】接口响应规范优化 --阿利 done
- 【Atp】接口响应规范优化 --媛媛 done
- 【MEP】接口响应规范优化  --阿利 done
- 【AppStore】应用在线体验 --阿利 done
- 【ATP】测试模型页面优化 --媛媛 done
- 【ATP】测试任务页面优化 --媛媛 done
- 【developer】input根据不同的操作系统，导入不同的初始化脚本 --媛媛 done
- 【developer】虚机mepagent的集成 --媛媛 done
- 【MECM】mecm支持虚机APP的实例化和终止 VM APP instantiation and termination support.--阿利 done
- 【MECM】mecm支持VIM预配置 MECM support the preconfig of OpenStack --阿利 done
- 【MECM】MECM支持虚机APP包同步和分发，MECM support app package management of VM app--阿利 done
- 【ATP】ATP支持虚机APP的实例化和终止用例 --媛媛 done
- 【MEP】timing api   ——张阿利 done
- 【MEP】transport api   ——张阿利 done
- 【MEP】关键数据持久化(移入迭代三)  ——张阿利 done
- 【MEP】 Code quality improvement(移入迭代三)  ——张阿利 done
- 【MEP】MEP adapter register to EdgeGallery MEP and UPF(移入迭代三)  ——张阿利 done
- 【MEP】Public the UPF capability to EdgeGallery(移入迭代三)  ——张阿利 done
- 【MEP】mep-agent实现订阅接口(移入迭代三)  ——张阿利 done
- 【MEP】服务发现增加鉴权认证(移入迭代三)  ——张阿利 done


**4. Test Case Management** 

A Test Case contains: ID, purpose, test tools, Prerequisites, test data, test steps, desired Test Results, actual test Results, Test Verdict.
测试用例包括：ID，测试目的，测试工具，前置条件，测试数据，测试步骤，预期结果，实际结果，测试结果等。

- 测试用例ID：EG-[Module ID]-TC-ID(ID三位数，从001开始)。
- 测试结果：pass/fail。

测试用例ID中Module ID指对应软件模块的缩写，如MEP、MECM、APPSTORE、DEV等；同时测试用例关联对应epic/story。

测试用例均在Gitee上创建并管理，关联EPIC任务。在测试用例标题中填入ID和测试目的。具体参见[操作指南](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_case_template.md)。

测试用例评审：召集社区的研发、测试等相关人员评审测试用例，确保测试用例有效、多方理解一致且覆盖版本需求。评审后，调整补充完善测试用例。可针对不同epic或不同模块分别组织评审会，每次会议不超30分钟。

**5. 缺陷管理 Bug Management** 

缺陷统一在Gitee中录入，点击[操作指南](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md)。

bug优先级为：严重、主要、次要、一般。

严重bug尽可能1个工作日内解决（非强制，需协商）；
主要bug尽可能3个工作日内解决（非强制，需协商）；
次要bug尽可能5个工作日内解决（非强制，需协商）。

测试执行阶段，每天邮件发送bug清单汇总，并对超期未关闭bug重点提醒。

- 针对测试人员提的bug，测试人员跟踪目标研发人员修复，完成修复后，测试人员回归验证并通过后设定bug状态为“已验收”。
- 针对于研发人员自己提的bug，建议bug修复后PTL协调研发人员交叉验证，通过后设置bug状态为“已验收”。
- 针对访客提的bug，由PTL安排人员跟踪修复，研发人员回归验证后设置bug为“已验收”。

版本发布后，针对测试人员/研发人员/访客提的bug，标签为已发布的版本号，如果研发人员确认该bug在下一版本修订，则将该bug同时打上下一版本号的标签，便于跟踪。针对已发布版本中新发现的bug，每周/两周发邮件提醒。


**6. relative Tools** 

|Tool Name|Version|Address | comments  |
|---|---|---|---|
|SonarQube|8.4.2 |http://159.138.132.188:9000/projects  ||
|Jacoco|0.7.7.201606060606  |None |   |
|go test| 1.15.5 |None |   |

**7. Test development info** 

| tools&Language | version | remark | link |
|----------------|---------|--------|------|
| Robotframework | v3.2.1        |        |      |
| selenium       | v3.141.0        |        |      |
| python         | v3.7.2        |        |      |
| IDE ride       | v1.7.4.2        |        |      |
| IDE pycharm    |         |        |      |
| visual studio code(user)    | 1.48.0        |        |      |

**8. Test env info** 

| infra  | Version | address | remarks |
|--------|---------|---------|---------|
| K8S    | v1.18.4 |         |         |
| k3s    | v1.18.4 |         |         |
| docker | v19.03.12|         |         |
| helm   | v3.2.4  |         |         |
| ....   | ......  |         |         |


    - Jenkins addr：http://jenkins.edgegallery.org/
    - repo
        - [integration-testing](https://gitee.com/EdgeGallery_group/integration-testing)()
        - [ci-management](https://gitee.com/edgegallery/ci-management)
        - [platform-mgmt](https://gitee.com/EdgeGallery_group/platform-mgmt)
        - [helm-charts](https://gitee.com/EdgeGallery_group/helm-charts)
    
    - Reference env
        - daily build jenkins：daily.developer.edgegallery.org;daily.appstore.edgegallery.org;daily.mecm.edgegallery.org
        - Weekly build Jenkins:weekly.developer.edgegallery.org;weekly.appstore.edgegallery.org;weekly.mecm.edgegallery.org
        - ......
    - Lab
        - 5GC env + Integration test；
        -

基于测试环境选定的软件版本，在社区CI/CD环境上完成测试环境搭建相关配置，自动搭建测试目标，以支撑自动测试(开发相应自动化测试脚本)或人工测试。

**9. Test Planning** 

| 时间  | 迭代研发  | 迭代测试  |
|---|---|---|
|0506-0523   | 第一次迭代  |   |
|0524-0606   | 第二次迭代  |第一次   |
|0607-0620   | 第三次迭代  |第二次  |
|0621-0704   | 修改bug  |第三次   |
|0705-0707   | 准备并发布  |准备并发布   |

针对测试人员，在第i次迭代测试开始前（即第i-1次测试过程中），需同研发人员澄清本轮测试需求，完成测试用例设计及评审；在第i次迭代测试执行中需执行所设计的测试用例（含自动化测试脚本），记录bug（及提出需求优化建议等），同时完成第i-1轮bug的回归测试。

**10. CI/CD Configuration Plan** 

**11. Criteria for Iterative Test Execution** 

    - Starting Criteria for Iterative test excution
        - 完成本次迭代的详细设计/架构更新Complete detail design/architecture updating for current iteration        
        - PTL确认所有task关闭（PTL confirm ）all tasks in current iteration are closed
        - PTL确认所有新开发代码都有自动化测试用例（PTL）confirm all recently developed code has autotest cases
        - PTL确认项目完成开发者自验证（PTL confirm）complete self-verification of project （including bugs from gitee visitors）
        - 近1天镜像自动构建成功率100% daily build for last 1 days is all succeeded【First Iteration】
        - 近1天镜像CICD成功率100% daily build for last 1 days is all succeeded【Second Iteration】  
        - 近1天镜像CICD成功率100%（含自动测试） daily build for last 1 days is all succeeded【Third Iteration】
    
    - Ending Criteria for test execution in Final Iteration
    
        - PTL确认bug单关闭（PTL confirm） vital/blocked bugs are all closed
        - 所有测试活动完整，测试通过 all test activities are completed and test results are passed
        - 无阻塞性及严重性问题遗留 No blocked or vital problems/bugs are left。
        - 确认所有依赖软件都在本地库中 confirm all dependent softwares are all stored locally
        - PTL确认满足单元测试覆盖率>=50%要求（PTL confirm） satisfy code coverage rate for unit test
        - PTL确认三方件归一化扫描结果（PTL confirm） 3rd-party components  are all scanned and have consistent versions（including license compliance）

**12. Risk analysis about testing** 

- 针对需求/设计调整和细化延迟的风险，尽可能提前接入需求分析，准备用例；
- 针对测试人员不足的风险，尽可能发挥现有人员的积极性，加强培训，提高自动化测试占比，提高效率。

**13. Test Summary** 

- Test Report（最后一次迭代测试执行完毕后，出具测试报告，含Known Issues&Defects，为项目发布服务）
- Test Work Summary（总结每位组员贡献，分享经验和不足，提出工作优化建议）

**Appendix. [Test category reference](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20Category/Edgeggallery_Test_Cateory.md)** 

