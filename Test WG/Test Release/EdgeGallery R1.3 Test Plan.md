**1. Prerequisites** 

- [release milestone]()
- [requirement list-epic](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&milestone_id=126164)
- [requirement list-story](https://gitee.com/OSDT/dashboard/issues?issue_type_id=199539&milestone_id=126164)

**2. Test target** 
    
     - Quality Index for R1.3

|指标|目标值|备注|
|---|---|---|
|需求完成率|90%|实际完成的需求个数/计划完成的需求个数|
|遗留DI|10|1、本迭代发现的不影响本迭代只影响下个迭代的问题，也要计入本迭代的遗留DI中；2、本迭代发布时说明遗留问题的影响及对策，版本经理同意后可特例发布；3、遗留问题不允许超2个版本周期；"	∑(不重要及以下问题个数*0.1+次要问题个数*1+主要问题个数*3+严重问题个数*10)
|主要及以上问题数|≤1	|主要≤1，严重≤0;∑(主要问题个数+严重问题个数)|
|开源软件禁选|0|开源软件禁选个数为0|
|代码静态检查告警|0|代码静态检查告警个数（findbugs、stylecheck、dependency check、PWD）|
|测试用例执行率|90%|计划执行的测试用例数/实际执行的测试用例数|
|单元测试代码覆盖率|	>=65%|


**3. Test Scope** 


 **3.1 预测试需求** 

-  **【HealthCheck】各Edge收集Applcm、mep健康信息--hmx【ok】** 
-  **【HealthCheck】Edge节点间交互收集其他节点健康信息--hmx【ok】** 
-  **【HealthCheck】根据edge交互情况投票总结此次健康检查--hmx【ok】** 
-  **【HealthCheck】各边缘节点初始化--hmx【ok】** 
-  **【HealthCheck】MECM层调用HealthCheck--hmx【ok】** 
-  【Test】API并行调用性能测试--张阿利、李媛媛*
-  【Test】故障注入测试--孟璞辉

 **3.2 第一次迭代测试需求** 

-  **【developer】应用开发者新建项目后可编辑项目信息  --李媛媛** 【ok】
-  **【developer】容器项目详情界面优化重构  --李媛媛** 【ok】
-  **【developer】虚机部署调测界面优化  --李媛媛** 【ok】
-  **【developer】应用开发者在虚机部署调测结束后，可以通过资源释放接口释放资源  --李媛媛** 【ok】
-  **【developer】虚机镜像文件支持分片上传至文件系统  --李媛媛** 【ok】
-  **【developer】项目名称支持中文，前台放开中文限制，后台应用包逻辑需要修改  --李媛媛** 【ok】
-  **【developer】普通用户和管理员可以通过系统-镜像管理-容器镜像管理进行增删改查容器镜像  --李媛媛** 【ok】
-  **【developer】eg-view组件使用指南，界面框架实现  --李媛媛** 【ok】
-  **【developer】eg-view组件使用指南，组件文档数据解析&展示  --李媛媛** 【ok】
-  **【developer】归一化组件  --李媛媛** 【ok】
-  **【developer】系统-沙箱管理页面优化  --李媛媛** 【ok】
-  **【developer】系统-系统镜像管理，页面添加容器镜像管理，页面排版优化  --李媛媛** 【ok】
-  **【ATP】管理面配置管理页面开发  --李媛媛** 【ok】
-  **【ATP】管理面配置管理后台接口开发  --李媛媛** 【ok】
-  **【ATP】用户支持上传自测报告  --李媛媛** 【ok】
-  **【ATP】EG报告是生成的+自测报告的整合，并支持下载  --李媛媛** 【ok】
-  **【ATP】后台支持保存用户上传的自测报告  --李媛媛** 【ok】
-  **【ATP】测试用例页面适配配置管理的修改  --李媛媛** 【ok】
-  **【ATP】测试用例接口适配配置管理的修改  --李媛媛** 【ok】
-  **【ATP】新增任务删除接口  --李媛媛** 【ok】
-  **【AppStore】接口响应规范化  --- 前台提示优化     --张阿利** 【ok】
-  **【AppStore】删除应用同时删除其测试报告、可推送应用列表中数据   --张阿利** 【ok】
-  **【AppStore】虚机应用在线体验   --张阿利** 【ok】
-  **【AppStore】appstore首页优化   --张阿利** 【ok】
-  **【AppStore】应用共享主界面优化   --张阿利** 【ok】
-  **【AppStore】我的应用界面优化   --张阿利** 【ok】
-  **【AppStore】下载包含镜像的应用包优化   --张阿利** 【ok】
-  **【YUAN】边缘节点展示前端 --张阿利** 【ok】
-  **【YUAN】附近的边缘节点前端 --张阿利** 【ok】
-  **【YUAN】后端与网页端MECM打通 --张阿利** 【ok】
-  **【EGMobile】后台服务框架搭建，支持restful接口和资源文件管理 --张阿利** 【ok】
-  **【MECM】Supports Metric data display and collection（CPU & MEM）   --张阿利** 【ok】
-  **【MEP】统计MEP接口调用方数据，供审计使用 -- xiaolong** 【ok】
-  **【MEP】Support confirm ready task --xiaolong** 【ok】
-  **【installer】eguser用户权限不足，导致无法写入log等文件 --许丹** 【ok】

 **3.3 第二次迭代测试需求** 

- 【developer】容器项目详情界面优化重构  --李媛媛【ok】
- 【developer】虚机部署调测界面优化  --李媛媛【ok】
- 【developer】应用开发者部署调测时，使用当前openstack默认主机组，应用发布时，选择界面填写的主机组配置  --李媛媛【ok】
- 【developer】管理员提供不同系统的虚机基础镜像  --李媛媛【ok】
- 【developer】【管理员权限】系统-能力中心，新增服务弹框中添加功能：输入在线体验url、增加图片上传功能  --李媛媛【ok】
- 【developer】【管理员权限】系统-能力中心管理列表，增加编辑功能  --李媛媛【ok】
- 【developer】应用发布页面的“应用规则配置、服务发布配置”回显问题  --李媛媛【ok】
- 【developer】部署调测界面，方式二改为选择已上传的镜像  --李媛媛【ok】
- 【developer】容器镜像分片上传、支持取消上传  --李媛媛【ok】
- 【developer】归一化组件  --李媛媛【ok】
- 【Developer】Developer修改使用applcm v2接口  --李媛媛【ok】
- 【developer】资源配置界面优化  --李媛媛【ok】
- 【developer】部署调测界面优化-步骤一/二/三  --李媛媛【ok】
- 【developer】应用发布界面优化-步骤1/2/三  --李媛媛【ok】
- 【AppStore】APPD转换工具集成到AppStore --张阿利【ok】
- 【AppStore】虚机应用镜像上传优化 --张阿利【ok】
- 【AppStore】应用仓库界面优化 --张阿利【ok】
- 【AppStore】文档界面优化 --张阿利【ok】
- 【AppStore】appstore系统管理界面优化 --张阿利【ok】
- 【AppStore】Appstore部署时将应用包路径挂载到宿主机上   --张阿利【ok】
- 【ATP】后台接口响应规范优化  --李媛媛【ok】
- 【ATP】前台接口响应规范优化  --李媛媛【ok】
- 【MEP】Service support multiple-endPointInfo--xiaolong【ok】
- 【MECM】Edge Autonomous Portal supports account login access control【ok】
- 支持FS管理应用生命周期（Support docking FS management application life cycle）【ok】
- 【HealthCheck】各Edge收集Applcm、mep健康信息 --minxi【ok】
- 【HealthCheck】Edge节点间交互收集其他节点健康信息--minxi【ok】
- 【installer】EG平台支持版本升级场景的数据持久化--liqiang【ok】
-  **【EGMobile】后台服务支持admin上传图片和视频等资源数据 --阿利【ok】** 
-  **【EGMobile】后台服务支持所有用户通过url的方式查看已经上传资源数据--阿利【ok】** 
-  **【EGMobile】后台服务支持admin根据资源ID删除资源文件--阿利【ok】** 
-  **【EGMobile】后台服务支持admin创建event事件--阿利【ok】** 
-  **【EGMobile】后台服务支持admin修改event事件--阿利【ok】** 
-  **【EGMobile】后台服务支持admin删除event事件--阿利【ok】** 
-  **【EGMobile】后台支持tenant对event进行点赞/转发的操作--阿利【ok】** 【收藏转入R1.4】
-  **【EGMobile】后台服务支持所有用户去查看已经发布出去的event事件--阿利【ok】** 
-  **【EGMobile】后台服务支持admin去查看所有的published/draft类型的event事件--阿利【ok】** 
-  **【EGMobile】后台服务支持所有用户去查看已经上线的app应用--阿利【ok】** 
-  **【EGMobile】后台服务支持所有用户对app列表进行过滤和排序，并支持分页查找--阿利【ok】** 
-  【YUAN】用户通过手机获取进行验证码注册  --张阿利【ok】
- 【YUAN】用户登录支持密码登录以及验证码登录  --张阿利【ok】
- 【YUAN】用户注册登录界面前端开发 --张阿利【ok】


 **3.4 第三次迭代测试需求** 

- 【AppStore】应用包签名   --张阿利【ok】
- 【MECM】统一MECM组件风格--张阿利【ok】
- 【MECM】Modification of the interface of each module of MECM 2.0 version   --张阿利【ok】
- 【MECM】接口响应规范优化-APPLCM-ContainerAPI   --张阿利【ok】
- 【HealthCheck】MECM层调用HealthCheck--minxi【ok】
- 【HealthCheck】各边缘节点初始化--minxi【ok】
- 【HealthCheck】根据edge交互情况投票总结此次健康检查--minxi【ok】
- 【EGMobile】后台服务支持所有用户去查看已经上线的app应用--张阿利【ok】
- 【EGMobile】后台服务支持所有用户对app列表进行过滤和排序，并支持分页查找--张阿利【ok】
- 【EGMobile】后台服务提供用户注册接口--张阿利【ok】
- 【EGMobile】后台支持支持不同权限用户的权限控制--张阿利【ok】
- 【EGMobile】手机端应用的数据填充--张阿利【ok】
- 【developer】应用开发者部署调测时，通过VNC远程登录到容器内部--李媛媛【to be tested】
- 【MEP】MEC application graceful termination/stop--xiaolong



**4. Test Case Management** 

A Test Case contains: ID, purpose, test tools, Prerequisites, test data, test steps, desired Test Results, actual test Results, Test Verdict.
测试用例包括：ID，测试目的，测试工具，前置条件，测试数据，测试步骤，预期结果，实际结果，测试结果等。

- 测试用例ID：EG-[Module ID]-TC-ID(ID三位数，从001开始)。
- 测试结果：pass/fail。

测试用例ID中Module ID指对应软件模块的缩写，如MEP、MECM、APPSTORE、DEV等；同时测试用例关联对应epic/story。

测试用例均在Gitee上创建并管理，关联EPIC任务。在测试用例标题中填入ID和测试目的。具体参见[操作指南](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_case_template.md)。

测试用例评审：召集社区的研发、测试等相关人员评审测试用例，确保测试用例有效、多方理解一致且覆盖版本需求。评审后，调整补充完善测试用例。可针对不同epic或不同模块分别组织评审会，每次会议不超30分钟。

**5. 缺陷管理 Bug Management** 

缺陷统一在Gitee中录入，点击[操作指南](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md)。

bug优先级为：严重、主要、次要、不重要、无优先级。

严重bug尽可能1个工作日内解决（非强制，需协商）；
主要bug尽可能3个工作日内解决（非强制，需协商）；
次要bug尽可能5个工作日内解决（非强制，需协商）。

测试执行阶段，每周（后期每半周）邮件发送bug清单汇总，并对超期未关闭bug重点提醒。

- 针对测试人员提的bug，测试人员跟踪目标研发人员修复，完成修复后，测试人员回归验证并通过后设定bug状态为“已验收”。
- 针对于研发人员自己提的bug，建议bug修复后PTL协调研发人员交叉验证，通过后设置bug状态为“已验收”。
- 针对访客提的bug，由PTL安排人员跟踪修复，研发人员回归验证后设置bug为“已验收”。

版本发布后，针对测试人员/研发人员/访客提的bug，标签为已发布的版本号，如果研发人员确认该bug在下一版本修订，则将该bug同时打上下一版本号的标签，便于跟踪。针对已发布版本中新发现的bug，研发人员及时跟踪。


**6. relative Tools** 

|Tool Name|Version|Address | comments  |
|---|---|---|---|
|SonarQube|8.4.2 |http://159.138.132.188:9000/projects  ||
|Jacoco|0.7.7.201606060606  |None |   |
|go test| 1.15.5 |None |   |

**7. Test development info** 

| tools&Language | version | remark | link |
|----------------|---------|--------|------|
| Robotframework | v3.2.1        |        |      |
| selenium       | v3.141.0        |        |      |
| python         | v3.7.2        |        |      |
| IDE ride       | v1.7.4.2        |        |      |
| IDE pycharm    |         |        |      |
| visual studio code(user)    | 1.48.0        |        |      |

**8. Test env info** 

| infra  | Version | address | remarks |
|--------|---------|---------|---------|
| K8S    | v1.18.4 |         |         |
| k3s    | v1.18.4 |         |         |
| docker | v19.03.12|         |         |
| helm   | v3.2.4  |         |         |
| ....   | ......  |         |         |


    - Jenkins addr：http://jenkins.edgegallery.org/
    - repo
        - [integration-testing](https://gitee.com/EdgeGallery_group/integration-testing)()
        - [ci-management](https://gitee.com/edgegallery/ci-management)
        - [platform-mgmt](https://gitee.com/EdgeGallery_group/platform-mgmt)
        - [helm-charts](https://gitee.com/EdgeGallery_group/helm-charts)
    
    - Reference env
        - daily build jenkins：daily.developer.edgegallery.org;daily.appstore.edgegallery.org;daily.mecm.edgegallery.org
        - Weekly build Jenkins:weekly.developer.edgegallery.org;weekly.appstore.edgegallery.org;weekly.mecm.edgegallery.org
        - ......
    - Lab
        - 5GC env + Integration test；
        -

基于测试环境选定的软件版本，在社区CI/CD环境上完成测试环境搭建相关配置，自动搭建测试目标，以支撑自动测试(开发相应自动化测试脚本)或人工测试。

**9. Test Planning** 

| 时间  | 迭代研发  | 迭代测试  |
|---|---|---|
|0726-0815   | 第一次迭代  | 预测试  |
|0816-0829   | 第二次迭代  |第一次   |
|0830-0912   | 第三次迭代  |第二次  |
|0913-0926   | 修改bug  |第三次   |
|0927-0930   | 准备并发布  |准备并发布   |

针对测试人员，在第i次迭代测试开始前（即第i-1次测试过程中），需同研发人员澄清本轮测试需求，完成测试用例设计及评审；在第i次迭代测试执行中需执行所设计的测试用例（含自动化测试脚本），记录bug（及提出需求优化建议等），同时完成第i-1轮bug的回归测试。

**10. CI/CD Configuration Plan** 

**11. Criteria for Iterative Test Execution** 

    - Starting Criteria for Iterative test excution
        - 完成本次迭代的详细设计/架构更新Complete detail design/architecture updating for current iteration        
        - PTL确认所有task关闭（PTL confirm ）all tasks in current iteration are closed
        - PTL确认所有新开发代码都有自动化测试用例（PTL）confirm all recently developed code has autotest cases
        - PTL确认项目完成开发者自验证（PTL confirm）complete self-verification of project （including bugs from gitee visitors）
        - 近1天镜像自动构建成功率100% daily build for last 1 days is all succeeded【First Iteration】
        - 近1天镜像CICD成功率100% daily build for last 1 days is all succeeded【Second Iteration】  
        - 近1天镜像CICD成功率100%（含自动测试） daily build for last 1 days is all succeeded【Third Iteration】
    
    - Ending Criteria for test execution in Final Iteration
    
        - PTL确认bug单关闭（PTL confirm） vital/blocked bugs are all closed
        - 所有测试活动完整，测试通过 all test activities are completed and test results are passed
        - 无阻塞性及严重性问题遗留 No blocked or vital problems/bugs are left。
        - 确认所有依赖软件都在本地库中 confirm all dependent softwares are all stored locally
        - PTL确认满足单元测试覆盖率>=50%要求（PTL confirm） satisfy code coverage rate for unit test
        - PTL确认三方件归一化扫描结果（PTL confirm） 3rd-party components  are all scanned and have consistent versions（including license compliance）

**12. Risk analysis about testing** 

- 针对需求/设计调整和细化延迟的风险，尽可能提前接入需求分析，准备用例；
- 针对测试人员不足的风险，尽可能发挥现有人员的积极性，加强培训，提高自动化测试占比，提高效率。

**13. Test Summary** 

- Test Report（最后一次迭代测试执行完毕后，出具测试报告，含Known Issues&Defects，为项目发布服务）
- Test Work Summary（总结每位组员贡献，分享经验和不足，提出工作优化建议）

**Appendix. [Test category reference](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20Category/Edgeggallery_Test_Cateory.md)** 

