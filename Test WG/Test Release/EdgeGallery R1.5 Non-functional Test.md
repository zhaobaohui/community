**1. Prerequisites** 

- [release milestone]()
- [requirement list-epic](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bissue_type_id%5D=238024)
- [requirement list-story](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bissue_type_id%5D=199539&is%5Blabel_ids%5D=86836775%2C86836832%2C86836889&openMore=1)

- 需要相关的测试工具的配合和支持
- EdgeGallery R1.5.1已成功部署

**2. Test target** 
   
- 确定软件/接口发生故障的极限；
- 确定测试对象在给定时间内能够持续处理的最大负载或工作量；
- 确定性能迅速降低的原因及建议；
- 确定垃圾数据/资源产生原因及建议；

**3. Test Scope** 



- 1、接口并发性/压力测试；（developer/appstore/mecm/孵化流水线后台接口为主；vm操作接口） 
-  **2、环境不稳定，环境崩溃，提供合理的分析建议（vm拉起很慢；拉起时获取容器状态较慢；拉起后，ssh登录vm很慢） ** 
- 3、vm镜像瘦身时，上传镜像大小需设置上限，多大的镜像能够瘦身成功，否则镜像太大可能导致瘦身失败 
-  **4、测试后对磁盘产生的垃圾文件，内存是否及时释放；及时清理**  
-  **5、容器场景下，namespace经常清理不掉，产生垃圾数据，导致后续namespace冲突等；--需要同研发讨论**  
- 6、很多接口做了限流（通过什么方式？website-gateway做的限流（可配置），防止暴力破解），出现too many request返回，无法压测--同研发确认 
-  **7、developer或mecm部署时，applcm打开文件过多的错误--打开的文件句柄过多，下个版本对提交文件句柄上限** 

【粗体部分优先解决】


**4. Test Case Management/缺陷管理Bug Management** 

测试用例/缺陷管理遵循EdgeGallery R1.5测试计划里的用例/缺陷管理方法。


**5. relative Tools** 

|Tool Name|Version|Address | comments  |
|---|---|---|---|
|SonarQube|8.4.2 |http://159.138.132.188:9000/projects  ||
|Jacoco|0.7.7.201606060606  |None |   |
|go test| 1.15.5 |None |   |

**6. Test Planning** 

| 时间  | 测试内容  | 测试人员  |
|---|---|---|
|0221-0306   | 接口压力测试  | xx  |
|0221-0306   | 环境稳定性测试  | xx  |
|0221-0306   | vm瘦身测试  | xx  |
|0221-0306   | 垃圾文件测试  | xx  |
|0221-0306   | 垃圾namespace测试  | xx  |
|0221-0306   | applcm文件句柄压力测试  | xx  |



 **7、环境稳定性测试** 

7.1 预期性能指标


7.2 测试目的

7.3 测试场景/方法/用例

7.4 测试过程和结果


 **8、垃圾文件测试** 

8.1 预期性能指标


8.2 测试目的

8.3 测试场景/方法/用例

8.4 测试过程和结果

 **9、垃圾namespace测试** 

9.1 预期性能指标


9.2 测试目的

9.3 测试场景/方法/用例

9.4 测试过程和结果

 **10、applcm文件句柄压力测试** 

10.1 预期性能指标


10.2 测试目的

10.3 测试场景/方法/用例

10.4 测试过程和结果




**11. Test Report** 


