一、引言Introduction

1.1 编写目的Writing Purpose

本测试报告为EdgeGallery Release1.2的测试报告，在于总结测试阶段的测试情况以及分析结果，描述预定需求的符合性及预定质量指标的符合性。作为测试质量参考文档提供给开源社区开发人员、测试人员、管理人员及感兴趣的其他人员等参考。

1.2 项目背景 Project Background

EdgeGallery社区聚焦5G边缘计算场景，通过开源协作构建起MEC边缘的资源、应用、安全、管理的基础框架和网络开放服务的事实标准，并实现同公有云的互联互通。在兼容边缘基础设施异构差异化的基础上，构建起统一的MEC应用生态系统。

1.3 [系统简介 System Overview](https://gitee.com/edgegallery/docs/blob/master/Get%20Started/Start%20from%20A%20Demo%20on%20EdgeGallery.md)

1.4 参考资料 References

1.Release V1.2需求

- [R1.2 EPIC](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&milestone_id=101482)
- [R1.2 Story](https://gitee.com/OSDT/dashboard/issues?issue_type_id=199539&milestone_id=101482)
- [R1.2需求文档](https://gitee.com/edgegallery/community/tree/master/Architecture%20WG/Requirements/v1.2)

2.[Release V1.2测试计划（含版本质量指标）](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20Release/EdgeGallery%20R1.2%20Test%20Plan.md)

二、测试概要 Test Overview

2.1 测试用例及脚本设计 Test Cases and Auto test scripts

设计测试用例共计108个。
请点击[测试用例清单](https://gitee.com/OSDT/dashboard/issues?issue_type_id=232575&milestone_id=101482)查看详情。

2.2 测试环境与配置 Test Env and related Configuration
| server ip |Usage   | password |
|-----------|---|----------|
| 119.8.36.45 | 中心  | xxxx    |


2.3 测试方法和工具 Test Method and Tools

a.单元测试代码覆盖率工具：Jacoco（Java语言）、GoTest（Go语言）
【基于当前实际情况，本次版本前端代码不做单元测试及代码覆盖率检查，改为界面检视】
  
代码覆盖率测试结果分析（SonarQube）：http://159.138.132.188:9000/projects?sort=-analysis_date

|Tool Name|Version|Address | comments  |
|---|---|---|---|
|SonarQube|8.7 |http://159.138.132.188:9000/projects  ||
|Jacoco|0.7.7.201606060606  |None |   |
|go test| 1.15.5 |None |   |

b.CICD工具Jenkins：http://119.8.34.36:8080/

c.镜像库：https://www.edgegallery.org/download/otherversion/

三、测试结果及缺陷分析 Test Results and Defects Analysis

3.1 测试执行情况及记录Test exceuction and Records

3.1.1 测试组织 Test Organizing

测试组负责人：刘辉

测试人员：侯敏熙、扈冰、许丹、李媛媛、张阿利、孟璞辉、于小龙

配置管理人员：许丹

3.1.2 测试时间（各负责人daily更新） Test Schedule

第一轮迭代测试2021年5月24日至6月6日。

- 【developer】支持input参数补齐  ——李媛媛 done
- 【developer】虚机镜像管理（用户，管理员权限）  ——李媛媛 done
- 【Developer】首页界面优化重构  ——李媛媛 done
- 【developer】虚机资源配置可编辑  ——李媛媛 done
- 【UserMgmt】登录的前端滑块验证方式优化为后台验证码   ——李强 done
- 【UserMgmt】发送验证码到用户手机或邮箱的前端滑块验证方式优化为后台验证码  ——李强  done
- 【UserMgmt】用户注册支持与后台交互的验证  ——李强 done
- 【ATP】ATP首页优化  ——李媛媛 done
- 【ATP】后台新增批量导入包解析接口  ——李媛媛 done
- 【ATP】前台新增批量导入入口  ——李媛媛 done
- 【ATP】后台新增贡献批量删除、脚本类型贡献下载接口  ——李媛媛 done
- 【ATP】前台新增贡献管理页面  ——李媛媛 done
- 【ATP】关键数据持久化——李媛媛 done
- 【ATP】测试任务页面优化——李媛媛 done
- 【AppStore】关键数据持久化  ——张阿利 done
- 【AppStore】操作分析界面优化  ——张阿利 done
- 【AppStore】文档界面优化  ——张阿利 done
- 【AppStore】上传应用时可设置应用展示模式  ——张阿利 done
- 【AppStore】支持修改应用属性  ——张阿利 done
- 【AppStore】支持显示应用部署类型虚机还是容器  ——张阿利 done
- 【MECM】边缘自治增强--mep合并到mepm  ——张阿利 done
- 【MECM】MECM后台统一servicecomb  ——张阿利 done
- 【MECM】MECM支持前台节点位置可自定义  ——张阿利 done

 第二轮迭代测试2021年6月7日至6月20日。

- 【Developer】统一Develop组件风格【poc】--李媛媛 done
- 【developer】支持配置app_configuration, 支持定义AK/SK, APP分流规则，DNS规则等--李媛媛 done
- 【developer】虚机资源配置可编辑--李媛媛 done
- 【developer】虚机镜像管理（用户，管理员权限）--李媛媛 done
- 【Developer】能力中心界面优化重构--李媛媛 done
- 【AppStore】应用管理界面  ——张阿利 done
- 【appstore】appstore支持虚机镜像的管理以及上传下载  ——张阿利 done
- 【ATP】用户面页面优化--李媛媛 done
- 【ATP】测试用例增强--李媛媛 done
- 【ATP】测试模型页面优化--李媛媛 done
- 【UserMgmt】个人帐号中心展示我的权限(角色+可访问平台)    ——yxl done
- 【UserMgmt】支持非guest用户90天后必须修改密码    ——yxl done
- 【UserMgmt】支持admin首次登录成功后强制修改密码     ——yxl done
- 【MECM】首页展示优化  ——张阿利 done
- 【MECM】MECM支持applcm和apprulemgr注册统一--lh done
- 【installer】支持docker-compose直接从镜像部署EG，不依赖k8s--xd、张阿利
- 【installer】ansible安装脚本强化与问题解决--xd（自动化测试）done


第三轮迭代测试2021年6月21日至7月4日。


- 【AppStore】APPD转换器【poc】 --阿利 done
- 【AppStore】接口响应规范优化 --阿利 done
- 【Atp】接口响应规范优化 --媛媛 done
- 【MEP】接口响应规范优化 --阿利 done
- 【AppStore】应用在线体验 --阿利 done
- 【ATP】测试模型页面优化 --媛媛 done
- 【ATP】测试任务页面优化 --媛媛 done
- 【developer】input根据不同的操作系统，导入不同的初始化脚本 --媛媛 done
- 【developer】虚机mepagent的集成 --媛媛 done
- 【MECM】mecm支持虚机APP的实例化和终止 VM APP instantiation and termination support.--阿利 done
- 【MECM】mecm支持VIM预配置 MECM support the preconfig of OpenStack --阿利 done
- 【MECM】MECM支持虚机APP包同步和分发，MECM support app package management of VM app--阿利 done
- 【ATP】ATP支持虚机APP的实例化和终止用例 --媛媛 done
- 【MEP】timing api ——张阿利 done
- 【MEP】transport api ——张阿利 done
- 【MEP】关键数据持久化(移入迭代三) ——张阿利 done
- 【MEP】 Code quality improvement(移入迭代三) ——张阿利 done
- 【MEP】MEP adapter register to EdgeGallery MEP and UPF(移入迭代三) ——张阿利 done
- 【MEP】Public the UPF capability to EdgeGallery(移入迭代三) ——张阿利 done
- 【MEP】mep-agent实现订阅接口(移入迭代三) ——张阿利 done
- 【MEP】服务发现增加鉴权认证(移入迭代三) ——张阿利 done


3.1.3 测试用例执行统计 analysis of Test Case execution

|需求点|TC总数|成功个数|失败个数|未执行个数|TC成功率|
|--|--|--|--|--|--|
| developer | 14| 14 | 0 | 0 | 100% |
| appstore | 44 |44  |0  |0  | 100% |
| usermgmt | 10 |10 | 0 |0  | 100% |
| mecm|18 |18  | 0 | 0 | 100% |
| ATP | 14 | 14 | 0 | 0 | 100% |
| MEP |8 | 8 | 0 | 0 | 100% |


3.2 覆盖分析 Coverage Analysis

3.2.1 需求覆盖 Requirement Coverage

软件实现100%覆盖Release V1.2需求。

3.2.2 单元测试代码覆盖（数据源于SonarQube） Code Coverage for Unit Test

根据TSC20200923会议决定，本次测试版本依然不考虑前端代码覆盖率，关注后台代码覆盖率。
所有数据来自于北京时间 2021.7.x 17:00 [SonarQube页面查询结果](http://159.138.132.188:9000/projects?sort=name)。

| 模块                      | 代码覆盖率 |
|---------------------------|-----------|
| appstore-be               | 60.5%     |
| atp-be                    | 63.1%     |
| developer-be              | 60.5%     |
| mecm-apm                  | 61.2%     |
| memc-applcm-k8splugin     | 62.2%     |
| memc-applcm-lcmcontroller | 63.0%     |
| memc-applcm-osplugin      | 73.1%     |
| mecm-appo                 | 62.6%     |
| mecm-apprulemgr           | 62.1%     |
| mecm-inventory            | 60.8%     |
| mep-agent                 | 60.8%     |
| mep-dnsserver             | 70.0%     |
| mep-mepauth               | 62.9%     |
| mep-mepserver             | 60.6%     |
| plugins                   | 70.2%     |
| toolchain                 | 0%        |
| user-mgmt-be              | 61.5%     |
| website-gateway           | 23.4%     |


**Note:** 

1. website-gateway代码覆盖率低的解释：website-gateway本身是一个框架工程只有200多行代码，没有业务逻辑，而且大部分代码都是实现spring的配置类，ut运行不到。目前我们只能对其中的login和logout接口做测试，所以覆盖率达不到60%。
2. toolchain代码覆盖率低的解释：由于toolchain项目目录结构调整，导致统计出错，显示0%。toolchain后台是集成的代码分析工具，只做了接口调用的封装，业务逻辑少，所以1.1版本覆盖率没有到达60%，由于功能简单，1.2版本并没有代码开发。后期也不会有功能开发，所以维持现状，后期也不会补充。

3.3 缺陷统计及分析 Statistical analysis of Defects/Bugs

请点击[缺陷清单](https://gitee.com/OSDT/dashboard/issues?issue_type_id=199540&milestone_id=101482)查看详情。

3.3.1 缺陷汇总 Defects Summary

已剔除 3个无效bug后，总缺陷数量： 186（含 18个安全类缺陷），已解决数量： 175个；未解决bug 8个（含8 个known issue，遗留到下一版本解决）。

3.3.2 缺陷分析 Defects Analysis

按缺陷类型分析：安全类bug 18个；其他类bug 168个。
按缺陷严重级别分析：严重bug 13个；主要bug 60个；次要bug 79个；不重要bug 10个；不指定24 个。
按需求点分析
缺陷收敛性分析：总计186 个bug（含 3个无效bug；含安全类bug 18）；迭代一 345 个bug（含 0个无效bug）；迭代二 52 个bug（含 1个无效bug）；迭代三 67 个bug（含 2个无效bug）；未制定迭代22 个bug（含 0个无效bug）

3.3.3 经评审遗留缺陷及未解决问题 Residual Defects and known issues

请点击[遗留清单](https://gitee.com/OSDT/dashboard/issues?issue_type_id=199540&milestone_id=101482&issue_state_ids=826576)
非安全问题遗留清单：
| 序号  |BugID|Bug描述|状态|未解决说明|预计处理安排|
| --|--|--|--|--|--|
|1| I2ZWKL |[【user-mgmt】用户登录超时后显示用户还在登录状态](https://gitee.com/OSDT/dashboard/issues?id=I2ZWKL)  |遗留 | 下个版本解决 |  |
|2| I3DZKV |[【UserMgmt】出现平台不同步登录的问题（不必现）](https://gitee.com/OSDT/dashboard/issues?id=I3DZKV)  |遗留 | 下个版本解决 |  |
|3| I3Y9J2 |[【developer】上传镜像功能建议新增一个取消上传按钮](https://gitee.com/OSDT/dashboard/issues?id=I3Y9J2)  |遗留 | 1.3版本做优化，支持“取消上传”功能 |  | 
|4| I24GTE |[【developer】插件上传页面，‘上传API’功能为必填项，建议优化](https://gitee.com/OSDT/dashboard/issues?id=I24GTE)  |遗留 | 下个版本解决 |  | 
|5| I2BKIW |[【developer】插件详情内容为用户上传的文档内容](https://gitee.com/OSDT/dashboard/issues?id=I2BKIW)  |遗留 | 下个版本解决 |  | 
|6| I2BJ2A |[【developer】插件下载后，下载次数没有叠加（目前是在评分之后才会叠加）](https://gitee.com/OSDT/dashboard/issues?id=I2BJ2A)  |遗留 | 下个版本解决 |  | 
|7| I2416V |[【developer】grafana界面资源显示有问题](https://gitee.com/OSDT/dashboard/issues?id=I2416V)  |遗留 | 下个版本解决 |  |


3.4 安全测试执行情况及记录（安全工作组负责）

.
 **安全测试结果及分析详见v1.2安全测试报告**  ：[EdgeGallery v1.2 Security Test Report](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Test%20Result/Test%20result%20Release%20V1.2/EG%20v1.2%20Security%20Test%20Report.md)
.


EdgeGallery v1.2 版本安全测试包括 **安全合规测试** 和 **渗透测试** ，安全测试依照 **v1.2安全测试计划** 开展，详见[v1.2 Security Test Plan](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Test%20Plan/Release-R1.2/EG%20R1.2%20Security%20Test%20Plan%20.md)。

3.4.1 安全测试组织Security Test Organizing

安全测试由 **安全工作组(SecurityWG)** 执行

3.4.2 安全测试执行情况 Security Test Execution

测试时间： 2021.5.24 ~ 2021.7.2

测试环境： x.x.x.x(敏感信息，不予公开)  Center+Edge

测试项目： AppStore, Developer, User-mgmt, MECM，MEP，Apt

测试人员： 扈冰


测试内容： 本次测试共包括sprint1，sprint2，sprint3三个迭代。首先针对各Story匹配测试用例，然后从安全合规测试和渗透测试2个维度展开测试。测试内容详见 [v1.2安全测试计划](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Test%20Plan/Release-R1.2/EG%20R1.2%20Security%20Test%20Plan%20.md)  **第5节“测试范围”** 相关内容。


安全合规测试用例： [安全合规测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases)

渗透测试用例： [渗透测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases)



3.4.3 安全测试结果及分析 summary analysis for Security test result


v1.2版本新增安全类问题 **12个** 。详见：[v1.2安全问题单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=101482&is%5Bsearch%5D=security&is%5Bissue_type_id%5D=199540&is%5Bsort%5D=created_at&is%5Bdirection%5D=desc)


按 **漏洞类型** 统计：认证1个，鉴权1个，信息泄露 2个，DOS 3个，输入校验 2个，日志3个。

按 **严重性** 统计：0个严重，9个主要，3个次要，0个不重要。

按 **修复状态** 统计：11个已修复，1个遗留。

按 **所属模块** 统计：UserMgmt 3个， AppStore 0个， Developer 4个， MECM 2个， ATP 3个， MEP 0个。

按 **所属迭代** 统计：sprint1 3个， sprint2 3个， sprint3 6个。



3.4.4 遗留安全类问题 Leaving Security Issues


v1.2版本新增遗留安全问题1个，历史版本遗留安全问题3个。


安全问题遗留清单：
| 序号  | Issue编号 | Issue标题        | 状态    | 原因             | 完成计划             | 严重性           |
| ---- | -------- | ------------------ | --------- | ----------------------- | ------------------ | -------------- |
|1|I3XUNS|[【user-mgmt】【Security】user-mgmt pod中usr/app/log目录下没有日志文件](https://e.gitee.com/OSDT/issues/list?is%5Bissue_state_ids%5D=826576&issue=I3XUNS)| 遗留 |暂无解决方案| 遗留至下个版本 |重要 |
|2|I3DP2D|[【mecm】【Security】lcm portal should add login/auth function, or is weak of DOS attack](https://e.gitee.com/OSDT/issues/list?issue=I3DP2D)| 遗留 | 	边缘自治Portal未做认证模块，具体认证方案在v1.3讨论，计划1.3转需求| 遗留至下个版本 |紧急 |
|3|I28F84|[【Security Test】Developer插件上传功能存在重放攻击漏洞](https://e.gitee.com/OSDT/dashboard?issue=I28F84)| 遗留 | 	目前仅通过hash校验防止重放攻击，防护强度较弱。需要讨论更完善解决方案。| 遗留至下个版本 |重要 | 
|4|I23DY8|[【Security Test】mecm-postgresql;developer-fe ;user-mgmt-redis-0 项目默认登录用户为root用户](https://e.gitee.com/OSDT/dashboard?issue=I23DY8)| 遗留 | 	正在讨论解决方案| 遗留至下个版本 |重要 |


四、测试结论及建议 Test Conclusion and Suggestion

测试主要包括交付需求测试，分别从系统功能性、稳定性、易用性以及安全性（详见安全测试报告）方面进行测试，同时针对个别场景进行专项测试，问题单提单量总计186个，其中安全类问题18个，经评审遗留11个，其中安全测试问题4个，功能性问题7个。

- 需求完成率:EPIC(剔除1个无效epic）23/23=100%，story(剔除14个无效story后）78/78=100%, 满足版本发布质量要求。
- 未解决的严重问题<=1,满足版本发布质量要求。
- 遗留Defect Index，即DI=5* 0.1+5* 1+1*3=8.5<10，满足版本发布质量要求。
- 测试用例执行率，单元测试代码覆盖率等指标满足版本发布质量要求。



