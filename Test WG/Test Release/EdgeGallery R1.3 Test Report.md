一、引言Introduction

1.1 编写目的Writing Purpose

本测试报告为EdgeGallery Release1.3的测试报告，在于总结测试阶段的测试情况以及分析结果，描述预定需求的符合性及预定质量指标的符合性。作为测试质量参考文档提供给开源社区开发人员、测试人员、管理人员及感兴趣的其他人员等参考。

1.2 项目背景 Project Background

EdgeGallery社区聚焦5G边缘计算场景，通过开源协作构建起MEC边缘的资源、应用、安全、管理的基础框架和网络开放服务的事实标准，并实现同公有云的互联互通。在兼容边缘基础设施异构差异化的基础上，构建起统一的MEC应用生态系统。

1.3 [系统简介 System Overview](https://gitee.com/edgegallery/docs/blob/master/Get%20Started/Start%20from%20A%20Demo%20on%20EdgeGallery.md)

1.4 参考资料 References

1.Release V1.3需求

- [R1.3 EPIC](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=126164&openMore=1)
- [R1.3 Story](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=126164&is%5Bissue_type_id%5D=199539&is%5Blabel_ids%5D=86836775%2C86836832%2C86836889&openMore=1)
- [R1.3需求文档](https://gitee.com/edgegallery/community/tree/master/Architecture%20WG/Requirements/v1.3)

2.[Release V1.3测试计划（含版本质量指标）](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20Release/EdgeGallery%20R1.3%20Test%20Plan.md)

二、测试概要 Test Overview

2.1 测试用例及脚本设计 Test Cases and Auto test scripts

设计测试用例共计123个。
请点击[测试用例清单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=126164&is%5Bissue_type_id%5D=232575)查看详情。

2.2 测试环境与配置 Test Env and related Configuration
| server ip |Usage   | password |
|-----------|---|----------|
| 119.8.36.45 | 中心  | xxxx    |


2.3 测试方法和工具 Test Method and Tools

a.单元测试代码覆盖率工具：Jacoco（Java语言）、GoTest（Go语言）
【基于当前实际情况，本次版本前端代码不做单元测试及代码覆盖率检查，改为界面检视】
  
代码覆盖率测试结果分析（SonarQube）：http://159.138.132.188:9000/projects?sort=-analysis_date

|Tool Name|Version|Address | comments  |
|---|---|---|---|
|SonarQube|8.7 |http://159.138.132.188:9000/projects  ||
|Jacoco|0.7.7.201606060606  |None |   |
|go test| 1.15.5 |None |   |

b.CICD工具Jenkins：http://119.8.34.36:8080/

c.镜像库：https://www.edgegallery.org/download/otherversion/

三、测试结果及缺陷分析 Test Results and Defects Analysis

3.1 测试执行情况及记录Test exceuction and Records

3.1.1 测试组织 Test Organizing

测试组负责人：刘辉

测试人员：侯敏熙、扈冰、许丹、李媛媛、张阿利、孟璞辉、于小龙、李强

配置管理人员：许丹

3.1.2 测试时间（各负责人daily更新） Test Schedule

预测试2021年7月26日至8月15日
-  **【HealthCheck】各Edge收集Applcm、mep健康信息--hmx** 【ok】
-  **【HealthCheck】Edge节点间交互收集其他节点健康信息--hmx**【ok】 
-  **【HealthCheck】根据edge交互情况投票总结此次健康检查--hmx** 【ok】
-  **【HealthCheck】各边缘节点初始化--hmx** 【ok】
-  **【HealthCheck】MECM层调用HealthCheck--hmx** 【ok】
-  **【EGMobile】EdgeGallery移动端开发(移入迭代三)--张阿利**【ok】 
-  【Test】API并行调用性能测试--张阿利、李媛媛*
-  【Test】故障注入测试--孟璞辉

第一轮迭代测试2021年8月16日至8月29日。

-  **【developer】应用开发者新建项目后可编辑项目信息  --李媛媛** 【ok】
-  **【developer】容器项目详情界面优化重构  --李媛媛** 【ok】
-  **【developer】虚机部署调测界面优化  --李媛媛** 【ok】
-  **【developer】应用开发者在虚机部署调测结束后，可以通过资源释放接口释放资源  --李媛媛** 【ok】
-  **【developer】虚机镜像文件支持分片上传至文件系统  --李媛媛** 【ok】
-  **【developer】项目名称支持中文，前台放开中文限制，后台应用包逻辑需要修改  --李媛媛** 【ok】
-  **【developer】普通用户和管理员可以通过系统-镜像管理-容器镜像管理进行增删改查容器镜像  --李媛媛** 【ok】
-  **【developer】eg-view组件使用指南，界面框架实现  --李媛媛** 【ok】
-  **【developer】eg-view组件使用指南，组件文档数据解析&展示  --李媛媛** 【ok】
-  **【developer】归一化组件  --李媛媛** 【ok】
-  **【developer】系统-沙箱管理页面优化  --李媛媛** 【ok】
-  **【developer】系统-系统镜像管理，页面添加容器镜像管理，页面排版优化  --李媛媛** 【ok】
-  **【ATP】管理面配置管理页面开发  --李媛媛** 【ok】
-  **【ATP】管理面配置管理后台接口开发  --李媛媛** 【ok】
-  **【ATP】用户支持上传自测报告  --李媛媛** 【ok】
-  **【ATP】EG报告是生成的+自测报告的整合，并支持下载  --李媛媛** 【ok】
-  **【ATP】后台支持保存用户上传的自测报告  --李媛媛** 【ok】
-  **【ATP】测试用例页面适配配置管理的修改  --李媛媛** 【ok】
-  **【ATP】测试用例接口适配配置管理的修改  --李媛媛** 【ok】
-  **【ATP】新增任务删除接口  --李媛媛** 【ok】
-  **【AppStore】接口响应规范化  --- 前台提示优化     --张阿利** 【ok】
-  **【AppStore】删除应用同时删除其测试报告、可推送应用列表中数据   --张阿利** 【ok】
-  **【AppStore】虚机应用在线体验   --张阿利** 【ok】
-  **【AppStore】appstore首页优化   --张阿利** 【ok】
-  **【AppStore】应用共享主界面优化   --张阿利** 【ok】
-  **【AppStore】我的应用界面优化   --张阿利** 【ok】
-  **【AppStore】下载包含镜像的应用包优化   --张阿利** 【ok】
-  **【YUAN】边缘节点展示前端 --张阿利** 【ok】
-  **【YUAN】附近的边缘节点前端 --张阿利** 【ok】
-  **【YUAN】后端与网页端MECM打通 --张阿利** 【ok】
-  **【EGMobile】后台服务框架搭建，支持restful接口和资源文件管理 --张阿利** 【ok】
-  **【MECM】Supports Metric data display and collection（CPU & MEM）   --张阿利** 【ok】
-  **【MEP】统计MEP接口调用方数据，供审计使用 -- xiaolong** 【ok】
-  **【MEP】Support confirm ready task --xiaolong** 【ok】
-  **【installer】eguser用户权限不足，导致无法写入log等文件 --许丹** 【ok】


 第二轮迭代测试2021年8月30日至9月12日。

- 【developer】容器项目详情界面优化重构  --李媛媛【ok】
- 【developer】虚机部署调测界面优化  --李媛媛【ok】
- 【developer】应用开发者部署调测时，使用当前openstack默认主机组，应用发布时，选择界面填写的主机组配置  --李媛媛【ok】
- 【developer】管理员提供不同系统的虚机基础镜像  --李媛媛【ok】
- 【developer】【管理员权限】系统-能力中心，新增服务弹框中添加功能：输入在线体验url、增加图片上传功能  --李媛媛【ok】
- 【developer】【管理员权限】系统-能力中心管理列表，增加编辑功能  --李媛媛【ok】
- 【developer】应用发布页面的“应用规则配置、服务发布配置”回显问题  --李媛媛【ok】
- 【developer】部署调测界面，方式二改为选择已上传的镜像  --李媛媛【ok】
- 【developer】容器镜像分片上传、支持取消上传  --李媛媛【ok】
- 【developer】归一化组件  --李媛媛【ok】
- 【Developer】Developer修改使用applcm v2接口  --李媛媛【ok】
- 【developer】资源配置界面优化  --李媛媛【ok】
- 【developer】部署调测界面优化-步骤一/二/三  --李媛媛【ok】
- 【developer】应用发布界面优化-步骤1/2/三  --李媛媛【ok】
- 【AppStore】APPD转换工具集成到AppStore --张阿利【ok】
- 【AppStore】虚机应用镜像上传优化 --张阿利【ok】
- 【AppStore】应用仓库界面优化 --张阿利【ok】
- 【AppStore】文档界面优化 --张阿利【ok】
- 【AppStore】appstore系统管理界面优化 --张阿利【ok】
- 【AppStore】Appstore部署时将应用包路径挂载到宿主机上   --张阿利【ok】
- 【ATP】后台接口响应规范优化  --李媛媛【ok】
- 【ATP】前台接口响应规范优化  --李媛媛【ok】
- 【MEP】Service support multiple-endPointInfo--xiaolong【ok】
- 【MECM】Edge Autonomous Portal supports account login access control【ok】
- 支持FS管理应用生命周期（Support docking FS management application life cycle）【ok】
-  **【HealthCheck】各Edge收集Applcm、mep健康信息 --minxi【ok】** 
-  **【HealthCheck】Edge节点间交互收集其他节点健康信息--minxi【ok】** 
-  **【installer】EG平台支持版本升级场景的数据持久化--liqiang【ok】** 
-  **【EGMobile】后台服务支持admin上传图片和视频等资源数据 --阿利【ok】** 
-  **【EGMobile】后台服务支持所有用户通过url的方式查看已经上传资源数据--阿利【ok】** 
-  **【EGMobile】后台服务支持admin根据资源ID删除资源文件--阿利【ok】** 
-  **【EGMobile】后台服务支持admin创建event事件--阿利【ok】** 
-  **【EGMobile】后台服务支持admin修改event事件--阿利【ok】** 
-  **【EGMobile】后台服务支持admin删除event事件--阿利【ok】** 
-  **【EGMobile】后台支持tenant对event进行点赞/转发的操作--阿利【ok】**【收藏转入R1.4】 
-  **【EGMobile】后台服务支持所有用户去查看已经发布出去的event事件--阿利【ok】** 
-  **【EGMobile】后台服务支持admin去查看所有的published/draft类型的event事件--阿利【ok】** 
-  **【EGMobile】后台服务支持所有用户去查看已经上线的app应用--阿利【ok】** 
-  **【EGMobile】后台服务支持所有用户对app列表进行过滤和排序，并支持分页查找--阿利【ok】** 
-  【YUAN】用户通过手机获取进行验证码注册  --张阿利【ok】
- 【YUAN】用户登录支持密码登录以及验证码登录  --张阿利【ok】
- 【YUAN】用户注册登录界面前端开发 --张阿利【ok】


第三轮迭代测试2021年9月13日至9月26日。

- 【AppStore】应用包签名   --张阿利【ok】
- 【MECM】统一MECM组件风格--张阿利【ok】
- 【MECM】接口响应规范优化-APPLCM-ContainerAPI   --张阿利【ok】
- 【HealthCheck】MECM层调用HealthCheck--minxi【ok】
- 【HealthCheck】各边缘节点初始化--minxi【ok】
- 【HealthCheck】根据edge交互情况投票总结此次健康检查--minxi【ok】
- 【EGMobile】后台服务支持所有用户去查看已经上线的app应用--张阿利【ok】
- 【EGMobile】后台服务支持所有用户对app列表进行过滤和排序，并支持分页查找--张阿利【ok】
- 【EGMobile】后台服务提供用户注册接口--张阿利【ok】
- 【EGMobile】后台支持支持不同权限用户的权限控制--张阿利【ok】
- 【EGMobile】手机端应用的数据填充--张阿利【ok】
- 【developer】应用开发者部署调测时，通过VNC远程登录到容器内部--李媛媛【ok】
- 【MEP】MEC application graceful termination/stop--xiaolong【testing】



3.1.3 测试用例执行统计 analysis of Test Case execution

|需求点|TC总数|成功个数|失败个数|未执行个数|TC成功率|
|--|--|--|--|--|--|
| developer | 40| 40 | 0 | 0 | 100% |
| appstore | 30 |40  |0  |0  | 100% |
| mecm|15 |15  | 0 | 0 | 100% |
| ATP | 13 | 13 | 0 | 0 | 100% |
| yuan |24 | 24 | 0 | 0 | 100% |
| healthcheck |1 | 1 | 0 | 0 | 100% |


3.2 覆盖分析 Coverage Analysis

3.2.1 需求覆盖 Requirement Coverage

软件实现100%覆盖Release V1.3需求。

3.2.2 单元测试代码覆盖（数据源于SonarQube） Code Coverage for Unit Test

根据TSC20200923会议决定，本次测试版本依然不考虑前端代码覆盖率，关注后台代码覆盖率。
所有数据来自于北京时间 2021.10.12 14:00 [SonarQube页面查询结果](http://192.168.1.40:9000/projects?sort=name)。

| 模块                      | 代码覆盖率 |
|---------------------------|-----------|
| appstore-be               | 65.2%     |
| atp-be                    | 66.3%     |
| developer-be              | 44.1%     |
| file-system               | 0%        |
| healthcheck               | 0%        |
| healthcheck-m             | 0%        |
| mecm-apm                  | 68.9%     |
| memc-applcm-k8splugin     | 72.8%     |
| memc-applcm-lcmcontroller | 72.1%     |
| memc-applcm-osplugin      | 47.3%     |
| mecm-appo                 | 70.3%     |
| mecm-apprulemgr           | 76.6%     |
| mecm-inventory            | 70.7%     |
| mep-agent                 | 60.8%     |
| mep-dnsserver             | 73.9%     |
| mep-mepauth               | 67.8%     |
| mep-mepserver             | 68.0%     |
| plugins                   | 70.2%     |
| toolchain                 | 44.7%     |
| imageops                  | 0%        |
| user-mgmt-be              | 61.5%     |
| website-gateway           | 14.7%     |


**Note:** 

1. developer-be代码覆盖率低的解释：下一版本计划做重构，流程优化，所以暂时不做测试用例的补充。等优化完后，完善测试用例。

2. file-system、healthcheck、healthcheck-m、imageops代码覆盖率为0的解释：这些为本次1.3版本新增特性，还未能有时间补充测试用例，下个版本补充完毕。

3. memc-applcm-osplugin代码覆盖率低的解释：社区伙伴参与开发项目，由于1.3版本改动较大以及人力不足原因，来不及增加新的测试用例，下个版本补充完毕。

4. mep-agent、user-mgmt-be代码覆盖率低的解释：1.3版本未做新代码开发，所以维持现状。

5. website-gateway代码覆盖率低的解释：website-gateway本身是一个框架工程只有200多行代码，没有业务逻辑，而且大部分代码都是实现spring的配置类，ut运行不到。目前我们只能对其中的login和logout接口做测试，所以覆盖率达不到60%。

6. toolchain代码覆盖率低的解释：toolchain后台是集成的代码分析工具，只做了接口调用的封装，业务逻辑少，所以1.1版本覆盖率没有到达60%。由于功能简单，1.2、1.3版本并没有代码开发，后期也不会有功能开发，所以维持现状，后期也不会补充。

3.3 缺陷统计及分析 Statistical analysis of Defects/Bugs

请点击[缺陷清单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=126164&is%5Bissue_type_id%5D=199540&openMore=1)查看详情。

3.3.1 缺陷汇总 Defects Summary

已剔除 4个无效bug后，总缺陷数量： 234（含 22个安全类缺陷），已解决数量： 226个；未解决bug 9个（含9 个known issue，遗留到下一版本解决）。

3.3.2 缺陷分析 Defects Analysis

按缺陷类型分析：安全类bug 23个；其他类bug 215个。

按缺陷严重级别分析：严重bug 12个；主要bug 72个；次要bug 132个；不重要bug 15个；不指定7 个。
 
按需求点分析 缺陷收敛性分析：总计238 个bug（含 4个无效bug；含安全类bug 23）；迭代一 50 个bug（含 2个无效bug）；迭代二 62 个bug（含 0个无效bug）；迭代三 107 个bug（含 2个无效bug）；未指定迭代19 个bug（含 0个无效bug）。

按缺陷所属模块分析：总计238 个bug；developer 85个、appstore 68个、mecm 38个、atp 20个、usermgmt 3个、mep 1个、installer 2个、未指定 13个

3.3.3 经评审遗留缺陷及未解决问题 Residual Defects and known issues

请点击[遗留清单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=126164&is%5Bissue_type_id%5D=199540&is%5Bissue_state_ids%5D=826576&openMore=1)
非安全问题遗留清单：
| 序号  |BugID|Bug描述|状态|未解决说明|预计处理安排|
| --|--|--|--|--|--|
|1| #I4D7LH |【developer】编辑项目，不修改图标，直接保存失败  | Remian| | 在1.4版本修复 |
|2| #I4D77M |【developer】服务发布配置“版本”字段需要加字符校验，并且建议加提示，如v1.0 | Remian|| 在1.4版本修复 |
|3| #I44LKH |【developer】24小时自动释放功能，前台显示已经释放可以再次部署，实际后台namespace未清除 |Remian || 在1.4版本修复 |
|4| #I4DFAU |【mepm】边缘应用市场会出现数据刷不出来的情况  |Remian |偶现| 需持续观察复现条件 |
|5| #I4C1JK |【mepm】“边缘应用市场”和“应用实例管理”的下拉搜索按钮有时会位置偏移  |Remian |1.3版本不交付“镜像仓库”|在1.4版本修复  |
|6| #I46RQI |【mecm】概览界面地图展示需优化 | Remian|| 在1.4版本修复 |
|7| #I42W40 |【MECM】环境重启后MECM前台无法访问 |Remian || 在1.4版本修复 |
|8| #I49HFJ |ERR：Destination directory /etc/network/interfaces.d does not exist |Remian || 在1.4版本修复 |
|9| #I44JCI |osplugin需要使用非root账号启动 |Remian || 在1.4版本修复 |


3.4 安全测试执行情况及记录（安全工作组负责）


安全测试结果及分析详见 **v1.3安全测试报告**  ：[EdgeGallery v1.3 Security Test Report](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Test%20Result/Test%20result%20Release%20V1.3/EG%20v1.3%20Security%20Test%20Report.md)


3.4.1 安全测试组织Security Test Organizing

安全测试由 安全工作组(SecurityWG) 执行

3.4.2 安全测试执行情况 Security Test Execution

测试时间： 2021.08.16 ~ 2021.10.10

测试环境： x.x.x.x(敏感信息，不予公开)  Center+Edge

测试项目： AppStore, Developer, User-mgmt, MECM，MEP，APT

测试人员： 扈冰

测试内容： 本次测试共包括sprint1，sprint2，sprint3三个迭代。从安全合规测试和渗透测试2个维度展开测试。安全测试依照 **v1.3安全测试计划** 开展，测试内容详见 [v1.3 Security Test Plan](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Test%20Plan/Release-R1.3/EG%20v1.3%20Security%20Test%20Plan%20.md) 


安全合规测试用例： [安全合规测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases)

渗透测试用例： [渗透测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases)



3.4.3 安全测试结果及分析 summary analysis for Security test result


v1.3新增安全类问题 **19个** 。安全类问题单标题均包含“【Security】”标签，详见：[v1.3安全问题单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=126164&is%5Bsearch%5D=Security&is%5Bissue_type_id%5D=199540&is%5Bsort%5D=created_at&is%5Bdirection%5D=desc)


各版本安全问题数量横向对比： v1.0版本22个，v1.1版本34个，v1.2版本12个，v1.3版本19个。

按 **漏洞类型** 统计：信息泄露4个、登录/认证3个、越权/鉴权3个、不安全的配置3个、会话管理2个、输入校验1个、DOS风险1个、文件上传1个、文件签名1个。

按 **严重性** 统计：1个严重，8个主要，9个次要，1个不重要。

按 **修复状态** 统计：19个已修复，0个遗留。

按 **所属模块** 统计：UserMgmt 1个， AppStore 3个， Developer 5个， MECM 2个， ATP 3个， MEPM 5个。

按 **所属迭代** 统计：sprint1 3个， sprint2 5个， sprint3 11个。



3.4.4 遗留安全类问题 Leaving Security Issues

历史版本遗留4个安全问题均已修复完毕（详见 [v1.3安全测试报告](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Test%20Result/Test%20result%20Release%20V1.3/EG%20v1.3%20Security%20Test%20Report.md)）。

v1.3对发现的安全问题均已修复完毕，无遗留安全问题。


安全问题遗留清单：
| 序号  | Issue编号 | Issue标题        | 状态    | 原因             | 完成计划             | 严重性           |
| ----- | --------- | ---------------- | ------- | ---------------- | -------------------- | ---------------- |
| | NA | NA |   | |   |  |



四、测试结论及建议 Test Conclusion and Suggestion

测试主要包括交付需求测试，分别从系统功能性、稳定性、易用性以及安全性（详见安全测试报告）方面进行测试，同时针对个别场景进行专项测试，问题单提单量总计238个，其中安全类问题23个。经评审遗留9个，其中安全测试问题0个，功能性问题9个。

- 需求完成率:EPIC(剔除1个无效epic）22/22=100%，story(剔除1个无效story后）126/126=100%, 满足版本发布质量要求。
- 未解决的严重问题<=1,满足版本发布质量要求。
- 遗留Defect Index，即DI=1* 0.1+8* 1+0*3=8.1<10，满足版本发布质量要求。
- 测试用例执行率，单元测试代码覆盖率(除例外说明)等指标满足版本发布质量要求。



