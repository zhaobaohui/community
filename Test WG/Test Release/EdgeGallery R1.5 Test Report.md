一、引言Introduction

1.1 编写目的Writing Purpose

本测试报告为EdgeGallery Release1.5的测试报告，在于总结测试阶段的测试情况以及分析结果，描述预定需求的符合性及预定质量指标的符合性。作为测试质量参考文档提供给开源社区开发人员、测试人员、管理人员及感兴趣的其他人员等参考。

1.2 项目背景 Project Background

EdgeGallery社区聚焦5G边缘计算场景，通过开源协作构建起MEC边缘的资源、应用、安全、管理的基础框架和网络开放服务的事实标准，并实现同公有云的互联互通。在兼容边缘基础设施异构差异化的基础上，构建起统一的MEC应用生态系统。

1.3 [系统简介 System Overview](https://gitee.com/edgegallery/docs/blob/master/Get%20Started/Start%20from%20A%20Demo%20on%20EdgeGallery.md)

1.4 参考资料 References

1.Release V1.3需求

- [R1.5 EPIC](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bissue_type_id%5D=238024)
- [R1.5 Story](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bissue_type_id%5D=199539&is%5Blabel_ids%5D=86836775%2C86836832%2C86836889%2C92160061&openMore=1)
- [R1.5需求文档](https://gitee.com/edgegallery/community/tree/master/Architecture%20WG/Requirements/v1.5)

2.[Release V1.3测试计划（含版本质量指标）](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20Release/EdgeGallery%20R1.5%20Test%20Plan.md)

二、测试概要 Test Overview

2.1 测试用例及脚本设计 Test Cases and Auto test scripts

设计测试用例共计128个。
请点击[测试用例清单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Blabel_ids%5D=86836775%2C86836832%2C86836889%2C92160061&is%5Bissue_type_id%5D=232575&openMore=1)查看详情。

2.2 测试环境与配置 Test Env and related Configuration
| server ip |Usage   | password |
|-----------|---|----------|
| 119.8.36.45 | 中心  | xxxx    |


2.3 测试方法和工具 Test Method and Tools

a.单元测试代码覆盖率工具：Jacoco（Java语言）、GoTest（Go语言）
【基于当前实际情况，本次版本前端代码不做单元测试及代码覆盖率检查，改为界面检视】
  
代码覆盖率测试结果分析（SonarQube）：http://159.138.132.188:9000/projects?sort=-analysis_date

|Tool Name|Version|Address | comments  |
|---|---|---|---|
|SonarQube|8.7 |http://159.138.132.188:9000/projects  ||
|Jacoco|0.7.7.201606060606  |None |   |
|go test| 1.15.5 |None |   |

b.CICD工具Jenkins：http://119.8.34.36:8080/

c.镜像库：https://www.edgegallery.org/download/otherversion/

三、测试结果及缺陷分析 Test Results and Defects Analysis

3.1 测试执行情况及记录Test exceuction and Records

3.1.1 测试组织 Test Organizing

测试组负责人：刘辉

测试人员：扈冰、许丹、李媛媛、张阿利、孟璞辉、于小龙、李强

配置管理人员：许丹

3.1.2 测试时间（各负责人daily更新） Test Schedule


第一轮迭代测试2021年11月22日至12月05日。

- 【imageops】使用eguser eggroup代替现在的root root（ok）--liqiang
- 【imageops】compress前做环境的空间检查，如果空间不足，报错退出（ok）--liqiang
- 【imageops】compress提供进度估计（ok）--liqiang
- 【imageops】check处理非qcow2格式的镜像时的处理（ok）--liqiang
- 【imageops】check和compress提供超时机制，超时会退出（ok）--liqiang
- 【imageops】提供日志功能（ok）--liqiang
- 【installer】deploy-tool镜像大小缩减（ok）--liqiang
- 【installer】将k3s的内容从常规安装包中剔除，出单独的k3s包（ok）--liqiang
- 【installer】所有java项目基础镜像修改，从jdk到jre（ok）--liqiang
- 【installer】镜像精简，使用nginx alpine镜像（ok）--liqiang
- 【edgegallery-fe】5G 边缘应用流水线主界面（ok）------李媛媛
- 【edgegallery-fe】页面左侧项目管理组件（ok）------李媛媛
- 【edgegallery-fe】edgegallery新版首页入口（ok）------李媛媛
- 【edgegallery-fe】新建项目表格（ok）------李媛媛
- 【edgegallery-fe】能力中心界面（ok）------李媛媛
- 【edgegellery-fe】沙箱选择引导页面以及沙箱详情（ok）------李媛媛
- 【edgegallery-fe】沙箱内部5G边缘应用架构图（ok）------李媛媛
- 【edgegallery-fe】边缘应用网络配置（ok）------李媛媛
- 【edgegallery-fe】边缘应用虚机配置（ok）------李媛媛
- 【edgegallery-fe】应用打包预览界面（ok）------李媛媛
- 【edgegallery-fe】打包完成界面弹窗（ok）------李媛媛
- 【edgegallery-fe】atp测试部分页面（ok）------李媛媛
- 【edgegallery-fe】虚机边缘应用配置完成状态（ok）------李媛媛
- 【AppStore】应用行业和类型重新划分  ------zhangali（测试完成ok）
- 【AppStore】应用上传对应用包做完整性校验  ------zhangali（测试完成ok）
- 【AppStore】应用仓库和我的应用获取应用包信息的接口分离，权限区分  ------zhangali（测试完成ok）
- 【datagen】自动上传应用镜像到环境（ok）------李媛媛
- 【datagen】自动获取k8s配置文件，并上传-----zhangali（测试完成ok）
- 【datagen】根据最新API接口工具调试优化-----zhangali（测试完成ok）
- 【filesystem】Check当前镜像时，非qcow2格式要增加返回值（ok）------李媛媛
- 【filesystem】Compress前检查环境，存储空间不足时立即报错退出（ok）------李媛媛
- 【developer】作为应用开发者，可以通过界面查看他上传的helmchart包的详情（ok）  ------李媛媛
- 【developer】作为应用开发者，可以上传一个k8s部署文件，完成应用的集成开发（ok）------李媛媛
- 【developer】支持虚机拉起后远程VNC（ok）------李媛媛
- 【developer】支持APPD细节参数在线修改（ok）------李媛媛
- 【developer】支持镜像文件在线修改能力（同应用包修改）（ok）------李媛媛
- 【developer】后台接口重构（ok）------李媛媛
- 【Developer】支持zip格式的应用包（ok）------李媛媛
- 【ATP】支持zip格式的应用包（ok）------李媛媛
- 【atp】atp镜像精简（ok）------李媛媛
- 【UserMgmt】支持通过proxy访问EdgeGallery（ok）


 第二轮迭代测试2021年12月06日至12月19日。
- 【datagen】【POC】developer自动上传应用------李媛媛（ok）
- 提供一套完整的加解密代码模板，供各模块嵌入使用---（ok）
- 【developer】作为应用开发者，可以上传一个完好的helmchart包到dev平台上，完成应用的集成开发------李媛媛（ok）
- 【developer】作为应用开发者，可以通过界面在回显的helmchart脚本中增加mep、mep-agent的依赖------李媛媛（ok）
- 【mepm-fe】边缘自治Portal支持基于OpenStack的虚机申请流程。Add Page for resource api use, implement lightweight cloud management features.------zhangali（ok）
- 【imageops】导致k8s file-system pod被驱逐(从迭代一移入迭代二)--李强（ok）
- 【installer】alpine基础镜像安装docker优化，精简大小--李强（ok）
- 【imageops】镜像精简--李强（ok）
- 【osplugin】镜像精简--李强（ok）
- 【developer】支持灵活打包能力，应用包二次修改------李媛媛（ok）
- 【lcm】lcm支持多虚机包解析，部署------zhangali（ok）
- 【developer】容器流程优化---李媛媛（ok）
- 【developer】解析profile生成部署文件------李媛媛（ok）
- 【developer】新增profile接口------李媛媛（ok）
- 【developer】更新profile接口------李媛媛（ok）
- 【developer】查询、批量查询、删除profile接口------李媛媛（ok）
- 【developer】选择profile创建项目界面------李媛媛（ok）
- 【developer】profile管理列表页面------李媛媛（ok）
- 【developer】profile新增、修改、删除页面功能------李媛媛（ok）
- 【能力中心】选择5款重量级AI应用，弥补当前能力中心的空白------李媛媛（2款app已提交）
- 【能力中心】将AI应用以webassembly方式在能力中心展示并支持运行------李媛媛（ok）
- 【MECM】支持zip格式的应用包 [MECM] Application packages in .zip format are supported.--zhangali（ok）
- 【AppStore】【POC】App变现界面设计优化------zhangali（ok）
- 【AppStore】应用在线体验流程优化  ------zhangali（ok）
- 【AppStore】【POC】App变现后台实现------zhangali（ok）
- 【developer】虚机镜像瘦身增强------李媛媛（ok）
- 【UserMgmt】支持与第三方IAM系统集成---（ok）
- 【UserMgmt】EdgeGallery支持北向接口---（ok）
- 【ThirdSystem】MEAO的调用通过三方系统，AppStore修改调用逻辑------zhangali（ok）
- 【ThirdSystem】三方系统管理，包含三方系统的增删改查等功能   ------zhangali（ok）
- 【file-system】性能优化---李媛媛（流程ok；速度已提升）



第三轮迭代测试2021年12月20日至2022年1月2日。

- 【developer】适配profile流程，developer后台代码修改------李媛媛（ok）
- 【developer】profile新增、修改、删除页面功能------李媛媛（ok）
- 【lcm】lcm支持网络创建------zhangali（移入补丁版本）
- 【AppStore】应用修改支持修改应用详情描述--张阿利（ok）



3.1.3 测试用例执行统计 analysis of Test Case execution

|需求点|TC总数|成功个数|失败个数|未执行个数|TC成功率|
|--|--|--|--|--|--|
| developer | 46| 46 | 0 | 0 | 100% |
| appstore | 22 |22  |0  |0  | 100% |
| mecm|25 |25  | 0 | 0 | 100% |
| edgegallery-fe | 12 | 12 | 0 | 0 | 100% |
| thirdsystem |5 | 5 | 0 | 0 | 100% |
| filesystem |4 | 4 | 0 | 0 | 100% |
| imagesops |8 | 8 | 0 | 0 | 100% |
| installer |5 | 5 | 0 | 0 | 100% |
| mepm |1 | 1 | 0 | 0 | 100% |

3.2 覆盖分析 Coverage Analysis

3.2.1 需求覆盖 Requirement Coverage

软件实现100%覆盖Release V1.5需求。

3.2.2 单元测试代码覆盖（数据源于SonarQube） Code Coverage for Unit Test

根据TSC20200923会议决定，本次测试版本依然不考虑前端代码覆盖率，关注后台代码覆盖率。
所有数据来自于北京时间 2021.10.12 14:00 [SonarQube页面查询结果](http://192.168.1.40:9000/projects?sort=name)。

| 模块                      | 代码覆盖率 |
|---------------------------|-----------|
| appstore-be               | 65.7%     |
| atp-be                    | 67.6%     |
| common-service-cbb        | 60.9%     |
| developer-be              | 65.9%     |
| file-system               | 30.4%     |
| healthcheck               | 0%        |
| healthcheck-m             | 0%        |
| mecm-apm                  | 68.7%     |
| memc-applcm-k8splugin     | 72.9%     |
| memc-applcm-lcmcontroller | 70.2%     |
| memc-applcm-osplugin      | 55.2%     |
| memc-applcm-rescontroller | 72.5%     |
| mecm-appo                 | 70.7%     |
| mecm-apprulemgr           | 76.6%     |
| mecm-inventory            | 68.6%     |
| mecm-north                | 2.2%      |
| mep-agent                 | 60.8%     |
| mep-dnsserver             | 73.9%     |
| mep-mepauth               | 70.8%     |
| mep-mepserver             | 66.9%     |
| plugins                   | 70.2%     |
| third-party-system        | 66.1%     |
| toolchain                 | 44.7%     |
| toolchain-appdtranstool   | 0%        |
| imageops                  | 90.9%     |
| user-mgmt-be              | 69.7%     |
| website-gateway           | 16.4%     |


**Note:** 

1. common-service-cbb、mecm-north代码覆盖率低的解释：1.5版本新项目，时间原因，覆盖率暂时达不到65%，下个版本补充完毕。

2. file-system、healthcheck、healthcheck-m、appdtranstool代码覆盖率低的解释：人力不足问题，下个版本补充完毕。

3. memc-applcm-osplugin代码覆盖率低的解释：社区伙伴参与开发项目，时间安排问题，下个版本补充完毕。

4. mep-agent代码覆盖率低的解释：1.5版本未做新代码开发，所以维持现状。

5. website-gateway代码覆盖率低的解释：website-gateway本身是一个框架工程只有200多行代码，没有业务逻辑，而且大部分代码都是实现spring的配置类，ut运行不到。目前我们只能对其中的login和logout接口做测试。

6. toolchain代码覆盖率低的解释：toolchain后台是集成的代码分析工具，只做了接口调用的封装，业务逻辑少，后期也不会有功能开发。

3.3 缺陷统计及分析 Statistical analysis of Defects/Bugs

请点击[缺陷清单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Blabel_ids%5D=86836775%2C86836832%2C86836889%2C92160061&is%5Bissue_type_id%5D=199540&openMore=1)查看详情。

3.3.1 缺陷汇总 Defects Summary

已剔除 4个无效bug后，总缺陷数量： 280（含 17个安全类缺陷），已解决数量： 273个；未解决bug 7个（含7 个known issue，遗留到下一版本解决）。

3.3.2 缺陷分析 Defects Analysis

按缺陷类型分析：安全类bug 17个；其他类bug 267个。

按缺陷严重级别分析：严重bug 15个；主要bug 69个；次要bug 173个；不重要bug 21个；不指定6 个。 

按需求点分析 缺陷收敛性分析：总计284 个bug（含 4个无效bug；含安全类bug 17）；迭代一 113 个bug（含 0个无效bug）；迭代二 162 个bug（含 4个无效bug）；未指定迭代9 个bug（含 0个无效bug）

按缺陷所属仓库分析：总计284 个bug：developer 140个、appstore 53个、mecm-mepm-fe 47个、mecm整个模块  21个、3rd-party-system 9个、installer 5个、atp 3个、usermgmt 2个、health-check 1个、未指定 3个

3.3.3 经评审遗留缺陷及未解决问题 Residual Defects and known issues

请点击[遗留清单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bissue_type_id%5D=199540&is%5Bissue_state_ids%5D=826576&openMore=1)
非安全问题遗留清单：
| 序号  |BugID|Bug描述|状态|未解决说明|预计处理安排|问题级别|
| --|--|--|--|--|--|--|
|1| I4E2BW |【healthcheck】mecm边缘节点使用healthcheck进行状态校验  | Remian|存在跨域问题，需要找其他解决方案 | 遗留到下个版本解决 |主要|
|2| I4OKVR |【edgegallery-fe】mecm融合平台有页面跳转不正确的情况|Remian|融合平台存在该问题，由于是嵌套的，没改变路径，所以均无法返回。| 下个版本统一解决 |次要|
|3| I4K3F0 |【developer】atp测试前系统没有释放虚机资源|Remian|--|下个版本统一解决|次要|
|4| I4NNIL |【developer】同时拉起2个虚机（间隔2、3秒），有分配到同一IP的情况|Remian|--|下个版本统一解决|次要|
|5| I4N998 |【developer】工作空间内容为空时，不应向用户弹框报错|Remian|新的孵化接口与独立平台工作空间接口不一致，规划在独立平台展示孵化流程|遗留到下个版本|次要|
|6| I4E02V |【mepm】概览页数据刷新有问题，有时kpi接口报404，有时package接口报404|Remian|后台接口不稳定|遗留到下个版本解决|次要|
|7| I4KCTV |【imageops】镜像瘦身超时后，前台已正常报瘦身失败，后台瘦身进程仍然未停止|Remian|--|--|次要|



3.4 安全测试执行情况及记录（安全工作组负责）


安全测试结果及分析详见 **v1.5安全测试报告**  ：[EdgeGallery v1.5 Security Test Report](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Test%20Result/Test%20result%20Release%20V1.5/EG%20v1.5%20Security%20Test%20Report.md)


3.4.1 安全测试组织Security Test Organizing

安全测试由 安全工作组(Security WG) 执行

3.4.2 安全测试执行情况 Security Test Execution

测试时间： 2021.11.22 ~ 2021.12.31

测试环境： x.x.x.x(敏感信息，不予公开)  Center+Edge

测试项目： AppStore, Developer, User-mgmt, MECM/MEPM，MEP，ATP，ThirdSystem

测试人员： 扈冰

测试内容： 本次测试共包括sprint1，sprint2两个迭代。从安全合规测试和渗透测试2个维度展开测试。安全测试依照 **v1.5安全测试计划** 开展，测试内容详见 [v1.5 Security Test Plan](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Test%20Plan/Release-R1.5/EG%20v1.5%20Security%20Test%20Plan%20.md) 


安全合规测试用例： [安全合规测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases)

渗透测试用例： [渗透测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases)



3.4.3 安全测试结果及分析 summary analysis for Security test result


v1.5新增安全类问题 **17个** 。安全类问题单标题均包含“【Security】”标签，详见：[v1.5安全问题单](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bsearch%5D=Security&is%5Bissue_type_id%5D=199540&is%5Bsort%5D=created_at&is%5Bdirection%5D=desc)


各版本安全问题数量横向对比： v1.0版本22个，v1.1版本34个，v1.2版本12个，v1.3版本19个，v1.5版本17个。

按 **漏洞类型** 统计：越权/鉴权4个、信息泄露3个、输入校验3个、前端问题3个、容器安全问题2个、登录/认证1个、日志问题1个。

按 **严重性** 统计：1个严重，7个主要，9个次要，0个不重要。

按 **修复状态** 统计：17个已修复，0个遗留。

按 **所属模块** 统计： AppStore 2个， Developer 1个， MECM/MEPM 4个， ThirdSystem 6个， Installer 1个，EdgeGallery-fe 3个。

按 **所属迭代** 统计：sprint1 4个， sprint2 13个。



3.4.4 遗留安全类问题 Leaving Security Issues

v1.5对发现的安全问题均已修复完毕， **无遗留安全问题** 。

安全问题遗留清单：
| 序号  | Issue编号 | Issue标题        | 状态    | 原因             | 完成计划             | 严重性           |
| ----- | --------- | ---------------- | ------- | ---------------- | -------------------- | ---------------- |
| | NA | NA |   | |   |  |



四、测试结论及建议 Test Conclusion and Suggestion

测试主要包括交付需求测试，分别从系统功能性、稳定性、易用性以及安全性（详见安全测试报告）方面进行测试，同时针对个别场景进行专项测试，问题单提单量总计280个，其中安全类问题17个。经评审遗留7个，其中安全测试问题0个，功能性问题7个。

- 需求完成率:EPIC(剔除1个无效epic）29/29=100%，story(剔除5个无效story后）83/83=100%, 满足版本发布质量要求。
- 未解决的严重问题0<=1,满足版本发布质量要求。
- 遗留Defect Index，即DI=6*1+1*3= 9<10，满足版本发布质量要求。
- 测试用例执行率，单元测试代码覆盖率(除例外说明)等指标满足版本发布质量要求。



