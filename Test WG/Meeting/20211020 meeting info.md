**meeting 会议时间**: 20211020 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.R1.3遗留问题讨论（如ARM版测试情况、MEP新增接口情况等） --owner all

Disucssion about unresolved problems for R1.3 --owner ZHANG Ali, LI Yuanyuan,all

2.安全测试分享 -- owner 扈冰（华为）

Sharing security testing process --owner HU Bing

3.预览 R1.4 需求初稿 --owner all

Preview requirement （in draft） for R1.4 --owner all



## meeting link

腾讯会议：https://meeting.tencent.com/dm/UVC4ps4o6MAH 会议 ID：729 247 070


## Minutes纪要
## 纪要人
刘辉

#遗留任务
- 1.gitee能否集成sonarqube，在达到代码质量要求时才能成功提交代码？---许丹
- 2.R1.3整个版本过程开发进度比计划慢，压缩了测试时间，反馈至tsc--刘辉、扈冰
- 3.mep新增接口的测试问题 --张阿利（已测试完成）
- 4.把发布前要做的事情提前，在迭代一让大家感知到（许丹），反馈至tsc --刘辉

- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利
- 8.MEP新增接口需要同印度团队确认 -- tsc、张阿利
     
      已经测通，因为我们环境不能访问某些在线mock网址，所以在印度同事帮助下，构造了mock脚本，才使接口测通

- 9.EdgeT进展需同印度团队沟通-- 张阿利
   
      edgeT这边目前的进展：
        1. now the execution option enabled in portal ，need to merge
        2.kanaga想知道我们希望如何使用edgeT
        3.kanaga想在新版中将edgeT与ATP合并（但是还没有提案）