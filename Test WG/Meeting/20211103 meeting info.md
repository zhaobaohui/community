**meeting 会议时间**: 20211103 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang  、chenruidong 、_zhaobaohui 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu、zhouyanbin、luoxiaoyun  （华为）
- _xiert_

## Topics议题

1.EG R1.5需求澄清 --owner 周艳兵、倍源、传雨、鹿鑫、罗晓云等

Requirement clarification for EG R1.5 --owner zhouyanbin,beiyuan, chuanyu, luxin, luoxiaoyun,etc



## meeting link

腾讯会议 https://meeting.tencent.com/dm/g5AbdRVPZsrn 会议 ID：369 290 044


## Minutes纪要
## 纪要人
刘辉

#遗留任务
- 1.gitee能否集成sonarqube，在达到代码质量要求时才能成功提交代码？---许丹
- 2.R1.3整个版本过程开发进度比计划慢，压缩了测试时间，反馈至tsc--刘辉、扈冰
- 3.mep新增接口的测试问题 --张阿利（已测试完成）
- 4.把发布前要做的事情提前，在迭代一让大家感知到（许丹），反馈至tsc --刘辉

- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利
- 8.MEP新增接口需要同印度团队确认 -- tsc、张阿利
     
      已经测通，因为我们环境不能访问某些在线mock网址，所以在印度同事帮助下，构造了mock脚本，才使接口测通

- 9.EdgeT进展需同印度团队沟通-- 张阿利
   
      edgeT这边目前的进展：
        1. now the execution option enabled in portal ，need to merge
        2.kanaga想知道我们希望如何使用edgeT
        3.kanaga想在新版中将edgeT与ATP合并（但是还没有提案）