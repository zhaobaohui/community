**meeting 会议时间**: 20210616 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- xiert

## Topics议题

1.审视第二次迭代测试情况 --owner all

Review test results for R1.2-sprint2--owner all

【bug情况：从5月初到6月16日，bug总量107个，其中严重bug3个，主要bug24个（2个已完成），次要bug27个（1个已完成），其他bug26个（2个已完成）；共计已完成bug5个】
【测试用例：83个】

2、第三次迭代测试分工 --owner all

Test Task Allocation for R1.2-Sprint3--owner all

## meeting link
https://meeting.tencent.com/s/lvvHGWPGxSIP 
腾讯会议 ID：625 989 209

## Minutes纪要
## 纪要人
刘辉

#遗留任务
1、社区是否在阿里云上申请相关云主机 --owner baohui
baohui update：能不申请就不申请

#迭代情况
**3.1 第一次迭代需求** 

- 【developer】支持input参数补齐  ——李媛媛--done
- 【developer】虚机镜像管理（用户，管理员权限）  ——李媛媛--done
- 【Developer】首页界面优化重构  ——李媛媛--done
- 【developer】虚机资源配置可编辑  ——李媛媛--done
- 【UserMgmt】登录的前端滑块验证方式优化为后台验证码   ——李强--done
- 【UserMgmt】发送验证码到用户手机或邮箱的前端滑块验证方式优化为后台验证码  ——李强  --done
- 【UserMgmt】用户注册支持与后台交互的验证  ——李强--done
- 【ATP】ATP首页优化  ——李媛媛--done
- 【ATP】后台新增批量导入包解析接口  ——李媛媛--done
- 【ATP】前台新增批量导入入口  ——李媛媛--done
- 【ATP】后台新增贡献批量删除、脚本类型贡献下载接口  ——李媛媛--done
- 【ATP】前台新增贡献管理页面  ——李媛媛--done
- 【ATP】关键数据持久化——李媛媛--done
- 【ATP】测试任务页面优化——李媛媛--done
- 【AppStore】关键数据持久化  ——张阿利--doing
- 【AppStore】操作分析界面优化  ——张阿利--done
- 【AppStore】文档界面优化  ——张阿利-done
- 【AppStore】上传应用时可设置应用展示模式  ——张阿利--done
- 【AppStore】支持修改应用属性  ——张阿利--done
- 【AppStore】支持显示应用部署类型虚机还是容器  ——张阿利--done
- 【HealthCheck】各边缘节点初始化
- 【HealthCheck】MECM层调用HealthCheck
- 【MECM】边缘自治增强--mep合并到mepm  ——张阿利--done
- 【MECM】MECM后台统一servicecomb  ——张阿利--done
- 【MECM】MECM支持前台节点位置可自定义  ——张阿利--done
- 【installer】ansible安装脚本强化与问题解决 --xd（自动化测试）


 **3.2 第二次迭代需求** 

- 【Developer】统一Develop组件风格【poc】--李媛媛--doing（工作空间尚未开发完成，其余页面开始测试）
- 【developer】支持配置app_configuration, 支持定义AK/SK, APP分流规则，DNS规则等--李媛媛 --开发未给出功能测试说明，测试尚未开始
- 【developer】虚机资源配置可编辑--李媛媛--因为虚拟相关调试尚未开发完成
- 【developer】虚机镜像管理（用户，管理员权限）--李媛媛--done
- 【Developer】能力中心界面优化重构--李媛媛 --done
- 【AppStore】应用管理界面  ——张阿利--done
- 【appstore】appstore支持虚机镜像的管理以及上传下载  ——张阿利--doing
- 【ATP】用户面页面优化--李媛媛--done
- 【ATP】测试用例增强--李媛媛--done
- 【ATP】测试模型页面优化--李媛媛--done
-  _【UserMgmt】个人帐号中心支持性别、公司信息设置_    ——yxl--需求取消
- 【UserMgmt】个人帐号中心展示我的权限(角色+可访问平台)    ——yxl--done
- 【UserMgmt】支持非guest用户90天后必须修改密码    ——yxl--done
- 【UserMgmt】支持admin首次登录成功后强制修改密码     ——yxl--done
- 【HealthCheck】各Edge收集Applcm、mep健康信息--hmx--放入迭代三
- 【HealthCheck】Edge节点间交互收集其他节点健康信息--hmx--放入迭代三
- 【HealthCheck】根据edge交互情况投票总结此次健康检查--hmx--放入迭代三
- 【MECM】首页展示优化  ——张阿利--done
- 【MECM】MECM支持applcm和apprulemgr注册统一--lh
- 【MEP】关键数据持久化  ——张阿利--放入迭代三
- 【MEP】 Code quality improvement  ——张阿利--放入迭代三
- 【MEP】MEP adapter register to EdgeGallery MEP and UPF  ——张阿利--放入迭代三
- 【MEP】Public the UPF capability to EdgeGallery  ——张阿利--放入迭代三
- 【MEP】mep-agent实现订阅接口  ——张阿利--放入迭代三
- 【MEP】服务发现增加鉴权认证  ——张阿利--放入迭代三
- 【installer】支持docker-compose直接从镜像部署EG，不依赖k8s--xd--脚本已经完成，遗留两个问题，app实例化无法执行，当前版本依赖ingress组件，ingress依赖k8s所以暂时不能用
- 【installer】ansible安装脚本强化与问题解决--xd（自动化测试）--xd--尚未开发完成，部分项目不支持任意密码导致部署时无法支持任意密码
- 【EGMobile】EdgeGallery移动端开发--李媛媛--尚未具备测试条件
- 【EGMobile】后台服务框架搭建，支持restful接口和资源文件管理--李媛媛--尚未具备测试条件
- 【EGMobile】后台服务框架搭建，支持restful接口和资源文件管理（放入迭代二）--李媛媛--尚未具备测试条件

 **3.3 第三次迭代需求** 

- 【AppStore】APPD转换器【poc】
- 【AppStore】接口响应规范优化
- 【Atp】接口响应规范优化
- 【MEP】接口响应规范优化
- 【AppStore】应用在线体验
- 【ATP】测试模型页面优化
- 【ATP】测试任务页面优化
- 【developer】input根据不同的操作系统，导入不同的初始化脚本
- 【developer】虚机mepagent的集成
- 【MECM】统一MECM组件风格【poc】
- 【MECM】mecm支持虚机APP的实例化和终止 VM APP instantiation and termination support.
- 【MECM】mecm支持VIM预配置 MECM support the preconfig of OpenStack
- 【MECM】MECM支持虚机APP包同步和分发，MECM support app package management of VM app
- 【ATP】ATP支持虚机APP的实例化和终止用例
- 【installer】支持边缘侧以虚机方式部署
- 【EGMobile】EdgeGallery移动端开发-后台服务支持Guest用户使用微信登陆，并且可以获取微信账号信息【POC】
- 【MEP】timing api   ——张阿利
- 【MEP】transport api   ——张阿利
- 【Example-apps】Example-apps enhancement
- 【installer】EG平台支持版本升级场景的数据持久化--lq
- 【Test】API并行调用性能测试--张阿利、李媛媛
- 【Test】故障注入测试--孟璞辉
