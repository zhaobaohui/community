**meeting 会议时间**: 20210922 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.第三次迭代测试情况讨论 --owner all

discuss test results for R1.3-sprint2 --owner all

【截止9月14日，bug总量124，其中严重5个，主要42个，次要63个，其他14个；已完成17个，取消2个，遗留1个，进行中104个；测试用例123个；】

【截止9月14日，sprint1 story共计50个，已完成31个，已取消2个；sprint2 story共计61个，已完成51个；sprint3 story共计23个，已完成12个】

【上上周：虚机主流程已通；容器主流程通，应用名称有中文可能导致developer沙箱测试不通过，已修复待验证；appstore虚拟应用在线体验已通；】

【上上周：应用包签名放入迭代三；镜像瘦身预计延期1周】

【截止9月22日09：30，bug总量148个，其中严重8个，主要50个，次要70个，其他20个；已完成22个，取消2个，遗留1个，进行中124个（待解决49个，待回归75）；测试用例123个；】

【截止9月22日09：30，sprint1 story共计51个，已完成35个，已取消2个；sprint2 story共计61个，已完成51个；sprint3 story共计24个，已完成12个】

【截止9月22日09：40，epic 28个，已完成7个，取消1个】

2.审视R1.3测试报告草稿 --owner all

Review R1.3 test report (draft) --owner all

## meeting link

瞩目会议：链接：https://zhumu.me/j/150218020   或输入瞩目会议号： 150 218 020 


## Minutes纪要
## 纪要人
刘辉

#遗留任务

- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利

