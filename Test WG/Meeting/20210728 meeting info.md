**meeting 会议时间**: 20210721 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.MECM R1.3需求分享--owner 李治谦

sharing requirements for MECM R1.3--owner lizhiqian

MECM v1.3 需求分析
https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v1.3/MECM-%E7%BB%84%E4%BB%B6%E5%BD%92%E4%B8%80%E5%8C%96.md
https://gitee.com/edgegallery/community/blob/master/MECM%20PT/Release%20V1.3/MECM%20V1.3%20requirements%20Collection.md

2.CICD最新进展情况 --owner xudan

Latest progress for CICD --owner xudan

3、R1.3测试计划审视 --owner all

Review test plan for R1.3 --owner all

## meeting link
 https://us02web.zoom.us/j/87179777658?pwd=em0zVmFmY091dEprZlNmL08vVStkZz09

## Minutes纪要
## 纪要人
刘辉

#遗留任务


- 1.项目组需补齐相关epic --owner PTL
- 2.EGMobile测试用例补齐 --owner 阿利
- 3.healthcheck并入deployment脚本 --owner 许丹 【已解决】
- 4.EGMobile CICD配置由项目组尽快完成 --owner 蔡舒豪&周文敬
- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉


