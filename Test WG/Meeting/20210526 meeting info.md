**meeting 会议时间**: 20210526 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- xiert

## Topics议题

1.审视第一次迭代测试情况 --owner all

Review test results for R1.2-sprint1--owner all

【mecm最新版主流程不通】

2.讨论1-2个R1.2的性能、可靠性/稳定性指标 --owner all

Discuss  one or two test indicators for R1.2 performance/reliability/stability--owner liuhui

- 【puhui：API并行调用性能（中心为主，边缘为辅），多长时间创建1000个虚拟/容器；租户资源分配；【采用--阿利、媛媛，jmeter等工具】】
- 【敏熙：测试应用在4层协议上的基准测试【TBD】；故障注入测试【采用，puhui】】
- 【阿利：空载下的内存消耗测试；100/1000个应用容器实例时mep资源消耗情况[TBD]】


3.测试环境讨论 --owner all

Discuss test environment --owner all

【增加访问白名单可联系baohui】

## meeting link
https://welink-meeting.zoom.us/j/972592257

## Minutes纪要
## 纪要人
刘辉

#遗留任务
1、社区是否在阿里云上申请相关云主机 --owner baohui
