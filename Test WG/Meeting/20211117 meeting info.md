**meeting 会议时间**: 20211117 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.R1.5第一次迭代测试准备 --owner all

Discussion about test preparation for EG R1.5 sprint1 --owner all

2.双网卡部署edgegallery平台的访问问题讨论 --owner lh

Discussion about access bug of EdgeGallery platform deployed on a server with two NICs --owner lh

【待同研发进一步确认是否可以在R1.5解决】

## meeting link

腾讯会议 https://meeting.tencent.com/dm/y8K3pOfn7Sf5 会议ID：982 957 020



## Minutes纪要
## 纪要人
刘辉

#遗留任务
- 1.平台运行的云主机/主机ip地址变化，如何在不重装平台的前提下让整个平台进入新ip的稳定运行状态？【预计放入R1.6解决】--owner xudan
- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利