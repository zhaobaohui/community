**meeting 会议时间**: 20210421 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、zhaobaohui_   （华为）
- xiert

## Topics议题

1.MEP功能及接口整体介绍 --owner 鹿鑫

Introduction for MEP features and APIs --owner luxin

2.审查R1.2需求 --owner all

Review requirements for R1.2.--owner all


3.审查R1.2测试计划草稿 --owner 刘辉

Review R1.2 Test Plan（draft）--owner liuhui

4.TC610 <<MEP测试规范>>大纲讨论 --liuhui

Discuss about TC610 "MEP TEST SPECIFICATION" outline --owner liuhui

## meeting link
https://welink-meeting.zoom.us/j/581444472

## Minutes纪要
## 纪要人
刘辉

#遗留任务
1、下周例会邀请鹿鑫参加例会讨论mep相关功能
