**meeting 会议时间**: 20210707 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.审阅R1.3需求 --owner all

Review requirements for R1.3 --owner all

2、审视R1.3测试质量指标 --owner all

Review quality --owner all

3、代码覆盖率及bug标签等事项讨论 --owner all

Discussion about code coverage & bug labelling --owner all

【在R1.3版本，bug除了里程碑，还需打版本标签，需在Gitee上配置R1.3的标签】


## meeting link
 https://us02web.zoom.us/j/87179777658?pwd=em0zVmFmY091dEprZlNmL08vVStkZz09

## Minutes纪要
## 纪要人
刘辉

#遗留任务


