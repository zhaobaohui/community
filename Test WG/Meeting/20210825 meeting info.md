**meeting 会议时间**: 20210825 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.第一次迭代测试情况讨论 --owner all

discuss test results for R1.3-sprint1 --owner all

【截止8月25日，bug总量51，其中严重3个，主要15个，次要29个，其他4个；已完成4个，取消1个，进行中46个；sprint1 story共计52个，已完成17个，已取消1个】

【虚机主流程尚未通，需验证；容器主流程通，应用名称有中文可能导致developer沙箱测试不通过；appstore虚拟应用在线体验未通；】

【webassembly相关story延期到第3次迭代；】

2.第二次迭代测试需求讨论 --owner all

discuss test requirement for R1.3-sprint2 --owner all

【第二次迭代开发交付低风险】

## meeting link

腾讯会议 https://meeting.tencent.com/dm/jfGbJNTrjtKl  会议 ID：257 409 777


## Minutes纪要
## 纪要人
刘辉

#遗留任务

- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利

