**meeting 会议时间**: 20210609 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- xiert

## Topics议题

1.审视第一次迭代测试情况 --owner all

Review test results for R1.2-sprint1--owner all

【bug情况：从5月初到6月9日，bug总量45个，其中严重bug2个，已完成bug3个】
【测试用例：82个】

2.第二次迭代测试交付情况讨论 --owner all

Discuss delivery of R1.2-spint2--owner all

3、网络隔离及mep/mepm部署模式测试需求澄清 -- owner liuhui

clarify test requirement for Network isolation and mep/mepm deployment model --owner liuhui

4、《MEP服务测试规范》分享--owner liuhui

## meeting link
https://welink-meeting.zoom.us/j/273783226

## Minutes纪要
## 纪要人
刘辉

#遗留任务
1、社区是否在阿里云上申请相关云主机 --owner baohui
