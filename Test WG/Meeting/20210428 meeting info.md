**meeting 会议时间**: 20210428 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、zhaobaohui_   （华为）
- xiert

## Topics议题

1.DEV-APPSTORE R1.2需求分享 --owner luoxiaoyun&hailong

Requirement Clearification for DEV-APPSTORE R1.2 --owner luoxiaoyun&hailong

【分享了DEV、appstore、usermgnt、atp、应用设计态等方面的需求--luoxioyun、hailong、huiling、beiyuan】

2.R1.2 MECM需求分享 --owner zhiqian

Requirement Clearification for MECM R1.2.--owner zhiqian

【分享了MECM R1.2的新增需求--zhiqian】

3.一些性能、稳定性测试指标讨论 --owner 刘辉

Discussion about some test items related with performance and stability--owner liuhui

4.TC610 <<MEP测试规范>>大纲讨论 --liuhui

Discuss about TC610 "MEP TEST SPECIFICATION" outline --owner liuhui

## meeting link
https://welink-meeting.zoom.us/j/845533086

## Minutes纪要
## 纪要人
刘辉

#遗留任务
1、第3、4两个议题放入下次会议讨论 --owner liuhui
