**meeting 会议时间**: 20210804 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.EdgeT进展分享 --owner kanag【放入下周】

Share progress for edgeT --owner kanag

2.第一次迭代测试准备讨论 --owner all

discuss prepration for R1.3-sprint1 --owner all

【healthcheck需要baohui准备单独的云主机环境】

## meeting link

https://meeting.tencent.com/s/CfBnLH0R8Y1m  会议 ID：362 9146 9091


## Minutes纪要
## 纪要人
刘辉

#遗留任务

- 1.下周edgeT进展分享 --owner kanag
- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利

