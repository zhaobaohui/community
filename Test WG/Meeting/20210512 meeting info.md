**meeting 会议时间**: 20210428 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、zhaobaohui_   （华为）
- xiert

## Topics议题

1.R1.2测试计划审视 --owner all

Review test plan for R1.2 --owner all

【审视了三次迭代的需求及计划，为打迭代标签的R1.2需求需要项目组进一步确认；审视了lab社区资源管理平台现有开发成果，需明确是否在1.2版测试范围】

2.一些性能、稳定性测试指标讨论 --owner 刘辉

Discussion about some test items related with performance and stability--owner liuhui

【讨论了CCSA TC5 <<5G 核心网边缘计算平台技术要求>>中有关性能、稳定性的指标；下次例会分享一些性能、稳定性测试工具、方法以及测试结果分析等】

3.TC610 <<MEP服务测试规范>>大纲讨论 --liuhui

Discuss about TC610 "TEST SPECIFICATION for MEP Services" outline --owner liuhui

【mep安全管理测试用例联系扈冰补充】

## meeting link
https://welink-meeting.zoom.us/j/706822855

## Minutes纪要
## 纪要人
刘辉

#遗留任务

- 1、R1.2需求story中未打迭代标签的需要协调tsc/项目组尽快明确 --owner liuhui【已提交tsc提醒项目组处理】
- 2、分享性能、可靠性测试工具、方法、测试结果分析 --owner minxi、mengpuhui【下次例会分享】
- 3、同mep项目组确认订阅机制实现的可靠性；明确应用终止确认/应用就绪api接口是否已实现 --owner zhangali

     - 目前订阅机制的实现比较简单，订阅接口中有callbackReference，根据callbackReference调ServiceComb的事件去处理
     - 应用终止确认接口，是应用终止接口吗？这个是有的
     - 应用就绪api接口，目前没有实现

- 4、跟踪lab模块是否在1.2版本开始测试 --owner liuhui/tsc【不在R1.2测试范围】

