**meeting 会议时间**: 20210804 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.EdgeT进展分享 --owner kanag【放入下周】

Share progress for edgeT --owner kanag [scheduled to next week]

2.接口并发测试情况分享 --owner zhangali

share results of concurrent test for EG MEP interfaces  --owner zhangali

3.第一次迭代需求分配 --owner all

test task allocation for Requirements with R1.3 sprint1 --owner all

## meeting link

https://meeting.tencent.com/s/ll0Ibcfb8TNH  会议 ID：261 733 662


## Minutes纪要
## 纪要人
刘辉

#遗留任务


- 1.项目组需补齐相关epic --owner PTL【跟倍源、鹿鑫、治谦确认，已补齐】
- 2.EGMobile测试用例补齐 --owner 阿利【已补齐】
- 3.healthcheck并入deployment脚本 --owner 许丹 【已解决】
- 4.EGMobile CICD配置由项目组尽快完成 --owner 蔡舒豪&周文敬【已结束】
- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 6.EdgeT进展分享 --owner kanag
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利

