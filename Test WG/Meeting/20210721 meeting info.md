**meeting 会议时间**: 20210721 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.Developer/appstore/MEP R1.3需求分享--owner 张海龙、luoxiaoyun、luxin
ATP v1.3 需求分析
https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v1.3/ATP%20v1.3%20%E9%9C%80%E6%B1%82%E5%88%86%E6%9E%90.md
AppStore v1.3 需求分析
https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v1.3/AppStore%20v1.3%20%E9%9C%80%E6%B1%82%E5%88%86%E6%9E%90.md
MEP v1.3 requirements
https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v1.3/MEP%20v1.3%20requirements.md
developer_v1.3需求分析
https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v1.3/developer_v1.3%E9%9C%80%E6%B1%82%E5%88%86%E6%9E%90.md

sharing requirements for Developer/appstore/MEP R1.3--owner zhanghailong, luoxiaoyun,luxin

2、EdgeGallery R1.2 故障测试情况分享 --owner mengpuhui

sharing fault testing results for EdgeGallery R1.2 --owner mengpuhui



## meeting link
 https://us02web.zoom.us/j/87179777658?pwd=em0zVmFmY091dEprZlNmL08vVStkZz09

## Minutes纪要
## 纪要人
刘辉

#遗留任务


