**meeting 会议时间**: 20210908 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.第二次迭代测试情况讨论 --owner all

discuss test results for R1.3-sprint2 --owner all

【截止9月8日，bug总量111，其中严重3个，主要34个，次要60个，其他14个；已完成14个，取消2个，遗留2个，进行中93个；测试用例113个；】

【截止9月8日，sprint1 story共计50个，已完成25个，已取消2个；sprint2 story共计61个，已完成34个；sprint3 story共计22个，已完成12个】

【本周：虚机主流程已通；容器主流程通，应用名称有中文可能导致developer沙箱测试不通过，已修复待验证；appstore虚拟应用在线体验已通；】

【本周：应用包签名放入迭代三；镜像搜身预计延期1周】

【本周：将gitee中尚未标记负责人的bug都指定了负责人】

2.第三次迭代测试需求讨论 --owner all

discuss test requirement for R1.3-sprint3 --owner all

3.webassembly技术分享--owner 许丹

share new skills：webassembly --owner xudan

## meeting link

腾讯会议 https://meeting.tencent.com/dm/qwVrSX3RSCNb?rs=25  会议 ID：422 445 739


## Minutes纪要
## 纪要人
刘辉

#遗留任务

- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利

