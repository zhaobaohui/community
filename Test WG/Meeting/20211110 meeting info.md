**meeting 会议时间**: 20211110 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.EG R1.5需求澄清 --owner 蔡舒豪、崔健伟等

Requirement clarification for EG R1.5 --owner caishuhao, cuijianwei etc

2.EG R1.5测试计划 --owner lh

Discussion about Test Plan for R1.5 --owner lh


## meeting link

腾讯会议 https://meeting.tencent.com/dm/PlT8rnheokzH  会议 ID：443 301 773


## Minutes纪要
## 纪要人
刘辉

#遗留任务
- 1.gitee能否集成sonarqube，在达到代码质量要求时才能成功提交代码？---许丹
   （Gitee开发了该功能，内测中）
- 2.各PTL需尽快把R1.5 story打上迭代标签--刘辉（tsc主席以通知ptl）
- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利
- 9.EdgeT进展需同印度团队沟通-- 张阿利
   （ _等待所有功能ready后在测试组演示_ ）
      edgeT这边目前的进展：
        1. now the execution option enabled in portal ，need to merge
        2.kanaga想知道我们希望如何使用edgeT
        3.kanaga想在新版中将edgeT与ATP合并（但是还没有提案）