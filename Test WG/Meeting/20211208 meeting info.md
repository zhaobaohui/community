**meeting 会议时间**: 20211208 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.R1.5第一次迭代测试情况讨论 --owner all

Discussion about test result for EG R1.5 sprint1 --owner all

【12月1日 bug总量71个，58个进行中，12个关闭，遗留1个】

【12月7日 bug总量108个，61个open，2个code，30个test，14个关闭，遗留1个】

【12月1日 story 82个，关闭0个】

【12月7日 story86个，关闭10个；POC story共计11个】

【12月1日 epic 19个，关闭0个】

【12月7日 epic 23个，关闭0个】

【12月1日 edgegallery-fe流水线界面主体功能ok，还在细节优化，尚缺容器支撑方面的界面】

【12月1日 部分story测试移入sprint2】

2.R1.5第二次迭代交付情况讨论 --owner all

Discussion about R1.5-sprint2 delivery  --owner all

【12月1日 需下周一研发例会明确第二次迭代交付情况】

【12月8日 第二次迭代交付延期，与倍源确认后预计下周一交付部署包】

## meeting link

腾讯会议 https://meeting.tencent.com/dm/alKNDUZkF5fA  会议号：763-359-809


## Minutes纪要
## 纪要人
刘辉

#遗留任务
- 1.平台运行的云主机/主机ip地址变化，如何在不重装平台的前提下让整个平台进入新ip的稳定运行状态？【预计放入R1.6解决】--owner xudan
