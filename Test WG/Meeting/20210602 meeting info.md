**meeting 会议时间**: 20210602 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- xiert

## Topics议题

1.审视第一次迭代测试情况 --owner all

Review test results for R1.2-sprint1--owner all

【bug情况：从5月初到6月2日，bug总量44个，其中严重bug2个，已完成bug2个】
【测试用例：50个】

2.第二次迭代测试分工 --owner all

Test Task Division for R1.2-spint2--owner liuhui

3.litmus在社区的试用 -- owner 孟璞辉

【基于web页面，分享了litmus的故障注入的操作流程】

## meeting link
https://welink-meeting.zoom.us/j/999502022

## Minutes纪要
## 纪要人
刘辉

#遗留任务
1、社区是否在阿里云上申请相关云主机 --owner baohui
