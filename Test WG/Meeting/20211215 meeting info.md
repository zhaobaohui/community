**meeting 会议时间**: 20211215 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.R1.5第二次迭代交付/测试情况讨论 --owner all

Discussion about EG-R1.5-sprint2 delivery and test --owner all

【12月1日 bug总量71个，58个进行中，12个关闭，遗留1个】

【12月7日 bug总量108个，61个open，2个code，30个test，14个关闭，遗留1个】

【12月15日 bug总量127个，67个open，2个code，42个test，16个关闭，1个遗留；7个严重，22个主要，81个次要，12个不重要，6个无优先级】

【12月1日 story 82个，关闭0个】

【12月7日 story86个，关闭10个；POC story共计11个】

【12月15日 story87个，关闭20个；POC story共计13个，其中1个取消，2个关闭】

【12月1日 epic 19个，关闭0个】

【12月7日 epic 23个，关闭0个】

【12月15日 epic 29个，关闭5个】


2.Log4j2漏洞解析及社区修补情况说明 --owner 扈冰，许丹,张阿利、李媛媛等

Specification for security problem about log4j2 and Edgegallery's solution  --owner hubing,xudan,zhangali,liyuanyuan


## meeting link

腾讯会议 https://meeting.tencent.com/dm/yJxo6E96eA8R  会议号：518-399-974


## Minutes纪要
## 纪要人
刘辉

#遗留任务
- 1.平台运行的云主机/主机ip地址变化，如何在不重装平台的前提下让整个平台进入新ip的稳定运行状态？【预计放入R1.6解决】--owner xudan
