**meeting 会议时间**: 20210519 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、zhaobaohui_   （华为）
- xiert

## Topics议题

1.分享性能、可靠性测试工具、方法、测试结果分析 --owner houminxi,mengpuhui

share some tools/methods about performance/stability test--owner houminxi,mengpuhui

【puhui：litmus介绍（stability test tool）；minxi：SLA/SLI/SLO介绍；开源TRex介绍（cisco）及与IXIA对比】

2.edgeT进展讨论 --owner kanaga

Discuss  development progress about edgeT--owner liuhui

3、审查R1.2第一次迭代需求分工情况 --owner liuhui

Review test-task allocation for R1.2 requirement --owner liuhui

## meeting link
https://welink-meeting.zoom.us/j/339408993

## Minutes纪要
## 纪要人
刘辉

#遗留任务
1、针对EdgeGalleyr当前成熟度情况，提出1-2个性能、可靠性/稳定性指标，并加入R1.2的测试计划--owner puhui、minxi
