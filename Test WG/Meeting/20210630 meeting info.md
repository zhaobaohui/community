**meeting 会议时间**: 20210630 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- xiert

## Topics议题

1.审视第三次迭代测试情况 --owner all

Review test results for R1.2-sprint3--owner all

【R1.2版98个story，尚有4个未关闭】

【bug情况：从5月初到6月30日，bug总量148个，其中严重bug8个(1个已完成)，主要bug53个（16个已完成），次要bug64个（17个已完成，1个已取消），其他bug23个（9个已完成）；共计已完成bug10个】

【当前共计12个遗留bug待处理】

【测试用例：96个】

【healthcheck：已交付3个story，尚有2个story未交付，联系宝辉配置访问权限，在开发环境下先行开展测试；】



【性能和故障测试预计延迟到R1.3版；】

2、审视R1.2测试报告 --owner all

Review test report for R1.2--owner all



## meeting link
 https://us02web.zoom.us/j/87179777658?pwd=em0zVmFmY091dEprZlNmL08vVStkZz09

## Minutes纪要
## 纪要人
刘辉

#遗留任务


1. 部署调测、atp里的沙箱是否包含mep，需同研发确认；--
2. applcm和rulemgnt的代码库是否需要合并，需要确认；--传雨 【R1.3版本周期内评估是否合并】
3. developer主流程不通，需研发尽快修改并在测试环境下更新版本；--当前主流程已通
4. examples-apps是否需要测试，需要确认；--传雨 【无须测试，不作为版本发布的内容】
5. 同UPF集成测试环境同鹿鑫确认；--鹿鑫 【在上周例会上已明确，接口基本畅通】
6. 在tsc确认healthcheck功能是否在R1.2内发布 --tsc【发布】
