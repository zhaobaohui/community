**meeting 会议时间**: 20210623 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- 李治谦（华为） 
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  于洋（华为）   
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- xiert

## Topics议题

1.审视第二次迭代测试情况 --owner all

Review test results for R1.2-sprint2--owner all

【bug情况：从5月初到6月23日，bug总量117个，其中严重bug7个(1个已完成)，主要bug41个（3个已完成），次要bug51个（4个已完成），其他bug18个（2个已完成）；共计已完成bug10个】

【测试用例：83个】

【虚拟机支撑的功能开始测试】

2、第三次迭代交付情况 --owner all

Discuss Delivery status for R1.2-Sprint3--owner all

【部署调测、atp里的沙箱是否包含mep，需同研发确认；applcm和rulemgnt的代码库是否需要合并，需要确认；developer主流程不通，需研发尽快修改并在测试环境下更新版本；EGMobile延期1.5w，是否需要在R1.2发布需要确认；examples-apps是否需要测试，需要确认；性能和故障测试放在7月5日这一周】

## meeting link
 https://us02web.zoom.us/j/87179777658?pwd=em0zVmFmY091dEprZlNmL08vVStkZz09

## Minutes纪要
## 纪要人
刘辉

#遗留任务
1、EGMobile延期1.5w，已确认不在R1.2发布，已延期到R1.3版本；
