**meeting 会议时间**: 20220105 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.R1.5测试情况总结 --owner all

【12月1日 bug总量71个，58个进行中，12个关闭，遗留1个】

【12月7日 bug总量108个，61个open，2个code，30个test，14个关闭，遗留1个】

【12月15日 bug总量127个，67个open，2个code，42个test，16个关闭，1个遗留；7个严重，22个主要，81个次要，12个不重要，6个无优先级】

【12月29日 bug总量277个，12个open，3个code，29个test，226个closed，4个cancel，4个遗留；15个严重，68个主要，168个次要，21个不重要，6个无优先级】

【1月5日 bug总量276个，272个closed，4个cancel；15个严重，68个主要，167个次要，21个不重要，6个无优先级】

【12月1日 story 82个，关闭0个】

【12月7日 story86个，关闭10个；POC story共计11个】

【12月15日 story87个，关闭20个；POC story共计13个，其中1个取消，2个关闭】

【12月29日 story89个,open1个，code 1个，test 51个，关闭31个，取消5个；POC story共计13个，其中code 1个，test 9个，closed 2个，取消1个】

【1月5日 story88个，83个closed，5个cancel；POC story共计13个，其中12个closed，1个取消】

【12月1日 epic 19个，关闭0个】

【12月7日 epic 23个，关闭0个】

【12月15日 epic 29个，关闭5个】

【12月29日 epic 34个，open 19个，code 4个，关闭11个】

【1月5日 epic 29个，closed 29个】

2.EG平台压力测试、性能测试讨论 --owner all

Discussion about stress&performance testing for EG platform --owner all

【需分析压力测试范围、性能测试指标范围】【一定资源配备下，中心侧同时管控多少边缘节点，模拟多用户同时在线访问平台，边缘侧API网关的转发能力等】

【R1.3版下已开展并发测试：针对mm5接口；并发能力忽大忽小，不稳定；研发未定位出原因】

3.专项测试讨论 --owner all

Discussion about special tests --owner all

【针对不同尺寸虚机镜像开展专项测试，1w--lyy】

## meeting link

腾讯会议 https://meeting.tencent.com/dm/WBjmpsf8JCkB  会议号：964-882-520



## Minutes纪要
## 纪要人
刘辉

#遗留任务
- 1.平台运行的云主机/主机ip地址变化，如何在不重装平台的前提下让整个平台进入新ip的稳定运行状态？【预计放入R1.6解决】--owner xudan
- 2.同倍源确认压力测试范围，性能测试指标范围 --owner lh、lq、zal、lyy等