**meeting 会议时间**: 20210929 16:30-17:30

## attendee参与人员
- 刘辉（紫金山实验室）
-  侯敏熙（Redhat） 
- _李治谦（华为） _
-    _姜伟（紫金山实验室）_   
- _ 张怡（联想）_ 
- 张阿利（中软）
-  李媛媛（中软）
- 孟璞辉(VMware) 
- 许丹（华为）
-  _于洋（华为）_
-  _ 丁宇卿（紫金山实验室）_ 
- 于小龙（紫金山实验室）
-   李强（华为） 
-  扈冰（华为） 
-    _kanaga（华为）_  
-  _shashikanth（华为）_ 
-  _蔡舒豪（华为）_ 
-  _徐军杰（九州云）_ 
- _yangyang 、zhangbeiyuan、liuhuiling、luxin、chenchuanyu 、chenruidong 、_zhaobaohui   （华为）
- _xiert_

## Topics议题

1.第三次迭代测试情况讨论 --owner all

discuss test results for R1.3-sprint2 --owner all

【截止9月14日，bug总量124，其中严重5个，主要42个，次要63个，其他14个；已完成17个，取消2个，遗留1个，进行中104个；测试用例123个；】

【截止9月14日，sprint1 story共计50个，已完成31个，已取消2个；sprint2 story共计61个，已完成51个；sprint3 story共计23个，已完成12个】

【截止9月22日09：30，bug总量148个，其中严重8个，主要50个，次要70个，其他20个；已完成22个，取消2个，遗留1个，进行中124个（待解决49个，待回归75）；测试用例123个；】

【截止9月22日09：30，sprint1 story共计51个，已完成35个，已取消2个；sprint2 story共计61个，已完成51个；sprint3 story共计24个，已完成12个】

【截止9月22日09：40，epic 28个，已完成7个，取消1个】

【截止9月29日13：30，bug总量222个，其中严重10个，主要70个，次要119个，其他23个；已完成109个，取消3个，遗留1个，进行中109个（待解决55个，待回归54个）；测试用例123个】

【截止9月29日13：30，sprint1 story共计51个，已完成42个，已取消1个；sprint2 story共计61个，已完成54个；sprint3 story共计24个，已完成15个】

【截止9月29日13：30，epic 28个，已完成10个，取消】

2.审视R1.3测试报告草稿 --owner all

Review R1.3 test report (draft) --owner all

## meeting link

腾讯会议：https://meeting.tencent.com/dm/R2jb1WdaZUA6 会议 ID：690 614 838

复制该信息，打开手机腾讯会议即可参与


## Minutes纪要
## 纪要人
刘辉

#遗留任务

- 5.梳理故障测试结果，提交到gitee release目录下 --owner 孟璞辉
- 7.EdgeGallery性能测试结果梳理，提交到gitee release目录下--owner 张阿利

