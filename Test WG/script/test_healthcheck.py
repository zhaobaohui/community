#! -*- coding:utf-8 -*-

import requests
import pytest
import json
import subprocess
import sys
import os

# Resolve script loss parameter on bash environment
my_env = os.environ.copy()
center_start_action = "http://192.168.100.33:32757/health-check/v1/center/action/start"
center_get_health = "http://192.168.100.33:32757/health-check/v1/center/health"
edge_self_check = "http://192.168.100.33:32759/health-check/v1/edge/health"
edge_check_other_edge = "http://192.168.100.33:32759/health-check/v1/edge/action/start"
body = {"mechostIP": ["119.8.47.5", "192.168.100.33", "192.168.100.106"]}
headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

# Here is request class, all of test request will use TestApi
class TestApi(object):
    def __init__(self, test_url, data=None, headers=None):
        self.url = test_url
        self.data = data
        self.headers = headers

    def health_check_get(self):
        test_get = requests.get(self.url)
        return test_get

    def health_check_post(self):
        test_post = requests.post(self.url, data=self.data, headers=self.headers)
        return test_post


# Here is a run shell cmd function
def runcmd(command):
    """
    :param command: input shell command
    :return: command result
    """
    ret = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                           encoding="utf-8", env=my_env)
    try:
        stdout, stderr = ret.communicate(timeout=3600)
    except TimeoutError:
        ret.kill()
        stdout, stderr = ret.communicate()
        sys.exit(1)
    else:
        if ret.returncode != 0:
            return ret.returncode
        else:
            pass
    return stdout



# Here
class TestClass(object):

# Test all service work as well
    def test_center_start(self):
        a = TestApi(center_start_action)
        center_start_check = a.health_check_get()
        center_start_check_response = center_start_check.json()
        # check response code similar with 2xx
        assert str(center_start_check.status_code).startswith("2")
        # check response contain checkedIp and condition key
        for i in center_start_check_response:
            assert "checkedIp" in i.keys() and "condition" in i.keys()

    def test_edge_self_check(self):
        b = TestApi(edge_self_check)
        edge_self_check_get = b.health_check_get()
        assert str(edge_self_check_get.status_code).startswith("2")

    def test_edge_self_check_response_body(self):
        b = TestApi(edge_self_check)
        edge_self_check_get = b.health_check_get()
        edge_self_check_response = edge_self_check_get.text
        assert "failed" not in edge_self_check_response

    def test_edge_check_other_node_response_body(self):
        c = TestApi(edge_check_other_edge, json.dumps(body))
        edge_check_other_node = c.health_check_post()
        edge_check_other_node_response = edge_check_other_node.json()
        assert str(edge_check_other_node.status_code).startswith("2")
        assert "checkerIp" in edge_check_other_node_response and "edgeCheckInfo" in edge_check_other_node_response

    def test_center_health_check(self):
        d = TestApi(center_get_health)
        center_health_check = d.health_check_get()
        assert str(center_health_check.status_code).startswith("2")

    def test_center_health_check_response(self):
        d = TestApi(center_get_health)
        center_health_check = d.health_check_get()
        center_health_check_response = center_health_check.text
        assert str(center_health_check.status_code).startswith("2")
        assert "ok" in center_health_check_response

# Test Delete healthcheckk8s service


class Test_edge_connection(object):
    @pytest.fixture()
    def Disable_edge_helm(self):
        runcmd('/usr/local/bin/helm delete healthcheck')
        runcmd('sleep 30')

    def test_edge_self_check(self, Disable_edge_helm):
        a = runcmd('/usr/bin/curl -X GET {}'.format(edge_self_check))
        # if return code not equal 0, adjust test failed
        assert 7 == a

    def test_edge_check_other_edge(self, Disable_edge_helm):
        a = runcmd('/usr/bin/curl -H {} -X POST {} -d {}'.format(headers, edge_check_other_edge, body))
        assert 7 == a

    def teardown_class(self):
        runcmd('/usr/local/bin/helm install healthcheck /usr/upload_data/healthcheck/helm-chart/healthcheck')
        runcmd('sleep 300')


class Test_center_connection(object):
    @pytest.fixture()
    def Disable_center_helm(self):
        runcmd('/usr/local/bin/helm delete healthcheck-m')
        runcmd('sleep 30')

    def test_center_start_action(self,Disable_center_helm):
        a = runcmd('/usr/bin/curl -X GET {}'.format(center_start_action))
        assert 7 == a

    def test_center_get_health(self, Disable_center_helm):
        a = runcmd('/usr/bin/curl -X GET {}'.format(center_get_health))
        assert 7 == a

    def teardown_class(self):
        runcmd('/usr/local/bin/helm install healthcheck /usr/upload_data/healthcheck/helm-chart/healthcheck-m')
        runcmd('sleep 300')