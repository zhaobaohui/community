# EdgeGallery v1.5 Security Test Plan

## EdgeGallery v1.5 安全测试计划



#### 1.测试目标 Test target

本测试计划为EdgeGallery v1.5 安全测试计划，目的在于发现应用程序在版本迭代过程中可能出现的安全缺陷/漏洞，并评估其严重等级，并提供修补建议。以确保应用程序在版本迭代过程中的安全性。

This test plan is the EdgeGallery v1.5 security test plan. The purpose is to discover possible bugs/vulnerabilities of the application during the version iteration process, and evaluate its threat-level, and then provide some repair suggestions. The finally purpose is to ensure the security of the application during the version iteration process.

#### 2.先决条件 Prerequisites

[v1.5里程碑计划 Release Milestone](https://gitee.com/edgegallery/community/tree/master/TSC/Release/v1.5)

[v1.5需求设计文档](https://gitee.com/edgegallery/community/tree/master/Architecture%20WG/Requirements/v1.5)

[v1.5需求任务列表 Requirement List](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866)

[v1.5特性任务列表 Story List](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bissue_type_id%5D=199539)

[v1.5安全设计文档 Security Design Docs](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Compliance%20Report/Version%201.5)

[Edgegallery安全设计规范 Security Design Guide](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Secure%20Design/Security%20Design%20Guide%20CN.md)

[EdgeGallery合规测试用例 Compliance Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases)

[EdgeGallery渗透测试用例 Penetration Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases)

#### 3.测试日期计划 Test Date Plain

v1.5版本整体分为2个迭代，由2021.11.08启动编码，计划于2022.01.10发布Release1.5版本。

| Date          | Coding Status    | Compliance Test          | Penetration Test          | Regression Test          |
| ------------- | ---------------- | ------------------------ | ------------------------- | ------------------------ |
| 11.08 - 11.21 | Sprint 1 coding  |                          |                           |                          |
| 11.22 - 12.05 | Sprint 2 coding  | Sprint 1 compliance test | Sprint 1 penetration test |                          |
| 12.06 - 12.19 | Bug-fixing       | Sprint 2 compliance test | Sprint 2 penetration test | Sprint 1 regression test |
| 12.20 - 01.02 | Bug-fixing       |                          |                           | Sprint 2 regression test |
| 01.03 - 01.10 | Ready to release |                          |                           |                          |

#### 4.测试人员及分工 Testers and Division

4.1 扈冰  Hu Bing

​    UserMgmt (合规测试/渗透测试)

​    Appstore (合规测试/渗透测试)

​    Developer (合规测试/渗透测试)

​    ATP (合规测试/渗透测试)

​    MECM (合规测试/渗透测试)

​    MEP (合规测试/渗透测试)

#### 5.测试范围 Test Scope

测试范围一般按照特性划分。

The test scope is generally divided according to stories. 

##### 5.1  sprint1:


| Story        | 开发状态   | 关联合规测试用例 |  合规测试状态  |关联渗透测试用例| 渗透测试状态  |
| --------   | :----:  | :----: | :----: |  :----: | :----:  |
| [【edgegallery-fe】边缘应用网络配置](https://e.gitee.com/OSDT/issues/list?issue=I4F9SS) |          | - |              |                  |              |
| [【edgegallery-fe】5G 边缘应用流水线主界面](https://e.gitee.com/OSDT/issues/list?issue=I4EZL5) |          | - |              |                  |              |
| [【edgegallery-fe】edgegallery新版首页入口](https://e.gitee.com/OSDT/issues/list?issue=I4F95A) |          | - |              |                  |              |
| [【edgegallery-fe】页面左侧项目管理组件](https://e.gitee.com/OSDT/issues/list?issue=I4EZLX) |          | - |              |                  |              |
| [【edgegallery-fe】新建项目表格](https://e.gitee.com/OSDT/issues/list?issue=I4F95Z) |          | - |              |                  |              |
| [【edgegallery-fe】能力中心界面](https://e.gitee.com/OSDT/issues/list?issue=I4F9DD) |          | - |              |                  |              |
| [【edgegellery-fe】沙箱选择引导页面以及沙箱详情](https://e.gitee.com/OSDT/issues/list?issue=I4F9L4) |          | - |              |                  |              |
| [【atp】atp镜像精简](https://e.gitee.com/OSDT/issues/list?issue=I4G9YM) |          | - |              |                  |              |
| [【edgegallery-fe】边缘应用虚机配置](https://e.gitee.com/OSDT/issues/list?issue=I4F9VH) |          | - |              |                  |              |
| [【installer】镜像精简，使用nginx alpine镜像](https://e.gitee.com/OSDT/issues/list?issue=I4GA3F) |          |                  |              |                  |              |
| [【MECM】支持zip格式的应用包 MECM Application packages in .zip format are supported.](https://e.gitee.com/OSDT/issues/list?issue=I4G5FD) |          | - |              |                  |              |
| [【developer】支持APPD细节参数在线修改](https://e.gitee.com/OSDT/issues/list?issue=I4GC2N) |          |                  |              |                  |              |
| [【developer】支持镜像文件在线修改能力（同应用包修改）](https://e.gitee.com/OSDT/issues/list?issue=I4GC3M) |          |                  |              |                  |              |
| [【imageops】check和compress提供超时机制，超时会退出](https://e.gitee.com/OSDT/issues/list?issue=I4E8KS) |          |                  |              |                  |              |
| [【filesystem】Check当前镜像时，非qcow2格式要增加返回值](https://e.gitee.com/OSDT/issues/list?issue=I4G4SH) |          |                  |              |                  |              |
| [【filesystem】Compress前检查环境，存储空间不足时立即报错退出](https://e.gitee.com/OSDT/issues/list?issue=I4G4SO) |          |                  |              |                  |              |
| [【ATP】支持zip格式的应用包](https://e.gitee.com/OSDT/issues/list?issue=I4G5FC) |          |                  |              |                  |              |
| [【UserMgmt】支持通过proxy访问EdgeGallery](https://e.gitee.com/OSDT/issues/list?issue=I4HTET) |          |                  |              |                  |              |
| [【AppStore】应用上传对应用包做完整性校验](https://e.gitee.com/OSDT/issues/list?issue=I4G54K) |          |                  |              |                  |              |
| [【edgegallery-fe】打包完成界面弹窗](https://e.gitee.com/OSDT/issues/list?issue=I4GD4G) |          | - |              |                  |              |
| [【edgegallery-fe】atp测试部分页面](https://e.gitee.com/OSDT/issues/list?issue=I4GE5X) |          | - |              |                  |              |
| [【edgegallery-fe】应用打包预览界面](https://e.gitee.com/OSDT/issues/list?issue=I4GCZ0) |          | - |              |                  |              |
| [【edgegallery-fe】沙箱内部5G边缘应用架构图](https://e.gitee.com/OSDT/issues/list?issue=I4F9PH) |          | - |              |                  |              |
| [【edgegallery-fe】虚机边缘应用配置完成状态](https://e.gitee.com/OSDT/issues/list?issue=I4GCJM) |          | - |              |                  |              |
| [【developer】后台接口重构](https://e.gitee.com/OSDT/issues/list?issue=I4GC3Z) |          |                  |              |                  |              |
| [【AppStore】应用行业和类型重新划分](https://e.gitee.com/OSDT/issues/list?issue=I4FFON) |          |                  |              |                  |              |
| [【AppStore】应用仓库和我的应用获取应用包信息的接口分离，权限区分](https://e.gitee.com/OSDT/issues/list?issue=I4GF8X) |          |                  |              |                  |              |
| [【developer】支持虚机拉起后远程VNC](https://e.gitee.com/OSDT/issues/list?issue=I4GC20) |          | 1.1, 1.2, 1.3, 1.5, 3.3, 5.1, 8.1, 8.2, 9.2, 9.3 |              |                  |              |
| [【AppStore】App变现----MECM提供配套接口](https://e.gitee.com/OSDT/issues/list?issue=I4HG7X) |          |                  |              |                  |              |
| [【ThirdSystem】三方系统管理，包含三方系统的增删改查等功能](https://e.gitee.com/OSDT/issues/list?issue=I4G5FO) |          | 1.2, 2.2, 3.3 |              |                  |              |
| [【Developer】支持zip格式的应用包](https://e.gitee.com/OSDT/issues/list?issue=I4G5FB) |          |                  |              |                  |              |
| [【AppStore】应用在线体验流程优化](https://e.gitee.com/OSDT/issues/list?issue=I4G4WD) |          |                  |              |                  |              |
| [【developer】作为应用开发者，可以通过界面查看他上传的helmchart包的详情](https://e.gitee.com/OSDT/issues/list?issue=I4G4TX) |          |                  |              |                  |              |
| [【developer】作为应用开发者，可以上传一个k8s部署文件，完成应用的集成开发](https://e.gitee.com/OSDT/issues/list?issue=I4HBXH) |          |                  |              |                  |              |


##### 5.2  sprint2:


| Story        | 开发状态   | 关联合规用例 | 合规测试状态 | 关联渗透用例  | 渗透测试状态  |
| :-------   | :----:  | :----: | :----: |  :----: | :----:  |
| [提供一套完整的加解密代码模板，供各模块嵌入使用](https://e.gitee.com/OSDT/issues/list?issue=I4G4ST) |          |              |              |              |              |
| [【developer】支持灵活打包能力，应用包二次修改](https://e.gitee.com/OSDT/issues/list?issue=I4GC3A) |          |              |              |              |              |
| [【mepm-fe】边缘自治Portal支持基于OpenStack的虚机申请流程。Add Page for resource api use, implement lightweight cloud management features.](https://e.gitee.com/OSDT/issues/list?issue=I4G4WY) |          |              |              |              |              |
| [【lcm】lcm支持多虚机包解析，部署](https://e.gitee.com/OSDT/issues/list?issue=I4GC4L) |          |              |              |              |              |
| [【lcm】lcm支持网络创建](https://e.gitee.com/OSDT/issues/list?issue=I4GC6H) |          |              |              |              |              |
| [【UserMgmt】EdgeGallery支持北向接口](https://e.gitee.com/OSDT/issues/list?issue=I4HTDB) |          | 1.1, 1.2, 1.3, 1.5, 3.3, 5.1, 8.1, 8.2, 9.2, 9.3 |              |              |              |
| [【UserMgmt】支持与第三方IAM系统集成](https://e.gitee.com/OSDT/issues/list?issue=I4HTBV) |          | 1.1 |              |              |              |
| [【AppStore】App变现后台实现](https://e.gitee.com/OSDT/issues/list?issue=I4HG7L) |          |              |              |              |              |
| [【developer】虚机镜像瘦身增强](https://e.gitee.com/OSDT/issues/list?issue=I4HGGO) |          |              |              |              |              |
| [【AppStore】App变现界面设计优化](https://e.gitee.com/OSDT/issues/list?issue=I4HG7J) |          | - |              |              |              |
| [【developer】查询、批量查询、删除profile接口](https://e.gitee.com/OSDT/issues/list?issue=I4GC9Q) |          |              |              |              |              |
| [【developer】profile管理列表页面](https://e.gitee.com/OSDT/issues/list?issue=I4GCAK) |          |              |              |              |              |
| [【developer】选择profile创建项目界面](https://e.gitee.com/OSDT/issues/list?issue=I4GCAA) |          | - |              |              |              |
| [【developer】新增profile接口](https://e.gitee.com/OSDT/issues/list?issue=I4GC99) |          |              |              |              |              |
| [【developer】profile新增、修改、删除页面功能](https://e.gitee.com/OSDT/issues/list?issue=I4GCAX) |          |              |              |              |              |
| [【developer】解析profile生成部署文件](https://e.gitee.com/OSDT/issues/list?issue=I4GC8A) |          |              |              |              |              |
| [【developer】更新profile接口](https://e.gitee.com/OSDT/issues/list?issue=I4GC9I) |          |              |              |              |              |
| [【ThirdSystem】MEAO的调用通过三方系统，AppStore修改调用逻辑](https://e.gitee.com/OSDT/issues/list?issue=I4G5FX) |          | 1.2, 2.2 |              |              |              |
| [【mecm】部署成功的应用支持执行配置脚本](https://e.gitee.com/OSDT/issues/list?issue=I4GHAB) |          | - |              |              |              |
| [【developer】作为应用开发者，可以上传一个完好的helmchart包到dev平台上，完成应用的集成开发](https://e.gitee.com/OSDT/issues/list?issue=I4G4T7) |          |              |              |              |              |
| [【developer】作为应用开发者，可以通过界面在回显的helmchart脚本中增加mep、mep-agent的依赖](https://e.gitee.com/OSDT/issues/list?issue=I4G4V6) |          |              |              |              |              |
| [【ThirdSystem】MECM打通通过三方系统调用OpenStack](https://e.gitee.com/OSDT/dashboard?issue=I4G5FZ) | | 1.2, 2.2 | | | |

#### 6.安全测试用例管理 Security Test Case Management

安全测试用例包括 [安全合规测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases) 和 [渗透测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases) 。

测试用例包括：测试用例ID，测试目的，测试描述，测试工具及其配置，测试步骤，预期结果等。

测试用例模板请参考 [测试用例模板](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Testing/Security%20Test%20Case%20Templeate.rst) 。

测试结果包括：通过 / 不通过

Security test cases include [Security Compliance Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases) and [Penetration Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases).

Test cases include: test case ID, test purpose, test description, test tool and configuration, test steps, expected results, etc.

Please refer to [Test Case Template](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Testing/Security%20Test%20Case%20Templeate.rst) for test case templates.

Test results include: pass / fail

#### 7.缺陷/漏洞管理 Bug/vulne Management

安全测试过程中发现的安全问题/漏洞统一在Gitee中录入，录入方法请参考[操作指南](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md)。

漏洞的严重性包括：严重，主要，次要，一般。

详细安全问题issue，请查看 [安全问题issue](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bsearch%5D=Security&is%5Bissue_type_id%5D=199540&is%5Bsort%5D=created_at&is%5Bdirection%5D=desc&openMore=1)。

Bugs/vulnerabilities are entered in Gitee. Please click [operation guide](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md).

The priority of bugs/vulnerabilities is divided into: serious, major, minor, and general.

#### 8.相关工具 Relative Tools

| No.  | Tool name | version   | purpose                                   | comment |
| ---- | --------- | --------- | ----------------------------------------- | ------- |
| 1    | BurpSuite | v2020.9.1 | Capture http package for analyzing        | ...     |
| 2    | OWASP ZAP | 2.9.0     | Comprehensive vulnerability scanning tool |         |
| 3    | SenInfo   | 2.0.10    | Scan for sensitive information            |         |

#### 9.手动开发测试脚本 Test scripts developed manually

| No.  | Tool name | version | purpose | comment |
| ---- | --------- | ------- | ------- | ------- |
|      | N/A       | ...     | ...     | ...     |

#### 10.测试总结 Test Summary

测试完毕后，将生成 “EdgeGallery v1.5安全测试报告”。

After the test, plain to export “EdgeGallery v1.5 Security Test Report”.

