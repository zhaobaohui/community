# EdgeGallery v1.2 Security Test Plan

## EdgeGallery v1.2 安全测试计划



#### 1.测试目标 Test target

本测试计划为EdgeGallery v1.2 安全测试计划，目的在于发现应用程序在版本迭代过程中可能出现的缺陷/漏洞，并评估其严重等级，并提供修补建议。以确保应用程序在版本迭代过程中的安全性。

This test plan is the EdgeGallery v1.2 security test plan. The purpose is to discover possible bugs/vulnerabilities of the application during the version iteration process, and evaluate its threat-level, and then provide some repair suggestions. The finally purpose is to ensure the security of the application during the version iteration process.

#### 2.先决条件 Prerequisites

[v1.2里程碑计划 Release Milestone](https://gitee.com/edgegallery/community/tree/master/TSC/Release/v1.2)

[v1.2需求设计文档](https://gitee.com/edgegallery/community/tree/master/Architecture%20WG/Requirements/v1.2)

[v1.2需求任务列表 Requirement List](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=101482&is%5Bissue_type_id%5D=238024)

[v1.2特性任务列表 Story List](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=101482&is%5Bissue_type_id%5D=199539)

[v1.2安全设计文档 Security Design Docs](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Compliance%20Report/Version%201.2)

[Edgegallery安全设计规范 Security Design Guide](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Secure%20Design/Security%20Design%20Guide%20CN.md)

[EdgeGallery合规测试用例 Compliance Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases)

[EdgeGallery渗透测试用例 Penetration Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases)

#### 3.测试日期计划 Test Date Plain

v1.2版本整体分为3个迭代，由2021.5.6启动编码，计划于2021.7.7发布Release1.2版本。

| Date        | Coding Status    | Compliance Test          | Penetration Test          | Regression Test            |
| ----------- | ---------------- | ------------------------ | ------------------------- | -------------------------- |
| 5.6 - 5.21  | Sprint 1 coding  |                          |                           |                            |
| 5.24 - 6.4  | Sprint 2 coding  | Sprint 1 compliance test | Sprint 1 penetration test |                            |
| 6.7 - 6.18  | Sprint 3 coding  | Sprint 2 compliance test | Sprint 2 penetration test |                            |
| 6.21 - 6.28 | Bug-fixing       | Sprint 3 compliance test | Sprint 3 penetration test | Sprint 1 regression test   |
| 6.29 - 7.5  | Bug-fixing       |                          |                           | Sprint 2,3 regression test |
| 7.6 - 7.7   | Ready to release |                          |                           |                            |

#### 4.测试人员及分工 Testers and Division

4.1 扈冰  Hu Bing

​    UserMgmt (合规测试/渗透测试)

​    Appstore (合规测试/渗透测试)

​    Developer (合规测试/渗透测试)

​    ATP (合规测试/渗透测试)

​    MECM (合规测试/渗透测试)

​    MEP (合规测试/渗透测试)

#### 5.测试范围 Test Scope

测试范围一般按照特性划分。

The test scope is generally divided according to stories. 

##### 5.1  sprint1:


| Story        | 开发状态   | 关联合规用例 |  合规安全测试  |关联渗透用例| 渗透测试状态  |
| --------   | :----:  | :----: | :----: |  :----: | :----:  |
| [【MECM】MECM后台统一servicecomb](https://e.gitee.com/OSDT/issues/list?issue=I3PVD8) | 已完成 |       |         |              |              |
| [【MECM】边缘自治增强--mep合并到mepm](https://e.gitee.com/OSDT/issues/list?issue=I3PV9W) | 已完成 |2.2|         |          |              |
| [【ATP】测试任务页面优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYTB) | 已完成 |         |      |      |       |
| [【MECM】MECM支持applcm和apprulemgr注册统一(sprint1,sprint2)](https://e.gitee.com/OSDT/issues/list?issue=I3PVDV) | 已完成 |    |  |    |   |        
| [【ATP】前台新增批量导入入口](https://e.gitee.com/OSDT/issues/list?issue=I3OYUA) | 已完成 |        |         |         |              |
| [【HealthCheck】各边缘节点初始化](https://e.gitee.com/OSDT/issues/list?issue=I3PS0N) | 已完成 |   |      |              |              |
| [【HealthCheck】MECM层调用HealthCheck](https://e.gitee.com/OSDT/issues/list?issue=I3PRZ3) | 已完成 | |      |              |              |
| [【developer】虚机资源配置可编辑](https://e.gitee.com/OSDT/issues/list?issue=I3P0F5) | 已完成 | 1.3|      |              |              |
| [【AppStore】文档界面优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYFZ) | 已完成 | | | | |
| [【developer】支持input参数补齐](https://e.gitee.com/OSDT/issues/list?issue=I3P09A) | 已完成 | | | | |
| [【AppStore】上传应用时可设置应用展示模式](https://e.gitee.com/OSDT/issues/list?issue=I3OYGC) | 已完成 | | | | |
| [【AppStore】支持显示应用部署类型虚机还是容器](https://e.gitee.com/OSDT/issues/list?issue=I3IL7I) | 已完成 | | | | |
| [【MECM】MECM支持前台节点位置可自定义](https://e.gitee.com/OSDT/issues/list?issue=I3PVE0) | 已完成 |1.3 |已完成，OK | | |
| [【UserMgmt】发送验证码到用户手机或邮箱的前端滑块验证方式优化为后台验证码](https://e.gitee.com/OSDT/issues/list?issue=I3PRRQ) | 已完成 |1.11, 1.16, 5.1 | 已完成，OK| 013|已完成，OK |
| [【UserMgmt】登录的滑块验证方式调整为与后台交互的验证码](https://e.gitee.com/OSDT/issues/list?issue=I3PRRH) | 已完成 |1.11, 1.16, 5.1 |已完成，OK | | |
| [【UserMgmt】用户注册支持与后台交互的验证](https://e.gitee.com/OSDT/issues/list?issue=I3PRSB) | 已完成 | 1.11, 1.16, 5.1| 已完成，OK| | |
| [【AppStore】操作分析界面优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYFR) | 已完成 | | | | |
| [【ATP】后台新增贡献批量删除、脚本类型贡献下载接口](https://e.gitee.com/OSDT/issues/list?issue=I3OYUK) | 已完成 |1.12, 9.2, 9.4 |已完成，OK | | |
| [【ATP】后台新增批量导入包解析接口](https://e.gitee.com/OSDT/issues/list?issue=I3OYU7) | 已完成 |1.3, 1.12, 5.3, 7.4, 9.2, 9.4 | | | |
| [【ATP】关键数据持久化](https://e.gitee.com/OSDT/issues/list?issue=I3OY7E) | 已完成 | | | |目录挂载安全测试 |
| [【AppStore】支持修改应用属性](https://e.gitee.com/OSDT/issues/list?issue=I3OYGM) | 已完成 | | | | |
| [【ATP】前台新增贡献管理页面](https://e.gitee.com/OSDT/issues/list?issue=I3OYUP) | 已完成 | | | | |
| [【ATP】ATP首页优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYSU) | 已完成 | | | | |
| [【AppStore】关键数据持久化](https://e.gitee.com/OSDT/issues/list?issue=I3OY4V) | 已完成 | | | |目录挂载安全测试 |


##### 5.2  sprint2:


| Story        | 开发状态   | 关联合规用例 | 合规测试状态 | 关联渗透用例  | 渗透测试状态  |
| :-------   | :----:  | :----: | :----: |  :----: | :----:  |
| [【MECM】ak/sk config interface add app info](https://e.gitee.com/OSDT/issues/list?issue=I3TKL6)| 已完成 ||    |     |   |
| [【MEP】服务发现增加鉴权认证](https://e.gitee.com/OSDT/issues/list?issue=I3S46O)  | 已完成 |   |       |              |              |
| [【appstore】appstore支持虚机镜像的管理以及上传下载](https://e.gitee.com/OSDT/issues/list?issue=I3ROLN)| 已完成 |  1.2  | 已完成，OK |007|已完成，OK|
|[【MECM】首页展示优化](https://e.gitee.com/OSDT/issues/list?issue=I3RO23)| 已完成 | |   |    |         |
|[【developer】将部署调测分为资源配置和部署调测两部分，部署调测部分可选](https://e.gitee.com/OSDT/issues/list?issue=I3RLMO)| 已完成 |  |   | |       |
|[【Developer】能力中心界面优化重构](https://e.gitee.com/OSDT/issues/list?issue=I3R0EN) | 已完成 |    |      |              |              |
|[【MECM】MECM支持applcm和apprulemgr注册统一](https://e.gitee.com/OSDT/issues/list?issue=I3PVDV)| 已完成 |  |   |    |     |
|[【UserMgmt】支持非guest用户90天后必须修改密码](https://e.gitee.com/OSDT/issues/list?issue=I3PS3I)| 已完成 |1.9, 1.14  | 已完成，OK |  功能绕过测试   |     |
|[【UserMgmt】支持admin首次登录成功后强制修改密码](https://e.gitee.com/OSDT/issues/list?issue=I3PS3B)| 已完成 |1.9, 1.14 |[I3VVX5](https://e.gitee.com/OSDT/issues/list?issue=I3VVX5)不通过，其余通过  |    功能绕过测试   |     |
|[【UserMgmt】个人帐号中心展示我的权限(角色+可访问平台)](https://e.gitee.com/OSDT/issues/list?issue=I3PS0D)| 已完成 |  |  |  |              |
|[【developer】虚机镜像管理（用户，管理员权限）](https://e.gitee.com/OSDT/issues/list?issue=I3P0FD)|已完成 |1.12 | 已完成，OK| |  |
|[【developer】虚机资源配置可编辑](https://e.gitee.com/OSDT/issues/list?issue=I3P0F5)| 已完成 |   |        |              |              |
|[【developer】支持配置app_configuration, 支持定义AK/SK, APP分流规则，DNS规则等](https://e.gitee.com/OSDT/issues/list?issue=I3P0EG)| 已完成 || | |  |
|[【ATP】测试用例增强](https://e.gitee.com/OSDT/issues/list?issue=I3OYTL)| 已完成 |      |        |              |              |
|[【AppStore】应用管理界面https://e.gitee.com/OSDT/issues/list?issue=I3OYGU]() | 已完成 |1.12  |    |              |              |
|[【MEP】关键数据持久化](https://e.gitee.com/OSDT/issues/list?issue=I3OY7Y) | 已完成 |  |  |              |              |
|[【MEP】 Code quality improvement](https://e.gitee.com/OSDT/issues/list?issue=I3OW9P) | 已完成 |  |      |              |              |
|[【MEP】Public the UPF capability to EdgeGallery](https://e.gitee.com/OSDT/issues/list?issue=I2E0SR) | 已完成 | |      |        |              |
|[【MEP】MEP adapter register to EdgeGallery MEP and UPF](https://e.gitee.com/OSDT/issues/list?issue=I2E0SN)| 已完成 |1.1, 1.3, 1.5, 2.2, 3.1 |   |  |              |



##### 5.3  sprint3:

| Story        | 开发状态   | 关联合规用例 |  合规测试状态  | 关联渗透用例  | 渗透测试状态 |
| --------   | -----:  | :----: | :----:  | :----:  | :----:  |
|[【atp】统一容器和虚拟机appd为zip](https://e.gitee.com/OSDT/issues/list?issue=I3U7P9)|已完成| | | | |
|[【MEP】Sync services bi-directional(1. UEG-EG, 2. EG-UEG)](https://e.gitee.com/OSDT/issues/list?issue=I3T0UU)|已完成| | | | |
|[【MEP】timing api](https://e.gitee.com/OSDT/issues/list?issue=I3S47J)|已完成|1.1, 1.3 |测试中 | | |
|[【MEP】transport api](https://e.gitee.com/OSDT/issues/list?issue=I3S47F)|已完成|1.1, 1.3 | 测试中| | |
|[【Developer】工作空间，文档（从集成工具中提到一级菜单）界面优化重构](https://e.gitee.com/OSDT/issues/list?issue=I3R0JM)|已完成| | | | |
|[【ATP】ATP支持虚机APP的实例化和终止用例](https://e.gitee.com/OSDT/issues/list?issue=I3QER1)|已完成| | | | |
|[【MECM】MECM支持虚机APP包同步和分发，MECM support app package management of VM app](https://e.gitee.com/OSDT/issues/list?issue=I3PVBT)|已完成| | | | |
|[【MECM】mecm支持VIM预配置 MECM support the preconfig of OpenStack](https://e.gitee.com/OSDT/issues/list?issue=I3PV5W)|已完成| | | | |
|[【MECM】mecm支持虚机APP的实例化和终止 VM APP instantiation and termination support.](https://e.gitee.com/OSDT/issues/list?issue=I3PV4Y)|已完成| | | | |
|[【developer】虚机mepagent的集成](https://e.gitee.com/OSDT/issues/list?issue=I3P0ES)|已完成| | | | |
|[【developer】支持配置app_configuration, 支持定义AK/SK, APP分流规则，DNS规则等](https://e.gitee.com/OSDT/issues/list?issue=I3P0EG)|已完成| | | | |
|[【developer】input根据不同的操作系统，导入不同的初始化脚本](https://e.gitee.com/OSDT/issues/list?issue=I3P0B6)|已完成| | | | |
|[【AppStore】应用在线体验](https://e.gitee.com/OSDT/issues/list?issue=I3OYLO)|已完成|1.2 |已完成，OK | 007|已完成，OK |
|[【User-Mgmt】接口响应规范优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYBM)|已完成| | | | |
|[【MEP】接口响应规范优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYBC)|已完成| | | | |
|[【Atp】接口响应规范优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYB6)|已完成| | | | |
|[【AppStore】接口响应规范优化](https://e.gitee.com/OSDT/issues/list?issue=I3OYAT)|已完成| | | | |
|[【User-Mgmt】关键数据持久化](https://e.gitee.com/OSDT/issues/list?issue=I3OY8N)|已完成| | | | |



#### 6.安全测试用例管理 Security Test Case Management

安全测试用例包括 [安全合规测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases) 和 [渗透测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases) 。

测试用例包括：测试用例ID，测试目的，测试描述，测试工具及其配置，测试步骤，预期结果等。

测试用例模板请参考 [测试用例模板](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Testing/Security%20Test%20Case%20Templeate.rst) 。

测试结果包括：通过 / 不通过

Security test cases include [Security Compliance Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases) and [Penetration Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases).

Test cases include: test case ID, test purpose, test description, test tool and configuration, test steps, expected results, etc.

Please refer to [Test Case Template](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Testing/Security%20Test%20Case%20Templeate.rst) for test case templates.

Test results include: pass / fail

#### 7.缺陷/漏洞管理 Bug/vulne Management

缺陷/漏洞统一在Gitee中录入，录入方法请参考[操作指南](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md)。

缺陷/漏洞优先级分为：严重，主要，次要，一般。

Bugs/vulnerabilities are entered in Gitee. Please click [operation guide](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md).

The priority of bugs/vulnerabilities is divided into: serious, major, minor, and general.

#### 8.相关工具 Relative Tools

| No.  | Tool name | version   | purpose                                   | comment |
| ---- | --------- | --------- | ----------------------------------------- | ------- |
| 1    | BurpSuite | v2020.9.1 | Capture http package for analyzing        | ...     |
| 2    | OWASP ZAP | 2.9.0     | Comprehensive vulnerability scanning tool |         |
| 3    | SenInfo   | 2.0.10    | Scan for sensitive information            |         |

#### 9.手动开发测试脚本 Test scripts developed manually

| No.  | Tool name | version | purpose | comment |
| ---- | --------- | ------- | ------- | ------- |
|      | N/A       | ...     | ...     | ...     |



#### 10.测试总结 Test Summary

测试完毕后，将生成 “EdgeGallery v1.2安全测试报告”。

After the test, plain to export “EdgeGallery v1.2 Security Test Report”.

