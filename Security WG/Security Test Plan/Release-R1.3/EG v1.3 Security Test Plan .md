# EdgeGallery v1.3 Security Test Plan

## EdgeGallery v1.3 安全测试计划



#### 1.测试目标 Test target

本测试计划为EdgeGallery v1.3 安全测试计划，目的在于发现应用程序在版本迭代过程中可能出现的缺陷/漏洞，并评估其严重等级，并提供修补建议。以确保应用程序在版本迭代过程中的安全性。

This test plan is the EdgeGallery v1.3 security test plan. The purpose is to discover possible bugs/vulnerabilities of the application during the version iteration process, and evaluate its threat-level, and then provide some repair suggestions. The finally purpose is to ensure the security of the application during the version iteration process.

#### 2.先决条件 Prerequisites

[v1.3里程碑计划 Release Milestone](https://gitee.com/edgegallery/community/tree/master/TSC/Release/v1.3)

[v1.3需求设计文档](https://gitee.com/edgegallery/community/tree/master/Architecture%20WG/Requirements/v1.3)

[v1.3需求任务列表 Requirement List](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=126164&is%5Bissue_type_id%5D=238024)

[v1.3特性任务列表 Story List](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=126164&is%5Bissue_type_id%5D=199539)

[v1.3安全设计文档 Security Design Docs](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Compliance%20Report/Version%201.3)

[Edgegallery安全设计规范 Security Design Guide](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Secure%20Design/Security%20Design%20Guide%20CN.md)

[EdgeGallery合规测试用例 Compliance Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases)

[EdgeGallery渗透测试用例 Penetration Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases)

#### 3.测试日期计划 Test Date Plain

v1.3版本整体分为3个迭代，由2021.7.26启动编码，计划于2021.9.30发布Release1.3版本。

| Date        | Coding Status    | Compliance Test          | Penetration Test          | Regression Test            |
| ----------- | ---------------- | ------------------------ | ------------------------- | -------------------------- |
| 7.26 - 8.15 | Sprint 1 coding  |                          |                           |                            |
| 8.16 - 8.29 | Sprint 2 coding  | Sprint 1 compliance test | Sprint 1 penetration test |                            |
| 8.30 - 9.12 | Sprint 3 coding  | Sprint 2 compliance test | Sprint 2 penetration test |                            |
| 9.13 - 9.19 | Bug-fixing       | Sprint 3 compliance test | Sprint 3 penetration test | Sprint 1 regression test   |
| 9.20 - 9.28 | Bug-fixing       |                          |                           | Sprint 2,3 regression test |
| 9.29 - 9.30 | Ready to release |                          |                           |                            |

#### 4.测试人员及分工 Testers and Division

4.1 扈冰  Hu Bing

​    UserMgmt (合规测试/渗透测试)

​    Appstore (合规测试/渗透测试)

​    Developer (合规测试/渗透测试)

​    ATP (合规测试/渗透测试)

​    MECM (合规测试/渗透测试)

​    MEP (合规测试/渗透测试)

#### 5.测试范围 Test Scope

测试范围一般按照特性划分。

The test scope is generally divided according to stories. 

##### 5.1  sprint1:


| Story        | 开发状态   | 关联合规测试用例 |  合规测试状态  |关联渗透测试用例| 渗透测试状态  |
| --------   | :----:  | :----: | :----: |  :----: | :----:  |
| [【AppStore】appstore首页优化](https://e.gitee.com/OSDT/issues/list?issue=I41VL7) |  | - | - | - | - |
| [【AppStore】应用包签名](https://e.gitee.com/OSDT/issues/list?issue=I41V8F) |          |                                         |              |                  |              |
| [【ATP】测试用例接口适配配置管理的修改](https://e.gitee.com/OSDT/issues/list?issue=I41YER) |          |                    -                    |      -       |        -         |      -       |
| [【ATP】管理面配置管理后台接口开发](https://e.gitee.com/OSDT/issues/list?issue=I41Y1N) |          |   1.3  1.12  5.3  8.1  9.1  9.2  9.4    | 测试完毕 |        越权       |      测试完毕   |
| [【YUAN】用户注册登录界面前端开发](https://e.gitee.com/OSDT/issues/list?issue=I43GFH) |          |                    -                    |      -       |        -         |      -       |
| [【ATP】管理面配置管理页面开发](https://e.gitee.com/OSDT/issues/list?issue=I41Y1A) |          |                    -                    |      -       |        -         |      -       |
| [【YUAN】用户通过手机获取进行验证码注册](https://e.gitee.com/OSDT/issues/list?issue=I44JVM) |          |                                         |              |                  |              |
| [【YUAN】用户登录支持密码登录以及验证码登录](https://e.gitee.com/OSDT/issues/list?issue=I44JXG) |          |                                         |              |                  |              |
| [【ATP】后台支持保存用户上传的自测报告](https://e.gitee.com/OSDT/issues/list?issue=I41YAH) |          |                    -                    |      -       |        -         |      -       |
| [【developer】虚机工作空间界面优化（容器中没有的）](https://e.gitee.com/OSDT/issues/list?issue=I41AX4) |          |                    -                    |      -       |        -         |      -       |
| [【developer】容器应用开发界面优化](https://e.gitee.com/OSDT/issues/list?issue=I435ED) |          |                    -                    |      -       |        -         |      -       |
| [【developer】容器应用开发者新建项目后能力选择可编辑项目信息](https://e.gitee.com/OSDT/issues/list?issue=I4373S) |          |                  接口                  | 测试完毕 | - | - |
| [【developer】应用开发者新建项目后基本信息可编辑项目信息](https://e.gitee.com/OSDT/issues/list?issue=I436OW) |          |                  接口                 | 测试完毕 | - | - |
| [【developer】容器能力选择界面优化](https://e.gitee.com/OSDT/issues/list?issue=I435R0) |          |                    -                    |      -       |        -         |      -       |
| [【developer】容器基本信息界面优化](https://e.gitee.com/OSDT/issues/list?issue=I435OV) |          |                    -                    |      -       |        -         |      -       |
| [【MEP】统计MEP接口调用方数据，供审计使用](https://e.gitee.com/OSDT/issues/list?issue=I424EL) |          |                                         |              |                  |              |
| [【MEP】Support confirm ready task](https://e.gitee.com/OSDT/issues/list?issue=I424GI) |          |                                         |              |                  |              |
| [【developer】应用开发者新建项目后可编辑项目信息](https://e.gitee.com/OSDT/issues/list?issue=I3Y70I) |          | - | - | - | - |
| [【YUAN】边缘节点展示前端](https://e.gitee.com/OSDT/issues/list?issue=I42TI3) |  | - | - | - | - |
| [【YUAN】附近的边缘节点前端](https://e.gitee.com/OSDT/issues/list?issue=I42TN9) |          |                    -                    |      -       |        -         |      -       |
| [【YUAN】后端与网页端MECM打通](https://e.gitee.com/OSDT/issues/list?issue=I42TPG) |  | | | | |
| [【AppStore】下载包含镜像的应用包优化](https://e.gitee.com/OSDT/issues/list?issue=I42H6M) |  | | | | |
| [【AppStore】应用共享主界面优化](https://e.gitee.com/OSDT/issues/list?issue=I41VYR) |  | - | - | - | - |
| [【developer】系统-系统镜像管理，页面添加容器镜像管理，页面排版优化](https://e.gitee.com/OSDT/issues/list?issue=I42HD2) |  | - | - | - | - |
| [【ATP】测试用例页面适配配置管理的修改](https://e.gitee.com/OSDT/issues/list?issue=I41YEN) |  | - | - | - | - |
| [【developer】系统-沙箱管理页面优化](https://e.gitee.com/OSDT/issues/list?issue=I42C5X) |  | - | - | - | - |
| [【MECM】Modification of the interface of each module of MECM 2.0 version](https://e.gitee.com/OSDT/issues/list?issue=I42G08) |  | - | - | - | - |
| [【MECM】接口响应规范优化-APPLCM-ContainerAPI](https://e.gitee.com/OSDT/issues/list?issue=I3OYB2) |  | - | - | - | - |
| [【installer】增加NFS节点共享目录同步配置](https://e.gitee.com/OSDT/issues/list?issue=I4418E) |  | | | | |
| [【AppStore】Appstore部署时将应用包路径挂载到宿主机上](https://e.gitee.com/OSDT/issues/list?issue=I42H6H) |  | | | | |
| [【installer】eguser用户权限不足，导致无法写入log等文件](https://e.gitee.com/OSDT/issues/list?issue=I40VCX) |  | 遗留问题 | 测试完毕 | | |
| [【AppStore】删除应用同时删除其测试报告、可推送应用列表中数据](https://e.gitee.com/OSDT/issues/list?issue=I41V3A) |  | - | - | - | - |
| [【developer】资源配置页面虚拟机名称的输入限制，不允许中文输入](https://e.gitee.com/OSDT/issues/list?issue=I43F0P) |  | 1.3  5.3 | 测试完毕 | - | - |
| [【developer】新建项目这里，应用名称的输入限制，在现有基础上添加允许中文输入](https://e.gitee.com/OSDT/issues/list?issue=I43F02) |  | 1.3  5.3 | 测试完毕 | - | - |
| [【installer】修改ansible部署脚本，支持创建或使用已有的应用签名证书](https://e.gitee.com/OSDT/issues/list?issue=I4416J) |  | | | | |
| [【developer】普通用户和管理员可以通过系统-镜像管理-容器镜像管理进行增删改查容器镜像](https://e.gitee.com/OSDT/issues/list?issue=I41J8I) |  | 1.1  1.3  1.12  9.2  9.4 | 测试完毕 | - | - |
| [【developer】eg-view组件使用指南，按照developer界面风格进行优化](https://e.gitee.com/OSDT/issues/list?issue=I44HEJ) |  | - | - | - | - |
| [【ATP】EG报告是生成的+自测报告的整合，并支持下载](https://e.gitee.com/OSDT/issues/list?issue=I41YA3) |  | | | 下载 | 测试完毕 |
| [【ATP】用户支持上传自测报告](https://e.gitee.com/OSDT/issues/list?issue=I41Y9U) |  | 1.1  1.3  1.12  5.3  7.4  8.1  9.2  9.4 | 测试完毕 | 上传 | 测试完毕 |
| [【developer】eg-view组件使用指南，组件文档数据解析&展示](https://e.gitee.com/OSDT/issues/list?issue=I41XSE) |  | - | - | - | - |
| [【developer】eg-view组件使用指南，界面框架实现](https://e.gitee.com/OSDT/issues/list?issue=I41XR0) |  | - | - | - | - |
| [【developer】应用开发者在虚机部署调测结束后，可以通过资源释放接口释放资源](https://e.gitee.com/OSDT/issues/list?issue=I41BVR) |  | | | | |
| [【developer】项目名称支持中文，前台放开中文限制，后台应用包逻辑需要修改](https://e.gitee.com/OSDT/issues/list?issue=I41CIF) |  | - | - | - | - |
| [【MECM】Supports Metric data display and collection（CPU & MEM）](https://e.gitee.com/OSDT/issues/list?issue=I42FZ6) |  | - | - | - | - |
| [【developer】容器项目详情界面优化重构](https://e.gitee.com/OSDT/issues/list?issue=I41AWG) |  | - | - | - | - |
| [【小程序架构】改造昇腾等平台应用为webassembly 形态，提供更多形态workload开放能力](https://e.gitee.com/OSDT/issues/list?issue=I3RNPH) |  | - | - | - | - |
| [【developer】虚机镜像文件支持分片上传至文件系统](https://e.gitee.com/OSDT/issues/list?issue=I41CCD) |  | 1.1  9.2  9.4 | | | |
| [【ATP】新增任务删除接口](https://e.gitee.com/OSDT/issues/list?issue=I41YHN) |  | 1.1  1.12  8.1  9.2  9.4 | 测试完毕，通过 | 越权，任意文件下载 | 测试完毕，通过 |
| [【AppStore】虚机应用在线体验](https://e.gitee.com/OSDT/issues/list?issue=I41VCM) |  | | | | |
| [【AppStore】我的应用界面优化](https://e.gitee.com/OSDT/issues/list?issue=I41VZC) |  | - | - | - | - |
| [【AppStore】接口响应规范化  --- 前台提示优化](https://e.gitee.com/OSDT/issues/list?issue=I41AR1) |  | - | - | - | - |
| [【YUAN】用户手机号快速注册登录](https://e.gitee.com/OSDT/issues/list?issue=I439J2) |  | | | | |


##### 5.2  sprint2:


| Story        | 开发状态   | 关联合规用例 | 合规测试状态 | 关联渗透用例  | 渗透测试状态  |
| :-------   | :----:  | :----: | :----: |  :----: | :----:  |
| [【AppStore】应用仓库界面优化](https://e.gitee.com/OSDT/issues/list?issue=I41VNC) |          | - | - | - | - |
| [【YUAN】用户注册登录界面前端开发](https://e.gitee.com/OSDT/issues/list?issue=I43GFH) |          | - | - | - | - |
| [【YUAN】用户登录支持密码登录以及验证码登录](https://e.gitee.com/OSDT/issues/list?issue=I44JXG) |          |              |              |              |              |
| [【YUAN】用户通过手机获取进行验证码注册](https://e.gitee.com/OSDT/issues/list?issue=I44JVM) |          |              |              |              |              |
| [【ATP】前台接口响应规范优化](https://e.gitee.com/OSDT/issues/list?issue=I41Y5U) |          | - | - | - | - |
| [【ATP】后台接口响应规范优化](https://e.gitee.com/OSDT/issues/list?issue=I41Y5H) |          | - | - | - | - |
| [【AppStore】虚机应用镜像上传优化](https://e.gitee.com/OSDT/issues/list?issue=I41VFI) |          |  |  |  |  |
| [【AppStore】文档界面优化](https://e.gitee.com/OSDT/issues/list?issue=I41VYU) |          | - | - | - | - |
| [【MECM】【osplugin】Query status interface responds according to the status of the plugin](https://e.gitee.com/OSDT/issues/list?issue=I46460) |          |              |              |              |              |
| [【AppStore】APPD转换工具集成到AppStore](https://e.gitee.com/OSDT/issues/list?issue=I41E65) |          |              |              |              |              |
| [【developer】虚机部署调测界面优化](https://e.gitee.com/OSDT/issues/list?issue=I438H8) |          | - | - | - | - |
| [【developer】容器应用发布界面优化](https://e.gitee.com/OSDT/issues/list?issue=I435VK) |          | - | - | - | - |
| [【Appstore】Appstore修改使用applcm v2接口](https://e.gitee.com/OSDT/issues/list?issue=I44GID) |          | - | - | - | - |
| [【MECM】【LCM】Query status interface responds according to the status of the plugin](https://e.gitee.com/OSDT/issues/list?issue=I46425) |          |              |              |              |              |
| [【Developer】Developer修改使用applcm v2接口](https://e.gitee.com/OSDT/issues/list?issue=I44GHM) |          | - | - | - | - |
| [【developer】【管理员权限】系统-能力中心，新增服务弹框中添加功能：输入在线体验url、增加图片上传功能](https://e.gitee.com/OSDT/issues/list?issue=I41CI2) |          |              |              | 越权，上传，输入 | 测试完毕 |
| [【MECM】MECM界面优化](https://e.gitee.com/OSDT/issues/list?issue=I45TJJ) |          | - | - | - | - |
| [【MEPM】MEPM-portal 各项操作需进行鉴权](https://e.gitee.com/OSDT/issues/list?issue=I44GKG) |          | cookie |              | 越权 | 测试完毕 |
| [【developer】虚机工作空间界面优化（容器中没有的）](https://e.gitee.com/OSDT/issues/list?issue=I41AX4) |          | - | - | - | - |
| [【developer】容器项目详情界面优化重构](https://e.gitee.com/OSDT/issues/list?issue=I41AWG) |          | - | - | - | - |
| [【MEP】MEC application graceful termination/stop](https://e.gitee.com/OSDT/issues/list?issue=I424G2) |          |              |              |              |              |
| [【AppStore】应用共享 /应用拉取界面优化](https://e.gitee.com/OSDT/issues/list?issue=I42HAH) |          | - | - | - | - |
| [【AppStore】应用共享 /应用推送界面优化](https://e.gitee.com/OSDT/issues/list?issue=I42HAF) |          | - | - | - | - |
| [【developer】eg-view组件指南嵌入到developer的文档页面下](https://e.gitee.com/OSDT/issues/list?issue=I41XU1) |          | - | - | - | - |
| [支持FS管理应用生命周期（Support docking FS management application life cycle）](https://e.gitee.com/OSDT/issues/list?issue=I42FYH) |          |              |              |              |              |
| [【developer】部署调测界面，删掉第一步（上传镜像包），入口移动到系统的镜像管理中](https://e.gitee.com/OSDT/issues/list?issue=I41J8X) | | - | - | - | - |
| [【developer】容器镜像分片上传、支持取消上传](https://e.gitee.com/OSDT/issues/list?issue=I41JCE) | | | | | |
| [【developer】管理员权限下，容器镜像支持同步操作](https://e.gitee.com/OSDT/issues/list?issue=I46UL9) | | | | 越权 | 测试完毕 |
| [【Appstore】appd界面嵌套在应用仓库界面中，通过按钮点击新打开appd转换页面](https://e.gitee.com/OSDT/issues/list?issue=I45P4A) | | - | - | - | - |
| [【developer】应用发布页面的“应用规则配置、服务发布配置”回显问题](https://e.gitee.com/OSDT/issues/list?issue=I41CIO) | | | | | |
| [【developer】应用开发者部署调测时，使用当前openstack默认主机组，应用发布时，选择界面填写的主机组配置](https://e.gitee.com/OSDT/issues/list?issue=I41BWL) | | | | | |
| [【ATP】测试用例增强](https://e.gitee.com/OSDT/issues/list?issue=I45ACR) | | - | - | - | - |
| [【developer】【管理员权限】系统-能力中心管理列表，增加编辑功能](https://e.gitee.com/OSDT/issues/list?issue=I41CIC) | | | | 越权，上传，输入 | 测试完毕 |
| [【developer】管理员提供不同系统的虚机基础镜像](https://e.gitee.com/OSDT/issues/list?issue=I41CAW) | | | | | |
| [【MEP】Service support multiple-endPointInfo](https://e.gitee.com/OSDT/issues/list?issue=I424FZ) | | | | | |
| [【developer】容器部署调测界面优化](https://e.gitee.com/OSDT/issues/list?issue=I435VH) | | - | - | - | - |
| [【developer】界面通用样式加提示框通用样式](https://e.gitee.com/OSDT/issues/list?issue=I44T2D) | | - | - | - | - |



##### 5.3  sprint3:

| Story        | 开发状态   | 关联合规用例 |  合规测试状态  | 关联渗透用例  | 渗透测试状态 |
| --------   | -----:  | :----: | :----:  | :----:  | :----:  |
| [【mepm】应用包管理，应用实例管理和边缘节点管理界面优化](https://e.gitee.com/OSDT/issues/list?issue=I49SSV) |          | - | - | - | - |
| [【developer】流程优化及代码重构](https://e.gitee.com/OSDT/issues/list?issue=I48GPB) |          |              |              |              |              |
| [【MECM】界面打磨优化](https://e.gitee.com/OSDT/issues/list?issue=I4887E) |  | - | - | - | - |
| [【AppStore】系统/沙箱管理界面优化](https://e.gitee.com/OSDT/issues/list?issue=I47UJ4) |          | - | - | - | - |
| [【developer】eg-view部署脚本配置](https://e.gitee.com/OSDT/issues/list?issue=I47R0E) |          |              |              |              |              |
| [【AppStore】应用详情界面优化](https://e.gitee.com/OSDT/issues/list?issue=I47QFK) |          | - | - | - | - |
| [【AppStore】应用包转换工具界面优化](https://e.gitee.com/OSDT/issues/list?issue=I47QFC) |          | - | - | - | - |
| [【developer】虚机镜像瘦身工具开发](https://e.gitee.com/OSDT/issues/list?issue=I46UGT) |          |              |              |              |              |
| [【AppStore】应用共享 /消息中心界面优化](https://e.gitee.com/OSDT/issues/list?issue=I42HAU) |          | - | - | - | - |
| [【MECM】Edge Autonomous Portal supports account login access control](https://e.gitee.com/OSDT/issues/list?issue=I42FXE) |          |              |              |              |              |
| [【MEP】MEC application graceful termination/stop](https://e.gitee.com/OSDT/issues/list?issue=I424G2) |          |              |              |              |              |
| [【developer】开发者不修改沙箱配置信息，可以同时部署调测多个虚机应用](https://e.gitee.com/OSDT/issues/list?issue=I41CJN) |          |              |              |              |              |
| [【developer】应用开发者部署调测时，通过VNC远程登录到容器内部](https://e.gitee.com/OSDT/issues/list?issue=I41CC0) |          |              |              |              |              |
| [【developer】应用开发者部署调测时，使用当前openstack默认主机组，应用发布时，选择界面填写的主机组配置](https://e.gitee.com/OSDT/issues/list?issue=I41BWL) |          |              |              |              |              |
| [【installer】EG平台支持版本升级场景的数据持久化](https://e.gitee.com/OSDT/issues/list?issue=I3ROCR) |          |              |              |              |              |
| [【HealthCheck】根据edge交互情况投票总结此次健康检查](https://e.gitee.com/OSDT/issues/list?issue=I3PS2D) |          |              |              |              |              |
| [【HealthCheck】各边缘节点初始化](https://e.gitee.com/OSDT/issues/list?issue=I3PS0N) |          |              |              |              |              |
| [【HealthCheck】MECM层调用HealthCheck](https://e.gitee.com/OSDT/issues/list?issue=I3PRZ3) |          |              |              |              |              |

#### 6.安全测试用例管理 Security Test Case Management

安全测试用例包括 [安全合规测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases) 和 [渗透测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases) 。

测试用例包括：测试用例ID，测试目的，测试描述，测试工具及其配置，测试步骤，预期结果等。

测试用例模板请参考 [测试用例模板](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Testing/Security%20Test%20Case%20Templeate.rst) 。

测试结果包括：通过 / 不通过

Security test cases include [Security Compliance Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases) and [Penetration Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases).

Test cases include: test case ID, test purpose, test description, test tool and configuration, test steps, expected results, etc.

Please refer to [Test Case Template](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Testing/Security%20Test%20Case%20Templeate.rst) for test case templates.

Test results include: pass / fail

#### 7.缺陷/漏洞管理 Bug/vulne Management

缺陷/漏洞统一在Gitee中录入，录入方法请参考[操作指南](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md)。

缺陷/漏洞优先级分为：严重，主要，次要，一般。

Bugs/vulnerabilities are entered in Gitee. Please click [operation guide](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md).

The priority of bugs/vulnerabilities is divided into: serious, major, minor, and general.

#### 8.相关工具 Relative Tools

| No.  | Tool name | version   | purpose                                   | comment |
| ---- | --------- | --------- | ----------------------------------------- | ------- |
| 1    | BurpSuite | v2020.9.1 | Capture http package for analyzing        | ...     |
| 2    | OWASP ZAP | 2.9.0     | Comprehensive vulnerability scanning tool |         |
| 3    | SenInfo   | 2.0.10    | Scan for sensitive information            |         |

#### 9.手动开发测试脚本 Test scripts developed manually

| No.  | Tool name | version | purpose | comment |
| ---- | --------- | ------- | ------- | ------- |
|      | N/A       | ...     | ...     | ...     |



#### 10.测试总结 Test Summary

测试完毕后，将生成 “EdgeGallery v1.3安全测试报告”。

After the test, plain to export “EdgeGallery v1.3 Security Test Report”.

