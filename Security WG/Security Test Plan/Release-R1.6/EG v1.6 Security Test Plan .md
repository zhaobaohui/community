# EdgeGallery v1.6 Security Test Plan

## EdgeGallery v1.6 安全测试计划



#### 1.测试目标 Test target

本测试计划为EdgeGallery v1.6 安全测试计划，目的在于发现应用程序在版本迭代过程中可能出现的安全缺陷/漏洞，并评估其严重等级，并提供修补建议。以确保应用程序在版本迭代过程中的安全性。

This test plan is the EdgeGallery v1.6 security test plan. The purpose is to discover possible bugs/vulnerabilities of the application during the version iteration process, and evaluate its threat-level, and then provide some repair suggestions. The finally purpose is to ensure the security of the application during the version iteration process.

#### 2.先决条件 Prerequisites

[v1.6需求设计文档](https://gitee.com/edgegallery/community/tree/master/Architecture%20WG/Requirements/v1.6)

[v1.6需求任务列表 Requirement List](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=155617&is%5Bissue_type_id%5D=238024&openMore=1)

[v1.6特性任务列表 Story List](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=155617&is%5Bissue_type_id%5D=199539&openMore=1)

[Edgegallery安全设计规范 Security Design Guide](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Secure%20Design/Security%20Design%20Guide%20CN.md)

[EdgeGallery合规测试用例 Compliance Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases)

[EdgeGallery渗透测试用例 Penetration Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases)

#### 3.测试日期计划 Test Date Plain

v1.6版本整体分为2个迭代，由2022.02.07启动编码，计划于2022.03.31发布Release1.6版本。

| Date          | Coding Status    | Compliance Test          | Penetration Test          | Regression Test          |
| ------------- | ---------------- | ------------------------ | ------------------------- | ------------------------ |
| 02.07 - 02.27 | Sprint 1 coding  |                          |                           |                          |
| 02.28 - 03.13 | Sprint 2 coding  | Sprint 1 compliance test | Sprint 1 penetration test |                          |
| 03.14 - 03.23 | Bug-fixing       | Sprint 2 compliance test | Sprint 2 penetration test | Sprint 1 regression test |
| 03.24 - 03.30 | Bug-fixing       |                          |                           | Sprint 2 regression test |
| 03.31 - 03.31 | Ready to release |                          |                           |                          |

#### 4.测试人员及分工 Testers and Division

4.1 扈冰  Hu Bing

​    UserMgmt (合规测试/渗透测试)

​    Appstore (合规测试/渗透测试)

​    Developer (合规测试/渗透测试)

​    ATP (合规测试/渗透测试)

​    MECM (合规测试/渗透测试)

​    MEP (合规测试/渗透测试)

#### 5.测试范围 Test Scope

##### 5.1 新增需求测试

测试范围一般按照特性划分。

The test scope is generally divided according to stories. 

###### 5.1.1  sprint1:


| Story        | 开发状态   | 关联合规测试用例 |  合规测试状态  |关联渗透测试用例| 渗透测试状态  |
| --------   | :----:  | :----: | :----: |  :----: | :----:  |
| [EG首页增加版本号](https://e.gitee.com/OSDT/issues/list?issue=I4TMEN) | | - | | - | |
| [各前台导航切换使用公共的Nav组件](https://e.gitee.com/OSDT/issues/list?issue=I4SYWP) | | - | | - | |
| [helmchart统一通过Value.global.ssh.enabled配置ssh](https://e.gitee.com/OSDT/issues/list?issue=I4STQ0) | | - | | - | |
| [ansible支持http部署配置](https://e.gitee.com/OSDT/issues/list?issue=I4STPE) | | 2.1 | | - | |
| [增加文件管理界面，后台接口为filesystem提供，并且可将文件管理中的镜像导入到resource mgr中的节点。](https://e.gitee.com/OSDT/issues/list?issue=I4UAWC) | | 1.1 1.2 1.3 1.12 1.13 2.2 8.1 9.2 9.3 9.4 | | - | |
| [地图和地域级联组件筛查](https://e.gitee.com/OSDT/issues/list?issue=I4SXBQ) | | - | | - | |
| [北向接口节点健康检查](https://e.gitee.com/OSDT/issues/list?issue=I4SXB7) | | - | | - | |
| [【MECM】资源管理界面虚机管理增加对虚机的操作，包括生成镜像，虚机启停等功能实现](https://e.gitee.com/OSDT/issues/list?issue=I4SX9H) | | 1.1 1.2 1.3 1.12 1.13 2.2 8.1 9.2 9.3 9.4 | | - | |
| [developer支持http部署配置](https://e.gitee.com/OSDT/issues/list?issue=I4SUGA) | | 1.1 1.2 1.8 1.11 2.1 3.1 3.3 | | 敏感信息泄露 | |
| [将系统管理界面从老的界面中迁移到新界面](https://e.gitee.com/OSDT/issues/list?issue=I4STL4) | | - | | - | |
| [【Appstore】Appstore支持下载大文件](https://e.gitee.com/OSDT/issues/list?issue=I4SSEJ) | | 1.1 1.2 1.3 1.12 1.13 2.2 9.2 9.3 9.4 | | 任意文件下载 | |
| [【Appstore】Appstore支持应用下架功能](https://e.gitee.com/OSDT/issues/list?issue=I4SSEE) | | 1.1 1.2 1.3 1.12 1.13 2.2 9.2 9.3 9.4 | | - | |
| [【AppStore】appstore组件统一](https://e.gitee.com/OSDT/issues/list?issue=I4TBNC) | | - | | - | |
| [osplugin支持虚机iso镜像通过挂载临时磁盘的方式拉起虚机](https://e.gitee.com/OSDT/issues/list?issue=I4STJZ) | | - | | - | |
| [【AppStore】appstore首页优化](https://e.gitee.com/OSDT/issues/list?issue=I4S3R4) | | - | | - | |
| [【AppStore】appstore界面组件风格与融合平台保持一致](https://e.gitee.com/OSDT/issues/list?issue=I4S3R0) | | - | | - | |


###### 5.1.2  sprint2:


| Story        | 开发状态   | 关联合规用例 | 合规测试状态 | 关联渗透用例  | 渗透测试状态  |
| :-------   | :----:  | :----: | :----: |  :----: | :----:  |
| [获取容器应用详情接口，服务信息新增protocol和name字段](https://e.gitee.com/OSDT/issues/list?issue=I4TLJZ) | | 1.1 1.2 1.3 1.12 1.13 2.2 9.2 9.3 9.4 | | | |
| [【MECM】资源管理抽象：以包和规格为计算标准，显示资源所能创建的应用数量](https://e.gitee.com/OSDT/issues/list?issue=I4SX9R) | | - | | - | |
| [mepm-fe风格适配eg-fe界面优化](https://e.gitee.com/OSDT/issues/list?issue=I4TM9Z) | | - | | - | |
| [mecm-fe风格适配eg-fe界面优化](https://e.gitee.com/OSDT/issues/list?issue=I4TM9Q) | | - | | - | |
| [edgegallery-fe界面显示问题、国际化、以及提示问题修改](https://e.gitee.com/OSDT/issues/list?issue=I4STLG) | | - | | - | |
| [【Appstore】appstore特性增强V1.6](https://e.gitee.com/OSDT/issues/list?issue=I4SSDL) | | 1.1 1.2 1.3 1.12 1.13 2.2 9.2 9.3 9.4 | | - | |
| [【lcm】lcm支持网络创建](https://e.gitee.com/OSDT/issues/list?issue=I4GC6H) | | 1.1 1.2 1.3 1.12 1.13 2.2 9.2 9.3 9.4 | | - | |

##### 5.2 综合Web漏洞扫描

通过OWASP-ZAP工具，动态扫描应用中可能存在的安全漏洞，如SQL注入、XSS、CSRF、目录遍历、远程代码执行、文件包含、CRLF注入、XXE等。

该项检查一般于版本最后一个迭代交付后开始。

##### 5.3 静态敏感信息筛查

通过静态敏感信息筛查工具，检查代码中可能存在的不安全加密算法、不安全协议、弱口令、硬编码密码、邮箱、工号等敏感信息。

该项检查一般于版本最后一个迭代交付后开始。

#### 6.安全测试用例管理 Security Test Case Management

安全测试用例包括 [安全合规测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases) 和 [渗透测试用例](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases) 。

测试用例包括：测试用例ID，测试目的，测试描述，测试工具及其配置，测试步骤，预期结果等。

测试用例模板请参考 [测试用例模板](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Testing/Security%20Test%20Case%20Templeate.rst) 。

测试结果包括：通过 / 不通过

Security test cases include [Security Compliance Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Design%20Compliance%20Test%20Cases) and [Penetration Test Cases](https://gitee.com/edgegallery/community/tree/master/Security%20WG/Security%20Testing/Penetration%20Test%20Cases).

Test cases include: test case ID, test purpose, test description, test tool and configuration, test steps, expected results, etc.

Please refer to [Test Case Template](https://gitee.com/edgegallery/community/blob/master/Security%20WG/Security%20Testing/Security%20Test%20Case%20Templeate.rst) for test case templates.

Test results include: pass / fail

#### 7.缺陷/漏洞管理 Bug/vulne Management

安全测试过程中发现的安全问题/漏洞统一在Gitee中录入，录入方法请参考[操作指南](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md)。

漏洞的严重性包括：严重，主要，次要，一般。

详细安全问题issue，请查看 [安全问题issue](https://e.gitee.com/OSDT/issues/list?is%5Bmilestone_id%5D=145866&is%5Bsearch%5D=Security&is%5Bissue_type_id%5D=199540&is%5Bsort%5D=created_at&is%5Bdirection%5D=desc&openMore=1)。

Bugs/vulnerabilities are entered in Gitee. Please click [operation guide](https://gitee.com/edgegallery/community/blob/master/Test%20WG/Test%20case-bug%20template/Gitee_test_bug_template.md).

The priority of bugs/vulnerabilities is divided into: serious, major, minor, and general.

#### 8.相关工具 Relative Tools

| No.  | Tool name | version   | purpose                                   | comment |
| ---- | --------- | --------- | ----------------------------------------- | ------- |
| 1    | BurpSuite | v2020.9.1 | Capture http package for analyzing        | ...     |
| 2    | OWASP ZAP | 2.9.0     | Comprehensive vulnerability scanning tool |         |
| 3    | SenInfo   | 2.0.10    | Scan for sensitive information            |         |

#### 9.手动开发测试脚本 Test scripts developed manually

| No.  | Tool name | version | purpose | comment |
| ---- | --------- | ------- | ------- | ------- |
|      | N/A       | ...     | ...     | ...     |

#### 10.测试总结 Test Summary

测试完毕后，将生成 “EdgeGallery v1.6安全测试报告”。

After the test, plain to export “EdgeGallery v1.6 Security Test Report”.

