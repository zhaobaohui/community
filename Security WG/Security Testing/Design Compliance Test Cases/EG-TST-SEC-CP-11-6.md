# EG-TST-SEC-CP-11-6



| Test Case ID            | EG-TST-SEC-CP-11-6                                           |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 不允许通过docker --privileged命令启动特权容器。              |
| Test purpose            | 通过该测试，检查是否存在特权容器。                           |
| Test description        | 如果启动特权容器，攻击者可以通过敏感目录挂载等方式，轻松实现容器逃逸。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 检查安装脚本，在docker命令中是否存在"--privileged"参数<br/>  |
| **Expected result**     | 测试步骤1中：未检索到"--priveleged"关键字<br/>               |
| References              |                                                              |

