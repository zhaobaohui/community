# EG-TST-SEC-CP-1-18



| Test Case ID            | EG-TST-SEC-CP-1-18                                           |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 特权用户的密码每隔一定周期必须强制更改。                     |
| Test purpose            |                                                              |
| Test description        | 特权用户由于权限较高，账号和口令一旦泄露，对系统影响较大，因此需要强制定期更改特权用户的密码。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 修改系统时间，或将数据库中“密码生效时间”字段（pweffecttime）修改为一个较早时间（如6个月前）。<br/><br/>示例代码：<br/>update tbl_tenant set pweffecttime = now() - interval '6 month' where username = 'admin'; |
| Test step 2             | 尝试登录管理员用户，查看是否弹出强制修改密码的页面。<br/>    |
| Test step 3             | 在强制修改密码前，尝试访问只允许登录后访问的页面。           |
| **Expected result**     | 测试步骤2中：弹出强制修改密码的页面。<br/>测试步骤3中：无法访问任何只允许登录后问的页面，尝试访问时都跳转到强制修改密码的页面。 |
| References              |                                                              |

