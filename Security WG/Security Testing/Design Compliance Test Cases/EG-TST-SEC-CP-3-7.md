# EG-TST-SEC-CP-3-7



| Test Case ID            | EG-TST-SEC-CP-3-7                                            |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 不要以任何形式发送密码或密码哈希给用户。                     |
| Test purpose            |                                                              |
| Test description        | 发送密码或密码哈希给用户是比较危险的行为，密码明文可能泄露，密码哈希也存在被破解的风险。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 识别可能存在返回密码或密码哈希的页面或接口（如注册、登陆、修改密码、忘记密码、配置环境密码等页面）。<br/> |
| Test step 2             | 正确调用以上页面或接口，查看http响应信息中是否存在密码或密码哈希。<br/> |
| Test step 3             | 错误调用以上页面或接口（如传入不符合要求的参数），查看http响应信息中是否存在密码或密码哈希。<br/> |
| **Expected result**     | 测试步骤2中：http响应信息中不存在密码或密码哈希。<br/>测试步骤3中：http响应信息中不存在密码或密码哈希。 |
| References              |                                                              |

