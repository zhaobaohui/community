# EG-TST-SEC-CP-1-17



| Test Case ID            | EG-TST-SEC-CP-1-17                                           |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 设置或修改密码时进行密码长度和复杂度检查。                   |
| Test purpose            |                                                              |
| Test description        |                                                              |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具或环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 检查用户注册页面，密码设置时是否有最小长度和复杂度要求。<br/> |
| Test step 2             | 检查修改密码、忘记密码或其他密码配置页面，密码设置时是否有最小长度和复杂度要求。<br/> |
| **Expected result**     | 测试步骤1中：密码设置时有最小长度和复杂度要求。<br/>测试步骤2中：密码设置时有最小长度和复杂度要求。 |
| References              |                                                              |

