# EG-TST-SEC-CP-11-10



| Test Case ID            | EG-TST-SEC-CP-11-10                                          |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 不应使用无认证的kubelet接口。                                |
| Test purpose            | 通过该测试，检查是否存在无认证的kubelet接口。                |
| Test description        | k8s的每个工作节点上都运行有kubelet进程，默认监听10250/10255等端口。①如果10250端口没有认证，访问10250端口就能获得该节点运行的所有pod信息，并在该节点的任意pod中执行命令。②10255端口提供了pod和node信息，接口以只读形式暴露出去，访问该端口不需要认证和鉴权。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 尝试用浏览器访问 https://x.x.x.x:10250/pods<br/>             |
| Test step 2             | 尝试用浏览器访问 https://x.x.x.x:10250/exec<br/>             |
| Test step 3             | 尝试用浏览器访问 https://x.x.x.x:10255/logs<br/>             |
| **Expected result**     | 测试步骤1中：访问失败<br/>测试步骤2中：访问失败<br/>测试步骤3中：访问失败 |
| References              |                                                              |

