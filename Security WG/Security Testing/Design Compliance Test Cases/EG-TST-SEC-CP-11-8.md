# EG-TST-SEC-CP-11-8



| Test Case ID            | EG-TST-SEC-CP-11-8                                           |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 不应向容器添加危险能力。尤其检查SYS_ADMIN 、 SYS_MODULE 、 DAC_READ_SEARCH 等。 |
| Test purpose            | 通过该测试，检查容器是否存在过高的危险能力。                 |
| Test description        | SYS_ADMIN 、 SYS_MODULE 、 DAC_READ_SEARCH 等危险能力可能导致容器逃逸。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 方法一：（白盒思路）<br/>检查安装脚本，搜索"--cap-add"关键字，检查参数值中是否包含SYS_ADMIN 、 SYS_MODULE 、 DAC_READ_SEARCH等危险能力 |
| Test step 2             | 方法二：（黑盒思路）<br/>                                    |
| Test step 3             | 进入容器内部，执行 cat /proc/1/status \| grep CapEff<br/>    |
| Test step 4             | 将上一步的返回值在容器外解码，执行 capsh --decode=xxxx \| tr ',' '\n' \| sort<br/>（其中xxxx为Test step 3返回的值） |
| Test step 5             | 检查解码后的能力，是否包含SYS_ADMIN 、 SYS_MODULE 、 DAC_READ_SEARCH等危险能力<br/> |
| **Expected result**     | 测试步骤1中：未检索到SYS_ADMIN 、 SYS_MODULE 、 DAC_READ_SEARCH等危险能力<br/>测试步骤2中：未检索到SYS_ADMIN 、 SYS_MODULE 、 DAC_READ_SEARCH等危险能力<br/>（注：两种方法不要求都执行，通过其中一种方法得到测试结果即可） |
| References              |                                                              |

