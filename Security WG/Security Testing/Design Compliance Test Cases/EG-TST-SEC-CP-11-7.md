# EG-TST-SEC-CP-11-7



| Test Case ID            | EG-TST-SEC-CP-11-7                                           |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 禁止挂载宿主机根目录，或其他敏感文件/目录。<br/>危险文件/目录除了根目录，主要包括①挂载docker.sock。特殊业务场景中（如Docker in docker），会挂载Dockerd  Socket或Containerd Socket到容器内部，攻击者可以利用暴露的Unix Socket文件实现容器逃逸。  ②挂载crontab。这可能导致攻击者写入宿主机计划任务实现容器逃逸。 ③挂载ssh目录。这可能导致攻击者写入ssh公钥然后登陆宿主机。   ④挂载/proc。攻击者可能通过core_pattern实现容器逃逸。 |
| Test purpose            | 通过该测试，检查容器是否存在危险挂载。                       |
| Test description        | 挂载宿主机根目录，或其他敏感文件/目录，可能导致攻击者进行容器逃逸，或导致宿主机敏感信息泄露。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 检查安装脚本中的挂载设置，查看是否挂载宿主机根目录<br/>      |
| Test step 2             | 检查安装脚本中的挂载设置，查看是否挂载宿主机docker.sock<br/> |
| Test step 3             | 检查安装脚本中的挂载设置，查看是否挂载宿主机crontab<br/>     |
| Test step 4             | 检查安装脚本中的挂载设置，查看是否挂载宿主机ssh目录<br/>     |
| Test step 5             | 检查安装脚本中的挂载设置，查看是否挂载宿主机/proc目录<br/>   |
| **Expected result**     | 测试步骤1-5中：均未挂载相关目录/文件<br/>                    |
| References              |                                                              |

