# EG-TST-SEC-CP-11-9



| Test Case ID            | EG-TST-SEC-CP-11-9                                           |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 禁止使用无认证的k8s API server接口。                         |
| Test purpose            | 通过该测试，检查是否存在无认证的k8s API server接口。         |
| Test description        | k8s API server是k8s里所有资源的增/删/改/查等操作的唯一入口。k8s API server通常使用6443/8080端口提供服务。如果被攻击者未授权访问，可能造成启动恶意容器，挂载宿主机敏感目录，从而获取宿主机权限。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 尝试用浏览器访问masterIP的8080端口，http://x.x.x.x:8080/<br/> |
| Test step 2             | 尝试远程访问masterIP的6443端口，执行 kubectl -s x.x.x.x:6443 get pods<br/> |
| Test step 3             | 检查安装脚本，查看是否存在"--insecure-bind-address=0.0.0.0"参数<br/> |
| **Expected result**     | 测试步骤1中：访问失败<br/>测试步骤2中：访问失败，不能远程执行get pods命令<br/>测试步骤2中：未检索到"--insecure-bind-address=0.0.0.0"参数 |
| References              |                                                              |

