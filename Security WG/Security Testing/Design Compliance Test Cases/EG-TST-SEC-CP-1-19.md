# EG-TST-SEC-CP-1-19



| Test Case ID            | EG-TST-SEC-CP-1-19                                           |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 认证失败后，不能提示过于详细的失败原因（如：密码错误），只能给出一般性提示（如：用户名或密码错误）。 |
| Test purpose            |                                                              |
| Test description        | 过于详细的失败原因可能导致被攻击者爆破用户名，然后针对已知用户爆破密码。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 在登录页面输入一个已存在的用户名，但输入错误密码，尝试登录，查看提示信息。<br/> |
| Test step 2             | 在登录页面输入一个不存在的用户名，随便输入符合长度和复杂度的密码，尝试登录，查看提示信息。<br/> |
| **Expected result**     | 测试步骤1中：提示信息类似“用户名或密码错误”，尤其不允许仅提示“密码错误”。<br/>测试步骤2中：提示信息类似“用户名或密码错误”，尤其不允许仅提示“用户名错误”。 |
| References              |                                                              |

