# EG-TST-SEC-CP-11-5



| Test Case ID            | EG-TST-SEC-CP-11-5                                           |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 若非十分必要，不允许默认以root用户启动容器。                 |
| Test purpose            | 通过该测试，检查是否存在以root用户启动的容器。               |
| Test description        | 以root用户启动容器会降低攻击者容器逃逸的难度，并增加敏感信息泄露的风险。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 在Server后台执行命令进入pod内部<br/>kubectl exec -it xxx_Pod_id -- sh<br/> |
| Test step 2             | 在pod内输入命令<br/>whoami<br/>查看默认登录用户              |
| **Expected result**     | 测试步骤2中：默认登录用户非root<br/>                         |
| References              |                                                              |

