# EG-TST-SEC-CP-11-11



| Test Case ID            | EG-TST-SEC-CP-11-11                                          |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | 不应存在未授权访问的etcd。                                   |
| Test purpose            | 通过该测试，检查是否存在无认证的etcd。                       |
| Test description        | etcd用于保存k8s集群数据，默认使用2379端口提供服务。如果etcd被未授权访问，等价于接管集群。 |
| Test Mode               | 手动                                                         |
| Test tool               |                                                              |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           |                                                              |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 方法一：<br/>通过浏览器或curl命令访问 curl -k https://x.x.x.x:2379/v2/keys/?recursive=true  查看是否返回了存储在服务器上的所有秘钥（一般是json格式）。 |
| Test step 2             | 方法二：<br/>通过客户端etcdctl访问：etcdctl --endpoints=http://x.x.x.x:2379 ls |
| **Expected result**     | 测试步骤1中：访问失败<br/>测试步骤2中：访问失败<br/>（注：两种方法不要求同时使用，使用其中一种即可验证测试结果） |
| References              |                                                              |

