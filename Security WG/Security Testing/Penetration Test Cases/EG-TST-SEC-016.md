# EG-TST-SEC-016



| Test Case ID            | EG-TST-SEC-016                                               |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | Brute Force of Remote Connection Port（暴力破解-远程连接端口） |
| Test purpose            | 通过远程连接端口暴力破解测试，来检查远程连接端口是否使用弱口令。 |
| Test description        | 主机/操作系统对外开放的远程端口（如SSH、RDP、数据库等），如果使用弱口令，将是非常危险的行为，可能导致整个服务器被攻击者攻陷。 |
| Test Mode               | 工具                                                         |
| Test tool               | Hydra<br/>Hydra是著名黑客组织thc的一款开源的暴力密码破解工具，可以在线破解多种密码。可支持AFP, Cisco AAA, Cisco auth, Cisco enable, CVS, Firebird, FTP,  HTTP-FORM-GET, HTTP-FORM-POST, HTTP-GET, HTTP-HEAD, HTTP-PROXY,  HTTPS-FORM-GET, HTTPS-FORM-POST, HTTPS-GET, HTTPS-HEAD, HTTP-Proxy, ICQ, IMAP, IRC, LDAP, MS-SQL, MYSQL, NCP, NNTP, Oracle, Oracle SID, Oracle, PC-Anywhere, PCNFS, POP3, POSTGRES, RDP, Rexec,  Rlogin, Rsh, SAP/R3, SIP, SMB, SMTP, SMTP Enum, SNMP, SOCKS5, SSH (v1  and v2), Subversion, Teamspeak (TS2), Telnet, VMware-Auth, VNC, XMPP等多种类型密码。 |
| Test tool configuration | Test tool and environment setup description                  |
| config step 1           | 下载安装Hydra：<br/>①访问 https://github.com/vanhauser-thc/thc-hydra 下载Hydra。<br>Windows操作系统可直接下载Windows版hydra： https://github.com/maaaaz/thc-hydra-windows/ |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 执行端口扫描(黑盒)，或登录操作系统执行netstat -an(白盒)查看远程连接端口的开放情况。<br/> |
| Test step 2             | 明确目标ip，并准备一个弱口令字典。（字典强度没有硬性要求，可根据实际需要自行确定。可以在GitHub搜索"弱口令"或"weak password"下载。如果没有弱口令字典，也可以使用hydra -p defaultpw命令加载默认字典）<br/> |
| Test step 3             | 针对已对互联网开放的远程连接端口，使用hydra爆破密码：<br/><br/>命令示例：<br/><br/>ssh：<br/>hydra -l root -P passwd.txt ssh://x.x.x.x:22<br/><br/>rdp：<br/>hydra -l administrator -P passwd.txt rdp://x.x.x.x:3389<br/><br/>vnc：<br/>hydra -l root -P passwd.txt vnc://x.x.x.x:5900<br/><br/>postgres：<br/>hydra -l root -P passwd.txt postgres://x.x.x.x:5432<br/><br/>redis：<br/>hydra -l root -P passwd.txt redis://x.x.x.x:6379<br/> |
| **Expected result**     | 测试步骤3中：hydra如果扫描到密码会提示类似"**[22] [ssh]  host:x.x.x.x  login:xxx  password:xxx**"<br/>其中**password:xxx**为连接密码。 |
