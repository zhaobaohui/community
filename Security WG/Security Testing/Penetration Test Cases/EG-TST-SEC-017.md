# EG-TST-SEC-017



| Test Case ID            | EG-TST-SEC-017                                               |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | CRLF Injection （CRLF注入）                                  |
| Test purpose            | 通过CRLF注入测试，来检查应用是否存在CRLF注入漏洞。           |
| Test description        | CR：回车符，LF：换行符。如果在用户输入信息中，Web应用把CRLF当做换行符，而未当做字符串处理，可能被攻击者利用，造成http响应头伪造（常见为Set-Cookie、Location响应头伪造）、日志伪造等攻击。 |
| Test Mode               | Manual                                                       |
| Test tool               | BurpSuite<br>注：配合Burp所用浏览器建议使用Burp内置浏览器（抓包结果更稳定），打开方法为Proxy - Intercept - Open Broswer |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           | 下载安装Burp：<br/>①访问 https://portswigger.net/burp/communitydownload 下载Burp社区版，点击安装即可。 |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 随机选取一处或多处URL，使用Burp抓取请求，在请求的URI后增加%0d%0aSet-Cookie:xxx=true<br/> |
| Test step 2             | 随机选取一处或多处URL，使用Burp抓取请求，在请求的URI后增加%0d%0a[2021-01-01%2011:11:11:123]%20INFO%20%20[This.is.a.hacker.CRLF.injection.log]<br/> |
| **Expected result**     | 测试步骤1中：http响应中看不到Set-Cookie响应头<br/>测试步骤2中：在日志中检索"hacker"或"CRLF"关键字，查看日志中是否伪造出新的一行日志"[2021-01-01%2011:11:11:123] INFO  [This.is.a.hacker.CRLF.injection.log]" |
| References              |                                                              |

