# EG-TST-SEC-018



| Test Case ID            | EG-TST-SEC-018                                               |
| :---------------------- | :----------------------------------------------------------- |
| **Test case name**      | Docker Remote API Unauthorized Access （Docker未授权访问）   |
| Test purpose            | 通过Docker未授权访问测试，来检查应用是否存在Docker未授权访问漏洞。 |
| Test description        | docker -H 0.0.0.0:2375会把docker直接暴露在互联网上，允许任何用户非授权执行docker命令，这可能导致攻击者通过容器挂载等方式修改宿主机的重要文件，进而获取宿主机权限。 |
| Test Mode               | Manual                                                       |
| Test tool               | Docker                                                       |
| Test tool configuration | 测试工具和环境配置方法                                       |
| config step 1           | 在测试者本地正确安装docker即可，不然无法执行测试步骤2。      |
| **Test steps**          | **Test step description and expected result**                |
| Test step 1             | 登录服务器后台，输入命令（以Ubuntu18为例）<br/>cat /etc/systemd/system/docker.service<br/>查看docker.service文件中ExecStart参数是否存在"-H tcp://0.0.0.0" |
| Test step 2             | 尝试从网络中的另一台计算机远程访问，输入命令<br/>docker -H tcp://x.x.x.x:2375 version<br/>其中x.x.x.x表示被测机器的ip<br/>查看是否成功执行了docker version命令 |
| **Expected result**     | 测试步骤1中：docker.service文件中不存在"-H tcp://0.0.0.0"<br/>测试步骤2中：无法成功执行任何docker命令。 |
| References              |                                                              |

