# EdgeGallery v1.5 Security Design rule for UserManament

|Requirements |Rules involved|
|----|----|
|[【UserMgmt】支持北向接口](https://e.gitee.com/OSDT/dashboard?issue=I4FU6R)| R1.1 R1.2 R1.3 R3.3 R5.1 R8.1 R9.2 R9.3|
|[【UserMgmt】支持与第三方IAM对接](https://e.gitee.com/OSDT/dashboard?issue=I4FU7G)| R1.1 |
|[【UserMgmt】支持通过nginx代理访问EdgeGallery](https://e.gitee.com/OSDT/dashboard?issue=I4FU8F)| None |

## 【UserMgmt】支持北向接口
### Rule 1.1
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.1 所有用于跨网络传输的机器对机器和人对机器接口都必须具有访问认证机制，并且认证过程必须在服务器上执行。 |
| Implementation details  | 为支持北向接口，提供北向认证接口供调用者调用，通过认证后返回AccessToken，只有拿到AccessToken方可成功的调用业务接口，以实现业务接口调用前的认证要求 |
| Related APIs            | 北向认证接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.2
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.2 对于每个需要授权的访问请求，服务器必须验证用户是否有权执行此操作。 |
| Implementation details  | 业务平台提供的接口通过北向网关提供给第三方调用，需要通过AccessToken校验，并且通过AccessToken识别用户角色。用户角色无权调用接口则会失败 |
| Related APIs            | 业务平台提供的接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.3
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.3 服务器必须验证所有不受信任的数据源的大小，类型，长度和特殊字符，并拒绝所有未通过验证的数据。 |
| Implementation details  | 对请求数据进行大小、类型、长度、特殊字符校验，校验不通过则返回400 |
| Related APIs            | 北向认证接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 3.3
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 3.3 所有敏感数据都通过HTTP消息正文发送到服务器 |
| Implementation details  | 北向认证接口的请求包括用户登录凭证（用户名/邮箱地址）和密码，均通过请求体发送。 |
| Related APIs            | 北向认证接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 5.1
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 5.1 必须具备反自动化功能，以防止违反凭据测试，暴力破解和帐户锁定攻击。 |
| Implementation details  | 北向认证接口支持失败后对帐户进行锁定，防止暴力破解。 |
| Related APIs            | 北向认证接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 8.1
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 8.1 所有公共功能接口，RESTful接口，本地功能接口，命令行接口以及用于身份验证的默认用户名和密码都必须在产品或应用程序文档中进行描述。 |
| Implementation details  | 北向认证接口需要在文档中描述。 |
| Related APIs            | 北向认证接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.2
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 确保记录所有与安全相关的成功和失败。 |
| Implementation details  | 北向认证接口实现中要记录认证成功与失败的日志。 |
| Related APIs            | 北向认证接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.3
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.3 请勿将敏感信息添加到日志或日志文件中。 |
| Implementation details  | 北向认证接口记录的日志不能包含敏感信息。 |
| Related APIs            | 北向认证接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

## 【UserMgmt】支持与第三方IAM对接
### Rule 1.1
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.1 所有用于跨网络传输的机器对机器和人对机器接口都必须具有访问认证机制，并且认证过程必须在服务器上执行。 |
| Implementation details  | EdgeGallery定义了一套需要第三方IAM实现的接口，包括登录用来完成身份认证。 |
| Related APIs            | EdgeGallery定义的需要第三方IAM实现的接口：登录   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

## 【UserMgmt】支持通过nginx代理访问EdgeGallery
不涉及。
该需求只是希望在EdgeGallery前面增加一个代理后，能够通过代理来访问EdgeGallery，不会影响EdgeGallery内部的架构、接口等影响安全性的设计。
