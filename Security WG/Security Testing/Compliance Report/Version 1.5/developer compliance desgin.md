# EdgeGallery v1.5 Security Design rule for developer


## 【developer】VNC远程登录
### Rule 1.1
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.1 所有用于跨网络传输的机器对机器和人对机器接口都必须具有访问认证机制，并且认证过程必须在服务器上执行。 |
| Implementation details  | VNC登录通过nigix转发，openstack提供登录的token信息 |
| Related APIs            | VNC接口接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.2
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.2 对于每个需要授权的访问请求，服务器必须验证用户是否有权执行此操作。 |
| Implementation details  | 业务平台提供的接口通过北向网关提供给第三方调用，需要通过AccessToken校验，并且通过AccessToken识别用户角色。用户角色无权调用接口则会失败 |
| Related APIs            | 业务平台提供的接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.3
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.3 服务器必须验证所有不受信任的数据源的大小，类型，长度和特殊字符，并拒绝所有未通过验证的数据。 |
| Implementation details  | 对请求数据进行大小、类型、长度、特殊字符校验，校验不通过则返回400 |
| Related APIs            | NA   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.5
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.5 根据最小特权原则，用于运行软件程序的帐户是低特权OS帐户。 |
| Implementation details  | 使用eguser用户来运行nigix。 |
| Related APIs            | NA   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 3.3
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 3.3 所有敏感数据都通过HTTP消息正文发送到服务器 |
| Implementation details  | 敏感信息加密处理。 |
| Related APIs            | 创建虚机接口，创建沙箱接口   |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 5.1
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 5.1 必须具备反自动化功能，以防止违反凭据测试，暴力破解和帐户锁定攻击。 |
| Implementation details  | NA  |
| Related APIs            | NA    |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 8.1
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 8.1 所有公共功能接口，RESTful接口，本地功能接口，命令行接口以及用于身份验证的默认用户名和密码都必须在产品或应用程序文档中进行描述。 |
| Implementation details  | 文档中描述。 |
| Related APIs            | NA    |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 8.2
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 8.2 系统运行和维护需要外部通讯连接。必须在端口矩阵文档中描述所有使用的通信端口。 |
| Implementation details  | nginx监控地址增加到接口矩阵 |
| Related APIs            | NA |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.2
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 确保记录所有与安全相关的成功和失败。 |
| Implementation details  | NA  |
| Related APIs            | NA  |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.3
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.3 请勿将敏感信息添加到日志或日志文件中。 |
| Implementation details  | NA  |
| Related APIs            | NA  |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps       | NA |
| GUI input example       | NA |
| Additional Info         | NA |
