# ATP V1.3 version Security Design rule involvement

|Requirements |Rules involved|
|----|----|
|[【MECM】增加Resource Controller提供EG轻量云管功能特性](https://e.gitee.com/OSDT/dashboard?issue=I4G4NN) | Rule 1.4 1.12 Rule 8.1 Rule 9.2 9.4.|
|[【MECM】Add Tenant isolation for CURD MECHost/Upload config functions.](https://e.gitee.com/OSDT/members/trend/chandler-lee?issue=I4JGUS) | Rule 1.4 1.12 Rule 8.1 Rule 9.2 9.4. |
|[【MECM】支持zip格式应用包.](https://e.gitee.com/OSDT/issues/list?is%5Bsearch%5D=Zip&issue=I4G5FD)| Not involved. Only adding some test cases. |
|[【mecm】部署成功的应用支持执行配置脚本 ](https://e.gitee.com/OSDT/issues/list?is%5Bsearch%5D=Profile&issue=I4GHAB)| Not involved. Only adding some test cases.| 


## 【ATP】增加Resource Controller提供EG轻量云管功能特性

### Rule 1.4 1.12

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Rule 1.4 Access Control.<br/> Rule 1.12 Role based Access control. <br/> 
| Implementation details  | All apis need access control, and tenant isolation.|
| Related APIs            | All POST APIs involved |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | the button in test task process page |
| GUI testing steps | 1.functions of cloud management.|
| GUI input example       | NA |
| Additional Info         | NA |



### Rule 8.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 8.1  restful interfaces must be described in doc |
| Implementation details  | add api description in docs. |
| Related APIs            | All APIs involved|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


### Rule 9.2 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 Sufficient Logging.<br/> 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Sufficient logs added in the exception flows, and avoid excessive logs. |
| Related APIs            | All APIs involved|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |



## 【MECM】Add Tenant isolation for CURD MECHost/Upload config functions.

### Rule 1.4 1.12

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Rule 1.4 Access Control.<br/> Rule 1.12 Role based Access control.|
| Implementation details  | All API of mechost need add tenant isolation.|
| Related APIs            | All API of mechost Query/Add/Delete/Modification Upload config|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        |config management menu |
| GUI testing steps | test add、update、delete、query in mechost management UI |
| GUI input example       | NA |
| Additional Info         | NA |



### Rule 8.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 8.1  restful interfaces must be described in doc |
| Implementation details  | add api description in docs. |
| Related APIs            | config related apis|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


### Rule 9.2 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 Sufficient Logging.<br/> 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Sufficient logs added in the exception flows, and avoid excessive logs. |
| Related APIs            |mechost related apis|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |
