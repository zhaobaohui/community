# MEP V1.3 version security design rule involvement

|Requirements |Rules involved|
|----|----|
|[【ThirdSystem】三方系统管理，包含三方系统的增删改查等功能](https://e.gitee.com/OSDT/dashboard?issue=I4G5FO) |  Rule 1.2, Rule 2.2, Rule 3.3 |
|[【ThirdSystem】MEAO的调用通过三方系统，AppStore修改调用逻辑](https://e.gitee.com/OSDT/dashboard?issue=I4G5FX) | Rule 1.2, Rule 2.2 |
|[【ThirdSystem】MECM打通通过三方系统调用OpenStack](https://e.gitee.com/OSDT/dashboard?issue=I4G5FZ)  | Rule 1.2, Rule 2.2 |

## 【ThirdSystem】三方系统管理，包含三方系统的增删改查等功能

### Rule 1.2

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.2对于每个需要授权的访问请求，服务器必须验证用户是否有权执行此操作。|
| Implementation details  | 对用户权限进行校验 |
| Related APIs            | 三方系统增删改查接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 2.2

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 2.2请勿使用SSL2.0，SSL3.0，TLS1.0或TLS1.1进行安全传输。建议使用TLS1.2和TLS1.3。 |
| Implementation details  | 使用TLS1.2 |
| Related APIs            | 三方系统增删改查接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 3.3

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 3.3所有敏感数据都通过HTTP消息正文发送到服务器 |
| Implementation details  | 敏感数据通过body体传递 |
| Related APIs            | 增加三方系统接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

## 【ThirdSystem】MEAO的调用通过三方系统，AppStore修改调用逻辑

### Rule 1.2

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.2对于每个需要授权的访问请求，服务器必须验证用户是否有权执行此操作。|
| Implementation details  | 对用户权限进行校验 |
| Related APIs            | MEAO相关接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 2.2

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 2.2请勿使用SSL2.0，SSL3.0，TLS1.0或TLS1.1进行安全传输。建议使用TLS1.2和TLS1.3。 |
| Implementation details  | 使用TLS1.2 |
| Related APIs            | MEAO相关接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

## 【ThirdSystem】MECM打通通过三方系统调用OpenStack
### Rule 1.2

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.2对于每个需要授权的访问请求，服务器必须验证用户是否有权执行此操作。|
| Implementation details  | 对用户权限进行校验 |
| Related APIs            | OpenStack相关接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 2.2

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 2.2请勿使用SSL2.0，SSL3.0，TLS1.0或TLS1.1进行安全传输。建议使用TLS1.2和TLS1.3。 |
| Implementation details  | 使用TLS1.2 |
| Related APIs            | OpenStack相关接口 |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


