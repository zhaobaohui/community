# MEP V1.3 version security design rule involvement

|Requirements |Rules involved|
|----|----|
|[【MEP】Service support multiple-endPointInfo.](https://e.gitee.com/OSDT/dashboard?issue=I424FZ) | Not involved. Only the number of records handled in the implementation has changed. |
|[【MEP】Support confirm ready task.](https://e.gitee.com/OSDT/dashboard?issue=I424GI) | Rule 1.1, Rule 1.3, Rule 9.1 , Rule 9.3, Rule 9.4 |
|[【MEP】MEC application graceful termination/stop.](https://e.gitee.com/OSDT/dashboard?issue=I424G2)  | ule 1.1, Rule 1.3, Rule 9.1 , Rule 9.3, Rule 9.4 |

## 【MEP】Support confirm ready task

### Rule 1.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.1 All machine-to-machine and human-to-machine interfaces used for cross-network transmission must have an access authentication mechanism, and the authentication process must be performed on the server. |
| Implementation details  | Already authenticated in Mp1 interface in api-gw. Reusing the Mp1 interface. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/{appInstanceId}/confirm_ready |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.3

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.3 The server must validate the size, type, length, and special characters of all untrusted data sources and reject any data that fails the validation. |
| Implementation details  | All input will be validated for the max length of the message. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/{appInstanceId}/confirm_ready |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.1 Do not log unsanitized user input. |
| Implementation details  | Unsanitized logging can allow an attacker to forge log entries or inject malicious content into logs. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/{appInstanceId}/confirm_ready |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.3

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.3 Do not add Sensitive Information into Log or Log file. |
| Implementation details  | Consider seriously the sensitivity of the information written into log files. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/{appInstanceId}/confirm_ready |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Suppress large numbers of duplicate log messages and replace them with periodic summaries. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/{appInstanceId}/confirm_ready |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


## 【MEP】MEC application graceful termination/stop

### Rule 1.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.1 All machine-to-machine and human-to-machine interfaces used for cross-network transmission must have an access authentication mechanism, and the authentication process must be performed on the server. |
| Implementation details  | Already authenticated in Mp1 interface in api-gw. Reusing the Mp1 interface. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/:appInstanceId/AppInstanceTermination |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.3

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.3 The server must validate the size, type, length, and special characters of all untrusted data sources and reject any data that fails the validation. |
| Implementation details  | All input will be validated for the max length of the message. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/:appInstanceId/AppInstanceTermination |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.1 Do not log unsanitized user input. |
| Implementation details  | Unsanitized logging can allow an attacker to forge log entries or inject malicious content into logs. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/:appInstanceId/AppInstanceTermination |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.3

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.3 Do not add Sensitive Information into Log or Log file. |
| Implementation details  | Consider seriously the sensitivity of the information written into log files. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/:appInstanceId/AppInstanceTermination |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Suppress large numbers of duplicate log messages and replace them with periodic summaries. |
| Related APIs            | POST /mep/mec_app_support/v1/applications/:appInstanceId/AppInstanceTermination |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |
