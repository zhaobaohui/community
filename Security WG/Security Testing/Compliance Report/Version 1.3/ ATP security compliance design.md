# ATP V1.3 version Security Design rule involvement

|Requirements |Rules involved|
|----|----|
|[【ATP】用户面前台页面优化](https://gitee.com/OSDT/dashboard/issues?search=atp&issue_type_id=238024&id=I42FXG) | Not involved. Only UI design changes.|
|[【ATP】接口响应规范](https://gitee.com/OSDT/dashboard/issues?search=atp&issue_type_id=238024&id=I41Y06) | Not involved. Only interface response body changes. |
|[【ATP】测试用例增强](https://gitee.com/OSDT/dashboard/issues?search=atp&issue_type_id=238024&id=I41XYW)| Not involved. Only adding some test cases. |
|[【ATP】支持用户上传自测报告 ](https://gitee.com/OSDT/dashboard/issues?search=atp&issue_type_id=238024&id=I41XUK)| Rule 1.3, 1.12,  7.4, 8.1,9.2, 9.4| 
|[【ATP】管理面新增配置管理](https://gitee.com/OSDT/dashboard/issues?search=atp&issue_type_id=238024&id=I41XTU)| Rule 1.3, 1.12, 5.3,8.1,9.1,9.2, 9.4 |
|[【ATP】新增任务删除接口](https://gitee.com/OSDT/dashboard/issues?search=atp&issue_type_id=238024&id=I41XVP)| Rule 1.12, 8.1,9.2, 9.4 |

## 【ATP】Supporting upload self-test report for user

### Rule 1.3 1.12 7.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Rule 1.3 Input Validation.<br/> Rule 1.12 Role based Access control. <br/> Rule 7.4 The upload file pattern validation|
| Implementation details  | upload file must be pdf file and upload self-test report APIs have been pre authorized for the role ATP_ADMIN.|
| Related APIs            | POST edgegallery/atp/v1/tasks/{taskId}/action/upload-report|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | the button in test task process page |
| GUI testing steps | 1.click upload self-test report button  2. upload pdf file|
| GUI input example       | NA |
| Additional Info         | NA |



### Rule 8.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 8.1  restful interfaces must be described in doc |
| Implementation details  | add api description in docs. |
| Related APIs            | POST edgegallery/atp/v1/tasks/{taskId}/action/upload-report|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


### Rule 9.2 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 Sufficient Logging.<br/> 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Sufficient logs added in the exception flows, and avoid excessive logs. |
| Related APIs            | POST edgegallery/atp/v1/tasks/{taskId}/action/upload-report|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |



## 【ATP】add config management

### Rule 1.3 1.12 5.3

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Rule 1.3 Input Validation.<br/> Rule 1.12 Role based Access control. <br/> Rule 5.3 Input Validation|
| Implementation details  | input params have length validation and APIs have been pre authorized|
| Related APIs            | config related apis, api detail can be seen in docs|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        |config management menu |
| GUI testing steps | test add、update、delete、query in config management UI |
| GUI input example       | NA |
| Additional Info         | NA |



### Rule 8.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 8.1  restful interfaces must be described in doc |
| Implementation details  | add api description in docs. |
| Related APIs            | config related apis|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


### Rule 9.2 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 Sufficient Logging.<br/> 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Sufficient logs added in the exception flows, and avoid excessive logs. |
| Related APIs            |config related apis|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

## 【ATP】add delete task interface

### Rule 1.12

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Rule 1.12 Role based Access control.|
| Implementation details  | API have been pre authorized for the role ATP_ADMIN and ATP_TENANT.|
| Related APIs            | DELETE edgegallery/atp/v1/tasks/{taskId}|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA|
| GUI testing steps | NA|
| GUI input example       | NA |
| Additional Info         | NA |



### Rule 8.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 8.1  restful interfaces must be described in doc |
| Implementation details  | add api description in docs. |
| Related APIs            | DELETE edgegallery/atp/v1/tasks/{taskId}|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


### Rule 9.2 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 Sufficient Logging.<br/> 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Sufficient logs added in the exception flows, and avoid excessive logs. |
| Related APIs            | DELETE edgegallery/atp/v1/tasks/{taskId}|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

