# AppStore V1.2 version security design rule involvement

|Story Name |Related Security Design Rule|
|----|----|
| [【appstore】appstore支持虚机镜像的管理以及上传下载 ](https://gitee.com/OSDT/dashboard/issues?id=I3ROLN) | Function optimization, no security design required |
| [【AppStore】文档界面优化 ](https://gitee.com/OSDT/dashboard/issues?id=I3OYFZ) | Function optimization, no security design required |
| [【AppStore】上传应用时可设置应用展示模式 ](https://gitee.com/OSDT/dashboard/issues?id=I3OYGC) | Function optimization, no security design required |
| [【AppStore】支持显示应用部署类型虚机还是容器 ](https://gitee.com/OSDT/dashboard/issues?id=I3IL7I) | Function optimization, no security design required |
| [【AppStore】应用管理界面 ](https://gitee.com/OSDT/dashboard/issues?id=I3OYGU) | Rule 1.12 |
| [【AppStore】APPD转换器 ](https://gitee.com/OSDT/dashboard/issues?id=I2E166) | Function optimization, no security design required |
| [【AppStore】应用在线体验 ](https://gitee.com/OSDT/dashboard/issues?id=I3OYLO) | Function optimization, no security design required |
| [【AppStore】接口响应规范优化 ](https://gitee.com/OSDT/dashboard/issues?id=I3OYAT) | Function optimization, no security design required |
| [【AppStore】操作分析界面优化 ](https://gitee.com/OSDT/dashboard/issues?id=I3OYFR) | Function optimization, no security design required |
| [【AppStore】支持修改应用属性 ](https://gitee.com/OSDT/dashboard/issues?id=I3OYGM) | Function optimization, no security design required |
| [【AppStore】关键数据持久化 ](https://gitee.com/OSDT/dashboard/issues?id=I3OY4V) | Function optimization, no security design required |
| [【Appstore】上传应用时设置应用展示模式 ](https://gitee.com/OSDT/dashboard/issues?id=I3O1E1) | Function optimization, no security design required |

## 【Appstore】Application management UI

### Rule 1.12

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Role based Access control. |
| Implementation details  | Only the APPSTORE_ADMIN role can view the application management page. This Page provides the following functions: management of recent popular applications, management of the application list displayed in the appstore, management of the list of shared applications.|
| Related APIs            | POST /mec/appstore/v1/apps/hotapps|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | https://ip:30091/#/appsManagement |
| GUI testing steps | NA|
| GUI input example       | NA |
| Additional Info         | NA |


