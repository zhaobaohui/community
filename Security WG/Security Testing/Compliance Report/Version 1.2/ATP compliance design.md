# ATP V1.2 version Security Design rule involvement

|Requirements |Rules involved|
|----|----|
|[【developer/mecm/appstore/installer/atp/mep】虚机支持增强](https://gitee.com/OSDT/dashboard?issue_id=I3OYEP) | Not involved. Only calling mecm interface changes.|
|[【ATP】测试用例增强](https://gitee.com/OSDT/dashboard?issue_id=I3OYQ6)| Not involved. Only adding some test cases. |
|[【ATP】前台页面重构、优化 ](https://gitee.com/OSDT/dashboard?issue_id=I3OYPB)|Not involved. Only UI design changes |
|[【ATP】贡献的测试用例在管理面显示 ](https://gitee.com/OSDT/dashboard?issue_id=I3OYOQ)| Rule 1.12, 9.2, 9.4| 
|[【ATP】支持测试场景、测试套、测试用例批量导入 ](https://gitee.com/OSDT/dashboard?issue_id=I3OYPQ)| Rule 1.3, 1.12, 5.3, 7.4, 9.2, 9.4 |

## 【ATP】Contributed test cases are shown in management UI

### Rule 1.12

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID |  Role based Access control. |
| Implementation details  | Contribution download and batch delete APIs have been pre authorized for the role ATP_ADMIN.|
| Related APIs            | POST edgegallery/atp/v1/contributions/batch_delete.<br/>  GET edgegallery/atp/v1/contributions/ {id}/action/download|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | https://ip:30094/#/contributionlist |
| GUI testing steps | 1.click download button and test download contribution function  2. click batch delete button and test batch delete function|
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 9.2 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 Sufficient Logging.<br/> 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Sufficient logs added in the exception flows, and avoid excessive logs. |
| Related APIs            | POST edgegallery/atp/v1/testmodels/action/import|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |



## 【ATP】Supporting test scenarios, test suites and test cases batch import

### Rule 1.3  5.3  7.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Rule 1.3 Input Validation.<br/> Rule 5.3 All input parameters must be limited to an appropriate size. <br/> Rule 7.4 The upload file must be limited file type |
| Implementation details  | the input file must be zip file, and we have bomb defense check in code, which limit the zip file 5G and unzip file 10G |
| Related APIs            | POST edgegallery/atp/v1/testmodels/action/import|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | https://ip:30094/#/testcasemanage |
| GUI testing steps       |click import button and test import function |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.12

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Role based Access control. |
| Implementation details  | test model importing API has been pre authorized for the role ATP_ADMIN.|
| Related APIs            | POST edgegallery/atp/v1/testmodels/action/import|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | https://ip:30094/#/testcasemanage |
| GUI testing steps       | click import button and test import function |
| GUI input example       | NA |
| Additional Info         | NA |


### Rule 9.2 9.4

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 9.2 Sufficient Logging.<br/> 9.4 Avoid Logging of Excessive Data. |
| Implementation details  | Sufficient logs added in the exception flows, and avoid excessive logs. |
| Related APIs            | POST edgegallery/atp/v1/testmodels/action/import|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

