# developer V1.2 version security design rule involvement

|Story Name |Related Security Design Rule|
|----|----|
| [【developer】helmchart支持namespace隔离 ](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&sort=created_at%20desc&id=I3OYH7) | Function optimization, no security design required |
| [【developer】沙箱环境配置与mecm解耦 ](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&sort=created_at%20desc&id=I3OYQR) | Function optimization, no security design required |
| [【developer】容器镜像管理，上传配置文件优化 ](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&sort=created_at%20desc&milestone_id=101482&id=I3OYGP) | Function optimization, no security design required |
| [【developer/mecm/appstore/installer/atp/mep】虚机支持增强 ](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&sort=created_at%20desc&milestone_id=101482&page=2&id=I3OYEP) | Rule 1.12 |
| [【developer】前台组件归一化 ](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&sort=created_at%20desc&milestone_id=101482&page=2&id=I2E11D) | Function optimization, no security design required |
| [【Developer】关键数据持久化 ](https://gitee.com/OSDT/dashboard/issues?issue_type_id=238024&sort=created_at%20desc&milestone_id=101482&page=2&id=I3OY70) | Function optimization, no security design required |


## 【developer】vm image management UI

### Rule 1.12

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | Role based Access control. |
| Implementation details  | Only the DEVELOPER_ADMIN role can view all iamge info. TDEVELOPER_TENANT role can view oneself and public image info |
| Related APIs            | POST /mec/developer/v1/systemimage|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | https://ip:30092/#/systemimage |
| GUI testing steps | NA|
| GUI input example       | NA |
| Additional Info         | NA |