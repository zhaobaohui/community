# MEP V1.2 version security design rule involvement

|Requirements |Rules involved|
|----|----|
|[【MEP】MEP data persistence.](https://gitee.com/OSDT/dashboard?issue_id=I3OZX3) | Not involved. Only the data store change. |
|[【MEP】UEG MP1 interface integration.](https://gitee.com/OSDT/dashboard?issue_id=I2E0RX) | Rule 1.1, Rule 1.3, Rule 1.5, Rule 2.2, Rule 3.1 |
|[【MEP】ESTI api implement.](https://gitee.com/OSDT/dashboard?issue_id=I3OZZ5) | Rule 1.1, Rule 1.3 |

## 【MEP】UEG MP1 interface integration

### Rule 1.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.1 All machine-to-machine and human-to-machine interfaces used for cross-network transmission must have an access authentication mechanism, and the authentication process must be performed on the server. |
| Implementation details  | Data path for all request from EG to UPF and vise versa goes via MEP-Adapter. MEP adapter performes the client certificate verification to provide the authentication. |
| Related APIs            | POST /mep-adapter/v1/service/\*.\*<br/>PUT /mep-adapter/v1/service/\*.\*<br/>GET /mep-adapter/v1/service/\*.\*<br/>DELETE /mep-adapter/v1/service/\*.\*<br/> POST /mep-adapter/v1/notification|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.3

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.3 The server must validate the size, type, length, and special characters of all untrusted data sources and reject any data that fails the validation. |
| Implementation details  | All input will be validated for the max length of the message. |
| Related APIs            | POST /mep-adapter/v1/service/\*.\*<br/>PUT /mep-adapter/v1/service/\*.\*<br/>GET /mep-adapter/v1/service/\*.\*<br/>DELETE /mep-adapter/v1/service/\*.\*<br/> POST /mep-adapter/v1/notification|
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.5

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.5 According to the principle of least privilege, accounts used to run software programs are low-privilege OS accounts. |
| Implementation details  | Mep-adapter runs in a docker container with least privileages. |
| Related APIs            | None |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 2.2

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 2.2 Do not use SSL2.0, SSL3.0, TLS1.0, or TLS1.1 for secure transmission. TLS1.2 and TLS1.3 are recommended. |
| Implementation details  | Mep-adapter uses TLS 1.2. |
| Related APIs            | None |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 3.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 3.1 Authentication credentials (such as passwords and keys) cannot be stored in plaintext in the system and must be encrypted. If plaintext does not need to be restored, use the irreversible PBKDF2 algorithm for encryption. If plaintext needs to be restored, you can use the AES-256 GCM algorithm for encryption. |
| Implementation details  | EG and UEG AK/SK and certificates are processed in mep-adapter, which needs to be cleared after the usage. |
| Related APIs            | None |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


## 【MEP】ESTI api implement.

### Rule 1.1

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.1 All machine-to-machine and human-to-machine interfaces used for cross-network transmission must have an access authentication mechanism, and the authentication process must be performed on the server. |
| Implementation details  | Already authenticated in Mp1 interface in api-gw. Reusing the Mp1 interface. |
| Related APIs            | GET mep/mec_service_mgmt/v1/transports<br/>GET mep/mec_app_support/v1/timing/current_time<br/>GET mep/mec_app_support/v1/timing/timing_caps |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.3

| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.3 The server must validate the size, type, length, and special characters of all untrusted data sources and reject any data that fails the validation. |
| Implementation details  | All input will be validated for the max length of the message. |
| Related APIs            | GET mep/mec_service_mgmt/v1/transports<br/>GET mep/mec_app_support/v1/timing/current_time<br/>GET mep/mec_app_support/v1/timing/timing_caps |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


