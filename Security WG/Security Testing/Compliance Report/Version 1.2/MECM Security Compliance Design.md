# MECM V1.2 version security design rule involvement

|Story Name |Related Security Design Rule|
|----|----|
| [【MECM】统一MECM组件风格](https://gitee.com/OSDT/dashboard/issues?id=I2P3CD)|    NA |
| [【HealthCheck】MECM层调用HealthCheck](https://gitee.com/OSDT/dashboard/issues?id=I3PRZ3)|    NA |
| [【MECM】MECM支持前台节点位置可自定义](https://gitee.com/OSDT/dashboard/issues?id=I3PVE0)|   NA |
| [【MECM】MECM支持applcm和apprulemgr注册统一](https://gitee.com/OSDT/dashboard/issues?id=I3PVDV)|   NA |
| [【MECM】MECM后台统一servicecomb](https://gitee.com/OSDT/dashboard/issues?id=I3PVD8)|    NA |
| [【MECM】边缘自治增强](https://gitee.com/OSDT/dashboard/issues?id=I3PV9W)|   Rule2.2 |
| [【MECM】MECM支持虚机APP包同步和分发，MECM support app package management of VM app](https://gitee.com/OSDT/dashboard/issues?id=I3PVBT)| NA |
| [【MECM】mecm支持虚机APP的实例化和终止 VM APP instantiation and termination support.--OpenStack](https://gitee.com/OSDT/dashboard/issues?id=I3PV4Y)| NA |
| [【MECM】mecm支持虚机APP的实例化和终止 VM APP instantiation and termination support.--FusionSphere](https://gitee.com/OSDT/dashboard/issues?id=I3PV4Y)| NA |
| [【MECM】mecm支持VIM预配置 MECM support the preconfig of OpenStack](https://gitee.com/OSDT/dashboard/issues?id=I3PV5W)|   NA | 
| [【MECM】接口响应规范优化](https://gitee.com/OSDT/dashboard/issues?id=I3OYB2)| NA |
| [【MECM】关键数据持久化](https://gitee.com/OSDT/dashboard/issues?id=I3OY79)|    NA |
| [【Example-apps】Example-apps enhancement](https://gitee.com/OSDT/dashboard/issues?id=I3PVF2)|NA |


## 【MECM】边缘自治增强

### Rule 2.2
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 2.2 Do not use SSL2.0, SSL3.0, TLS1.0, or TLS1.1 for secure transmission. TLS1.2 and TLS1.3 are recommended. |
| Implementation details  | Mep-adapter uses TLS 1.2. |
| Related APIs            | GET lcmcontroller/v1/mep/* |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |




