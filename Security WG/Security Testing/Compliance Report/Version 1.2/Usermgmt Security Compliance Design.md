# Usermgmt V1.2 version Security Design rule involvement

|Requirements |Rules involved|
|----|----|
|[【UserMgmt】验证码优化](https://gitee.com/OSDT/dashboard?issue_id=I3PRPV)| R1.11 R1.16 R5.1 |
|[【UserMgmt】支持用户强制修改密码](https://gitee.com/OSDT/dashboard?issue_id=I3PS2T) | R1.9 R1.14 | 


## 【UserMgmt】验证码优化
### Rule 1.11
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.11确保没有捕获-重放攻击的可能性。 |
| Implementation details  | 测试步骤：通过侦听身份验证数据包并尝试使用相同的内容重播它，不应允许其登录，并且身份验证必须失败。 |
| Related APIs            |    |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.16
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.16 网页上的登录表单必须使用安全的验证码。 |
| Implementation details  | 测试步骤：1.验证码内容不能在网页前端源码中出现。 2.验证码使用一次后（不论成功或失败）立即失效，下次使用时重新生成。 3.验证码和登录账户密码必须在同一个http请求中提交，后端必须先进行验证码校验，验证码校验通过后再进行用户名密码校验。 |
| Related APIs            |    |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |


### Rule 5.1
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 5.1必须具备反自动化功能，以防止违反凭据测试，暴力破解和帐户锁定攻击。 |
| Implementation details  | 测试步骤 1.设计用户登录脚本以模拟残酷的登录尝试，确认用户帐户将被锁定。 2.使用JMeter针对API模拟DOS attack，检查系统的运行状态。 |
| Related APIs            |    |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

## 【UserMgmt】支持用户强制修改密码

### Rule 1.9
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.9更改密码功能必须包括旧密码，新密码和密码确认。 |
| Implementation details  | 测试步骤： 1.登录系统并使用更改密码功能，检查界面包括旧密码，新密码和密码确认。 |
| Related APIs            |    |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

### Rule 1.14
| Content                 | Related Information                                          |
| ----------------------- | ------------------------------------------------------------ |
| Security design rule ID | 1.14在口令框中输入的口令不能明文显示和拷贝/剪切，密码框也不允许使用自动填充功能。 |
| Implementation details  | 测试步骤： 1.尝试将密码框中内容拷贝/剪切出来，并查看输入框类型是否为password： < input type="password"> 2.输入密码时观察是否存在自动填充功能，并查看输入框参数是否为< input autocomplete="off">。 |
| Related APIs            |    |
| Example API payload     | NA |
| Related files           | NA |
| Related GUI URLs        | NA |
| GUI testing steps | NA |
| GUI input example       | NA |
| Additional Info         | NA |

