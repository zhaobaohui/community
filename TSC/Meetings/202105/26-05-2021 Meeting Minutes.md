| Meeting Day | Meeting Time                                | Note Taker |
| ----------- | ------------------------------------------- | ---------- |
| Wednesdays  | 7:00 PM to 8:00 PM (UTC + 8，Beijing Time） |            |

#### Conference Bridge

Zoom Meeting ID




## Attendees

TSC members who attended:

- 徐雷 （联通 毋涛代表）
- 刘芷若 （信通院）     
- 丁韩宇  （中移动）    
- 龚永生     （九州云 范桂飓代表）      
- 刘辉           （紫金山）     
- 袁国平      （安恒 李剑锋代表） 
- 陈炜   （腾讯 冯佳新代表）    
- 于洋             （华为）    

## Agenda Items

| Item                  | Owner                                                        |
| --------------------- | ------------------------------------------------------------ |
| v1.2版本&架构讨论           | @ALL                                                      |
| CVE流程讨论           | @扈冰                                                         |
| 各工作组进展          | 高维涛（华为），袁国平（安恒），刘芷若（信通院），刘辉（紫金山），陈燕军（中移动），冯杰（西电） |


## Discussion Items

- 同意目前通过CVE官网申请漏洞，并通过官网公告披露的流程
- 测试组启动第一次迭代的测试

## Item

TSC Voting to Approve ABC

| **Name** | **Affiliation** | **Email** | **Voting Items (Y/N)** |
| -------- | --------------- | --------- | ---------------------- |
| Name     | Company         | Email     | Y                      |


## Action Items

| Done? | Item | Responsible | Due Date |
| ----- | ---- | ----------- | -------- |
|       | item | who         | due_date |
|       |      |             |          |

## Other Notes & Information

N/A